// @dart=2.9
class ErrorMessage{
  static const String ERROR_DATA_NOT_PROPER_FORM = "Something went wrong !";

  //API errors------------------------------------------
  static const String ERROR_REQUEST_CANCEL = "Request to API server was cancelled";
  static const String ERROR_CONNECTION_TIME_OUT = "Connection timeout with API server";
  static const String ERROR_DEFAULT_CONNECTION = "Connection to API server failed due to internet connection";
  static const String ERROR_RECEIVE_TIME_OUT = "Receive timeout in connection with API server";
  static const String ERROR_SEND_TIME_OUT = "Send timeout in connection with API server";
  //status code errors ---------------------------------
  static const String ERROR_400 = "400 Bad Request Error !";
  static const String ERROR_401 = "Authentication failed due to incorrect entries";
  static const String ERROR_403 = "You do not have permission to access the resource !";
  static const String ERROR_404 = "404 Data Not Found !";
  static const String ERROR_408= "Send timeout in connection with API server";
  static const String ERROR_500 = "500 Internal Server Error !";
  static const String ERROR_503 = "503 Services Unavailable !";


}