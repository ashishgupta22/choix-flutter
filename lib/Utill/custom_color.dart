// @dart=2.9

import 'dart:ui';

class CustomColors{
  static var lightPinkColor = const Color(0xFFFEF1F6);
  static var darkPinkColor = const Color(0xFFF40057);
  static var veryLightPinkColor = const Color(0xFFFFF8FB);
  static var blackColor = const Color(0xFF1A1A1A);
  static var catBluerColor = const Color(0xE8474747);
  static var offerCartColor = const Color(0xFFE1DCFC);
  static var offerCartUnSelectedColor = const Color(0xE8474747);
  static var orderProgress = const Color(0xB4000000);
  static var hintColor = const Color(0xFFD5D5D5);
  static var otpInputBorderColor = const Color(0xE8474747);
  static var unCheckedColor = const Color(0xFFD5D5D5);
  static var grey = const Color(0xFFEEEEEE);

  static var otpBorderSize = 2.5;
}