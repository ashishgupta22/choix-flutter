// @dart=2.9
import 'package:intl/intl.dart';

class Utils{

  static String formatCurrency(num value,{int fractionDigits = 2}) {
    ArgumentError.checkNotNull(value, 'value');

    // convert cents into hundreds.
    value = value / 100;

    return NumberFormat.currency(
        customPattern: '###,###.##',
        // using Netherlands because this country also
        // uses the comma for thousands and dot for decimal separators.
        locale: 'nl_NL'
    ).format(value);
  }

}