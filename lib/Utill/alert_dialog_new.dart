// @dart=2.9
import 'package:choix_customer/Data/CancelReason.dart';
import 'package:choix_customer/Data/PendingPaymentOrder.dart';
import 'package:choix_customer/Data/UserData.dart';
import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:choix_customer/ui/dashboard_screen.dart';
import 'package:choix_customer/widget/permission_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:location/location.dart';

import 'AppSharedPreferences.dart';
import 'custom_color.dart';

class AlertDialogNew {
  static void show_dialog(
      BuildContext context,
      DialogSelectedListner selectedListner,
      String message,
      String btnYes,
      String btnNo) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Stack(
          children: <Widget>[
            Center(
              child: Container(
                padding: EdgeInsets.only(
                    // left: 20,
                    // top: Constants.padding,
                    // right: 20,
                    bottom: 20),
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black,
                          offset: Offset(0, 10),
                          blurRadius: 10),
                    ]),
                child: Container(
                  padding: EdgeInsets.only(
                      // left: 20,
                      // top: Constants.padding,
                      // right: 20,
                      bottom: 20),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: Constants.dialog_titile_height,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10)),
                          gradient: LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment(2.8, 0.0),
                              colors: [
                                CustomColors.darkPinkColor,
                                Colors.white
                              ]),
                        ),
                        child: Container(
                          margin: EdgeInsets.only(
                            left: 20,
                            // top: Constants.padding,
                            right: 20,
                          ),
                          child: Center(
                            child: Text(
                              AlertMessage.msgAlertTitle,
                              style: TextStyle(
                                  fontSize: 25,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0, right: 8),
                        child: Text(
                          message,
                          style: TextStyle(fontSize: 16),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      Container(
                        padding: EdgeInsets.only(
                          left: 20,
                          // top: Constants.padding,
                          right: 20,
                        ),
                        child: Row(
                          mainAxisAlignment: btnNo.isNotEmpty
                              ? MainAxisAlignment.start
                              : MainAxisAlignment.center,
                          children: [
                            if (btnNo.isNotEmpty)
                              Padding(
                                padding: const EdgeInsets.only(right: 20),
                                child: Container(
                                  height: Constants.dialog_btn_height,
                                  width: 80,
                                  decoration: BoxDecoration(
                                    color: CustomColors.blackColor,
                                    borderRadius: BorderRadius.circular(50),
                                  ),
                                  child: Center(
                                    child: InkWell(
                                      onTap: () {
                                        isDialogShowing = false;
                                        if (selectedListner != null)
                                          selectedListner.onSelected(false);
                                        else
                                          Navigator.pop(context);
                                      },
                                      child: Text(
                                        btnNo,
                                        style: TextStyle(
                                            fontSize: 18, color: Colors.white),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            if (btnNo.isNotEmpty) Spacer(),
                            InkWell(
                              child: Container(
                                height: Constants.dialog_btn_height,
                                width: 100,
                                decoration: BoxDecoration(
                                  color: CustomColors.darkPinkColor,
                                  borderRadius: BorderRadius.circular(50),
                                ),
                                child: Center(
                                  child: Text(
                                    btnYes,
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.white),
                                  ),
                                ),
                              ),
                              onTap: () {
                                isDialogShowing = false;
                                if (selectedListner != null)
                                  selectedListner.onSelected(true);
                                else
                                  Navigator.pop(context);
                                // Navigator.push(context, MaterialPageRoute(builder: (context) => LoginScreen(),));
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
    // showDialog(
    //   barrierDismissible: false,
    //   context: context,
    //   builder: (context) => AlertDialog(
    //     contentPadding: EdgeInsets.zero,
    //     shape: RoundedRectangleBorder(
    //         borderRadius: BorderRadius.all(Radius.circular(10.0))),
    //     content: Builder(
    //       builder: (context) {
    //         // Get available height and width of the build area of this widget. Make a choice depending on the size.
    //         var width = MediaQuery.of(context).size.width;
    //
    //         return Container(
    //           width: width,
    //           height: MediaQuery.of(context).size.height * 0.45,
    //           padding: EdgeInsets.only(bottom: 20),
    //           child: Column(
    //             children: [
    //               Container(
    //                 width: width,
    //                 height: MediaQuery.of(context).size.height * 0.09,
    //                 decoration: BoxDecoration(
    //                   borderRadius: BorderRadius.only(
    //                       topLeft: Radius.circular(10),
    //                       topRight: Radius.circular(10)),
    //                   gradient: LinearGradient(
    //                       begin: Alignment.centerLeft,
    //                       end: Alignment(2.8, 0.0),
    //                       colors: [CustomColors.darkPinkColor, Colors.white]),
    //                 ),
    //                 child: Center(
    //                     child: Text(
    //                   AlertMessage.msgAlertTitle,
    //                   style: TextStyle(color: Colors.white, fontSize: 25, fontFamily: "Montserrat",),
    //                 )),
    //               ),
    //               Padding(
    //                 padding: const EdgeInsets.all(30),
    //                 child: Text(
    //                   message,
    //                   textAlign: TextAlign.center,
    //                   style: TextStyle(fontSize: 16,fontFamily: "Montserrat"),
    //                 ),
    //               ),
    //               Padding(
    //                 padding: const EdgeInsets.only(left: 15, right: 15),
    //                 child: Row(
    //                   crossAxisAlignment: CrossAxisAlignment.end,
    //                   children: [
    //                     if (btnNo.isNotEmpty)
    //                     InkWell(
    //                       onTap: () {
    //                         selectedListner.onSelected(false);
    //                       },
    //                       child: Container(
    //                         height: 40,
    //                         width: 100,
    //                         decoration: BoxDecoration(
    //                           color: CustomColors.blackColor,
    //                           borderRadius: BorderRadius.circular(50),
    //                         ),
    //                         child: Center(
    //                             child: Text(
    //                               btnNo,
    //                           style:
    //                               TextStyle(color: Colors.white, fontSize: 15),
    //                         )),
    //                       ),
    //                     ),
    //                     Spacer(),
    //
    //                       InkWell(
    //                         onTap: () {
    //                           selectedListner.onSelected(true);
    //                         },
    //                         child: Container(
    //                           height: 40,
    //                           width: 100,
    //                           decoration: BoxDecoration(
    //                             color: CustomColors.darkPinkColor,
    //                             borderRadius: BorderRadius.circular(50),
    //                           ),
    //                           child: Center(
    //                               child: Text(
    //                                 btnYes,
    //                             style: TextStyle(
    //                                 color: Colors.white, fontSize: 15),
    //                           )),
    //                         ),
    //                       ),
    //                   ],
    //                 ),
    //               ),
    //             ],
    //           ),
    //         );
    //       },
    //     ),
    //   ),
    // );
  }

  static void showConfirmationDialog(
      BuildContext context,
      DialogSelectedListner selectedListner,
      String message,
      String btnYes,
      String btnNo,
      PendingPaymentOrderData listData,
      UserData userData,
      ApiProvider provider) {
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => WillPopScope(
        onWillPop: () async => false,
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Stack(
            children: <Widget>[
              Center(
                child: Container(
                  padding: EdgeInsets.only(bottom: 20),
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black,
                            offset: Offset(0, 10),
                            blurRadius: 10),
                      ]),
                  child: Container(
                    padding: EdgeInsets.only(
                        // left: 20,
                        // top: Constants.padding,
                        // right: 20,
                        bottom: 20),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: Constants.dialog_titile_height,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                topRight: Radius.circular(10)),
                            gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment(2.8, 0.0),
                                colors: [
                                  CustomColors.darkPinkColor,
                                  Colors.white
                                ]),
                          ),
                          child: Container(
                            margin: EdgeInsets.only(
                              left: 20,
                              // top: Constants.padding,
                              right: 20,
                            ),
                            child: Stack(children: [
                              Center(
                                child: Text(
                                  AlertMessage.msgAlertTitle,
                                  style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white),
                                ),
                              ),
                              Align(
                                alignment: Alignment.centerRight,
                                child: IconButton(
                                    onPressed: () {
                                      callAcceptRejectAPI(
                                          null,
                                          context,
                                          listData.id.toString(),
                                          "Accept",
                                          "",
                                          userData,
                                          provider);
                                    },
                                    icon: Icon(
                                      Icons.clear,
                                      color: Colors.white,
                                    )),
                              )
                            ]),
                          ),
                        ),
                        SizedBox(
                          height: 30,
                        ),
                        Align(
                          alignment: Alignment.topCenter,
                          child: Padding(
                            padding: const EdgeInsets.only(left: 8.0, right: 10),
                            child: Text(
                              "Order No : ${listData.orderNumber}",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, right: 10),
                          child: Text(
                            "As per our vendor, he delivered ${listData.cat_name} to you at ₹${listData.totalAmount}. Is it correct?",
                            style: TextStyle(fontSize: 16),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        SizedBox(
                          height: 35,
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 20,
                            // top: Constants.padding,
                            right: 20,
                          ),
                          child: Row(
                            mainAxisAlignment: btnNo.isNotEmpty
                                ? MainAxisAlignment.start
                                : MainAxisAlignment.center,
                            children: [
                              InkWell(
                                child: Container(
                                  height: Constants.dialog_btn_height,
                                  width: 100,
                                  decoration: BoxDecoration(
                                    color: CustomColors.darkPinkColor,
                                    borderRadius: BorderRadius.circular(50),
                                  ),
                                  child: Center(
                                    child: Text(
                                      btnYes,
                                      style: TextStyle(
                                          fontSize: 18, color: Colors.white),
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  callAcceptRejectAPI(
                                      null,
                                      context,
                                      listData.id.toString(),
                                      "Accept",
                                      "",
                                      userData,
                                      provider);
                                  // selectedListner.onSelected(true);
                                  // Navigator.push(context, MaterialPageRoute(builder: (context) => LoginScreen(),));
                                },
                              ),
                              if (btnNo.isNotEmpty) Spacer(),
                              if (btnNo.isNotEmpty)
                                Padding(
                                  padding: const EdgeInsets.only(right: 20),
                                  child: Container(
                                    height: Constants.dialog_btn_height,
                                    width: 80,
                                    decoration: BoxDecoration(
                                      color: CustomColors.blackColor,
                                      borderRadius: BorderRadius.circular(50),
                                    ),
                                    child: Center(
                                      child: InkWell(
                                        onTap: () {
                                          callAPICancelReason(listData, context,
                                              userData, provider);
                                        },
                                        child: Text(
                                          btnNo,
                                          style: TextStyle(
                                              fontSize: 18, color: Colors.white),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  static void callAcceptRejectAPI(
      BuildContext context,
      BuildContext context2,
      String order_id,
      String type,
      String resionID,
      UserData userData,
      ApiProvider provider) async {
    CheckPermission.checkLocationPermissionOnly(context2).then((value) async {
      if (value) {
        EasyLoading.show();
        if (userData == null)
          userData = await AppSharedPreferences.instance.getUserDetails();
        Location location = new Location();
        LocationData _pos = await location.getLocation();

        String latitude = _pos.latitude.toString();
        String longitude = _pos.longitude.toString();
        Map<String, dynamic> dictParam = {
          'order_id': order_id,
          'latitude': latitude,
          'longitude': longitude,
          'type': type,
          'reason': resionID
        };
        print(dictParam);
        provider
            .requestPostForApi(
                context2,
                dictParam,
                WebApiConstaint.URL_ACCEPTREJECT,
                userData.sessionKey,
                userData.authorization)
            .then((responce) {
          EasyLoading.dismiss();
          if (responce != null) {
            try {
              if (responce.data["error"] == false) {
                // Fluttertoast.showToast(msg: responce.data["message"]);
                if (context != null) {
                  Navigator.pop(context);
                }
                Navigator.pop(context2);

                if (type.endsWith("Reject"))
                  AlertDialogNew.show_dialog(
                      context, null, responce.data["message"], "Ok", "");
                else {
                  isDialogShowing = false;
                  Fluttertoast.showToast(msg: responce.data["message"]);
                }
              } else {
                Fluttertoast.showToast(msg: responce.data["message"]);
              }
            } catch (e) {
              ToastUtils.showCustomToast(context2, e.toString());
            }
          }
        });
      }
    });
  }

  static void show(
      BuildContext context2,
      List<CancelReasonData> data,
      PendingPaymentOrderData listData,
      UserData userData,
      ApiProvider provider) {
    int lastchecked = 0;
    if (data.length > 0) {
      data[0].selected = true;
      showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(10),
            topLeft: Radius.circular(10),
          ),
        ),
        builder: (context) {
          return StatefulBuilder(
            builder: (BuildContext context1, StateSetter setState) {
              return SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: CustomColors.lightPinkColor,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10)),
                      ),
                      child: Column(
                        children: [
                          Padding(
                              padding: EdgeInsets.only(top: 20, bottom: 10)),
                          Row(
                            children: [
                              Padding(
                                  padding: EdgeInsets.only(
                                left: 15,
                              )),
                              Text(
                                "Name",
                                style: TextStyle(
                                    fontFamily: "Montserrat",
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                              Padding(padding: EdgeInsets.only(right: 3)),
                              Text(
                                listData.vendor_name != null
                                    ? listData.vendor_name
                                    : "",
                                style: TextStyle(
                                    fontFamily: "Montserrat", fontSize: 14),
                              ),
                              Spacer(),
                              Text(
                                listData.created != null
                                    ? listData.created
                                    : "",
                                style: TextStyle(
                                    fontFamily: "Montserrat",
                                    fontSize: 12,
                                    color: Colors.grey),
                              ),
                              Padding(padding: EdgeInsets.only(right: 15))
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(bottom: 15)),
                          Row(
                            children: [
                              Padding(
                                  padding: EdgeInsets.only(left: 15, top: 8)),
                              Flexible(
                                  child: RichText(
                                      text: TextSpan(
                                style: new TextStyle(
                                  fontFamily: "Montserrat",
                                  fontSize: 14.0,
                                  color: Colors.black,
                                ),
                                children: <TextSpan>[
                                  new TextSpan(
                                      text: 'Address : ',
                                      style: new TextStyle(
                                          fontWeight: FontWeight.bold)),
                                  new TextSpan(
                                      text: listData.address != null
                                          ? listData.address
                                          : "",
                                      style: new TextStyle(fontSize: 14)),
                                ],
                              ))),
                              Padding(
                                  padding: EdgeInsets.only(
                                right: 15,
                              )),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(bottom: 15))
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10, left: 15, right: 15),
                      child: Text(
                        "Order Issues",
                        style: TextStyle(
                            fontFamily: "Montserrat-Bold",
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      ),
                    ),
                    ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: data.length,
                        itemBuilder: (context, index) {
                          return Container(
                              margin: EdgeInsets.only(
                                left: 15,
                                right: 15,
                              ),
                              child: Row(
                                children: [
                                  Checkbox(
                                      value: data[index].selected,
                                      checkColor: Colors.white,
                                      activeColor: CustomColors.darkPinkColor,
                                      onChanged: (bool value) {
                                        setState(() {
                                          data[lastchecked].selected = false;
                                          lastchecked = index;
                                          data[index].selected = value;
                                        });
                                      }),
                                  Padding(padding: EdgeInsets.only(left: 5)),
                                  Flexible(
                                    child: Text(
                                      data[index].title,
                                      style: TextStyle(
                                          fontFamily: "Montserrat",
                                          fontSize: 14),
                                    ),
                                  ),
                                  Padding(padding: EdgeInsets.only(right: 5)),
                                ],
                              ));
                        }),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 20, right: 20, bottom: 20, top: 8),
                      child: InkWell(
                        onTap: () {
                          callAcceptRejectAPI(
                              context,
                              context2,
                              listData.id.toString(),
                              "Reject",
                              data[lastchecked].title,
                              userData,
                              provider);
                        },
                        child: Container(
                          padding: EdgeInsets.only(left: 25, right: 25),
                          alignment: Alignment.center,
                          height: 45,
                          decoration: new BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: CustomColors.darkPinkColor,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                          ),
                          child: Text(
                            "Submit",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          );
        },
        context: context2,
      );
    }
  }

  static void callAPICancelReason(PendingPaymentOrderData dataList,
      BuildContext context, UserData userData, ApiProvider provider) async {
    EasyLoading.show();
    if (userData == null)
      userData = await AppSharedPreferences.instance.getUserDetails();
    String strUrl = WebApiConstaint.URL_CANCELREASON +"?type=dispute";
    provider
        .requestGetForApi(
            context, strUrl, userData.sessionKey, userData.authorization)
        .then((responce) {
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            CancelReason order = CancelReason.fromJson(responce.data);
            show(context, order.data, dataList, userData, provider);
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }
}

abstract class DialogSelectedListner {
  onSelected(bool isSelected);
}
