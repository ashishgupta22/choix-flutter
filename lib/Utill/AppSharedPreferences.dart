// @dart=2.9
import 'dart:convert';

import 'package:choix_customer/Data/UserData.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppSharedPreferences {
  AppSharedPreferences._privateConstructor();

  static final AppSharedPreferences instance =
      AppSharedPreferences._privateConstructor();

  static String userData1 = "user_data";

  static String userName = "user_name";
  static String useremail = "user_email";
  static String usermobile_no = "user_mobile_no";
  static String session_key = "session_key";
  static String authorization_key = "authorization";
  static String isLogin = "isLogin";
  static String isIntro = "isIntro";
  static String fcmToken = "fcmToken";
  UserData user;

  Future<String> getUserName(String key) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString(key);
  }

  // Future<bool> setUserDetails(String name, String email, String mobile_no,
  //     String sessionkey, String authorization) async {
  //   final SharedPreferences prefs = await SharedPreferences.getInstance();
  //   prefs.setString(userName, name);
  //   prefs.setString(useremail, email);
  //   prefs.setString(usermobile_no, mobile_no);
  //   prefs.setString(session_key, sessionkey);
  //   prefs.setString(authorization, authorization);
  //   return true;
  // }

  Future<UserData> getUserDetails() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    Map json = jsonDecode(pref.getString(userData1));
    user = UserData.fromJson(json);
    return user;
  }

  Future<bool> setUserData(Object value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(userData1, value);
  }

  Future<bool> setLogin(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(isLogin, value);
  }
  Future<bool> getIsLogin() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getBool(isLogin) == null ? false : pref.getBool(isLogin);
  }
  Future<bool> setIntro(bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(isIntro, value);
  }
  Future<bool> getIsIntro() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getBool(isIntro) == null ? false : pref.getBool(isIntro);
  }
  void setUserDetails(Map<dynamic, dynamic> dictData) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(userData1, json.encode(dictData));
    print(json.encode(dictData));
  }
  Future<String> getFCMToken() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString(fcmToken) == null ? "" : pref.getString(fcmToken);
  }

  void setFCMToken(String fcmtoken) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString(fcmToken, json.encode(fcmtoken));
  }
}
