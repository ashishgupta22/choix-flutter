// @dart=2.9
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ToastUtils {
  static Timer toastTimer;
  static OverlayEntry _overlayEntry;

  static void showCustomToast(BuildContext context,
      String message) {

    if (toastTimer == null || !toastTimer.isActive) {
      _overlayEntry = createOverlayEntry(context, message);
      Overlay.of(context).insert(_overlayEntry);
      toastTimer = Timer(Duration(seconds: 5), () {
        if (_overlayEntry != null) {
          _overlayEntry.remove();
        }
      });
    }

  }

  static void showCustomToastOTP(BuildContext context,
      String message) {

    if (toastTimer == null || !toastTimer.isActive) {
      _overlayEntry = createOverlayEntryOTP(context, message);
      Overlay.of(context).insert(_overlayEntry);
      toastTimer = Timer(Duration(seconds: 8), () {
        if (_overlayEntry != null) {
          _overlayEntry.remove();
        }
      });
    }

  }
  static void showToastOTP(FToast fToast,String message) {
    Widget toast = Container(
      padding: const EdgeInsets.symmetric(horizontal: 24.0, vertical: 12.0),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25.0),
        color: Colors.grey,
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Flexible(child: Text(message)),
        ],
      ),
    );

    fToast.showToast(
      child: toast,
      gravity: ToastGravity.BOTTOM,
      toastDuration: Duration(seconds: 8),
    );

    // fToast.showToast(
    //     child: toast,
    //     toastDuration: Duration(seconds: 8),
    //     positionedToastBuilder: (context, child) {
    //       return Positioned(
    //         child: child,
    //         top: 16.0,
    //         left: 16.0,
    //       );
    //     });
  }
  static OverlayEntry createOverlayEntry(BuildContext context,
      String message) {

    return OverlayEntry(
      builder: (context) => Positioned(
        //top: 50.0,
        bottom: 20,
        left: 10,
        right: 10,
        //width: MediaQuery.of(context).size.width-40,
        child: Material(
          elevation: 10.0,
          borderRadius: BorderRadius.circular(10),
          child: Container(
            padding:
            EdgeInsets.only(left: 10, right: 10,
                top: 13, bottom: 10),
            decoration: BoxDecoration(
                color: Color(0xffe53e3f),
                borderRadius: BorderRadius.circular(10)),
            child: Align(
              alignment: Alignment.center,
              child: Text(
                message,
                textAlign: TextAlign.center,
                softWrap: true,
                style: TextStyle(
                  fontSize: 18,
                  color: Color(0xFFFFFFFF),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
  static OverlayEntry createOverlayEntryOTP(BuildContext context,
      String message) {

    return OverlayEntry(
      builder: (context) => Positioned(
        //top: 50.0,
        bottom: 20,
        left: 10,
        right: 10,
        //width: MediaQuery.of(context).size.width-40,
        child: Material(
          elevation: 10.0,
          borderRadius: BorderRadius.circular(10),
          child: Container(
            padding:
            EdgeInsets.only(left: 10, right: 10,
                top: 13, bottom: 10),
            decoration: BoxDecoration(
                color: Color(0xff212121),
                borderRadius: BorderRadius.circular(10)),
            child: Align(
              alignment: Alignment.center,
              child: Text(
                message,
                textAlign: TextAlign.center,
                softWrap: true,
                style: TextStyle(
                  fontSize: 18,
                  color: Color(0xFFFFFFFF),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}