// @dart=2.9
import 'package:choix_customer/Data/FetchAddress.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/ui/savedAddress.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SavedAddressItem extends StatefulWidget {

  FetchAddressData addressList;
  ItemAddressListner addressListner;
  SavedAddressItem(FetchAddressData this.addressList,ItemAddressListner this.addressListner) : super();

  @override
  _SavedAddressItemState createState() => _SavedAddressItemState();

}

class _SavedAddressItemState extends State<SavedAddressItem> {
  bool isSelected = false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print(widget.addressList);
  }
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(5),
        boxShadow: [
          // color: Colors.white, //background color of box
          BoxShadow(
            color: Colors.grey,
            blurRadius: 3.0, // soften the shadow
            spreadRadius: 1.0, //extend the shadow
            offset: Offset(
              1.0, // Move to right 10  horizontally
              1.0, // Move to bottom 10 Vertically
            ),
          )
        ],
      ),

      margin: EdgeInsets.only(
          left: 20.0, top: 20.0, right: 20.0, bottom: 10.0),
      child: InkWell(
        onTap: (){
          widget.addressListner.onAddressSelected(widget.addressList,0);
        },
        child: Padding(
          padding: EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    widget.addressList.tags != null ? widget.addressList.tags : "Other",
                    style: TextStyle(
                        fontFamily: "Montserrat", fontSize: 18,fontWeight: FontWeight.w600),
                  ),
                  Spacer(),
                  IconButton(
                    onPressed: () {
                        setState(() {
                          isSelected = isSelected == true ? false : true;
                        });

                    },
                    icon : InkWell(
                      onTap: (){
                        widget.addressListner.onAddressSelected(widget.addressList,1);
                      },
                      child: Icon(
                        Icons.delete,
                        color: CustomColors.darkPinkColor,
                      ),
                    ),
                  ),
                ],
              ),
              Text(
                widget.addressList.address,
                style:
                TextStyle(fontFamily: "Montserrat", fontSize: 15,color: Colors.grey),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
