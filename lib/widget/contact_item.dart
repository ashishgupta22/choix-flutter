// @dart=2.9
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/ui/referral_screen.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ContactItem extends StatelessWidget {
  Contact contact;
  InviteClickListner inviteClickListner;

  ContactItem(Contact this.contact, InviteClickListner this.inviteClickListner)
      : super();

  @override
  Widget build(BuildContext context) {
    // return Text("hi");
    return Container(
      color: Colors.white,
      // color: CustomColors.lightPinkColor,

      margin: EdgeInsets.only(left: 5, right: 5, top: 5, bottom: 5),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(20)),
            margin: EdgeInsets.only(top: 8, left: 15, right: 8, bottom: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                //Padding(padding: EdgeInsets.only(left: 10)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      //color: Colors.grey,
                      // margin: EdgeInsets.only(top: 10),
                      height: 45,
                      width: 45,
                      decoration: BoxDecoration(
                        color: CustomColors.darkPinkColor,
                        // image: backgroundImage != null
                        //     ? new DecorationImage(image: backgroundImage, fit: BoxFit.cover)
                        //     : null,

                        shape: BoxShape.circle,
                      ),
                      // child: Image.asset(
                      //   "assets/images/bel.png",
                      //   height: 20,
                      //   color: Colors.white,
                      // ),
                      child: Center(
                        child: Text(
                          contact.displayName != null
                              ? (contact.displayName.substring(0, 1))
                              : "",
                          style: TextStyle(fontSize: 18, color: Colors.white),
                        ),
                      ),
                    ),

                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              contact.displayName ?? "",
                              maxLines: 1,
                              style: TextStyle(
                                  fontFamily: "Montserrat",
                                  fontWeight: FontWeight.w600,
                                  fontSize: 15),
                            ),
                            Text((contact.phones.toList().length > 0
                                ? contact.phones.toList()[0].value
                                : "Phone Not Available")),
                          ],
                        ),
                      ),
                    ),
                    Spacer(),
                    // IconButton(
                    //    onPressed: () {},
                    //    icon:
                    Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(12),
                            // color: CustomColors.darkPinkColor,
                            border: Border.all(
                                color: CustomColors.darkPinkColor, width: 1)),
                        child: Container(
                          margin: EdgeInsets.only(
                              left: 7, right: 7, top: 4, bottom: 4),
                          child: InkWell(
                            onTap: () {
                              inviteClickListner.onSelected(contact);
                            },
                            child: Text(
                              'INVITE',
                              style: TextStyle(
                                  fontFamily: "Montserrat",
                                  fontSize: 13,
                                  fontWeight: FontWeight.w500,
                                  color: CustomColors.darkPinkColor),
                            ),
                          ),
                        ))
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class ItemsTile extends StatelessWidget {
  ItemsTile(this._title, this._items);

  final Iterable<Item> _items;
  final String _title;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        ListTile(title: Text(_title)),
        Column(
          children: _items
              .map(
                (i) => Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16.0),
                  child: ListTile(
                    title: Text(i.label ?? ""),
                    trailing: Text(i.value ?? ""),
                  ),
                ),
              )
              .toList(),
        ),
      ],
    );
  }
}
