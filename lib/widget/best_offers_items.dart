// @dart=2.9
import 'package:choix_customer/Data/GetOffers.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/ui/offers/best_offres.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BestOfferItem extends StatelessWidget {
  OfferSelectedListner selectedListner;
  GetOffersData offerList;
  int index;
  BestOfferItem(GetOffersData this.offerList, OfferSelectedListner this.selectedListner,int this.index);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (){
        selectedListner.onOfferSelected(offerList,index);
      },
      child: Container(
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20)),
        margin: EdgeInsets.only(
            left: 30.0, top: 0.0, right: 30.0, bottom: 10.0),
        child: Padding(
          padding: EdgeInsets.only(left: 10,right: 10,bottom: 25,top: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Text(
                    offerList.name != null ? offerList.name : "",
                    style: TextStyle(
                        fontFamily: "Montserrat-SemiBold", fontSize: 25,),
                  ),
                  Spacer(),
                  IconButton(
                    onPressed: () {
                      selectedListner.onOfferSelected(offerList,index);
                      },
                    icon: offerList.isSelected ? Icon(
                      Icons.check_circle,
                      size: 30,
                      color: CustomColors.darkPinkColor,
                    ) : Icon(
                      Icons.circle_outlined,
                      size: 30,
                      color: CustomColors.unCheckedColor,
                    ),
                  ),
                ],
              ),
              Container(
                decoration: BoxDecoration(
                    color: CustomColors.lightPinkColor,
                    borderRadius: BorderRadius.circular(10)),
                margin:
                EdgeInsets.symmetric(horizontal: 0, vertical: 2),
                child: Padding(
                  padding: const EdgeInsets.all(3.0),
                  child: Text(offerList.code != null ? offerList.code :"",
                      style: TextStyle(
                        fontFamily: "Montserrat-Bold",
                        fontSize: 9,
                        color: Colors.black,
                      )),
                ),
              ),
              Text(
                offerList.description != null ?offerList.description : "" ,
                style:
                TextStyle(fontFamily: "Montserrat", fontSize: 12),
              ),
            ],
          ),
        ),
      ),
    );


  }

}