// @dart=2.9
import 'package:choix_customer/Data/TimeSlots.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/ui/select_time_slot.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SlotItem extends StatefulWidget {
  TimeSlotData prodcutCatList;
  TimeSelectedListner  timeSelectedListner;
  int index;
  SlotItem(TimeSlotData this.prodcutCatList,this.timeSelectedListner,this.index) : super();

  @override
  _SlotItemState createState() => _SlotItemState();
}

class _SlotItemState extends State<SlotItem> {
  bool isSelecetd = false;
  int value = 0;
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            child: Padding(
                padding: EdgeInsets.all(8),
                child: InkWell(
                  child: ListTile(
                    // onTap: () {
                    //   // savedData.timeSlot = widget.prodcutCatList;
                    //   // Navigator.push(context, MaterialPageRoute(
                    //   //   builder: (context) {
                    //   //     return ChooseAddress();
                    //   //   },
                    //   // ));
                    // },
                    leading: Icon(
                      Icons.watch_later_sharp,
                      color: CustomColors.darkPinkColor,
                    ),
                    title: Text(
                      widget.prodcutCatList.startTime +
                          ' - ' +
                          widget.prodcutCatList.endTime,
                      style: TextStyle(color: CustomColors.blackColor),
                    ),
                    // trailing: IconButton(
                    //     icon: Icon(
                    //   Icons.radio_button_off_sharp,
                    //   //color: CustomColors.darkPinkColor
                    // ),
                    // ),
                    trailing:   IconButton(
                      onPressed: () {
                        widget.timeSelectedListner.onOfferSelected(widget.prodcutCatList, widget.index);
                      },
                      icon: widget.prodcutCatList.isSelected ? Icon(
                        Icons.check_circle,
                        size: 30,
                        color: CustomColors.darkPinkColor,
                      ) : Icon(
                        Icons.circle_outlined,
                        size: 30,
                        color: CustomColors.unCheckedColor,
                      ),
                    ),
                  ),
                ))),
        Divider(
          height: 0,
          thickness: 1,
          indent: 30,
          endIndent: 35,
        ),
      ],
    );
  }
}
