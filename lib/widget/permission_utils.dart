// @dart=2.9
import 'package:flutter/cupertino.dart';
import 'package:permission/permission.dart';

class CheckPermission {
  static CheckPermission _instance;
  factory CheckPermission() => _instance ??= new CheckPermission._();

  CheckPermission._();


  static Future<bool> checkLocationPermission(BuildContext context) async {
    bool isPermission = false;
    Permission.getPermissionsStatus([PermissionName.Location])
        .then((value) async {
      print("ssss" + value[0].permissionStatus.toString());
      var status = value[0].permissionStatus;
      if (status.toString().contains("allow")) {
        isPermission = true;
        return isPermission;
      } else {
        print("test");
        // ToastUtils.showCustomToast(context, "Location permission not allowed");
        List<PermissionName> permissionNames = [];
        permissionNames.add(PermissionName.Location);
        permissionNames.add(PermissionName.WhenInUse);
        var message = '';
        await Permission.requestPermissions(permissionNames).then((value) {
          value.forEach((permission) {
            message +=
                '${permission.permissionName}: ${permission.permissionStatus}\n';
            print("mainPermission" + message);
            if (permission.permissionStatus.toString().contains("allow")) {
              isPermission = true;
              return isPermission;
            } else {
              isPermission = false;
              return isPermission;
            }
          });
        });
      }
    });
    return isPermission;
  }

  static Future<bool> checkLocationPermissionDash(
      BuildContext context, PermissionCheckListner checkListner) async {
    bool isPermission = false;
    Permission.getPermissionsStatus([PermissionName.Location])
        .then((value) async {
      print("ssss" + value[0].permissionStatus.toString());
      var status = value[0].permissionStatus;
      if (status.toString().contains("allow")) {
        isPermission = true;
        checkListner.permissionCheck(true);
        return isPermission;
      } else {
        print("test");
        // ToastUtils.showCustomToast(context, "Location permission not allowed");
        List<PermissionName> permissionNames = [];
        permissionNames.add(PermissionName.Location);
        permissionNames.add(PermissionName.WhenInUse);
        var message = '';
        await Permission.requestPermissions(permissionNames).then((value) {
          value.forEach((permission) {
            message +=
                '${permission.permissionName}: ${permission.permissionStatus}\n';
            print("mainPermission" + message);
            if (permission.permissionStatus.toString().contains("allow")) {
              isPermission = true;
              checkListner.permissionCheck(true);
              return isPermission;
            } else {
              isPermission = false;
              checkListner.permissionCheck(false);
              return isPermission;
            }
          });
        });
      }
    });
    return isPermission;
  }

  static Future<bool> checkLocationPermissionOnly(BuildContext context) async {
    bool isPermission = false;
    await Permission.getPermissionsStatus([PermissionName.Location])
        .then((value) async {
      print("ssss" + value[0].permissionStatus.toString());
      var status = value[0].permissionStatus;
      if (status.toString().contains("allow")) {
        isPermission = true;
        return isPermission;
      } else {
        print("test");
        isPermission = false;
        return isPermission;
      }
    });
    return isPermission;
  }
}

abstract class PermissionCheckListner {
  void permissionCheck(bool isGranted);
}
