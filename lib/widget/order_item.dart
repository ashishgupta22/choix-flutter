// @dart=2.9
import 'package:choix_customer/Data/ProductSubcategory.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/ui/orders/order_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';



class OrderItem extends StatefulWidget {
  ProductSubCatData selectedItem;
  ItemChangeListner changeListner;
  int index;
  OrderItem(ProductSubCatData this.selectedItem, int this.index,
      ItemChangeListner this.changeListner)
      : super();

  @override
  _OrderItem createState() => _OrderItem();
}

class _OrderItem extends State<OrderItem> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
      margin: EdgeInsets.only(left: 0, right: 0, top: 5, bottom: 5),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(20)),
            child: Column(
              children: [
                Row(
                  children: [
                    Flexible(
                      child: Row(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(5),
                                  topLeft: Radius.circular(5),
                                ),
                                image: DecorationImage(
                                    image: NetworkImage(widget.selectedItem.image),
                                    fit: BoxFit.cover)),
                            width: 80,
                            height: 70,
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                left: 20.0, top: 0.0, right: 0.0, bottom: 0.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      widget.selectedItem.name,
                                      style: TextStyle(
                                          color: CustomColors.blackColor,
                                          fontSize: 18,
                                          fontFamily: "Montserrat"),
                                    ),
                                    // SizedBox(
                                    //                                     //   width: 140,
                                    //                                     // ),
                                    Align(
                                      alignment: Alignment.topRight,
                                      child: InkWell(
                                          // onTap: onTapFun(2),
                                          child: Icon(Icons.delete_forever)),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    InkWell(
                                      onTap: onTapFun(0),
                                      child: Icon(
                                        Icons.remove_circle_outline_sharp,
                                        color: CustomColors.darkPinkColor,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    Text(
                                     widget.selectedItem.count.toString(),
                                      style: TextStyle(
                                          color: CustomColors.darkPinkColor),
                                    ),
                                    SizedBox(
                                      width: 10,
                                    ),
                                    InkWell(
                                      onTap: onTapFun(1),
                                      child: Icon(
                                        Icons.add_circle_sharp,
                                        color: CustomColors.darkPinkColor,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 30,
                                    ),
                                    Text(
                                      '1 Kg',
                                      style: TextStyle(
                                          color: CustomColors.blackColor,
                                          fontSize: 12,
                                          fontFamily: "Montserrat-Bold"),
                                    ),
                                    SizedBox(
                                      width: 30,
                                    ),
                                    Container(
                                      child: Text(
                                        '\$' + widget.selectedItem.price,
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.black,
                                            fontFamily: "Montserrat-Bold"),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  onTapFun(int type) {
    widget.changeListner.myProtocal(type, widget.index);
  }
}
