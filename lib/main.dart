// @dart=2.9

import 'dart:async';
import 'package:choix_customer/ui/dashboard_screen.dart';
import 'package:choix_customer/widget/permission_utils.dart';

import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import 'Utill/AppSharedPreferences.dart';
import 'ui/login_screen.dart';
import 'Data/intro_screen.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

const _kShouldTestAsyncErrorOnInit = false;

// Toggle this for testing Crashlytics in your app locally.
const _kTestingCrashlytics = true;

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();
  print('Handling a background message ${message.notification}');
  if (message.data.isNotEmpty) {
    if (message.data["type"] == "popup") {
      floorDatabase(message.data);
    }
    if (message.data["type"] == "popupAccept") {
      floorDatabase(message.data);
    }
  }
}

void floorDatabase(Map<String, dynamic> data) async {
  // print("Message not null main");
  // final database =
  //     await $FloorAppDatabaseCustomer.databaseBuilder('app_database.db').build();
  // //
  // final personDao = database.personDao;
  // NotificationModel firstUser = NotificationModel(
  //     message: data["message"],
  //     orderID: data["orderid"],
  //     isRead: false,
  //     status: data["status"]);
  // await personDao.insertPerson(firstUser);
}

AndroidNotificationChannel channel;
FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  // await SystemChrome.setPreferredOrientations(
  //   [DeviceOrientation.portraitUp],
  // );
  await Firebase.initializeApp();
  // FirebaseCrashlytics.instance.crash();

  // Set the background messaging handler early on, as a named top-level function
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

  if (!kIsWeb) {
    channel = const AndroidNotificationChannel(
      'fcmcha12s11', // id
      'High Importance Notifications', // title
      'This channel is used for important notifications.', // description
      importance: Importance.high,
    );

    flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    /// Create an Android Notification Channel.
    ///
    /// We use this channel in the `AndroidManifest.xml` file to override the
    /// default FCM channel to enable heads up notifications.
    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    /// Update the iOS foreground notification presentation options to allow
    /// heads up notifications.
    await FirebaseMessaging.instance
        .setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true,
    );
  }

  // runApp(MyApp());
  runZonedGuarded(() {
    runApp(MyApp());
  }, FirebaseCrashlytics.instance.recordError);
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  // static FirebaseAnalytics analytics = FirebaseAnalytics();
  // static FirebaseAnalyticsObserver observer =
  // FirebaseAnalyticsObserver(analytics: analytics);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    return MaterialApp(
      title: 'Choix Customer',
      theme: ThemeData(
        //primarySwatch: CustomColors.darkPinkColor,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      debugShowCheckedModeBanner: false,
      // navigatorObservers: <NavigatorObserver>[observer],
      home: SplashScreen(),
      builder: EasyLoading.init(),
      //builder: EasyLoading.init(),
    );
  }
}

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> implements PermissionCheckListner{

  //
  Future<void> _testAsyncErrorOnInit() async {
    Future<void>.delayed(const Duration(seconds: 2), () {
      final List<int> list = <int>[];
      print(list[100]);
    });
  }

  // Define an async function to initialize FlutterFire
  Future<void> _initializeFlutterFire() async {
    // Wait for Firebase to initialize

    if (_kTestingCrashlytics) {
      // Force enable crashlytics collection enabled if we're testing it.
      await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
    } else {
      // Else only enable it in non-debug builds.
      // You could additionally extend this to allow users to opt-in.
      await FirebaseCrashlytics.instance
          .setCrashlyticsCollectionEnabled(!kDebugMode);
    }

    // Pass all uncaught errors to Crashlytics.
    Function originalOnError = FlutterError.onError;
    FlutterError.onError = (FlutterErrorDetails errorDetails) async {
      await FirebaseCrashlytics.instance.recordFlutterError(errorDetails);
      // Forward to original handler.
      originalOnError(errorDetails);
    };

    if (_kShouldTestAsyncErrorOnInit) {
      await _testAsyncErrorOnInit();
    }
  }

  // _sendAnalyticsEvent() async {
  //   FirebaseAnalytics analytics = FirebaseAnalytics();
  //
  //   await analytics.logEvent(
  //     name: "save_plant",
  //     parameters: <String, dynamic>{
  //       'plant_name':"test",
  //     },
  //   );
  // }
  bool isLogin = false;
  bool isIntro = false;

  @override
  void initState() {
    super.initState();
    _initializeFlutterFire();
    // _sendAnalyticsEvent();
    checkIsLogin();
    // floorDatabase("test Notification", false);
    // return;
    CheckPermission.checkLocationPermissionDash(context, this);

  }
void runSplash(){
  Timer(
    Duration(seconds: 3),
        () => {
      if (isLogin)
        {
          // Navigator.of(context).pushReplacement(
          //   MaterialPageRoute(
          //     builder: (BuildContext context) => DashboardPage(),
          //   ),
          // ),
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => DashboardPage(),
              ),
              ModalRoute.withName('/signature'))
        }
      else
        {
          if (isIntro)
            {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginScreen()))
            }
          else
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(
                builder: (BuildContext context) => IntroPage(),
              ),
            ),
        }
    },
  );
}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Image.asset("assets/images/logo.png"),
      ),
    );
  }

  Future<void> checkIsLogin() async {
    isLogin = await AppSharedPreferences.instance.getIsLogin();
    isIntro = await AppSharedPreferences.instance.getIsIntro();
    // double.parse(isLogin.toString());
    // List list
    // list.add("add");
  }

  @override
  void permissionCheck(bool isGranted) {
    runSplash();
    // TODO: implement permissionCheck
  }
}
