// @dart=2.9
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomTextFiled extends StatelessWidget{
  bool autoFocus;
  FocusNode myFocusNode;
  TextEditingController txtPhoneController;
  bool isPhnValidate;
  String hintText;
  String labelText;
  TextInputType textInputType;
  TextInputAction textInputAction;
  String validation;
  int maxLength;
  bool isEnable;
  CustomTextFiled(bool this.autoFocus, FocusNode this.myFocusNode, TextEditingController this.txtPhoneController, bool this.isPhnValidate,String this.hintText,String this.labelText, TextInputType this.textInputType, TextInputAction this.textInputAction, String this.validation,int this.maxLength,this.isEnable);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  TextField(
      focusNode: myFocusNode,
      autofocus: autoFocus,
      controller: txtPhoneController,
      maxLength: maxLength,
      keyboardType: textInputType,
      textInputAction: textInputAction,
      //style: TextStyle(color: CustomColors.side_bar_text),
      decoration: InputDecoration(
        enabled: isEnable,
        counter: Offstage(),
        filled: true,
        fillColor: Colors.white,
        hintText: hintText,
        hintStyle: TextStyle(color: CustomColors.hintColor),
        labelText: labelText,
        labelStyle: TextStyle(
            color: myFocusNode.hasFocus
                ? Colors.black
                : Colors.black,
            fontFamily: "Montserrat"),
        //hintStyle: TextStyle(color: CustomColors.side_bar_text),
        contentPadding: const EdgeInsets.only(
            left: 20.0, bottom: 8.0, top: 8.0, right: 20),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[300]),
            borderRadius:
            BorderRadius.all(Radius.circular(30))),

        errorText: !isPhnValidate ? validation : null,
        disabledBorder:OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[200]),
            borderRadius:
            BorderRadius.all(Radius.circular(30))),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[200]),
            borderRadius:
            BorderRadius.all(Radius.circular(30))),
        errorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[200]),
            borderRadius:
            BorderRadius.all(Radius.circular(30))),
        focusedErrorBorder:  OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[200]),
            borderRadius:
            BorderRadius.all(Radius.circular(30))),
      ),
    );
  }

}