
import 'package:choix_customer/Data/Notification.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/ui/notification.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NotificationWidget extends StatelessWidget {

  NotificationData dataList;
  NotificationSelectedListner selectedListner;
  NotificationWidget(NotificationData this.dataList,NotificationSelectedListner this.selectedListner) : super();

  @override
  Widget build(BuildContext context) {
    // return Text("hi");
    // initializeDateFormatting('IST');
    DateFormat sdf = new DateFormat("dd-MM-yyyy HH:mm");
    DateTime date = sdf.parse(dataList.created);
    var now =  DateTime.now();
    var days = now.difference(date).inDays;
    var hours = now.difference(date).inHours;
    var mintus = now.difference(date).inMinutes;
    String time = days > 0 ? days.toString() + "day" : (hours > 0 ? hours.toString() + "hr" : mintus.toString()+"min");
    return Card(
      color: Colors.white,
      // color: CustomColors.lightPinkColor,
      margin: EdgeInsets.only(left: 5, right: 5, top: 5, bottom: 5),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(20)),
            margin: EdgeInsets.only(top: 8, left: 15, right: 8, bottom: 8),
            padding: EdgeInsets.only(bottom: 10),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      //color: Colors.grey,
                      // margin: EdgeInsets.only(top: 10),
                      height: 45,
                      width: 45,
                      decoration: BoxDecoration(
                        color: CustomColors.darkPinkColor,
                        // image: backgroundImage != null
                        //     ? new DecorationImage(image: backgroundImage, fit: BoxFit.cover)
                        //     : null,

                        shape: BoxShape.circle,
                      ),
                      child: Image.asset(
                        "assets/images/bel.png",
                        height: 20,
                        color: Colors.white,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(right: 10.0),
                    ),
                    Flexible(
                      fit: FlexFit.tight,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            dataList.title,
                            style: TextStyle(
                              fontFamily: "Montserrat",
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            dataList.data,
                            style: TextStyle(
                              fontFamily: "Montserrat",
                            ),
                          ),
                        ],
                      ),
                    ),
                    // SizedBox(
                    //   width: MediaQuery.of(context).size.width * .15,
                    // ),
                    // Spacer(),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        InkWell(
                          child: Icon(
                            Icons.delete,
                            color: CustomColors.darkPinkColor,
                          ),
                          onTap: (){
                            selectedListner.isSelected(1,  dataList);
                          },
                        ),
                        Text(
                          time,
                          style:
                              TextStyle(fontFamily: "Montserrat", fontSize: 10),
                        )
                      ],
                    )
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
