// @dart=2.9
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomRegisterTextFiled extends StatelessWidget{
  bool autoFocus;
  FocusNode myFocusNode;
  TextEditingController txtPhoneController;
  bool isPhnValidate;
  String hintText;
  String labelText;
  IconData remove_red_eye_outlined;
  TextInputType textInputType;
  TextInputAction textInputAction;
  int maxLength;
  String valitadion;
  CustomRegisterTextFiled(bool this.autoFocus, FocusNode this.myFocusNode, TextEditingController this.txtPhoneController, bool this.isPhnValidate,String this.hintText,String this.labelText, IconData this.remove_red_eye_outlined, TextInputType this.textInputType, TextInputAction this.textInputAction,int this.maxLength,String this.valitadion);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return   TextField(
      // focusNode: myFocusNode,
      autofocus: true,
      controller: txtPhoneController,
      keyboardType: textInputType,
      textInputAction: textInputAction,
      maxLength: maxLength,
      //style: TextStyle(color: CustomColors.side_bar_text),
      decoration: InputDecoration(
        counter: Offstage(),
        suffixIcon: IconButton(
          onPressed: (){},
          icon: Icon(remove_red_eye_outlined),
        ),
        filled: true,
        fillColor: Colors.white,
        hintStyle: TextStyle(color: CustomColors.hintColor),
        hintText: hintText,
        labelText: labelText,

        labelStyle: TextStyle(
            color: myFocusNode.hasFocus
                ? Colors.black
                : Colors.black,
            fontFamily: "Montserrat"

        ),
        //hintStyle: TextStyle(color: CustomColors.side_bar_text),
        contentPadding:
        const EdgeInsets.only(
            left: 20.0, bottom: 8.0, top: 8.0, right: 20),
        focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[300]),
            borderRadius: BorderRadius.all(Radius.circular(
                30))
        ),

        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[200]),
            borderRadius: BorderRadius.all(Radius.circular(
                30)),
        ),
        errorText: !isPhnValidate ? valitadion : null,
        errorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[200]),
            borderRadius:
            BorderRadius.all(Radius.circular(30))),
        focusedErrorBorder:  OutlineInputBorder(
            borderSide: BorderSide(color: Colors.grey[200]),
            borderRadius:
            BorderRadius.all(Radius.circular(30))),
      ),
    );
  }

}