// @dart=2.9
import 'dart:async';
import 'dart:io';
import 'dart:ui';
import 'package:choix_customer/Data/PendingPaymentOrder.dart';
import 'package:choix_customer/Data/PopupModel.dart';
import 'package:choix_customer/Data/Productcategory.dart';
import 'package:choix_customer/Data/UserData.dart';
import 'package:choix_customer/Data/VenderList.dart';
import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/RetryClickListner.dart';
import 'package:choix_customer/Utill/alert_dialog_new.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:choix_customer/ui/all_product.dart';
import 'package:choix_customer/ui/choose_address.dart';
import 'package:choix_customer/ui/offers/offer_screen.dart';
import 'package:choix_customer/ui/orders/order_history.dart';
import 'package:choix_customer/ui/orders/order_list.dart';
import 'package:choix_customer/ui/payments/wallet_screen.dart';
import 'package:choix_customer/ui/privacyPolicy_Terms.dart';
import 'package:choix_customer/ui/profile.dart';
import 'package:choix_customer/ui/savedAddress.dart';
import 'package:choix_customer/ui/tomorrow_add_to_cart.dart';
import 'package:choix_customer/widget/permission_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:location/location.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:url_launcher/url_launcher.dart';
import 'NoDataFound.dart';
import 'about_us.dart';
import 'login_screen.dart';
import 'notification.dart';
import 'orders/OnGoingOrders.dart';
import 'orders/accept_pending_payment.dart';
import 'referral_screen.dart';
import 'package:intl/intl.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

GlobalKey<ScaffoldState> _keyNavigation = new GlobalKey<ScaffoldState>();
List<CatData> prodcutCatList = [];

List<TodayTimings> todayTiming = [];

List<TodayTimings> tommorowTiming = [];

UserData userData;
ApiProvider provider = ApiProvider();

StreamSubscription fcmListener;
Timer _timer;
bool isDialogShowing = false;
int timerSeconds = 7;

class DashboardPage extends StatefulWidget {
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage>
    with WidgetsBindingObserver
    implements DialogSelectedListner {
  Widget homePage;
  List<Widget> bottomSheet = [];
  int currentState = 0;
  String profileImage = "";

  bool  isRunBG = false, inProgressSocket = false;
  String userName = "";

  String versionCode = "";

  bool isReferralShow = false;

  @override
  void initState() {
    // TODO: implement initState
    WidgetsBinding.instance.addObserver(this);
    _keyNavigation = new GlobalKey<ScaffoldState>();
    super.initState();
    callInit();
    homePage = HomePage();
    bottomSheet = [
      homePage,
      NotificationScreen(),
      OrderHistoryScreen(),
      OfferScreen(),
    ];
    setState(() {
      bottomSheet;
    });
    getNotification();
    getUserData();
    // requestPermissions();
    // getPendingOrders();
    versionCode1();
    callTimer();
  }

  void callTimer() {
    if(mounted) {
      cancelTimer();
      _timer = new Timer.periodic(Duration(seconds: timerSeconds), (timer) {
        print("timerDashboard : " + timer.tick.toString());
        print("timerActive : " + timer.isActive.toString());
        if (!isDialogShowing) callPopupAPI();
      });
    }
  }

  cancelTimer() {
    if (_timer != null) {
       _timer.cancel();
    }
  }

  void versionCode1() {
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      String version = packageInfo.version;
      String buildNumber = packageInfo.buildNumber;
      if (Platform.isAndroid) {
        versionCode = version;
      } else {
        versionCode = buildNumber;
      }
    });
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // state = state;
    // print(state);
    print(
        ":::::::---------------------------------------------------------------1111");
    if (AppLifecycleState.resumed == state) {
      print(
          ":::::::---------------------------------------------------------------ONResume");
      if(mounted)
      callTimer();
      // getPendingOrders();
      getNotification();
    } else if (AppLifecycleState.paused == state) {
      print(
          ":::::::---------------------------------------------------------------OnPause");
      cancelTimer();
      // getPendingOrders();
      // getNotification();
    }
  }

  Future<void> requestPermissions() async {
    // PermissionName permissionName = PermissionName.Internet;
    //
    // List<PermissionName> permissionNames = [];
    // permissionNames.add(PermissionName.Location);
    // permissionNames.add(PermissionName.WhenInUse);
    // var message = '';
    // var permissions = await Permission.requestPermissions(permissionNames);
    // permissions.forEach((permission) {
    //   message +=
    //       '${permission.permissionName}: ${permission.permissionStatus}\n';
    //   print("mainPermission" + message);
    // });

    // var status = await Permission.location.status;
    //
    // if (await Permission.location.isRestricted) {
    //   // The OS restricts access, for example because of parental controls.
    // }

    // setState(() {});
    getLocationData();
  }

  void floorDatabase(Map<String, dynamic> data, String type) async {
    // print("Message not null dashboard1010");
    // print("Message ${data["status"]}");
    //
    // if (type == "popupAccept") {
    //   final database = await $FloorAppDatabaseCustomer
    //       .databaseBuilder('app_database.db')
    //       .build();
    //   //
    //   final personDao = database.personDao;
    //   final oldNotification =
    //       await personDao.findNotifiationByOrderId(data["orderid"]);
    //   if (oldNotification != null) {
    //     print("Message not null dashboard");
    //   } else {
    //     NotificationModel firstUser = NotificationModel(
    //         message: data["message"],
    //         orderID: data["orderid"],
    //         isRead: false,
    //         status: data["status"]);
    //     await personDao.insertPerson(firstUser);
    //     getNotification();
    //   }
    // } else {
    //   final database = await $FloorAppDatabaseCustomer
    //       .databaseBuilder('app_database.db')
    //       .build();
    //   final personDao = database.personDao;
    //   print("Message not null dashboard else");
    //   NotificationModel firstUser = NotificationModel(
    //       message: data["message"],
    //       orderID: data["orderid"],
    //       isRead: false,
    //       status: data["status"]);
    //   await personDao.insertPerson(firstUser);
    //   getNotification();
    //   // 22-9-2021
    //   // var oldNotification =
    //   //     await personDao.findNotifiationByOrderIdAll(data["orderid"]);
    //   // if (oldNotification.isNotEmpty) {
    //   //   bool isAlreadyShow = true;
    //   //   oldNotification.forEach((element) {
    //   //     if (data["orderid"] == element.orderID &&
    //   //         data["status"] == element.status) {
    //   //       isAlreadyShow = false;
    //   //     }
    //   //   });
    //   //   if (isAlreadyShow) {
    //   //     print("Message not null dashboard else");
    //   //     NotificationModel firstUser = NotificationModel(
    //   //         message: data["message"],
    //   //         orderID: data["orderid"],
    //   //         isRead: false,
    //   //         status: data["status"]);
    //   //     await personDao.insertPerson(firstUser);
    //   //     getNotification();
    //   //   }
    //   // } else {
    //   //   print("Message not null dashboard else");
    //   //   NotificationModel firstUser = NotificationModel(
    //   //       message: data["message"],
    //   //       orderID: data["orderid"],
    //   //       isRead: false,
    //   //       status: data["status"]);
    //   //   await personDao.insertPerson(firstUser);
    //   //   oldNotification =
    //   //       await personDao.findNotifiationByOrderIdAll(data["orderid"]);
    //   //   getNotification();
    //   // }
    // }
  }

  void getNotification() async {
    // try {
    //   final database = await $FloorAppDatabaseCustomer
    //       .databaseBuilder('app_database.db')
    //       .build();
    //
    //   final personDao = database.personDao;
    //
    //   await personDao.findAllPersons(false).then((value) {
    //     print("Testsss${value.length}");
    //     if (value.isNotEmpty && value.length > 0) {
    //       if (!isDialogShowing) {
    //         isDialogShowing = true;
    //         if (mounted) {
    //           value[0].isRead = true;
    //           personDao.updateNotification(value[0]);
    //           showDialogPopup(personDao, value[0]);
    //         }
    //       }
    //     }
    //   });
    // } catch (_) {}
  }

  // final database=$FloorAppDatabase.databaseBuilder('tododatabase.db').build();
//     database.then((onValu){
// // find the dao here
//       onValu.todoDao.getMaxTodo().then((onValue){
// // max id
//         int maxId = onValue
//
  getLocationData() async {
    // CheckPermission.checkLocationPermission(context).then((value) {});
    // _locationData = await location.getLocation();
    // location.enableBackgroundMode(enable: true);
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        onWillPop: _onWillPop,
        child: Scaffold(
          drawer: Container(
            width: MediaQuery.of(context).size.width / 1.5,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30),
                  bottomRight: Radius.circular(30)),
              color: CustomColors.darkPinkColor,
            ),
            child: Padding(
              padding: EdgeInsets.only(left: 20, right: 20, top: 50),
              child: Stack(
                children: [
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Padding(
                      padding: EdgeInsets.all(30),
                      child: Text("VERSION V $versionCode",
                          style: TextStyle(
                              color: Colors.white60,
                              fontSize: 12,
                              fontWeight: FontWeight.w400)),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      print("onTap2");
                    },
                    child: Container(
                      margin: EdgeInsets.only(bottom: 50),
                      child: SingleChildScrollView(
                        child: Column(
                          children: [
                            Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.only(top: 40),
                              alignment: Alignment.centerLeft,
                              child: Row(
                                children: [
                                  InkWell(
                                    child: Container(
                                      width: 45.0,
                                      height: 45.0,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                          image: profileImage.isNotEmpty
                                              ? NetworkImage(
                                                  userData.profileImage)
                                              : AssetImage(
                                                  "assets/images/logo.png",
                                                ),
                                          fit: BoxFit.cover,
                                        ),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(100.0)),
                                        border: Border.all(
                                          color: Colors.white,
                                          width: 1.0,
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 0,
                                    width: 15,
                                  ),
                                  Flexible(
                                    child: Text(userName,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16,
                                            fontWeight: FontWeight.normal)),
                                  )
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 50,
                              width: 0,
                            ),
                            InkWell(
                                onTap: () {
                                  if (_keyNavigation
                                      .currentState.isDrawerOpen) {
                                    _keyNavigation.currentState.openEndDrawer();
                                  }
                                  setState(() {
                                    currentState = 0;
                                  });
                                },
                                child: _naveWidth(
                                    "HOME", "assets/images/home.png")),
                            InkWell(
                                onTap: () {
                                  if (_keyNavigation
                                      .currentState.isDrawerOpen) {
                                    _keyNavigation.currentState.openEndDrawer();
                                  }
                                  cancelTimer();
                                  Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  OnGoingOrders()))
                                      .then((value) => callTimer());
                                },
                                child: _naveWidth(
                                    "ONGOING ORDERS", "assets/images/bag.png")),
                            InkWell(
                                onTap: () {
                                  if (_keyNavigation
                                      .currentState.isDrawerOpen) {
                                    _keyNavigation.currentState.openEndDrawer();
                                  }
                                  cancelTimer();
                                  Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  AllProduct()))
                                      .then((value) => callTimer());
                                },
                                child: _naveWidth(
                                    "PRICES", "assets/images/product.png")),
                            // InkWell(
                            //     onTap: () {
                            //       if (_keyNavigation.currentState.isDrawerOpen) {
                            //         _keyNavigation.currentState.openEndDrawer();
                            //       }
                            //       setState(() {
                            //         currentState = 2;
                            //       });
                            //     },
                            //     child: _naveWidth("ORDER HISTORY",
                            //         "assets/images/history.png")),
                            // InkWell(
                            //     onTap: () {
                            //       if (_keyNavigation.currentState.isDrawerOpen) {
                            //         _keyNavigation.currentState.openEndDrawer();
                            //       }
                            //       setState(() {
                            //         currentState = 1;
                            //       });
                            //     },
                            //     child: _naveWidth(
                            //         "NOTIFICATION", "assets/images/bel.png")),
                            InkWell(
                                onTap: () {
                                  if (_keyNavigation
                                      .currentState.isDrawerOpen) {
                                    _keyNavigation.currentState.openEndDrawer();
                                  }
                                  cancelTimer();
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              ProfileScreen())).then((value) {
                                    // getPendingOrders();
                                    callTimer();
                                    getUserData();
                                  });
                                },
                                child: _naveWidth(
                                    "PROFILE", "assets/images/user_avtar.png")),
                            // InkWell(
                            //     onTap: () {
                            //       if (_keyNavigation.currentState.isDrawerOpen) {
                            //         _keyNavigation.currentState.openEndDrawer();
                            //       }
                            //       setState(() {
                            //         currentState = 3;
                            //       });
                            //     },
                            //     child: _naveWidth(
                            //         "OFFERS", "assets/images/Offers.png")),
                            InkWell(
                                onTap: () {
                                  if (_keyNavigation
                                      .currentState.isDrawerOpen) {
                                    _keyNavigation.currentState.openEndDrawer();
                                  }
                                  cancelTimer();
                                  Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) =>
                                                  SavedAddress([], 1)))
                                      .then((value) => callTimer());
                                },
                                child: _naveWidth("SAVED ADDRESS",
                                    "assets/images/location.png")),
                            // InkWell(
                            //     onTap: (){},
                            //     child: _naveWidth("SETTING","assets/images/setting.png")
                            // ),
                            if (isReferralShow)
                              InkWell(
                                  onTap: () {
                                    if (_keyNavigation
                                        .currentState.isDrawerOpen) {
                                      _keyNavigation.currentState
                                          .openEndDrawer();
                                    }
                                    cancelTimer();
                                    Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    Referral_screen()))
                                        .then((value) => callTimer());
                                  },
                                  child: _naveWidth(
                                      "REFERRAL", "assets/images/mike.png")),
                            InkWell(
                                onTap: () {
                                  if (_keyNavigation
                                      .currentState.isDrawerOpen) {
                                    _keyNavigation.currentState.openEndDrawer();
                                  }
                                  cancelTimer();
                                  Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                              builder: (context) => AboutUs()))
                                      .then((value) => callTimer());
                                },
                                child: _naveWidth("ABOUT US",
                                    "assets/images/information.png")),
                            InkWell(
                                onTap: () {
                                  if (_keyNavigation
                                      .currentState.isDrawerOpen) {
                                    _keyNavigation.currentState.openEndDrawer();
                                  }
                                  redirectURL(WebApiConstaint.TERMS_URL,
                                      "TERMS & CONDITIONS");
                                },
                                child: _naveWidth("TERMS&CONDITIONS",
                                    "assets/images/terms_condition.png")),
                            InkWell(
                                onTap: () {
                                  if (_keyNavigation
                                      .currentState.isDrawerOpen) {
                                    _keyNavigation.currentState.openEndDrawer();
                                  }
                                  redirectURL(WebApiConstaint.POLICY_URL,
                                      "PRIVACY POLICY");
                                },
                                child: _naveWidth("PRIVACY POLICY",
                                    "assets/images/privacy_policy.png")),
                            InkWell(
                                onTap: () {
                                  if (_keyNavigation
                                      .currentState.isDrawerOpen) {
                                    _keyNavigation.currentState.openEndDrawer();
                                  }
                                  showDialog(
                                    context: context,
                                    builder: (context) => Padding(
                                      padding: const EdgeInsets.only(
                                          left: 20, right: 20),
                                      child: Stack(
                                        children: <Widget>[
                                          Center(
                                            child: Container(
                                              padding: EdgeInsets.only(
                                                  // left: 20,
                                                  // top: Constants.padding,
                                                  // right: 20,
                                                  bottom: 20),
                                              decoration: BoxDecoration(
                                                  shape: BoxShape.rectangle,
                                                  color: Colors.white,
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  boxShadow: [
                                                    BoxShadow(
                                                        color: Colors.black,
                                                        offset: Offset(0, 10),
                                                        blurRadius: 10),
                                                  ]),
                                              child: Container(
                                                padding: EdgeInsets.only(
                                                    // left: 20,
                                                    // top: Constants.padding,
                                                    // right: 20,
                                                    bottom: 20),
                                                child: Column(
                                                  mainAxisSize:
                                                      MainAxisSize.min,
                                                  children: <Widget>[
                                                    Container(
                                                      width:
                                                          MediaQuery.of(context)
                                                              .size
                                                              .width,
                                                      height: 80,
                                                      decoration: BoxDecoration(
                                                        borderRadius:
                                                            BorderRadius.only(
                                                                topLeft: Radius
                                                                    .circular(
                                                                        10),
                                                                topRight: Radius
                                                                    .circular(
                                                                        10)),
                                                        gradient:
                                                            LinearGradient(
                                                                begin: Alignment
                                                                    .centerLeft,
                                                                end: Alignment(
                                                                    2.8, 0.0),
                                                                colors: [
                                                              CustomColors
                                                                  .darkPinkColor,
                                                              Colors.white
                                                            ]),
                                                      ),
                                                      child: Center(
                                                        child: Text(
                                                          AlertMessage
                                                              .msgAlertTitle,
                                                          style: TextStyle(
                                                              fontSize: 25,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .w600,
                                                              color:
                                                                  Colors.white),
                                                        ),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: 35,
                                                    ),
                                                    Text(
                                                      AlertMessage.msgAppLogout,
                                                      style: TextStyle(
                                                          fontSize: 20),
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                    SizedBox(
                                                      height: 35,
                                                    ),
                                                    Align(
                                                      alignment:
                                                          Alignment.bottomRight,
                                                      child: Container(
                                                        padding:
                                                            EdgeInsets.only(
                                                          left: 20,
                                                          // top: Constants.padding,
                                                          right: 20,
                                                        ),
                                                        child: Row(
                                                          children: [
                                                            InkWell(
                                                              child: Padding(
                                                                padding:
                                                                const EdgeInsets
                                                                    .only(
                                                                    left:
                                                                    20),
                                                                child:
                                                                Container(
                                                                  height: 50,
                                                                  width: 80,
                                                                  decoration:
                                                                  BoxDecoration(
                                                                    color: CustomColors
                                                                        .darkPinkColor,
                                                                    borderRadius:
                                                                    BorderRadius.circular(
                                                                        50),
                                                                  ),
                                                                  child: Center(
                                                                    child: Text(
                                                                      WebApiConstaint
                                                                          .btnYes,
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                          18,
                                                                          color:
                                                                          Colors.white),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                              onTap: () {
                                                                CallAPILogout();
                                                                // Navigator.push(context, MaterialPageRoute(builder: (context) => LoginScreen(),));
                                                              },
                                                            ),
                                                            Spacer(),
                                                            Padding(
                                                              padding:
                                                              const EdgeInsets
                                                                  .only(
                                                                  right:
                                                                  20),
                                                              child: Container(
                                                                height: 50,
                                                                width: 80,
                                                                decoration:
                                                                BoxDecoration(
                                                                  color: CustomColors
                                                                      .blackColor,
                                                                  borderRadius:
                                                                  BorderRadius
                                                                      .circular(
                                                                      50),
                                                                ),
                                                                child: Center(
                                                                  child:
                                                                  InkWell(
                                                                    onTap: () =>
                                                                        Navigator.of(context)
                                                                            .pop(false),
                                                                    child: Text(
                                                                      WebApiConstaint
                                                                          .btnNo,
                                                                      style: TextStyle(
                                                                          fontSize:
                                                                          18,
                                                                          color:
                                                                          Colors.white),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  );
                                },
                                child: _naveWidth(
                                    "SIGN OUT", "assets/images/logout.png"))
                          ],
                        ),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      print("onTap");
                      if (_keyNavigation.currentState.isDrawerOpen) {
                        _keyNavigation.currentState.openEndDrawer();
                      }
                    },
                    child: Container(
                      height: 30,
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Container(
                            width: 26,
                            height: 26,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              color: Colors.white,
                            ),
                            child: Padding(
                              padding: EdgeInsets.all(5.0),
                              child:
                                  Image.asset("assets/images/arrow_back.png"),
                            )),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          key: _keyNavigation,
          body: Stack(
            children: [
              bottomSheet[currentState],
              Align(
                alignment: FractionalOffset.bottomCenter,
                child: Padding(
                  padding: EdgeInsets.all(15),
                  child: Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.only(left: 15, right: 15),
                    height: 65,
                    width: MediaQuery.of(context).size.width,
                    decoration: new BoxDecoration(
                      gradient: LinearGradient(
                          begin: const Alignment(0.0, -2.0),
                          end: const Alignment(0.0, 0.3),
                          colors: [Colors.white, Colors.white]),
                      shape: BoxShape.rectangle,
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(30)),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 4,
                          blurRadius: 50,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Row(
                      children: [
                        Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Column(
                              children: [
                                Container(
                                  child: Divider(
                                    thickness: 4,
                                    height: 4,
                                    color: currentState == 0
                                        ? CustomColors.darkPinkColor
                                        : Colors.white,
                                  ),
                                  margin: EdgeInsets.only(
                                      left:
                                          MediaQuery.of(context).size.width *
                                              0.06,
                                      right:
                                          MediaQuery.of(context).size.width *
                                              0.06),
                                ),
                                SizedBox(
                                  height: 11,
                                ),
                                InkWell(
                                    onTap: () {
                                      setState(() {
                                        callTimer();
                                        currentState = 0;
                                      });
                                    },
                                    child: Image.asset(
                                      "assets/images/home.png",
                                      height: 30,
                                      width: 30,
                                      color: currentState == 0
                                          ? CustomColors.darkPinkColor
                                          : Colors.grey,
                                    )),
                              ],
                            )

                            //)
                            ),
                        Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Column(
                              children: [
                                Container(
                                  child: Divider(
                                    thickness: 4,
                                    height: 4,
                                    color: currentState == 1
                                        ? CustomColors.darkPinkColor
                                        : Colors.white,
                                  ),
                                  margin: EdgeInsets.only(
                                      left:
                                          MediaQuery.of(context).size.width *
                                              0.06,
                                      right:
                                          MediaQuery.of(context).size.width *
                                              0.06),
                                ),
                                SizedBox(
                                  height: 13,
                                ),
                                InkWell(
                                    onTap: () {
                                      setState(() {
                                        callTimer();
                                        currentState = 1;
                                      });
                                    },
                                    child: Image.asset(
                                      "assets/images/notification_inactive.png",
                                      height: 30,
                                      width: 30,
                                      color: currentState == 1
                                          ? CustomColors.darkPinkColor
                                          : Colors.grey,
                                    )),
                              ],
                            )),
                        Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Column(
                              children: [
                                Container(
                                  child: Divider(
                                    thickness: 4,
                                    height: 4,
                                    color: currentState == 2
                                        ? CustomColors.darkPinkColor
                                        : Colors.white,
                                  ),
                                  margin: EdgeInsets.only(
                                      left:
                                          MediaQuery.of(context).size.width *
                                              0.06,
                                      right:
                                          MediaQuery.of(context).size.width *
                                              0.06),
                                ),
                                SizedBox(
                                  height: 13,
                                ),
                                InkWell(
                                    onTap: () {
                                      setState(() {
                                        callTimer();
                                        currentState = 2;
                                      });
                                    },
                                    child: Image.asset(
                                      "assets/images/history_inactive.png",
                                      height: 30,
                                      width: 30,
                                      color: currentState == 2
                                          ? CustomColors.darkPinkColor
                                          : Colors.grey,
                                    )),
                              ],
                            )),
                        Flexible(
                          flex: 1,
                          fit: FlexFit.tight,
                          child: InkWell(
                              onTap: () {
                                cancelTimer();
                                Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                WalletScreen()))
                                    .then((value) => callTimer());
                              },
                              child: Image.asset(
                                "assets/images/wallet_inactive.png",
                                height: 30,
                                width: 30,
                                color: Colors.grey,
                              )),
                        ),
                        Flexible(
                            flex: 1,
                            fit: FlexFit.tight,
                            child: Column(
                              children: [
                                Container(
                                  child: Divider(
                                    thickness: 4,
                                    height: 4,
                                    color: currentState == 3
                                        ? CustomColors.darkPinkColor
                                        : Colors.white,
                                  ),
                                  margin: EdgeInsets.only(
                                      left:
                                          MediaQuery.of(context).size.width *
                                              0.06,
                                      right:
                                          MediaQuery.of(context).size.width *
                                              0.06),
                                ),
                                SizedBox(
                                  height: 14,
                                ),
                                InkWell(
                                    onTap: () {
                                      setState(() {
                                        callTimer();
                                        currentState = 3;
                                      });
                                    },
                                    child: Image.asset(
                                      "assets/images/Offers.png",
                                      height: 30,
                                      width: 30,
                                      color: currentState == 3
                                          ? CustomColors.darkPinkColor
                                          : Colors.grey,
                                    )),
                              ],
                            )),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ));
  }

  void showPickUp(
      BuildContext context, String message, String btnYes, String btnNo) {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        contentPadding: EdgeInsets.zero,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(10.0))),
        content: Builder(
          builder: (context) {
            // Get available height and width of the build area of this widget. Make a choice depending on the size.
            var width = MediaQuery.of(context).size.width;
            return Container(
              width: width,
              height: MediaQuery.of(context).size.height * 0.32,
              padding: EdgeInsets.only(bottom: 20),
              child: Column(
                children: [
                  Container(
                    width: width,
                    height: MediaQuery.of(context).size.height * 0.09,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10)),
                      gradient: LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment(2.8, 0.0),
                          colors: [CustomColors.darkPinkColor, Colors.white]),
                    ),
                    child: Center(
                        child: Text(
                      AlertMessage.msgAlertTitle,
                      style: TextStyle(color: Colors.white, fontSize: 25),
                    )),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(30),
                    child: Text(
                      message,
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 20),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 15, right: 15),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        if (btnNo.isNotEmpty)
                          InkWell(
                            onTap: () {
                              Navigator.of(context).pop(false);
                            },
                            child: Container(
                              height: 40,
                              width: 100,
                              decoration: BoxDecoration(
                                color: CustomColors.blackColor,
                                borderRadius: BorderRadius.circular(50),
                              ),
                              child: Center(
                                  child: Text(
                                btnNo,
                                style: TextStyle(
                                    color: Colors.white, fontSize: 15),
                              )),
                            ),
                          ),
                        Spacer(),
                        InkWell(
                          onTap: () {
                            Navigator.of(context).pop(false);
                            CallAPILogout();
                          },
                          child: Container(
                            height: 40,
                            width: 100,
                            decoration: BoxDecoration(
                              color: CustomColors.darkPinkColor,
                              borderRadius: BorderRadius.circular(50),
                            ),
                            child: Center(
                                child: Text(
                              "Yes",
                              style:
                                  TextStyle(color: Colors.white, fontSize: 15),
                            )),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _naveWidth(String categoryName, String iconUrl) {
    return Container(
      height: 40,
      padding: EdgeInsets.only(right: 15, left: 15),
      width: MediaQuery.of(context).size.width,
      alignment: Alignment.centerLeft,
      child: Row(
        children: [
          Image.asset(
            iconUrl,
            height: 16,
            width: 16,
            color: Colors.white,
          ),
          SizedBox(
            height: 0,
            width: 15,
          ),
          Text(categoryName,
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                  fontWeight: FontWeight.normal))
        ],
      ),
    );
  }

  Future<void> CallAPILogout() async {
    if (userData == null) {
      userData = await AppSharedPreferences.instance.getUserDetails();
    }
    EasyLoading.show();
    provider
        .requestGetForApi(context, WebApiConstaint.URL_LOGOUT,
            userData.sessionKey, userData.authorization)
        .then((responce) {
      print("SessionKey : " +
          userData.sessionKey +
          " authKey : " +
          userData.authorization);
      EasyLoading.dismiss();

      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            Fluttertoast.showToast(msg: responce.data["message"]);
            AppSharedPreferences.instance.setLogin(false);
            cancelTimer();
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => LoginScreen()));
          } else {
            if (responce.data["authenticate"] == false) {
              cancelTimer();
              AppSharedPreferences.instance.setLogin(false);
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginScreen()));
            }
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }

  Future<bool> _onWillPop() async {
    return (await showDialog(
          context: context,
          builder: (context) => Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Stack(
              children: <Widget>[
                Center(
                  child: Container(
                    padding: EdgeInsets.only(bottom: 20),
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black,
                              offset: Offset(0, 10),
                              blurRadius: 10),
                        ]),
                    child: Container(
                      padding: EdgeInsets.only(
                          // left: 20,
                          // top: Constants.padding,
                          // right: 20,
                          bottom: 20),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width,
                            height: 80,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(10),
                                  topRight: Radius.circular(10)),
                              gradient: LinearGradient(
                                  begin: Alignment.centerLeft,
                                  end: Alignment(2.8, 0.0),
                                  colors: [
                                    CustomColors.darkPinkColor,
                                    Colors.white
                                  ]),
                            ),
                            child: Center(
                              child: Text(
                                AlertMessage.msgAlertTitle,
                                style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 35,
                          ),
                          Text(
                            AlertMessage.msgAppExit,
                            style: TextStyle(fontSize: 20),
                            textAlign: TextAlign.center,
                          ),
                          SizedBox(
                            height: 35,
                          ),
                          Align(
                            alignment: Alignment.bottomRight,
                            child: Container(
                              padding: EdgeInsets.only(
                                left: 20,
                                // top: Constants.padding,
                                right: 20,
                              ),
                              child: Row(
                                children: [
                                  InkWell(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: 20),
                                      child: Container(
                                        height: 50,
                                        width: 80,
                                        decoration: BoxDecoration(
                                          color: CustomColors.darkPinkColor,
                                          borderRadius:
                                          BorderRadius.circular(50),
                                        ),
                                        child: Center(
                                          child: Text(
                                            WebApiConstaint.btnYes,
                                            style: TextStyle(
                                                fontSize: 18,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ),
                                    onTap: () {
                                      Navigator.of(context).pop(true);
                                      // Navigator.push(context, MaterialPageRoute(builder: (context) => LoginScreen(),));
                                    },
                                  ),

                                  Spacer(),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 20),
                                    child: Container(
                                      height: 50,
                                      width: 80,
                                      decoration: BoxDecoration(
                                        color: CustomColors.blackColor,
                                        borderRadius: BorderRadius.circular(50),
                                      ),
                                      child: Center(
                                        child: InkWell(
                                          onTap: () => Navigator.pop(context),
                                          child: Text(
                                            WebApiConstaint.btnNo,
                                            style: TextStyle(
                                                fontSize: 18,
                                                color: Colors.white),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),

                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        )) ??
        false;
  }

  Future<void> getPendingOrders() async {
    if (userData == null)
      userData = await AppSharedPreferences.instance.getUserDetails();
    provider
        .requestGetForApi(context, WebApiConstaint.URL_PENDINGPAYMENTORDER,
            userData.sessionKey, userData.authorization)
        .then((responce) {
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            PendingPaymentOrder order =
                PendingPaymentOrder.fromJson(responce.data);
            if (order.data != null && order.data.created != null) {
              isDialogShowing = true;
              AlertDialogNew.showConfirmationDialog(context, null, "", "Yes",
                  "No", order.data, userData, provider);
              // showDialog(
              //   context: context,
              //   barrierDismissible: false,
              //   builder: (_) => AcceptPendingPaymentOrder(order.data),
              // );
            }
          }
        } catch (e) {
          ToastUtils.showCustomToast(context, e.toString());
        }
      }
    });
    return;
  }

  void callInit() async {
    // strToggle =  getTranslated(context, 'offline').toString();
    print("message callinit");
    // FirebaseMessaging.instance.getToken().then((token) {
    //   print("token " + token);
    // });
    if (fcmListener != null) fcmListener.cancel();

    fcmListener = FirebaseMessaging.onMessage
        .asBroadcastStream()
        .listen((RemoteMessage message) {
      // do stuff
      print("message " + "Notification call${message.messageId}");
      // RemoteNotification notification = message.notification;
      // if (message.data.isNotEmpty) {
      //   if (message.data["type"] == "popup") {
      //     floorDatabase(message.data, message.data["type"]);
      //   }
      //   if (message.data["type"] == "popupAccept") {
      //     floorDatabase(message.data, message.data["type"]);
      //   }
      // }
      // getPendingOrders();
    });

    // FirebaseMessaging.onMessage.listen((RemoteMessage message) {
    //   print("message " + "Notification call${message.messageId}");
    //   RemoteNotification notification = message.notification;
    //   if(message.data.isNotEmpty){
    //     if(message.data["type"] == "popup") {
    //       floorDatabase(message.data,message.data["type"]);
    //     }
    //     if(message.data["type"] == "popupAccept") {
    //       floorDatabase(message.data,message.data["type"]);
    //     }
    //   }
    //   // if (notification != null) {
    //   //   String msgBody = notification.body;
    //   //   print("message " + msgBody);
    //   //   getPendingOrders();
    //   //   // getNotification();
    //   // } else {
    //   //   print("message " + "body is null");
    //   // }
    // });
    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) {
      print("open from message");
      // getPendingOrders();
      getNotification();
    });
    FirebaseMessaging.instance
        .requestPermission(
          alert: true,
          announcement: true,
          badge: true,
          carPlay: false,
          criticalAlert: false,
          provisional: false,
          sound: true,
        )
        .whenComplete(() => null);
    // getNotification();
  }

  Future<void> getUserData() async {
    userData = await AppSharedPreferences.instance.getUserDetails();
    setState(() {
      userData;
      profileImage = userData.profileImage;
      userName = userData.name != null ? userData.name : "";
    });
  }

  @override
  onSelected(bool isSelected) {
    // TODO: implement onSelected
    print(isSelected);
    Navigator.of(context).pop(isSelected);
  }

  void redirectURL(String url, String title) async {
    cancelTimer();
    print(url);
    Navigator.push(context,
            MaterialPageRoute(builder: (context) => Web_View(url, title)))
        .then((value) => callTimer());
  }

  void showDialogPopup(String message) {
    message = message.replaceAll("\\n", "\n");
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => WillPopScope(
        onWillPop: () async => false,
        child: Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Stack(
            children: <Widget>[
              Center(
                child: Container(
                  padding: EdgeInsets.only(
                      // left: 20,
                      // top: Constants.padding,
                      // right: 20,
                      bottom: 20),
                  decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black,
                            offset: Offset(0, 10),
                            blurRadius: 10),
                      ]),
                  child: Container(
                    padding: EdgeInsets.only(
                        // left: 20,
                        // top: Constants.padding,
                        // right: 20,
                        bottom: 20),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: Constants.dialog_titile_height,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                                topRight: Radius.circular(10)),
                            gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment(2.8, 0.0),
                                colors: [
                                  CustomColors.darkPinkColor,
                                  Colors.white
                                ]),
                          ),
                          child: Container(
                            margin: EdgeInsets.only(
                              left: 20,
                              // top: Constants.padding,
                              right: 20,
                            ),
                            child: Center(
                              child: Padding(
                                padding: const EdgeInsets.only(
                                    left: 8.0, right: 8.0),
                                child: Text(
                                  AlertMessage.msgAlertTitle,
                                  style: TextStyle(
                                      fontSize: 25,
                                      fontWeight: FontWeight.w600,
                                      color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 35,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                          child: Text(
                            message,
                            style: TextStyle(fontSize: 16),
                            textAlign: TextAlign.center,
                          ),
                        ),
                        SizedBox(
                          height: 35,
                        ),
                        Container(
                          padding: EdgeInsets.only(
                            left: 20,
                            // top: Constants.padding,
                            right: 20,
                          ),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              InkWell(
                                child: Container(
                                  height: Constants.dialog_btn_height,
                                  width: 100,
                                  decoration: BoxDecoration(
                                    color: CustomColors.darkPinkColor,
                                    borderRadius: BorderRadius.circular(50),
                                  ),
                                  child: Center(
                                    child: Text(
                                      "Ok",
                                      style: TextStyle(
                                          fontSize: 18, color: Colors.white),
                                    ),
                                  ),
                                ),
                                onTap: () {
                                  // if(selectedListner != null)
                                  //   selectedListner.onSelected(true);
                                  // else
                                  isDialogShowing = false;
                                  Navigator.pop(context);
                                  // Navigator.push(context, MaterialPageRoute(builder: (context) => LoginScreen(),));
                                },
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> callPopupAPI() async {
    if (userData == null)
      userData = await AppSharedPreferences.instance.getUserDetails();
    provider
        .requestGetForApi(context, WebApiConstaint.URL_POPUP,
            userData.sessionKey, userData.authorization)
        .then((responce) {
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            if (isReferralShow != responce.data["check_refferel_menu"]) {
              isReferralShow = responce.data["check_refferel_menu"];
              setState(() {
                isReferralShow;
              });
            }

            // Fluttertoast.showToast(msg: responce.data["message"]);
            PopupModel order = PopupModel.fromJson(responce.data);
            if (order.data != null) {
              if (order.data.type.endsWith("Complete")) {
                getPendingOrders();
              } else {
                isDialogShowing = true;
                showDialogPopup(order.data.data);
              }
            }
          } else {
            if (responce.data["authenticate"] == false) {
              AppSharedPreferences.instance.setLogin(false);
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                  ModalRoute.withName("/Home"));
            }
            cancelTimer();
            Fluttertoast.showToast(msg: responce.data["message"]);
          }
        } catch (e) {
          ToastUtils.showCustomToast(context, e.toString());
        }
      }
    });
  }
}

class PendingPaymentDialogBox extends StatefulWidget {
  PendingPaymentOrderData data;

  PendingPaymentDialogBox(PendingPaymentOrderData this.data) : super();

  @override
  _PendingPaymentDialogBoxState createState() =>
      _PendingPaymentDialogBoxState();
}

class _PendingPaymentDialogBoxState extends State<PendingPaymentDialogBox> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20),
      ),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  contentBox(context) {
    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
              left: Constants.padding,
              top: Constants.avatarRadius + Constants.padding,
              right: Constants.padding,
              bottom: Constants.padding),
          margin: EdgeInsets.only(top: Constants.avatarRadius),
          decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(Constants.padding),
              boxShadow: [
                BoxShadow(
                    color: Colors.black, offset: Offset(0, 10), blurRadius: 10),
              ]),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                "Order Confirmation",
                style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                "New Order Received. Please check and accept now",
                style: TextStyle(fontSize: 14),
                textAlign: TextAlign.center,
              ),
              SizedBox(
                height: 22,
              ),
              Align(
                alignment: Alignment.bottomRight,
                child: FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => AcceptPendingPaymentOrder(
                                  widget.data.createdAt != null
                                      ? widget.data
                                      : null))).then((value) => {});
                    },
                    child: Text(
                      "View",
                      style: TextStyle(fontSize: 18),
                    )),
              ),
            ],
          ),
        ),
        Positioned(
          left: Constants.padding,
          right: Constants.padding,
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: Constants.avatarRadius,
            child: ClipRRect(
                borderRadius:
                    BorderRadius.all(Radius.circular(Constants.avatarRadius)),
                child: Image.asset("assets/images/logo.png")),
          ),
        ),
      ],
    );
  }

  void getPendingOrders() async {
    if (userData == null)
      userData = await AppSharedPreferences.instance.getUserDetails();
    provider
        .requestGetForApi(context, WebApiConstaint.URL_PENDINGPAYMENTORDER,
            userData.sessionKey, userData.authorization)
        .then((responce) {
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            Fluttertoast.showToast(msg: responce.data["message"]);
            PendingPaymentOrder order =
                PendingPaymentOrder.fromJson(responce.data);
            if (order.data != null) {
              showDialog(
                context: context,
                barrierDismissible: false,
                builder: (_) => PendingPaymentDialogBox(order.data),
              );
            }
          } else {
            if (responce.data["authenticate"] == false) {
              AppSharedPreferences.instance.setLogin(false);
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                  ModalRoute.withName("/Home"));
            }
            Fluttertoast.showToast(msg: responce.data["message"]);
          }
        } catch (e) {
          ToastUtils.showCustomToast(context, e.toString());
        }
      }
    });
  }
}

class Constants {
  static double avatarRadius = 20;

  static double padding = 20;

  static double dialog_btn_height = 40;

  static double dialog_titile_height = 60;
}

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    implements RetryClickListner, PermissionCheckListner {
  //product Cat List
  GoogleMapController mapController;
  LatLng _center = const LatLng(0, 0);
  Location _location;
  bool noRecordFound = false;

  List<VendorData> vandorList = [];
  Set<Marker> allMarkers = Set();

  var myLocationEnabled = false;

  //
  void _onMapCreated(GoogleMapController controller) {
    CheckPermission.checkLocationPermissionOnly(context).then((value) async {
      if (value) {
        myLocationEnabled = true;
        mapController= controller;
        mapController.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(target: _center, zoom: 15),
          ),
        );
        setState(() {

        });
        updateLocation();
      }
    });

  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getProductCate(context);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // state = state;
    // print(state);
    // print(
    //     ":::::::---------------------------------------------------------------2222");
    // if (AppLifecycleState.resumed == state) {
    //   getAllVendors(context);
    // }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: CustomColors.darkPinkColor,
          toolbarHeight: 0,
        ),
        body: RefreshIndicator(
          color: CustomColors.darkPinkColor,
          onRefresh: () {
            return getPendingOrders();
          },
          child: Container(
            child: Column(
              children: [
                Container(
                  alignment: Alignment.bottomLeft,
                  width: MediaQuery.of(context).size.width,
                  height: 60,
                  decoration: BoxDecoration(
                    borderRadius:
                    BorderRadius.only(bottomRight: Radius.circular(30)),
                    color: CustomColors.darkPinkColor,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: InkWell(
                      onTap: () async {
                        // Fluttertoast.showToast(
                        //     msg: result.name);
                        _keyNavigation.currentState.openDrawer();
                      },
                      child: Container(
                          width: 30,
                          height: 30,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(5)),
                            color: Colors.white,
                          ),
                          child: Padding(
                            padding: EdgeInsets.all(5.0),
                            child: Image.asset("assets/images/menu.png"),
                          )),
                    ),
                  ),
                ),
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        if (allMarkers.isNotEmpty)
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 10.0, bottom: 10, right: 10, left: 12.0),
                            child: Text(
                              "There are vendors found in your area. Please start ordering!!",
                              style: TextStyle(
                                  color: CustomColors.darkPinkColor,
                                  fontFamily: "Montserrat",
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                        Container(
                          height: MediaQuery.of(context).size.height / 3,
                          child: myLocationEnabled ?
                          GoogleMap(
                            onMapCreated: _onMapCreated,
                            myLocationEnabled: myLocationEnabled,
                            mapType: MapType.normal,
                            scrollGesturesEnabled: true,
                            initialCameraPosition: CameraPosition(
                              target: _center,
                              zoom: 13.0,
                            ),
                            gestureRecognizers:
                            <Factory<OneSequenceGestureRecognizer>>[
                              new Factory<OneSequenceGestureRecognizer>(
                                    () => new EagerGestureRecognizer(),
                              ),
                            ].toSet(),
                            markers: allMarkers,
                          ) :  SizedBox(),
                        ),
                        Container(
                          padding: EdgeInsets.only(bottom: 80, top: 10),
                          child: noRecordFound
                              ? NoDataFound(this)
                              : StaggeredGridLayout(),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          )),
        );
  }

  updateLocation() {
    CheckPermission.checkLocationPermissionOnly(context).then((value) async {
      if (value) {
        if (_location == null) _location = Location();
        _location.changeSettings(distanceFilter: 10);
        _location.onLocationChanged.listen((l) {
          if (mapController != null) {
            mapController.animateCamera(
              CameraUpdate.newCameraPosition(
                CameraPosition(
                    target: LatLng(l.latitude, l.longitude), zoom: 15),
              ),
            );
          }
        });
      }
    });
  }

  Future<void> getProductCate(BuildContext context) async {
    await AppSharedPreferences.instance.getUserDetails().then((userData1) {
      EasyLoading.show();
      provider
          .requestGetForApi(context, WebApiConstaint.URL_GETPRODUCTCATEGORy,
              userData1.sessionKey, userData1.authorization)
          .then((responce) {
        getAllVendors(context);
        print("SessionKey : " +
            userData1.sessionKey +
            " authKey : " +
            userData1.authorization);
        EasyLoading.dismiss();

        if (responce != null && responce.data != null) {
          try {
            if (responce.data["error"] == false) {
              var resposdata = Productcategory.fromJson(responce.data);
              noRecordFound = false;
              prodcutCatList = resposdata.data.catData;
              todayTiming = resposdata.data.todayTimings;
              tommorowTiming = resposdata.data.tommorowTimings;
              check_update(responce.data["data"]);
              setState(() {});
            } else {
              if (responce.data["authenticate"] == false) {
                cancelTimer();
                AppSharedPreferences.instance.setLogin(false);
                Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginScreen()))
                    .then((value) => getPendingOrders());
              } else {
                noRecordFound = true;
                setState(() {});
              }
              Fluttertoast.showToast(msg: responce.data["message"]);
              print(responce.data["message"]);
            }
          } catch (_) {
            noRecordFound = true;
            setState(() {});
            ToastUtils.showCustomToast(
                context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
          }
        } else {
          noRecordFound = true;
          setState(() {});
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      });
    });
  }

  Future<void> getPendingOrders() async {
    if (userData == null)
      userData = await AppSharedPreferences.instance.getUserDetails();
    provider
        .requestGetForApi(context, WebApiConstaint.URL_PENDINGPAYMENTORDER,
            userData.sessionKey, userData.authorization)
        .then((responce) {
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            PendingPaymentOrder order =
                PendingPaymentOrder.fromJson(responce.data);
            if (order.data != null && order.data.created != null) {
              isDialogShowing = true;
              AlertDialogNew.showConfirmationDialog(context, null, "", "Yes",
                  "No", order.data, userData, provider);
              // showDialog(
              //   context: context,
              //   barrierDismissible: false,
              //   builder: (_) => AcceptPendingPaymentOrder(order.data),
              // );
            }
          }
        } catch (e) {
          ToastUtils.showCustomToast(context, e.toString());
        }
      }
    });
    return;
  }

  Future<void> getAllVendors(BuildContext context) async {
    if (userData == null)
      userData = await AppSharedPreferences.instance.getUserDetails();
    CheckPermission.checkLocationPermissionDash(context, this)
        .then((value) async {
      if (value != null && value) {
        myLocationEnabled = true;
        EasyLoading.show();
        Location location = new Location();
        LocationData _pos = await location.getLocation();
        _center = LatLng(_pos.latitude, _pos.longitude);
        updateLocation();
        if(mapController != null)
        mapController.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
                target: LatLng(_pos.latitude, _pos.longitude), zoom: 15),
          ),
        );
        setState(() {
          print(myLocationEnabled);
        });
      }
    });
  }

  getAllVendorAPI() {
      CheckPermission.checkLocationPermissionOnly(context).then((value) async {
        if (value != null && value) {
          myLocationEnabled = true;
        EasyLoading.show();
        Location location = new Location();
        LocationData _pos = await location.getLocation();
        _center = LatLng(_pos.latitude, _pos.longitude);
        updateLocation();
        if(mapController != null)
        mapController.animateCamera(
          CameraUpdate.newCameraPosition(
            CameraPosition(
                target: LatLng(_pos.latitude, _pos.longitude), zoom: 15),
          ),
        );
        print(WebApiConstaint.URL_ALLVENDOR +
            "?latitude=${_pos.latitude}&longitude=${_pos.longitude}");
        provider
            .requestGetForApi(
                context,
                WebApiConstaint.URL_ALLVENDOR +
                    "?latitude=${_pos.latitude}&longitude=${_pos.longitude}",
                userData.sessionKey,
                userData.authorization)
            .then((responce) {
          print("SessionKey : " +
              userData.sessionKey +
              " authKey : " +
              userData.authorization);
          EasyLoading.dismiss();

          if (responce != null) {
            try {
              if (responce.data["error"] == false) {
                // setState(() {});
                // Fluttertoast.showToast(
                //      msg: responce.data["message"]);
                var resposdata = VendorList.fromJson(responce.data);
                noRecordFound = false;
                allMarkers.clear();
                if (resposdata.data.isNotEmpty) {
                  vandorList = resposdata.data;
                  vandorList.forEach((element) {
                    if (element.latitude != null && element.latitude != null) {
                      if (element.latitude != "" && element.latitude != "") {
                        allMarkers.add(Marker(
                            markerId: MarkerId(element.latitude.toString()),
                            draggable: false,
                            position: LatLng(
                                double.parse(element.latitude.toString()),
                                double.parse(element.longitude.toString()))));
                      }
                    }
                  });
                }
                setState(() {
                  allMarkers;
                });
              } else {
                if (responce.data["authenticate"] == false) {
                  AppSharedPreferences.instance.setLogin(false);
                  cancelTimer();
                  Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => LoginScreen()))
                      .then((value) => getPendingOrders());
                } else {
                  noRecordFound = true;
                  setState(() {});
                }
                Fluttertoast.showToast(msg: responce.data["message"]);
                print(responce.data["message"]);
              }
            } catch (_) {
              noRecordFound = true;
              setState(() {});
              ToastUtils.showCustomToast(
                  context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
            }
          } else {
            noRecordFound = true;
            setState(() {});
          }
        });
      }
    });
  }

  @override
  void onClickListner() {
    getProductCate(context);
  }

  Future<void> check_update(data) async {
    //app_update_message
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      String version = packageInfo.version;
      String buildNumber = packageInfo.buildNumber;
      print("Version : $version");
      if (Platform.isAndroid) {
        if (data["version"] != version) {
          updateDialog(Platform.isAndroid, data["app_update_message"]);
        }
      } else if (data["build_version"] != buildNumber) {
        updateDialog(Platform.isIOS, data["app_update_message"]);
      }
    });
  }

  updateDialog(bool isAndroid, String updateMessage) async {
    await showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context1) {
        String title = "New Update Available";
        String message = updateMessage;
        String btnLabel = "Update Now";
        String btnLabelCancel = "Exit";
        return new CupertinoAlertDialog(
          title: Text(title),
          content: Text(message),
          actions: <Widget>[
            FlatButton(
                child: Text(btnLabel),
                onPressed: () {
                  Navigator.pop(context1);
                  if (Platform.isIOS)
                    _launchURL(WebApiConstaint.APP_STORE_URL);
                  else
                    _launchURL(WebApiConstaint.PLAY_STORE_URL);
                }),
            FlatButton(
              child: Text(btnLabelCancel),
              onPressed: () {
                Navigator.pop(context1);
                if (Platform.isAndroid) {
                  SystemNavigator.pop();
                } else {}
              },
            ),
          ],
        );
      },
    );
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void permissionCheck(bool isGranted) {
    // TODO: implement permissionCheck
    if (isGranted) {
      getAllVendorAPI();
    }
  }

  cancelTimer() {
    if (_timer != null) {
      if (_timer.isActive) _timer.cancel();
    }
  }
}

class StaggeredGridLayout extends StatefulWidget {
  @override
  _StaggeredGridLayout createState() => _StaggeredGridLayout();
}

class _StaggeredGridLayout extends State<StaggeredGridLayout> {
  bool todayTimeAvail = false;
  bool tomrrowTimeAvail = false;

  @override
  Widget build(BuildContext context) {
    return StaggeredGridView.count(
      physics: NeverScrollableScrollPhysics(),
      //addAutomaticKeepAlives: true,
      crossAxisCount: 2,
      staggeredTiles: _cardTileList(),
      children: _childlist(context),
      shrinkWrap: true,
      mainAxisSpacing: 4.0,
      crossAxisSpacing: 4.0,
    );
  }

  //List of Cards with size
  List<StaggeredTile> _cardTileList() {
    List<StaggeredTile> _cardTile = [];
    for (int i = 0; i < prodcutCatList.length; i++) {
      // if (i == 0)
      //   _cardTile.add(StaggeredTile.count(2, 2));
      // else
      _cardTile.add(StaggeredTile.count(1, i.isEven ? 1.2 : 1.2));
    }
    return _cardTile;
  }

  List<Widget> _childlist(BuildContext context) {
    List<Widget> _listTile = [];
    for (int i = 0; i < prodcutCatList.length; i++) {
      _listTile.add(Padding(
        padding: EdgeInsets.only(
            left: 5.0, right: 5, top: i == 1 ? 5 : 5, bottom: 5),
        child: Container(
          alignment: Alignment.center,
          child: InkWell(
            onTap: () {
              todayTimeAvail = false;
              tomrrowTimeAvail = false;
              checkAvailabel();
              // AlertDialogNew.showPickUp(context, null,"testing message","Ok","");
              // return;
              showModalBottomSheet(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(10),
                      topLeft: Radius.circular(10),
                    ),
                  ),
                  context: context,
                  builder: (context) {
                    return Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              color: CustomColors.lightPinkColor,
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(10),
                                topLeft: Radius.circular(10),
                              )),
                          child: Container(
                            margin: EdgeInsets.only(
                                left: 30.0,
                                top: 30.0,
                                right: 30.0,
                                bottom: 30.0),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Text(
                                prodcutCatList[i].name.toString(),
                                style: TextStyle(
                                    fontSize: 20,
                                    color: CustomColors.blackColor,
                                    fontWeight: FontWeight.bold,
                                    fontFamily: "Montserrat"),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        ListTile(
                            leading: CircleAvatar(
                              child:
                                  new Icon(Icons.flash_on, color: Colors.white),
                              backgroundColor: CustomColors.darkPinkColor,
                            ),
                            title: Column(
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    new Text(
                                      'Today',
                                      style: TextStyle(
                                          fontFamily: "Montserrat",
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    if (!todayTimeAvail)
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 10.0),
                                        child: new Text(
                                          '(Not Available)',
                                          style: TextStyle(
                                              fontFamily: "Montserrat",
                                              fontSize: 12,
                                              color:
                                                  CustomColors.darkPinkColor),
                                        ),
                                      ),
                                  ],
                                ),
                                Text(
                                  'If you choose to buy today, you can choose from options available with the vendor!',
                                  style: TextStyle(
                                      fontFamily: "Montserrat", fontSize: 12),
                                ),
                                SizedBox(
                                  height: 20,
                                )
                              ],
                            ),
                            onTap: () {
                              if (todayTimeAvail) {
                                callAPICheckBalance(prodcutCatList[i], 2);
                              }
                            }),
                        ListTile(
                            leading: CircleAvatar(
                              child: new Icon(
                                Icons.calendar_today_outlined,
                                size: 20,
                                color: Colors.white,
                              ),
                              backgroundColor: CustomColors.darkPinkColor,
                            ),
                            title: Column(
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    new Text(
                                      'Tomorrow',
                                      style: TextStyle(
                                          fontFamily: "Montserrat",
                                          fontSize: 12,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    if (!tomrrowTimeAvail)
                                      Padding(
                                        padding:
                                            const EdgeInsets.only(left: 10.0),
                                        child: new Text(
                                          '(Not Available)',
                                          style: TextStyle(
                                              fontFamily: "Montserrat",
                                              fontSize: 12,
                                              color:
                                                  CustomColors.darkPinkColor),
                                        ),
                                      ),
                                  ],
                                ),
                                Text(
                                  'If you buy tomorrow, you could get your specific choices!',
                                  style: TextStyle(
                                      fontFamily: "Montserrat", fontSize: 12),
                                ),
                                SizedBox(
                                  height: 50,
                                )
                              ],
                            ),
                            onTap: () {
                              if (tomrrowTimeAvail) {
                                callAPICheckBalance(prodcutCatList[i], 1);

                                // Navigator.pop(context);
                              }
                            }),
                      ],
                    );
                  });
            },
            child: Stack(
              children: <Widget>[
                ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  child: FadeInImage.assetNetwork(
                    image: prodcutCatList[i].image,
                    placeholder: 'assets/images/logo.png',
                    height: MediaQuery.of(context).size.width,
                    width: MediaQuery.of(context).size.width,
                    fit: BoxFit.cover,
                  ),
                ),
                // Image.network(prodcutCatList[i].image),
                Container(
                  child: Container(
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: CustomColors.catBluerColor,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(
                          15,
                        ),
                        bottomRight: Radius.circular(15),
                      ),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        prodcutCatList[i].name,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontFamily: "Montserrat",
                          fontSize: 18,
                        ),
                      ),
                    ),
                  ),
                  alignment: Alignment.bottomCenter,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(15),
                      bottomRight: Radius.circular(15),
                    ),
                  ),
                ),
              ],
            ),
          ),
          decoration: BoxDecoration(
              color: Colors.amber, borderRadius: BorderRadius.circular(15)),
        ),
      ));
    }
    return _listTile;
  }

  void checkAvailabel() {
    var now = new DateTime.now();
    var formatter1 = new DateFormat('yyyy-MM-dd');
    var formatter = new DateFormat('yyyy-MM-dd HH:mm');
    String formattedDate = formatter1.format(now);
    print(formattedDate); // 2016-01-25
    if (todayTiming.isNotEmpty) {
      bool isAvailable = false;
      for (int i = 0; i < todayTiming.length; i++) {
        String startTime = formattedDate + " " + todayTiming[i].startTime;
        String endTime = formattedDate + " " + todayTiming[i].endTime;
        DateTime startDate = formatter.parse(startTime);
        DateTime endDate = formatter.parse(endTime);
        int startMilisecond = startDate.millisecondsSinceEpoch;
        int endMilisecond = endDate.millisecondsSinceEpoch;
        int currentMilisecond = now.millisecondsSinceEpoch;
        print(
            "start : $startDate end miliSecond : $endDate currentMili : $now");
        if (startMilisecond < currentMilisecond &&
            endMilisecond > currentMilisecond) {
          isAvailable = true;
          print(isAvailable);
          break;
        } else {
          isAvailable = false;
        }
      }
      if (isAvailable) {
        todayTimeAvail = true;
      }
    }
    if (tommorowTiming.isNotEmpty) {
      bool isAvailable = false;
      for (int i = 0; i < tommorowTiming.length; i++) {
        String startTime = formattedDate + " " + tommorowTiming[i].startTime;
        String endTime = formattedDate + " " + tommorowTiming[i].endTime;
        DateTime startDate = formatter.parse(startTime);
        DateTime endDate = formatter.parse(endTime);
        int startMilisecond = startDate.millisecondsSinceEpoch;
        int endMilisecond = endDate.millisecondsSinceEpoch;
        int currentMilisecond = now.millisecondsSinceEpoch;
        if (startMilisecond < currentMilisecond &&
            endMilisecond > currentMilisecond) {
          isAvailable = true;
          break;
        } else {
          isAvailable = false;
        }
      }
      if (isAvailable) {
        tomrrowTimeAvail = true;
      }
    }
  }

  void callAPICheckBalance(CatData prodcutCatList, int type) {
    EasyLoading.show();
    if (savedData.orderType == "Tomorrow") {}
    provider
        .requestGetForApi(context, WebApiConstaint.URL_WALLET_STATUS,
            userData.sessionKey, userData.authorization)
        .then((responce) {
      EasyLoading.dismiss();
      print(responce);

      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            if (type == 1) {
              savedData.category_id = prodcutCatList.id.toString();
              cancelTimer();
              Navigator.push(context, MaterialPageRoute(
                builder: (context) {
                  return TomorrowAddToCart(prodcutCatList);
                },
              )).then((value) => callTimer());
            } else {
              savedData.orderType = 'Today';
              savedData.category_id = prodcutCatList.id.toString();
              cancelTimer();
              Navigator.push(context, MaterialPageRoute(
                builder: (context) {
                  return ChooseAddress();
                },
              )).then((value) => callTimer());
            }
          } else {
            print(responce.data["message"]);
            ToastUtils.showCustomToast(context, responce.data["message"]);
            // Fluttertoast.showToast(msg: responce.data["message"]);
          }
        } catch (e) {
          ToastUtils.showCustomToast(context, e.toString());
        }
      }
    });

    //1 for Tomorrow  2 for today
  }

  void showDialogPopup(String message) {
    message = message.replaceAll("\\n", "\n");
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (context) => Padding(
        padding: const EdgeInsets.only(left: 20, right: 20),
        child: Stack(
          children: <Widget>[
            Center(
              child: Container(
                padding: EdgeInsets.only(
                    // left: 20,
                    // top: Constants.padding,
                    // right: 20,
                    bottom: 20),
                decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black,
                          offset: Offset(0, 10),
                          blurRadius: 10),
                    ]),
                child: Container(
                  padding: EdgeInsets.only(
                      // left: 20,
                      // top: Constants.padding,
                      // right: 20,
                      bottom: 20),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: Constants.dialog_titile_height,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(10),
                              topRight: Radius.circular(10)),
                          gradient: LinearGradient(
                              begin: Alignment.centerLeft,
                              end: Alignment(2.8, 0.0),
                              colors: [
                                CustomColors.darkPinkColor,
                                Colors.white
                              ]),
                        ),
                        child: Container(
                          margin: EdgeInsets.only(
                            left: 20,
                            // top: Constants.padding,
                            right: 20,
                          ),
                          child: Center(
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 8.0, right: 8.0),
                              child: Text(
                                AlertMessage.msgAlertTitle,
                                style: TextStyle(
                                    fontSize: 25,
                                    fontWeight: FontWeight.w600,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                        child: Text(
                          message,
                          style: TextStyle(fontSize: 16),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      Container(
                        padding: EdgeInsets.only(
                          left: 20,
                          // top: Constants.padding,
                          right: 20,
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            InkWell(
                              child: Container(
                                height: Constants.dialog_btn_height,
                                width: 100,
                                decoration: BoxDecoration(
                                  color: CustomColors.darkPinkColor,
                                  borderRadius: BorderRadius.circular(50),
                                ),
                                child: Center(
                                  child: Text(
                                    "Ok",
                                    style: TextStyle(
                                        fontSize: 18, color: Colors.white),
                                  ),
                                ),
                              ),
                              onTap: () {
                                // if(selectedListner != null)
                                //   selectedListner.onSelected(true);
                                // else
                                isDialogShowing = false;
                                Navigator.pop(context);
                                // Navigator.push(context, MaterialPageRoute(builder: (context) => LoginScreen(),));
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> callPopupAPI() async {
    if (userData == null)
      userData = await AppSharedPreferences.instance.getUserDetails();
    provider
        .requestGetForApi(context, WebApiConstaint.URL_POPUP,
            userData.sessionKey, userData.authorization)
        .then((responce) {
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            // Fluttertoast.showToast(msg: responce.data["message"]);
            PopupModel order = PopupModel.fromJson(responce.data);
            if (order.data != null) {
              if (order.data.type.endsWith("Complete")) {
                getPendingOrders();
              } else {
                isDialogShowing = true;
                showDialogPopup(order.data.data);
              }
            }
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
          }
        } catch (e) {
          ToastUtils.showCustomToast(context, e.toString());
        }
      }
    });
  }

  void callTimer() {
    cancelTimer();
    _timer = new Timer.periodic(Duration(seconds: timerSeconds), (timer) {
      print("timer : " + timer.tick.toString());
      print("timerActive : " + timer.isActive.toString());
      if (!isDialogShowing) callPopupAPI();
    });
  }

  cancelTimer() {
    if (_timer != null) {
      if (_timer.isActive) _timer.cancel();
    }
  }

  Future<void> getPendingOrders() async {
    if (userData == null)
      userData = await AppSharedPreferences.instance.getUserDetails();
    provider
        .requestGetForApi(context, WebApiConstaint.URL_PENDINGPAYMENTORDER,
            userData.sessionKey, userData.authorization)
        .then((responce) {
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            PendingPaymentOrder order =
                PendingPaymentOrder.fromJson(responce.data);
            if (order.data != null && order.data.created != null) {
              isDialogShowing = true;
              AlertDialogNew.showConfirmationDialog(context, null, "", "Yes",
                  "No", order.data, userData, provider);
              // showDialog(
              //   context: context,
              //   barrierDismissible: false,
              //   builder: (_) => AcceptPendingPaymentOrder(order.data),
              // );
            }
          }
        } catch (e) {
          ToastUtils.showCustomToast(context, e.toString());
        }
      }
    });
    return;
  }
}
