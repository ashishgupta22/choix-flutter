// @dart=2.9
import 'dart:io';

import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:choix_customer/ui/dashboard_screen.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';

class OtpPin extends StatefulWidget {
  String mobileNO;

  OtpPin(this.mobileNO) : super();

  static const String ROUTE_ID = 'SecurePin';

  @override
  OtpPinState createState() => OtpPinState();
}

class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}

class OtpPinState extends State<OtpPin> {
  ApiProvider provider = ApiProvider();
  String authKey, deviceType, deviceId;
  double ScreenHeight;
  TextEditingController controller1,
      controller2,
      controller3,
      controller4,
      controller5,
      controller6;

  //  controller5, controller6;
  FocusNode focusNode1 = FocusNode();
  FocusNode focusNode2 = FocusNode();
  FocusNode focusNode3 = FocusNode();
  FocusNode focusNode4 = FocusNode();
  FocusNode focusNode5 = FocusNode();
  FocusNode focusNode6 = FocusNode();

  String tokenMain = "";
  FToast fToast;

  @override
  void initState() {
    super.initState();
    controller1 = TextEditingController(text: "");
    controller2 = TextEditingController(text: "");
    controller3 = TextEditingController(text: "");
    controller4 = TextEditingController(text: "");
    controller5 = TextEditingController(text: "");
    controller6 = TextEditingController(text: "");
    fToast = FToast();
    fToast.init(context);
    FirebaseMessaging.instance.getToken().then((token) {
      tokenMain = token;
      AppSharedPreferences.instance.setFCMToken(tokenMain);
    });

    initPlatformState();
  }

  @override
  void dispose() {
    controller1.dispose();
    controller2.dispose();
    controller3.dispose();
    controller4.dispose();
    controller5.dispose();
    controller6.dispose();
    focusNode1.dispose();
    focusNode2.dispose();
    focusNode3.dispose();
    focusNode4.dispose();
    focusNode5.dispose();
    focusNode6.dispose();
    fToast.removeCustomToast();
    super.dispose();
  }

  Future<void> initPlatformState() async {
    DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        AndroidDeviceInfo info = await deviceInfoPlugin.androidInfo;
        deviceType = "a";
        deviceId = info.androidId;
        print('device id............${deviceId.toString()}');
      } else if (Platform.isIOS) {
        IosDeviceInfo info = await deviceInfoPlugin.iosInfo;
        deviceType = "i";
        deviceId = info.identifierForVendor;
        print('device id............$deviceId');
        print(await deviceInfoPlugin.iosInfo
          ..identifierForVendor);
      }
    } on PlatformException {
      print("Failed to get platform version.");
      Fluttertoast.showToast(msg: "Failed to get platform version.");
    }
  }

  @override
  Widget build(BuildContext context) {
    ScreenHeight = MediaQuery.of(context).size.height;
    var screenWidth = MediaQuery.of(context).size.width;
    return WillPopScope(
      onWillPop: willPop,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          toolbarHeight: 0,
          backgroundColor: Colors.white,
        ),
        body: Builder(
          builder: (BuildContext context1) {
            return SingleChildScrollView(
              child:  Column(
                children: [
                  Padding(
                      padding:
                      EdgeInsets.only(left: 0, bottom: 0, right: 0),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        color: Colors.white,
                        child: Image.asset(
                          "assets/images/top_bg.png",
                          fit: BoxFit.fitWidth,
                        ),
                      )),
                  SingleChildScrollView(
                      child: Container(
                        padding: EdgeInsets.only(
                            left: 0.05 * screenWidth,
                            top: 0.01 * ScreenHeight,
                            right: 0.05 * screenWidth,
                            bottom: 0.01 * ScreenHeight),
                        child: Container(
                          // padding: EdgeInsets.only(top: .16 * MediaQuery.of(context).size.height),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              //  mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                SizedBox(
                                  height:
                                  MediaQuery.of(context).size.height * 0.08,
                                  width: 0,
                                ),

                                Text(
                                  "Verification",
                                  style: TextStyle(
                                      fontSize: 50,
                                      fontWeight: FontWeight.w600,
                                      fontFamily: "Nueva"),
                                ),

                                //SizedBox(height: 5,width: 0,),
                                Text(
                                  "You will get a OTP via SMS",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontFamily: "Montserrat",
                                      fontWeight: FontWeight.w300),
                                ),
                                SizedBox(
                                  height: 20,
                                  width: 0,
                                ),

                                Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      width: screenWidth / 8,
                                      alignment: Alignment.center,
                                      child: TextField(
                                        keyboardType: TextInputType.number,
                                        controller: controller1,
                                        showCursor: false,
                                        focusNode: focusNode1,
                                        //readOnly: true,
                                        enabled: true,
                                        textAlign: TextAlign.center,
                                        maxLength: 1,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            color: Colors.black,
                                            fontSize: 20),
                                        decoration: InputDecoration(
                                            enabledBorder: UnderlineInputBorder(
                                              borderSide: BorderSide(
                                                  color: CustomColors
                                                      .otpInputBorderColor,
                                                  width:
                                                  CustomColors.otpBorderSize),
                                            ),
                                            contentPadding: EdgeInsets.all(5.0),
                                            counterText: "",
                                            border: null),
                                        onChanged: (String str) {
                                          if (str.isNotEmpty)
                                            focusNode2.requestFocus();
                                        },
                                      ),
                                    ),
                                    Container(
                                        width: screenWidth / 8,
                                        child: RawKeyboardListener(
                                          focusNode: FocusNode(),
                                          onKey: (event) {
                                            if (event.logicalKey ==
                                                LogicalKeyboardKey.backspace) {
                                              if (controller2.text.length == 0) {
                                                focusNode1
                                                    .requestFocus(); // = true;
                                              }
                                            }
                                          },
                                          child: TextField(
                                            keyboardType: TextInputType.number,
                                            controller: controller2,
                                            showCursor: false,
                                            focusNode: focusNode2,
                                            //readOnly: true,
                                            enabled: true,
                                            textAlign: TextAlign.center,
                                            maxLength: 1,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black,
                                                fontSize: 20),
                                            decoration: InputDecoration(
                                                enabledBorder:
                                                UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: CustomColors
                                                          .otpInputBorderColor,
                                                      width: CustomColors
                                                          .otpBorderSize),
                                                ),
                                                contentPadding:
                                                EdgeInsets.all(5.0),
                                                counterText: "",
                                                border: null),
                                            onChanged: (String str) {
                                              if (str.isEmpty) {
                                                focusNode1.requestFocus();
                                              } else {
                                                focusNode3.requestFocus();
                                              }
                                            },
                                          ),
                                        )),
                                    Container(
                                        width: screenWidth / 8,
                                        child: RawKeyboardListener(
                                          focusNode: FocusNode(),
                                          onKey: (event) {
                                            if (event.logicalKey ==
                                                LogicalKeyboardKey.backspace) {
                                              if (controller3.text.length == 0) {
                                                focusNode2
                                                    .requestFocus(); // = true;
                                              }
                                            }
                                          },
                                          child: TextField(
                                            keyboardType: TextInputType.number,
                                            controller: controller3,
                                            showCursor: false,
                                            focusNode: focusNode3,
                                            //readOnly: true,
                                            enabled: true,
                                            textAlign: TextAlign.center,
                                            maxLength: 1,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black,
                                                fontSize: 20),
                                            decoration: InputDecoration(
                                                enabledBorder:
                                                UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: CustomColors
                                                          .otpInputBorderColor,
                                                      width: CustomColors
                                                          .otpBorderSize),
                                                ),
                                                contentPadding:
                                                EdgeInsets.all(5.0),
                                                counterText: "",
                                                border: null),
                                            onChanged: (String str) {
                                              if (str.isEmpty) {
                                                focusNode2.requestFocus();
                                              } else {
                                                focusNode4.requestFocus();
                                              }
                                            },
                                          ),
                                        )),
                                    Container(
                                        width: screenWidth / 8,
                                        child: RawKeyboardListener(
                                          focusNode: FocusNode(),
                                          onKey: (event) {
                                            if (event.logicalKey ==
                                                LogicalKeyboardKey.backspace) {
                                              if (controller4.text.length == 0) {
                                                focusNode3
                                                    .requestFocus(); // = true;
                                              }
                                            }
                                          },
                                          child: TextField(
                                            keyboardType: TextInputType.number,
                                            controller: controller4,
                                            focusNode: focusNode4,
                                            showCursor: false,
                                            //readOnly: true,
                                            enabled: true,
                                            textAlign: TextAlign.center,
                                            maxLength: 1,

                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black,
                                                fontSize: 20),
                                            decoration: InputDecoration(
                                                enabledBorder:
                                                UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: CustomColors
                                                          .otpInputBorderColor,
                                                      width: CustomColors
                                                          .otpBorderSize),
                                                ),
                                                contentPadding:
                                                EdgeInsets.all(5.0),
                                                counterText: "",
                                                border: null),
                                            onChanged: (String str) {
                                              if (str.isEmpty) {
                                                focusNode3.requestFocus();
                                              } else {
                                                focusNode5.requestFocus();
                                              }
                                            },
                                          ),
                                        )),
                                    Container(
                                        width: screenWidth / 8,
                                        child: RawKeyboardListener(
                                          focusNode: FocusNode(),
                                          onKey: (event) {
                                            if (event.logicalKey ==
                                                LogicalKeyboardKey.backspace) {
                                              print("print5");
                                              if (controller5.text.length == 0) {
                                                focusNode4
                                                    .requestFocus(); // = true;
                                              }
                                            }
                                          },
                                          child: TextField(
                                            keyboardType: TextInputType.number,
                                            controller: controller5,
                                            showCursor: false,
                                            focusNode: focusNode5,
                                            //readOnly: true,
                                            enabled: true,
                                            textAlign: TextAlign.center,
                                            maxLength: 1,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black,
                                                fontSize: 20),
                                            decoration: InputDecoration(
                                                enabledBorder:
                                                UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: CustomColors
                                                          .otpInputBorderColor,
                                                      width: CustomColors
                                                          .otpBorderSize),
                                                ),
                                                contentPadding:
                                                EdgeInsets.all(5.0),
                                                counterText: "",
                                                border: null),
                                            onChanged: (String str) {
                                              if (str.isEmpty) {
                                                focusNode4.requestFocus();
                                              } else {
                                                focusNode6.requestFocus();
                                              }
                                            },
                                          ),
                                        )),
                                    Container(
                                        width: screenWidth / 8,
                                        child: RawKeyboardListener(
                                          focusNode: FocusNode(),
                                          onKey: (event) {
                                            if (event.logicalKey ==
                                                LogicalKeyboardKey.backspace) {
                                              print("print6");
                                              if (controller6.text.length == 0) {
                                                focusNode5
                                                    .requestFocus(); // = true;
                                              }
                                            }
                                          },
                                          child: TextField(
                                            keyboardType: TextInputType.number,
                                            controller: controller6,
                                            showCursor: false,
                                            //readOnly: true,
                                            focusNode: focusNode6,
                                            enabled: true,
                                            textAlign: TextAlign.center,
                                            maxLength: 1,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.black,
                                                fontSize: 20),
                                            decoration: InputDecoration(
                                                enabledBorder:
                                                UnderlineInputBorder(
                                                  borderSide: BorderSide(
                                                      color: CustomColors
                                                          .otpInputBorderColor,
                                                      width: CustomColors
                                                          .otpBorderSize),
                                                ),
                                                contentPadding:
                                                EdgeInsets.all(5.0),
                                                counterText: "",
                                                border: null),
                                            onChanged: (String str) {
                                              if (str.isEmpty) {
                                                focusNode5.requestFocus();
                                              }
                                            },
                                          ),
                                        )),
                                  ],
                                ),
                                SizedBox(
                                  height: 30.0,
                                ),
                                Align(
                                  alignment: Alignment.topRight,
                                  child: InkWell(
                                    child: Text(
                                      'Resend OTP',
                                      style: TextStyle(
                                          fontSize: 13,
                                          fontFamily: "Montserrat",
                                          color: CustomColors.darkPinkColor,
                                          decoration: TextDecoration.underline),
                                    ),
                                    onTap: () {
                                      FocusScope.of(context)
                                          .requestFocus(FocusNode());
                                      resendOtpAPI(context);
                                    },
                                  ),
                                ),
                                SizedBox(height: 50),

                                Container(
                                  height: 150,
                                  child: Column(
                                    children: [
                                      InkWell(
                                        child: Container(
                                          alignment: Alignment.center,
                                          padding: EdgeInsets.only(
                                              left: 15, right: 15),
                                          height: 50,
                                          width:
                                          MediaQuery.of(context).size.width,
                                          decoration: new BoxDecoration(
                                            gradient: LinearGradient(
                                                begin: const Alignment(0.0, -2.0),
                                                end: const Alignment(0.0, 0.3),
                                                colors: [
                                                  Colors.white,
                                                  CustomColors.darkPinkColor
                                                ]),
                                            shape: BoxShape.rectangle,
                                            color: CustomColors.darkPinkColor,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(30)),
                                            boxShadow: [
                                              BoxShadow(
                                                color: Colors.pink[100],
                                                spreadRadius: 5,
                                                blurRadius: 10,
                                                offset: Offset(0,
                                                    2), // changes position of shadow
                                              ),
                                            ],
                                          ),
                                          child: Text(
                                            "Continue",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontFamily: "Montserrat",
                                                fontWeight: FontWeight.bold,
                                                fontSize: 15),
                                          ),
                                        ),
                                        onTap: () {
                                          FocusScope.of(context)
                                              .requestFocus(FocusNode());
                                          verifyOtpAPI(context);
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            )),
                      )),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  verifyOtpAPI(BuildContext context) async {
    String otp = controller1.text +
        controller2.text +
        controller3.text +
        controller4.text +
        controller5.text +
        controller6.text;
    if (otp.split("").length < 5) {
      Fluttertoast.showToast(msg: "Please enter 6 digit OTP");
      return;
    }
    EasyLoading.show();
    if (tokenMain.isEmpty) {
      tokenMain = await AppSharedPreferences.instance.getFCMToken();
    }
    Map<String, String> dictParam = {
      'mobile_no': widget.mobileNO,
      'mobile_otp': otp,
      'device_type': "android",
      'device_id': deviceId,
      'device_token': tokenMain.replaceAll("\"", "")
    };
    print(dictParam);
    provider
        .requestPostForApi(
            context, dictParam, WebApiConstaint.URL_VERIFYOTP, "", "")
        .then((responce) {
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["data"]);
            // AppSharedPreferences.instance.setUserDetails(responce.data["data"]["name"],responce.data["data"]["email"],responce.data["data"]["mobile_no"],responce.data["data"]["session_key"],responce.data["data"]["authorization"]);
            AppSharedPreferences.instance.setUserDetails(responce.data["data"]);
            AppSharedPreferences.instance.setLogin(true);
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => DashboardPage(),
                ),
                ModalRoute.withName('/login_screen'));
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
          }
        } catch (e) {
          ToastUtils.showCustomToast(context, e.toString());
        }
      }
    });
  }

  resendOtpAPI(BuildContext context) async {
    EasyLoading.show();
    String strUrl =
        WebApiConstaint.URL_RSENDOTP + "mobile_no=" + widget.mobileNO;
    provider.requestGetForApi(context, strUrl, "", "").then((responce) {
      print(strUrl);
      EasyLoading.dismiss();

      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            controller1.text = "";
            controller2.text = "";
            controller3.text = "";
            controller4.text = "";
            controller5.text = "";
            controller6.text = "";

            ToastUtils.showToastOTP(fToast, responce.data["message"]);
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      } else
        ToastUtils.showCustomToast(
            context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
    });
  }

  Future<bool> willPop() {
    //SystemNavigator.pop();
    Navigator.of(context).pop(true);
  }
}
