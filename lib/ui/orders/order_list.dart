// @dart=2.9
import 'package:choix_customer/Data/SevedData.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/ui/select_time_slot.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';


SavedData savedData = SavedData();

class OrderList extends StatefulWidget {
  @override
  _OrderList createState() => _OrderList();
}

class _OrderList extends State<OrderList> {
  double total = 0;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // countTotal();
    print("testtee");
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: CustomColors.darkPinkColor,
        title: Text('Order list'),
      ),
      body: Container(
        margin: EdgeInsets.only(left: 20.0, top: 0.0, right: 20.0, bottom: 0.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: savedData.selectedItem.length,
                  itemBuilder: (context, i) {
                    return Card(
                      margin:
                          EdgeInsets.only(left: 0, right: 0, top: 5, bottom: 5),
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20)),
                        child: Row(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(5),
                                    topLeft: Radius.circular(5),
                                  ),
                                  image: DecorationImage(
                                      image: NetworkImage(savedData
                                          .selectedItem[i].image),
                                      fit: BoxFit.cover)),
                              width: 80,
                              height: 70,
                            ),
                            SizedBox(
                              width: 8,
                              height: 0,
                            ),
                            Container(
                              child: Expanded(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,

                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                          flex : 7,
                                          child: Text(
                                            savedData.selectedItem[i].name,
                                            style: TextStyle(
                                                color: CustomColors
                                                    .blackColor,
                                                fontSize: 18,
                                                fontFamily:
                                                    "Montserrat"),
                                          ),
                                        ),
                                        Spacer(),
                                        Container(
                                          height: 25,
                                          padding: EdgeInsets.only(right: 6),
                                          child: InkWell(
                                              onTap: () {
                                                updateCount(2, i);
                                              },
                                              child: Icon(CupertinoIcons.clear_circled,color: Colors.grey,size: 25,)),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: [
                                        Row(
                                          children: [
                                            InkWell(
                                              onTap: () {
                                                updateCount(0, i);
                                              },
                                              child: Icon(
                                                Icons
                                                    .remove_circle_outline_sharp,
                                                color: CustomColors
                                                    .darkPinkColor,
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Text(
                                              savedData.selectedItem[i].count
                                                  .toString(),
                                              style: TextStyle(
                                                  color: CustomColors
                                                      .darkPinkColor),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            InkWell(
                                              onTap: () {
                                                updateCount(1, i);
                                              },
                                              child: Icon(
                                                Icons.add_circle_sharp,
                                                color: CustomColors
                                                    .darkPinkColor,
                                              ),
                                            ),
                                          ],
                                        ),

                                        // SizedBox(
                                        //   width: 30,
                                        // ),
                                        // Text(
                                        //   'Per/${savedData.selectedItem[i]
                                        //       .product_type}',
                                        //   style: TextStyle(
                                        //       color: CustomColors
                                        //           .blackColor,
                                        //       fontSize: 12,
                                        //       fontFamily:
                                        //           "Montserrat-Bold"),
                                        // ),
                                        Spacer(),
                                        SizedBox(
                                          width: 30,
                                        ),
                                        Container(
                                          padding: EdgeInsets.only(right: 9),
                                          child: Text(
                                            '\u{20B9}' +
                                                savedData.selectedItem[i]
                                                    .price + "/" + savedData.selectedItem[i]
                                                .product_type,
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.black,
                                                fontFamily:
                                                    "Montserrat-Bold"),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }

                  //OrderItem(widget.selectedItem[i], i, this)

                  ),
            ),
            SizedBox(
              height: 10,
            ),
            // Divider(
            //   color: Colors.grey,
            // ),
            SizedBox(
              height: 10,
            ),
            // Total amount
            // Row(
            //   children: [
            //     Container(
            //       margin: EdgeInsets.only(
            //           left: 10.0, top: 0.0, right: 10.0, bottom: 0.0),
            //       child: Text(
            //         'Total Amount',
            //         style: TextStyle(
            //             fontFamily: "Montserrat-Bold",
            //             fontSize: 16,
            //             color: Colors.black),
            //       ),
            //     ),
            //     Spacer(),
            //     Container(
            //       margin: EdgeInsets.only(
            //           left: 10.0, top: 0.0, right: 10.0, bottom: 0.0),
            //       child: Text(
            //         '\$' + total.toString(),
            //         style: TextStyle(
            //             fontFamily: "Montserrat-Bold",
            //             fontSize: 16,
            //             color: Colors.black),
            //       ),
            //     )
            //   ],
            // ),
            SizedBox(
              height: 30,
            ),
            InkWell(
              child: Container(
                alignment: Alignment.center,
                padding: EdgeInsets.only(left: 30, right: 30),
                height: 50,
                width: MediaQuery.of(context).size.width,
                decoration: new BoxDecoration(
                  gradient: LinearGradient(
                      begin: const Alignment(0.0, -2.0),
                      end: const Alignment(0.0, 0.3),
                      colors: [Colors.white, CustomColors.darkPinkColor]),
                  shape: BoxShape.rectangle,
                  color: CustomColors.darkPinkColor,
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.pink[100],
                      spreadRadius: 5,
                      blurRadius: 10,
                      offset: Offset(0, 2), // changes position of shadow
                    ),
                  ],
                ),
                child: Text(
                  "Proceed",
                  style: TextStyle(
                      color: Colors.white,
                      fontFamily: "Montserrat",
                      fontWeight: FontWeight.bold,
                      fontSize: 15),
                ),
              ),
              onTap: () {
                if(savedData.selectedItem.length > 0) {
                  Navigator.push(context, MaterialPageRoute(
                    builder: (context) {
                      return SelectTimeSlot();
                    },
                  ));
                }else{
                  Fluttertoast.showToast(msg: "Cart can't blank");
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  void updateCount(int type, int index) {
    // type 0 = remove, 1 = Add, 2 delete,
    if (type == 0) {
      if (savedData.selectedItem != null && savedData.selectedItem.length > 0) {
        savedData.selectedItem[index].count -= 1;
        if(savedData.selectedItem[index].count == 0){
          savedData.selectedItem.removeAt(index);
        }
      }
    } else if (type == 1) {
      savedData.selectedItem[index].count += 1;
    } else if (type == 2) {
      if (savedData.selectedItem != null && savedData.selectedItem.length > 0) {
        savedData.selectedItem.removeAt(index);
      }
    }
    setState(() {});
    // countTotal();
  }

  // void countTotal() {
  //   total = 0;
  //   if (widget.selectedItem.length > 0) {
  //     for (int i = 0; i < widget.selectedItem.length; i++) {
  //       int totalItem = widget.selectedItem[i].count;
  //       double totalAmount =
  //           (totalItem * double.parse(widget.selectedItem[i].price));
  //       total += totalAmount;
  //     }
  //   }
  //   setState(() {});
  // }
}

abstract class ItemChangeListner {
  void myProtocal(int type, int index);
}
