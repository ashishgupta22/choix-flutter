// @dart=2.9
import 'package:choix_customer/Data/CancelReason.dart';
import 'package:choix_customer/Data/PendingOrder.dart';
import 'package:choix_customer/Data/PendingPaymentOrder.dart';
import 'package:choix_customer/Data/UserData.dart';
import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/RetryClickListner.dart';
import 'package:choix_customer/Utill/alert_dialog_new.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:choix_customer/widget/permission_utils.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:location/location.dart';
import 'package:url_launcher/url_launcher.dart';

import '../NoDataFound.dart';
import '../vendor_order_acceptance.dart';

class OnGoingOrders extends StatefulWidget {
  @override
  _OnGoingOrdersState createState() => _OnGoingOrdersState();
}

class _OnGoingOrdersState extends State<OnGoingOrders>
    implements RetryClickListner {
  bool status7 = false;
  bool isSwitchOn = false;
  String strToggle = "Offline";
  ApiProvider provider = ApiProvider();
  UserData userData;

  List<PendingOrders> pendingOrderData = [];
  List<PendingOrders> ongoingOrderData = [];
  bool notAvailable = false;
  @override
  void initState() {
    // TODO: implement initState
    requestPermissions();
    super.initState();
    // FirebaseMessaging.onMessage.listen((RemoteMessage message) {
    //   RemoteNotification notification = message.notification;
    //   if (notification != null) {
    //     String msgBody = notification.body;
    //     print("message " + msgBody);
    //     getPendingData(context);
    //   }
    // });

    getPendingData(context);
    // getPendingOrders();
    // FirebaseMessaging.instance
    //     .requestPermission(
    //   alert: true,
    //   announcement: true,
    //   badge: true,
    //   carPlay: false,
    //   criticalAlert: false,
    //   provisional: false,
    //   sound: true,
    // )
    //     .whenComplete(() => null);

    FirebaseMessaging.instance.getToken().then((token) {
      print("token " + token);
    });
    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification notification = message.notification;
      if (notification != null) {
        String msgBody = notification.body;
        print("message " + msgBody);
        // getPendingOrders();
        getPendingData(context);
      } else {
        print("message " + "body is null");
      }
    });
  }

  Future<void> requestPermissions() async {
    CheckPermission.checkLocationPermission(context).then((value){

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: new Text("Orders"),
        backgroundColor: CustomColors.darkPinkColor,
        leading: InkWell(child: Icon(Icons.arrow_back,color: Colors.white,),onTap: (){
          Navigator.pop(context);
        },),
        actions: <Widget>[
          SizedBox(
            width: 20,
          )
        ],
      ),
      body: RefreshIndicator(
        color: CustomColors.darkPinkColor,
        onRefresh: () {
          return getPendingData(context);
        },
        child: SingleChildScrollView(
          physics: AlwaysScrollableScrollPhysics(),
          child: Container(
            margin: EdgeInsets.only(bottom: 90),
            child: Column(
              children: [
                if (notAvailable)
                  NoDataFound(this),
                if (pendingOrderData.length > 0)
                  Container(
                    height: 50.0,
                    color: CustomColors.lightPinkColor,
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            "New Request",
                            style: const TextStyle(
                              fontSize: 15,
                                color: Colors.black87,
                                fontWeight: FontWeight.bold,
                                fontFamily: "Montserrat"),
                          ),
                        ),
                      ],
                    ),
                  ),
                Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: headerChild(pendingOrderData, context)),
                if (ongoingOrderData.length > 0)
                  Container(
                    height: 50.0,
                    color: CustomColors.lightPinkColor,
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: <Widget>[
                        Expanded(
                          child: Text(
                            "Ongoing Order",
                            style: const TextStyle(
                              fontSize: 15,
                                color: Colors.black87,
                                fontWeight: FontWeight.bold,
                                fontFamily: "Montserrat"),
                          ),
                        ),
                      ],
                    ),
                  ),
                Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: headerChildOngoing(ongoingOrderData, context)),
              ],
            ),
          ),
        ),
      ),
    );
  }
  Future<void> getPendingOrders() async {
    EasyLoading.show();
    if (userData == null)
      userData = await AppSharedPreferences.instance.getUserDetails();
    provider
        .requestGetForApi(context, WebApiConstaint.URL_PENDINGPAYMENTORDER,
        userData.sessionKey, userData.authorization)
        .then((responce) {
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            PendingPaymentOrder order =
            PendingPaymentOrder.fromJson(responce.data);
            if (order.data != null && order.data.created != null) {
              AlertDialogNew.showConfirmationDialog(context, null, "", "Yes", "No", order.data, userData, provider);
              // showDialog(
              //   context: context,
              //   barrierDismissible: false,
              //   builder: (_) => AcceptPendingPaymentOrder(order.data),
              // );
            }
          }
        } catch (e) {
          ToastUtils.showCustomToast(context, e.toString());
        }
      }
    });
    return;
  }

  // Future<bool> _onWillPop() async {
  //   return (await showDialog(
  //         context: context,
  //         builder: (context) => new AlertDialog(
  //           title: new Text(AlertMessage.msgAlertTitle),
  //           content: new Text(AlertMessage.msgAppExit),
  //           actions: <Widget>[
  //             new FlatButton(
  //               onPressed: () => Navigator.of(context).pop(false),
  //               child: new Text(WebApiConstaint.btnNo),
  //             ),
  //             new FlatButton(
  //               onPressed: () => SystemNavigator.pop(),
  //               child: new Text(WebApiConstaint.btnYes),
  //             ),
  //           ],
  //         ),
  //       )) ??
  //       false;
  // }

  getPendingData(BuildContext context) async {
    try {
      EasyLoading.show();
      userData = await AppSharedPreferences.instance.getUserDetails();
      String strUrl = WebApiConstaint.URL_PENDINGORDER;
      provider
          .requestGetForApi(
          context, strUrl, userData.sessionKey, userData.authorization)
          .then((responce) {
        print(responce);
        print("session : " +
            userData.sessionKey +
            " authkey : " +
            userData.authorization);
        EasyLoading.dismiss();
        if (responce != null) {
          try {
            if (responce.data["error"] == false) {
              // Fluttertoast.showToast(msg: responce.data["message"]);
              PendingOrderData order = PendingOrderData.fromJson(responce.data);
              // if (order.data.length > 0) {
              pendingOrderData = order.data.pendingOrders;
              ongoingOrderData = order.data.ongoingOrders;
              notAvailable = false;
              notAvailable =
              ongoingOrderData.length <= 0 && pendingOrderData.length <= 0
                  ? true
                  : false;
              setState(() {});
              // }
            } else {
              setState(() {
                notAvailable = true;
              });
              // Fluttertoast.showToast(msg: responce.data["message"]);
              print(responce.data["message"]);
            }
          } catch (_EX) {
            if(mounted)
            setState(() {
              notAvailable = true;
            });
            print(_EX);
            // ToastUtils.showCustomToast(
            //     context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
          }
        } else {
          setState(() {
            notAvailable = true;
          });
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      });
    }catch(_){
      EasyLoading.dismiss();
      ToastUtils.showCustomToast(
          context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
    }
  }

  callAcceptRejectAPI(BuildContext context, String order_id, String type,
      String resionID) async {

    CheckPermission.checkLocationPermissionOnly(context).then((value) async {
      if(value){
        EasyLoading.show();
        Location location = new Location();
        LocationData _pos = await location.getLocation();

        String latitude = _pos.latitude.toString();
        String longitude = _pos.longitude.toString();
        Map<String, dynamic> dictParam = {
          'order_id': order_id,
          'latitude': latitude,
          'longitude': longitude,
          'type': type,
          'reason': resionID
        };
        print(dictParam);
        provider
            .requestPostForApi(context, dictParam, WebApiConstaint.URL_ACCEPTREJECT,
            userData.sessionKey, userData.authorization)
            .then((responce) {
          EasyLoading.dismiss();

          if (responce != null) {
            try {

              if (responce.data["error"] == false) {
                Fluttertoast.showToast(msg: responce.data["message"]);
                getPendingData(context);
              } else {
                print(responce);
                Fluttertoast.showToast(msg: responce.data["message"]);
              }
            } catch (e) {
              ToastUtils.showCustomToast(context, e.toString());
            }
          }else{
            Fluttertoast.showToast(msg: "Something went wrong");
          }
        });
      }
    });
  }

  List<Widget> headerChild(List<PendingOrders> child, BuildContext context) {
    var listWidget = List<Widget>();

    child.forEach((listData) {
      listWidget.add(Stack(
        children: [
          InkWell(
            onTap: () {
              // Navigator.push(context, MaterialPageRoute(
              //   builder: (context) {
              //     return VendorOrderAcceptance(listData);
              //   },
              // )).then((value) => getPendingData(context));
            },
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 20, top: 10, right: 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Order Type : ${listData.orderType}",
                          style: TextStyle(
                            fontSize: 14,
                            fontFamily: "Montserrat-Medium",
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    Spacer(),
                    Padding(
                      padding: EdgeInsets.only(left: 20, top: 10, right: 20),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          "#${listData.orderNumber}",
                          style: TextStyle(
                            fontSize: 14,
                            fontFamily: "Montserrat-Medium",
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, top: 10, right: 20),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Order Date : ${listData.created}",
                      style: TextStyle(
                        fontSize: 14,
                        fontFamily: "Montserrat-Medium",
                      ),
                    ),
                  ),
                ),
                Padding(
                    padding: EdgeInsets.only(
                        left: 20, top: 10, bottom: 10, right: 20),
                    child: ListView.builder(
                        itemCount: listData.productData.length,
                        shrinkWrap: true,
                        itemBuilder: (context, index2) {
                          return Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                fit: FlexFit.tight,
                                flex: 2,
                                child: Text(
                                  listData.productData[index2].name != null
                                      ? listData.productData[index2].name
                                      : "",
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontFamily: "Montserrat-Medium"),
                                ),
                              ),
                              Flexible(
                                fit: FlexFit.tight,
                                flex: 1,
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    "${listData.productData[index2].quantity != null ? listData.productData[index2].quantity : ""}",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontFamily: "Montserrat-Medium"),
                                  ),
                                ),
                              ),
                              Flexible(
                                fit: FlexFit.tight,
                                flex: 1,
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    "₹" +
                                                listData
                                                    .productData[index2].price
                                                    .toString() !=
                                            null
                                        ?"₹"+ listData.productData[index2].price
                                            .toString()
                                        : "0",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontFamily: "Montserrat-Medium"),
                                  ),
                                ),
                              ),
                            ],
                          );
                        })),
                Padding(
                  padding: EdgeInsets.only(bottom: 5),
                  child: Row(
                    // mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 15),
                        child: InkWell(
                          onTap: () {
                            callAPICancelReason(listData);
                          },
                          child: Container(
                            padding: EdgeInsets.all(7),
                            alignment: Alignment.center,
                            height: 35,
                            width: 150,
                            decoration: new BoxDecoration(
                              shape: BoxShape.rectangle,
                              color: CustomColors.blackColor,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30)),
                              // boxShadow: [
                              //   BoxShadow(
                              //     color: Colors.grey.withOpacity(0.5),
                              //     spreadRadius: 4,
                              //     blurRadius: 5,
                              //     offset: Offset(0, 2), // changes position of shadow
                              //   ),
                              //],
                            ),
                            child: Text(
                              "Cancel Order",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: "Montserrat",
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15),
                            ),
                          ),
                        ),
                      ),

                      Spacer(),
                      // Padding(
                      //   padding: EdgeInsets.only(right: 20),
                      //   child: InkWell(
                      //     onTap: () {
                      //       Navigator.push(context, MaterialPageRoute(
                      //         builder: (context) {
                      //           return VendorOrderAcceptance(
                      //               listData.id.toString());
                      //         },
                      //       ));
                      //     },
                      //     child: Container(
                      //       width: 40,
                      //       height: 40,
                      //       child: Icon(
                      //         Icons.navigation,
                      //         size: 27,
                      //         color: Colors.white,
                      //       ),
                      //       decoration: BoxDecoration(
                      //         shape: BoxShape.circle,
                      //         color: CustomColors.darkPinkColor,
                      //       ),
                      //       //child: ,
                      //     ),
                      //   ),
                      // ),
                    ],
                  ),
                ),
                new Divider(
                  color: Colors.black87,
                ),
              ],
            ),
          )
        ],
      ));
    });
    return listWidget;
  }

  List<Widget> headerChildOngoing(
      List<PendingOrders> child, BuildContext context) {
    var listWidget = List<Widget>();

    child.forEach((listData) {
      listWidget.add(Stack(
        children: [
          InkWell(
            onTap: () {
              if (listData.status != "Pending Payment")
                Navigator.push(context, MaterialPageRoute(
                  builder: (context) {
                    return VendorOrderAcceptance(listData);
                  },
                )).then((value) => getPendingData(context));
            },
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 20, top: 10, right: 20),
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          "Order Type : ${listData.orderType}",
                          style: TextStyle(
                            fontSize: 14,
                            fontFamily: "Montserrat-Medium",
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    Spacer(),
                    Padding(
                      padding: EdgeInsets.only(left: 20, top: 10, right: 20),
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          "#${listData.orderNumber}",
                          style: TextStyle(
                            fontSize: 14,
                            fontFamily: "Montserrat-Medium",
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(left: 20, top: 10, right: 20),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Order Date : ${listData.created}",
                      style: TextStyle(
                        fontSize: 14,
                        fontFamily: "Montserrat-Medium",
                      ),
                    ),
                  ),
                ),
                Padding(
                    padding: EdgeInsets.only(
                        left: 20, top: 10, bottom: 10, right: 20),
                    child: ListView.builder(
                        itemCount: listData.productData.length,
                        shrinkWrap: true,
                        itemBuilder: (context, index2) {
                          return Row(
                            mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                            children: [
                              Flexible(
                                fit: FlexFit.tight,
                                flex: 2,
                                child: Text(
                                  listData.productData[index2].name,
                                  style: TextStyle(
                                      fontSize: 14,
                                      fontFamily: "Montserrat-Medium"),
                                ),
                              ),
                              Flexible(
                                fit: FlexFit.tight,
                                flex: 1,
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    "${listData.productData[index2].quantity}",
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontFamily: "Montserrat-Medium"),
                                  ),
                                ),
                              ),
                              Flexible(
                                fit: FlexFit.tight,
                                flex: 1,
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: Text(
                                    "₹" +
                                        listData.productData[index2].price
                                            .toString(),
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontFamily: "Montserrat-Medium"),
                                  ),
                                ),
                              ),
                            ],
                          );
                        })),
                if (listData.status != "Pending Payment")
                Padding(
                  padding: EdgeInsets.only(bottom: 5),
                  child: Row(
                    // mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 20),
                      ),
                      //child :
                      InkWell(
                        // onTap: () {
                        //   if (listData.status == "Accepted") {
                        //     callAcceptRejectAPI(
                        //         context, listData.id.toString(), "Onway", 0);
                        //   }
                        // },
                        child: Container(
                          padding: EdgeInsets.only(left: 7),
                          alignment: Alignment.center,
                          height: 35,
                          width: 145,
                          decoration: new BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: listData.status == "Accepted"
                                ? CustomColors.darkPinkColor
                                : Colors.green,
                            borderRadius:
                                BorderRadius.all(Radius.circular(30)),
                          ),
                          child: Text(
                            "On the Way",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.only(left: 15),
                        child: InkWell(
                          onTap: () {
                            if (listData.status == "Accepted") {
                              // callAcceptRejectAPI(
                              //     context, listData.id.toString(), "reject");
                              callAPICancelReason(listData);
                            } else {
                              String phoneNo = listData.vendorMobile;
                              _makePhoneCall('tel:$phoneNo');
                            }
                          },
                          child: Container(
                            padding: EdgeInsets.all(7),
                            alignment: Alignment.center,
                            height: 35,
                            width: 100,
                            decoration: new BoxDecoration(
                              shape: BoxShape.rectangle,
                              color: listData.status == "Accepted"
                                  ? CustomColors.blackColor
                                  : CustomColors.darkPinkColor,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30)),
                            ),
                            child: Text(
                              listData.status.contains("Accepted")
                                  ? "Cancel"
                                  : "Call",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: "Montserrat",
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15),
                            ),
                          ),
                        ),
                      ),

                      Spacer(),
                      Padding(
                        padding: EdgeInsets.only(right: 20),
                        child: InkWell(
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(
                              builder: (context) {
                                return VendorOrderAcceptance(listData);
                              },
                            ));
                          },
                          child: Container(
                            width: 40,
                            height: 40,
                            child: Icon(
                              Icons.navigation,
                              size: 27,
                              color: Colors.white,
                            ),
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              color: CustomColors.darkPinkColor,
                            ),
                            //child: ,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                // if (listData.status == "Pending Payment")
                //   Align(
                //     alignment: Alignment.centerRight,
                //     child: InkWell(
                //       onTap: () {
                //         Navigator.push(
                //                 context,
                //                 MaterialPageRoute(
                //                     builder: (context) =>
                //                         AcceptPendingPaymentOrder(null)))
                //             .then((value) => getPendingData(context));
                //       },
                //       child: Container(
                //
                //         width: 110,
                //         decoration: BoxDecoration(
                //             color: CustomColors.lightPinkColor,
                //             borderRadius: BorderRadius.circular(15)),
                //         margin: EdgeInsets.only(
                //             left: 30.0, top: 20.0, right: 20.0, bottom: 10.0),
                //         child: Padding(
                //           padding: const EdgeInsets.all(10.0),
                //           child: Center(
                //             child: Text(
                //               'Complete Now',
                //               style: TextStyle(
                //                 fontFamily: "Montserrat",
                //                 fontSize: 10,
                //                 color: CustomColors.darkPinkColor,
                //               ),
                //             ),
                //           ),
                //         ),
                //       ),
                //     ),
                //   ),
                new Divider(
                  color: Colors.black87,
                ),
              ],
            ),
          )
        ],
      ));
    });
    return listWidget;
  }

  Future<void> _makePhoneCall(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  void onClickListner() {
    getPendingData(context);
  }

  Future<void> callAPICancelReason(PendingOrders listData) async {
    if (userData == null)
      userData = await AppSharedPreferences.instance.getUserDetails();
    EasyLoading.show();
    String strUrl = WebApiConstaint.URL_CANCELREASON;
    provider
        .requestGetForApi(
            context, strUrl, userData.sessionKey, userData.authorization)
        .then((responce) {
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            CancelReason order = CancelReason.fromJson(responce.data);
            show(context, order.data, listData);
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }

  void show(BuildContext context, List<CancelReasonData> data,
      PendingOrders listData) {
    int lastchecked = 0;
    if (data.length > 0) {
      data[0].selected = true;
      showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(10),
            topLeft: Radius.circular(10),
          ),
        ),
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: CustomColors.lightPinkColor,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10)),
                      ),
                      child: Column(
                        children: [
                          Padding(
                              padding: EdgeInsets.only(top: 20, bottom: 10)),
                          Row(
                            children: [
                              Padding(
                                  padding: EdgeInsets.only(
                                left: 15,
                              )),
                              Text(
                                "Name :",
                                style: TextStyle(
                                    fontFamily: "Montserrat",
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                              Padding(padding: EdgeInsets.only(right: 3)),
                              Text(
                                listData.vendorName.isNotEmpty ? listData.vendorName : "XXXX",
                                style: TextStyle(
                                    fontFamily: "Montserrat", fontSize: 14),
                              ),
                              Spacer(),
                              Text(
                                listData.created ?? "",
                                style: TextStyle(
                                    fontFamily: "Montserrat",
                                    fontSize: 12,
                                    color: Colors.grey),
                              ),
                              Padding(padding: EdgeInsets.only(right: 15))
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(bottom: 15)),
                          Row(
                            children: [
                              Padding(
                                  padding: EdgeInsets.only(left: 15, top: 8)),
                              Flexible(
                                  child: RichText(
                                      text: TextSpan(
                                style: new TextStyle(
                                  fontFamily: "Montserrat",
                                  fontSize: 14.0,
                                  color: Colors.black,
                                ),
                                children: <TextSpan>[
                                  new TextSpan(
                                      text: 'Address : ',
                                      style: new TextStyle(
                                          fontWeight: FontWeight.bold)),
                                  new TextSpan(
                                      text: listData.address ?? "",
                                      style: new TextStyle(fontSize: 14)),
                                ],
                              ))),
                              Padding(
                                  padding: EdgeInsets.only(
                                right: 15,
                              )),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(bottom: 15))
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10, left: 15, right: 15),
                      child: Text(
                        "Order issues",
                        style: TextStyle(
                            fontFamily: "Montserrat-Bold",
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      ),
                    ),
                    ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: data.length,
                        itemBuilder: (context, index) {
                          return Container(
                              margin: EdgeInsets.only(
                                  left: 15, right: 15, bottom: 8),
                              child: Row(
                                children: [
                                  Checkbox(
                                      value: data[index].selected,
                                      checkColor: Colors.white,
                                      activeColor: CustomColors.darkPinkColor,
                                      onChanged: (bool value) {
                                        setState(() {
                                          data[lastchecked].selected = false;
                                          lastchecked = index;
                                          data[index].selected = value;
                                        });
                                      }),
                                  Padding(padding: EdgeInsets.only(left: 5)),
                                  Flexible(
                                    child: Text(
                                      data[index].title,
                                      style: TextStyle(
                                          fontFamily: "Montserrat",
                                          fontSize: 14),
                                    ),
                                  ),
                                  Padding(padding: EdgeInsets.only(right: 5)),
                                ],
                              ));
                        }),
                    Padding(
                      padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                      child: InkWell(
                        onTap: () {
                          callAcceptRejectAPI(context, listData.id.toString(),
                              "Cancel", data[lastchecked].title);
                          Navigator.pop(context);
                        },
                        child: Container(
                          padding: EdgeInsets.only(left: 25, right: 25),
                          alignment: Alignment.center,
                          height: 35,
                          decoration: new BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: CustomColors.darkPinkColor,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                          ),
                          child: Text(
                            "Submit",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          );
        },
      );
    }
  }
}
