// @dart=2.9
import 'package:choix_customer/Data/OrderHistory.dart';
import 'package:choix_customer/Data/UserData.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/RetryClickListner.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../NoDataFound.dart';

class OrderHistoryScreen extends StatefulWidget {
  @override
  StateOrderHistory createState() => StateOrderHistory();
}

class StateOrderHistory extends State<OrderHistoryScreen>  implements RetryClickListner{
  UserData userData;
  ApiProvider provider = ApiProvider();
  bool noRecordFound = false;
  List<OrderHistoryData> orderListData = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getOrderHistory(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: CustomColors.darkPinkColor,
          title: Text(
            "Order History",
            style: TextStyle(fontFamily: "Montserrat"),
          ),
        ),

        body: RefreshIndicator(
            color: CustomColors.darkPinkColor,
            onRefresh: () {
              return getOrderHistory(context);
            },
            child: SingleChildScrollView(
              physics: AlwaysScrollableScrollPhysics(),
              child: noRecordFound ? NoDataFound(this) :Container(
                margin: EdgeInsets.only(bottom: 90),
                child: ListView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: orderListData.length,
                    itemBuilder: (context, index) {
                      return Container(
                        margin: EdgeInsets.only(
                            left: 20, right: 20, top: 10, bottom: 10),
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            color: CustomColors.lightPinkColor),
                        child: Column(
                          children: [
                            InkWell(
                              onTap: () {
                                setState(() {
                                  orderListData[index].isVisible = !orderListData[index].isVisible;
                                });
                              },
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    "Order #${orderListData[index].orderNo}",
                                    style: TextStyle(
                                        fontSize: 14, fontFamily: "Montserrat"),
                                  ),
                                  Icon(
                                    orderListData[index].isVisible ? Icons.keyboard_arrow_down : Icons.keyboard_arrow_up ,
                                    size: 18,
                                  )
                                ],
                              ),
                            ),
                            Visibility(
                                visible: orderListData[index].isVisible,
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(top: 10),
                                      child: Row(
                                        children: [
                                          Text(
                                            "Name: ",
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontFamily: "Montserrat-Medium"),
                                          ),
                                          Text(
                                            orderListData[index].userName,
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontFamily: "Montserrat"),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(top: 5),
                                      child: Row(
                                        children: [
                                          Text(
                                            "Order Time: ",
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontFamily: "Montserrat-Medium"),
                                          ),
                                          Text(
                                            orderListData[index].orderTime,
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontFamily: "Montserrat"),
                                          ),
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(top: 5),
                                      child: Row(
                                        children: [
                                          Text(
                                            orderListData[index].status == "Cancel" ? "Cancel Time: " :  "Deliver Time: ",
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontFamily: "Montserrat-Medium"),
                                          ),
                                          Text(
                                            orderListData[index].status == "Cancel" ? orderListData[index].cancel_time :orderListData[index].deliverTime,
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontFamily: "Montserrat"),
                                          ),
                                        ],
                                      ),
                                    ),
                                    if(orderListData[index].status == "Cancel")
                                      Padding(
                                        padding: EdgeInsets.only(top: 5),
                                        child: Row(
                                          children: [
                                            Text(
                                              "Cancel Reason: ",
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontFamily: "Montserrat-Medium"),
                                            ),
                                            Text(
                                              orderListData[index].cancel_reason,
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontFamily: "Montserrat"),
                                            ),
                                          ],
                                        ),
                                      ),
                                    if(orderListData[index].status != "Cancel")
                                      Column(
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.only(top: 25),
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                              children: [
                                                Flexible(
                                                  fit: FlexFit.tight,
                                                  flex: 2,
                                                  child: ListView.builder(
                                                      itemCount: orderListData[index].productData.length,
                                                      shrinkWrap: true,
                                                      itemBuilder: (context, index2) {
                                                        return Row(
                                                          mainAxisAlignment:
                                                          MainAxisAlignment.spaceBetween,
                                                          children: [
                                                            Flexible(
                                                              fit: FlexFit.tight,
                                                              flex: 2,
                                                              child: Text(
                                                                orderListData[index].productData[index2].name,
                                                                style: TextStyle(
                                                                    fontSize: 14,
                                                                    fontFamily: "Montserrat-Medium"),
                                                              ),
                                                            ),
                                                            Flexible(
                                                              fit: FlexFit.tight,
                                                              flex: 1,
                                                              child: Align(
                                                                alignment: Alignment.centerRight,
                                                                child: Text(
                                                                  "${orderListData[index].productData[index2].quantity}",
                                                                  style: TextStyle(
                                                                      fontSize: 14,
                                                                      fontFamily:
                                                                      "Montserrat-Medium"),
                                                                ),
                                                              ),
                                                            ),
                                                            Flexible(
                                                              fit: FlexFit.tight,
                                                              flex: 1,
                                                              child: Align(
                                                                alignment: Alignment.centerRight,
                                                                child: Text(
                                                                  "₹" + orderListData[index].productData[index2].price.toString(),
                                                                  style: TextStyle(
                                                                      fontSize: 14,
                                                                      fontFamily:
                                                                      "Montserrat-Medium"),
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        );
                                                      }
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding(
                                            padding: EdgeInsets.only(top: 5),
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                              children: [
                                                Flexible(
                                                  fit: FlexFit.tight,
                                                  flex: 3,
                                                  child: Align(
                                                    alignment: Alignment.centerRight,
                                                    child: Text(
                                                      "Purchase",
                                                      style: TextStyle(
                                                          fontSize: 14,
                                                          fontWeight: FontWeight.bold,
                                                          fontFamily:
                                                          "Montserrat-Medium"),
                                                    ),
                                                  ),
                                                ),
                                                Flexible(
                                                  fit: FlexFit.tight,
                                                  flex: 1,
                                                  child: Align(
                                                    alignment: Alignment.centerRight,
                                                    child: Text(
                                                      "₹" + orderListData[index].totalAmount.toString(),
                                                      style: TextStyle(
                                                          fontSize: 14,
                                                          fontFamily:
                                                          "Montserrat-Medium"),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding (
                                            padding: EdgeInsets.only(top: 5),
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                              children: [
                                                Flexible(
                                                  fit: FlexFit.tight,
                                                  flex: 3,
                                                  child: Align(
                                                    alignment: Alignment.centerRight,
                                                    child: Text(
                                                      "Convenience Fee",
                                                      style: TextStyle(
                                                          fontSize: 14,
                                                          fontFamily:
                                                          "Montserrat-Medium"),
                                                    ),
                                                  ),
                                                ),
                                                Flexible(
                                                  fit: FlexFit.tight,
                                                  flex: 1,
                                                  child: Align(
                                                    alignment: Alignment.centerRight,
                                                    child: Text(
                                                      "₹" + (orderListData[index].convenienceFee  + orderListData[index].discount_amount).toString(),
                                                      style: TextStyle(
                                                          fontSize: 14,
                                                          fontFamily:
                                                          "Montserrat-Medium"),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                          Padding (
                                            padding: EdgeInsets.only(top: 5),
                                            child: Column(
                                              children: [
                                                Row(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Flexible(
                                                      fit: FlexFit.tight,
                                                      flex: 3,
                                                      child: Align(
                                                        alignment: Alignment.centerRight,
                                                        child: Text(
                                                          "(-) Discount",
                                                          style: TextStyle(
                                                              fontSize: 14,
                                                              fontFamily:
                                                              "Montserrat-Medium"),
                                                        ),
                                                      ),
                                                    ),

                                                    Flexible(
                                                      fit: FlexFit.tight,
                                                      flex: 1,
                                                      child: Align(
                                                        alignment: Alignment.centerRight,
                                                        child: Text(
                                                          "₹" + orderListData[index].discount_amount.toString() + "",
                                                          style: TextStyle(
                                                              fontSize: 14,
                                                              color: Colors.green,
                                                              fontFamily:
                                                              "Montserrat-Medium"),
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Divider(color: Colors.black,height: 3,)
                                              ],
                                            ),
                                          ),
                                          Padding (
                                            padding: EdgeInsets.only(top: 5),
                                            child: Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                              children: [
                                                Flexible(
                                                  fit: FlexFit.tight,
                                                  flex: 3,
                                                  child: Align(
                                                    alignment: Alignment.centerRight,
                                                    child: Text(
                                                      "Actual Convenience Fee",
                                                      style: TextStyle(
                                                          fontSize: 14,
                                                          fontWeight: FontWeight.bold,
                                                          fontFamily:
                                                          "Montserrat-Medium"),
                                                    ),
                                                  ),
                                                ),
                                                Flexible(
                                                  fit: FlexFit.tight,
                                                  flex: 1,
                                                  child: Align(
                                                    alignment: Alignment.centerRight,
                                                    child: Text(
                                                      "₹" + orderListData[index].convenienceFee.toString(),
                                                      style: TextStyle(
                                                          fontSize: 14,
                                                          fontFamily:
                                                          "Montserrat-Medium"),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),

                                        ],
                                      ),


                                  ],
                                ))
                          ],
                        ),
                      );
                    }),
              ),
            )));
  }

  Future<void> getOrderHistory(BuildContext context) async {
    userData = await AppSharedPreferences.instance.getUserDetails();

    EasyLoading.show();
    provider
        .requestGetForApi(context, WebApiConstaint.URL_ORDERHSISTORY,
            userData.sessionKey, userData.authorization)
        .then((responce) {
          print( WebApiConstaint.URL_ORDERHSISTORY);
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          print(responce);
          if (responce.data["error"] == false) {

            var resposdata = OrderHistory.fromJson(responce.data);
            // prodcutSubCatList = resposdata.data;

            // OrderHistory data = responce.data;
            if(resposdata.data.isNotEmpty){
              noRecordFound = false;
              orderListData = resposdata.data;
              print("orderListlength : " + orderListData.length.toString());
              orderListData[0].isVisible = true;
            }else{
              noRecordFound = true;
            }
            setState(() {});
          } else {
            noRecordFound = true;
            setState(() {});
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
          }
        } catch (ex) {
          noRecordFound = true;
          setState(() {});
          ToastUtils.showCustomToast(
              context, ex.toString());
        }
      }
    });
  }

  @override
  void onClickListner() {
    // TODO: implement onClickListner
    getOrderHistory(context);
  }
}
