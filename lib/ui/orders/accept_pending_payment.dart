// @dart=2.9
import 'package:choix_customer/Data/CancelReason.dart';
import 'package:choix_customer/Data/PendingPaymentOrder.dart';
import 'package:choix_customer/Data/UserData.dart';
import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:choix_customer/widget/permission_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:location/location.dart';
import '../dashboard_screen.dart';

class AcceptPendingPaymentOrder extends StatefulWidget {
  PendingPaymentOrderData listData;

  AcceptPendingPaymentOrder(PendingPaymentOrderData this.listData) : super();

  @override
  _AcceptPendingPaymentOrderState createState() =>
      _AcceptPendingPaymentOrderState();
}

class _AcceptPendingPaymentOrderState extends State<AcceptPendingPaymentOrder> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  FocusNode myFocusNode = new FocusNode();
  bool isSelected = false;
  TextEditingController txtAmountController = TextEditingController();
  bool isAmountValidate = true;
  ApiProvider provider = ApiProvider();
  UserData userData;
  bool isBackparess = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (widget.listData == null) {
      getPendingOrders();
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (isBackparess) {
          Navigator.pop(context);
        }
      },
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: new Text(
              "Order #${widget.listData != null ? widget.listData.orderNumber : ""}"),
          backgroundColor: CustomColors.darkPinkColor,
        ),
        body: Container(
          padding: EdgeInsets.only(top: 30, left: 20, right: 20),
          color: CustomColors.lightPinkColor,
          child: Column(
            children: [
              Row(
                children: [
                  Text(
                    "Name : ",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text(widget.listData != null
                      ? widget.listData.customerName
                      : "XXXXXX"),
                ],
              ),
              SizedBox(
                height: 20,
              ),

              Row(
                children: [
                  Text(
                    "Order Number : ",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text(widget.listData != null
                      ? widget.listData.orderNumber
                      : "XXXXXX"),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              Divider(
                color: Colors.grey,
                height: 1,
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: EdgeInsets.only(top: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      fit: FlexFit.tight,
                      flex: 2,
                      child: ListView.builder(
                          itemCount: widget.listData != null
                              ? widget.listData.productData.length
                              : 0,
                          shrinkWrap: true,
                          itemBuilder: (context, index2) {
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Flexible(
                                  fit: FlexFit.tight,
                                  flex: 2,
                                  child: Text(
                                    widget.listData.productData[index2].name,
                                    style: TextStyle(
                                        fontSize: 14,
                                        fontFamily: "Montserrat-Medium"),
                                  ),
                                ),
                                Flexible(
                                  fit: FlexFit.tight,
                                  flex: 1,
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: Text(
                                      "${widget.listData.productData[index2].quantity}",
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: "Montserrat-Medium"),
                                    ),
                                  ),
                                ),
                                Flexible(
                                  fit: FlexFit.tight,
                                  flex: 1,
                                  child: Align(
                                    alignment: Alignment.centerRight,
                                    child: Text(
                                      "₹" +
                                          widget.listData.productData[index2]
                                              .price
                                              .toString(),
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: "Montserrat-Medium"),
                                    ),
                                  ),
                                ),
                              ],
                            );
                          }),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 8,
              ),
              Padding(
                padding: EdgeInsets.only(top: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      fit: FlexFit.tight,
                      flex: 3,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          "Convenience Fee: ",
                          style: TextStyle(
                              fontSize: 14, fontFamily: "Montserrat-Medium"),
                        ),
                      ),
                    ),
                    Flexible(
                      fit: FlexFit.tight,
                      flex: 1,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          "₹" +
                              (widget.listData.convenienceFee +
                                      widget.listData.discount_amount)
                                  .toString(),
                          style: TextStyle(
                              fontSize: 14, fontFamily: "Montserrat-Medium"),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      fit: FlexFit.tight,
                      flex: 3,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          "Discount : ",
                          style: TextStyle(
                              fontSize: 14, fontFamily: "Montserrat-Medium"),
                        ),
                      ),
                    ),
                    Flexible(
                      fit: FlexFit.tight,
                      flex: 1,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          "-₹" + widget.listData.discount_amount.toString(),
                          style: TextStyle(
                              fontSize: 14, fontFamily: "Montserrat-Medium"),
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              // Padding(
              //   padding: EdgeInsets.only(top: 5),
              //   child: Row(
              //     mainAxisAlignment:
              //     MainAxisAlignment.spaceBetween,
              //     children: [
              //       Flexible(
              //         fit: FlexFit.tight,
              //         flex: 3,
              //         child: Align(
              //           alignment: Alignment.centerRight,
              //           child: Text(
              //             "Total",
              //             style: TextStyle(
              //                 fontSize: 14,
              //                 fontFamily:
              //                 "Montserrat-Medium"),
              //           ),
              //         ),
              //       ),
              //       Flexible(
              //         fit: FlexFit.tight,
              //         flex: 1,
              //         child: Align(
              //           alignment: Alignment.centerRight,
              //           child: Text(
              //             "₹" + (widget.listData != null ? widget.listData.totalAmount.toString() :""),
              //             style: TextStyle(
              //                 fontSize: 14,
              //                 fontFamily:
              //                 "Montserrat-Medium"),
              //           ),
              //         ),
              //       ),
              //     ],
              //   ),
              // ),

              Padding(
                padding: EdgeInsets.only(top: 5),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      fit: FlexFit.tight,
                      flex: 3,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          "Purchase: ",
                          style: TextStyle(
                              fontSize: 14, fontFamily: "Montserrat-Medium"),
                        ),
                      ),
                    ),
                    Flexible(
                      fit: FlexFit.tight,
                      flex: 1,
                      child: Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          "₹" +
                              (widget.listData != null
                                  ? widget.listData.totalAmount.toString()
                                  : ""),
                          style: TextStyle(
                              fontSize: 14, fontFamily: "Montserrat-Medium"),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Row(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  FloatingActionButton.extended(
                    onPressed: () {
                      callAcceptRejectAPI(
                          context, widget.listData.id.toString(), "Accept", "");
                    },
                    backgroundColor: CustomColors.darkPinkColor,
                    label: Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20),
                      child: Text(
                        "Yes",
                        style: TextStyle(fontFamily: "Montserrat"),
                      ),
                    ),
                    heroTag: 1,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  FloatingActionButton.extended(
                    onPressed: () {
                      callAPICancelReason(widget.listData.id);
                    },
                    backgroundColor: CustomColors.blackColor,
                    label: Padding(
                      padding: const EdgeInsets.only(left: 20.0, right: 20),
                      child: Text("No"),
                    ),
                    heroTag: 2,
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Future<void> callAPICancelReason(int id) async {
    EasyLoading.show();
    if (userData == null)
      userData = await AppSharedPreferences.instance.getUserDetails();
    String strUrl = WebApiConstaint.URL_CANCELREASON;
    provider
        .requestGetForApi(
            context, strUrl, userData.sessionKey, userData.authorization)
        .then((responce) {
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            CancelReason order = CancelReason.fromJson(responce.data);
            show(context, order.data, id);
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }

  void show(BuildContext context2, List<CancelReasonData> data, int listData) {
    int lastchecked = 0;
    if (data.length > 0) {
      data[0].selected = true;
      showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(10),
            topLeft: Radius.circular(10),
          ),
        ),
        context: _scaffoldKey.currentContext,
        builder: (context) {
          return StatefulBuilder(
            builder: (BuildContext context1, StateSetter setState) {
              return SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: CustomColors.lightPinkColor,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10)),
                      ),
                      child: Column(
                        children: [
                          Padding(
                              padding: EdgeInsets.only(top: 20, bottom: 10)),
                          Row(
                            children: [
                              Padding(
                                  padding: EdgeInsets.only(
                                left: 15,
                              )),
                              Text(
                                "Name",
                                style: TextStyle(
                                    fontFamily: "Montserrat",
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                              Padding(padding: EdgeInsets.only(right: 3)),
                              Text(
                                widget.listData.customerName != null
                                    ? widget.listData.customerName
                                    : "",
                                style: TextStyle(
                                    fontFamily: "Montserrat", fontSize: 14),
                              ),
                              Spacer(),
                              Text(
                                widget.listData.created != null
                                    ? widget.listData.created
                                    : "",
                                style: TextStyle(
                                    fontFamily: "Montserrat",
                                    fontSize: 12,
                                    color: Colors.grey),
                              ),
                              Padding(padding: EdgeInsets.only(right: 15))
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(bottom: 15)),
                          Row(
                            children: [
                              Padding(
                                  padding: EdgeInsets.only(left: 15, top: 8)),
                              Flexible(
                                  child: RichText(
                                      text: TextSpan(
                                style: new TextStyle(
                                  fontFamily: "Montserrat",
                                  fontSize: 14.0,
                                  color: Colors.black,
                                ),
                                children: <TextSpan>[
                                  new TextSpan(
                                      text: 'Address : ',
                                      style: new TextStyle(
                                          fontWeight: FontWeight.bold)),
                                  new TextSpan(
                                      text: widget.listData.address != null
                                          ? widget.listData.address
                                          : "",
                                      style: new TextStyle(fontSize: 14)),
                                ],
                              ))),
                              Padding(
                                  padding: EdgeInsets.only(
                                right: 15,
                              )),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(bottom: 15))
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10, left: 15, right: 15),
                      child: Text(
                        "Order Issues",
                        style: TextStyle(
                            fontFamily: "Montserrat-Bold",
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      ),
                    ),
                    ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: data.length,
                        itemBuilder: (context, index) {
                          return Container(
                              margin: EdgeInsets.only(
                                left: 15,
                                right: 15,
                              ),
                              child: Row(
                                children: [
                                  Checkbox(
                                      value: data[index].selected,
                                      checkColor: Colors.white,
                                      activeColor: CustomColors.darkPinkColor,
                                      onChanged: (bool value) {
                                        setState(() {
                                          data[lastchecked].selected = false;
                                          lastchecked = index;
                                          data[index].selected = value;
                                        });
                                      }),
                                  Padding(padding: EdgeInsets.only(left: 5)),
                                  Flexible(
                                    child: Text(
                                      data[index].title,
                                      style: TextStyle(
                                          fontFamily: "Montserrat",
                                          fontSize: 14),
                                    ),
                                  ),
                                  Padding(padding: EdgeInsets.only(right: 5)),
                                ],
                              ));
                        }),
                    Padding(
                      padding: EdgeInsets.only(
                          left: 20, right: 20, bottom: 20, top: 8),
                      child: InkWell(
                        onTap: () {
                          callAcceptRejectAPI(context2, listData.toString(),
                              "Reject", data[lastchecked].title);
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          padding: EdgeInsets.only(left: 25, right: 25),
                          alignment: Alignment.center,
                          height: 35,
                          decoration: new BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: CustomColors.darkPinkColor,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                          ),
                          child: Text(
                            "Submit",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          );
        },
      );
    }
  }

  callAcceptRejectAPI(BuildContext context, String order_id, String type,
      String resionID) async {
    CheckPermission.checkLocationPermission(context).then((value) async {
      if (value) {
        EasyLoading.show();
        if (userData == null)
          userData = await AppSharedPreferences.instance.getUserDetails();
        Location location = new Location();
        LocationData _pos = await location.getLocation();

        String latitude = _pos.latitude.toString();
        String longitude = _pos.longitude.toString();
        Map<String, dynamic> dictParam = {
          'order_id': order_id,
          'latitude': latitude,
          'longitude': longitude,
          'type': type,
          'reason': resionID
        };
        print(dictParam);
        provider
            .requestPostForApi(
                context,
                dictParam,
                WebApiConstaint.URL_ACCEPTREJECT,
                userData.sessionKey,
                userData.authorization)
            .then((responce) {
          EasyLoading.dismiss();
          if (responce != null) {
            try {
              if (responce.data["error"] == false) {
                Fluttertoast.showToast(msg: responce.data["message"]);
                print(responce.data["data"]);
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => DashboardPage()),
                    ModalRoute.withName("/Home"));
              } else {
                Fluttertoast.showToast(msg: responce.data["message"]);
              }
            } catch (e) {
              ToastUtils.showCustomToast(context, e.toString());
            }
          }
        });
      }
    });
  }

  void getPendingOrders() async {
    if (userData == null)
      userData = await AppSharedPreferences.instance.getUserDetails();

    provider
        .requestGetForApi(context, WebApiConstaint.URL_PENDINGPAYMENTORDER,
            userData.sessionKey, userData.authorization)
        .then((responce) {
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            PendingPaymentOrder order =
                PendingPaymentOrder.fromJson(responce.data);
            if (order.data != null) {
              setState(() {
                widget.listData = order.data;
              });
            }
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
          }
        } catch (e) {
          ToastUtils.showCustomToast(context, e.toString());
        }
      }
    });
  }
}
