// @dart=2.9
import 'dart:io';

import 'package:choix_customer/Data/Referral.dart';
import 'package:choix_customer/Data/UserData.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:permission/permission.dart';
import 'package:share/share.dart';

import 'contacts/ReferralScreenProvider.dart';
import 'contacts/referral_screen.dart';
import 'login_screen.dart';

class Referral_screen extends StatefulWidget {
  @override
  _Referral_screenState createState() => _Referral_screenState();
}

class _Referral_screenState extends State<Referral_screen>
    implements InviteClickListner {
  UserData userData;
  ApiProvider provider = ApiProvider();

  ReferralData orderListData;

  List<Contact> _contacts = [];
  List<Contact> _contactsMain = [];

  FocusNode myFocusNode = new FocusNode();
  ScrollController controller1;
  double customHeight = 200.0;

  var pageNo = 0;

  bool isExpended = true;

  @override
  void initState() {
    // TODO: implement initState
    requestPermissions();
    super.initState();
    controller1 = new ScrollController()..addListener(_scrollListener);
    getReferrel(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.darkPinkColor,
      appBar: AppBar(
        toolbarHeight: 0,
        title: Text("Referral"),
        backgroundColor: CustomColors.darkPinkColor,
      ),
      body: Scaffold(
          backgroundColor: CustomColors.darkPinkColor,
          appBar: AppBar(
            title: Text("Referral"),
            leading: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
            ),
            backgroundColor: CustomColors.darkPinkColor,
          ),
          body: Stack(
            children: [
              Container(
                margin: EdgeInsets.all(15),
                //height: MediaQuery.of(context).size.height *0.7,
                // height: 30,
                color: CustomColors.darkPinkColor,
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Image.asset(
                        "assets/images/referral.png",
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 20),
                    ),
                    Text(
                      "EARN ₹ " +
                          (orderListData != null
                              ? orderListData.referralAmount.toString()
                              : "0") +
                          "!",
                      style: TextStyle(
                          fontFamily: "Montserrat",
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                          color: Colors.white),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 20, right: 20, top: 5),
                      height: 1,
                      color: Colors.white,
                    ),
                    Container(
                      margin: EdgeInsets.only(
                          left: 15, right: 15, bottom: 5, top: 10),
                      child: Text(
                        (orderListData != null
                            ? orderListData.referralDesc
                            : ""),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 15,
                          color: Colors.white,
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Align(
                  alignment: Alignment.bottomRight,
                  child: FloatingActionButton(
                    backgroundColor: Colors.white,
                    onPressed: () {
                      shareURL = orderListData.url;
                      if(shareURL != null) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    PlaylistScreenProvider()));
                      }
                    },
                    child: Icon(Icons.share, color: Colors.grey, size: 29,),
                    // ...FloatingActionButton properties...
                  ),//ClipRRect(
                    // borderRadius: BorderRadius.only(
                    //     topRight: Radius.circular(15),
                    //     topLeft: Radius.circular(15)),
                    // child: Container(
                    //     height: customHeight,
                    //     //margin: EdgeInsets.only(top: 20),
                    //     color: Colors.white,
                    //     child: SingleChildScrollView(
                    //       controller: controller1,
                    //       child: Column(
                    //         children: [
                    //           InkWell(
                    //             onTap: () {
                    //               setState(() {
                    //                 if (isExpended) {
                    //                   isExpended = false;
                    //                   customHeight =
                    //                       MediaQuery.of(context).size.height;
                    //                 } else {
                    //                   isExpended = true;
                    //                   customHeight = 200.00;
                    //                 }
                    //                 print(customHeight);
                    //               });
                    //             },
                    //             child: Container(
                    //               height: 130,
                    //               child: Column(
                    //                 children: [
                    //                   Padding(padding: EdgeInsets.only(top: 15)),
                    //                   Text(
                    //                     "Invite",
                    //                     style: TextStyle(
                    //                         fontFamily: "Montserrat",
                    //                         fontWeight: FontWeight.w600,
                    //                         fontSize: 17),
                    //                   ),
                    //                   Container(
                    //                     margin: EdgeInsets.only(top: 15),
                    //                     height: 1,
                    //                     color: Colors.grey,
                    //                   ),
                    //                   Container(
                    //                     margin: EdgeInsets.only(
                    //                         left: 20, right: 20, top: 15),
                    //                     //color: Colors.blue,
                    //                     height: 50,
                    //                     child: TextField(
                    //                       focusNode: myFocusNode,
                    //                       // autofocus: true,
                    //                       keyboardType: TextInputType.name,
                    //                       textInputAction: TextInputAction.next,
                    //                       //style: TextStyle(color: CustomColors.side_bar_text),
                    //                       onChanged: (value) {
                    //                         addFillter(value);
                    //                       },
                    //                       decoration: InputDecoration(
                    //                         suffixIcon: IconButton(
                    //                           icon: Icon(Icons.search),
                    //                           onPressed: () {},
                    //                         ),
                    //                         filled: true,
                    //                         fillColor: Colors.white,
                    //                         hintText: "Search",
                    //                         //   \n (breank) \\ (\)
                    //                         // labelText: 'Search',
                    //                         // labelStyle: TextStyle(
                    //                         //     color: myFocusNode.hasFocus ? Colors.black : Colors.black,
                    //                         //     fontFamily: "Montserrat"
                    //                         //
                    //                         // ),
                    //                         //hintStyle: TextStyle(color: CustomColors.side_bar_text),
                    //                         contentPadding: const EdgeInsets.only(
                    //                             left: 20.0,
                    //                             bottom: 8.0,
                    //                             top: 8.0,
                    //                             right: 20),
                    //                         focusedBorder: OutlineInputBorder(
                    //                             borderSide: BorderSide(
                    //                                 color: Colors.grey[200]),
                    //                             borderRadius: BorderRadius.all(
                    //                                 Radius.circular(10))),
                    //
                    //                         // errorText: !isNameValidate
                    //                         //     ? "Can\'t be empty" : null,
                    //                         enabledBorder: OutlineInputBorder(
                    //                             borderSide: BorderSide(
                    //                                 color: Colors.grey[200]),
                    //                             borderRadius: BorderRadius.all(
                    //                                 Radius.circular(10))),
                    //                       ),
                    //                     ),
                    //                   ),
                    //                 ],
                    //               ),
                    //             ),
                    //           ),
                    //
                    //           PlaylistScreen(),
                    //           // ListView.builder(
                    //           //     //controller: controller1,
                    //           //     shrinkWrap: true,
                    //           //     itemCount: _contacts.length,
                    //           //     physics: NeverScrollableScrollPhysics(),
                    //           //     itemBuilder: (context, i) {
                    //           //       return ContactItem(_contacts[i], this);
                    //           //     })
                    //         ],
                    //       ),
                    //     )),
                  ),
              ),
              // DraggableScrollableSheet(
              //        initialChildSize: 0.25,
              //        minChildSize: 0.25,
              //        maxChildSize: 1.0,
              //        builder: (context, controller) {
              //        //  controller = controller1;
              //          // controller1 = controller;
              //          // controller1.addListener(_scrollListener1(controller));
              //          return
              //
              //        }),
            ],
          )),
      // Stack(
      //   fit: StackFit.expand,
      //   children: [
      //     Scaffold(
      //       backgroundColor: CustomColors.darkPinkColor,
      //       appBar: AppBar(
      //         title: Text("Referral"),
      //         leading: InkWell(
      //           onTap: () {
      //             Navigator.pop(context);
      //           },
      //           child: Icon(
      //             Icons.arrow_back,
      //             color: Colors.white,
      //           ),
      //         ),
      //         backgroundColor: CustomColors.darkPinkColor,
      //       ),
      //       body: Container(
      //         margin: EdgeInsets.all(15),
      //         //height: MediaQuery.of(context).size.height *0.7,
      //         // height: 30,
      //         color: CustomColors.darkPinkColor,
      //         child: Column(
      //           children: [
      //             Container(
      //               margin: EdgeInsets.only(top: 10),
      //               child: Image.asset(
      //                 "assets/images/referral.png",
      //               ),
      //             ),
      //             Padding(
      //               padding: EdgeInsets.only(top: 20),
      //             ),
      //             Text(
      //               "EARN ₹ " +
      //                   (orderListData != null
      //                       ? orderListData.referralAmount.toString()
      //                       : "0") +
      //                   "!",
      //               style: TextStyle(
      //                   fontFamily: "Montserrat",
      //                   fontWeight: FontWeight.bold,
      //                   fontSize: 30,
      //                   color: Colors.white),
      //             ),
      //             Container(
      //               margin: EdgeInsets.only(left: 20, right: 20, top: 5),
      //               height: 1,
      //               color: Colors.white,
      //             ),
      //             Container(
      //               margin: EdgeInsets.only(
      //                   left: 15, right: 15, bottom: 5, top: 10),
      //               child: Text(
      //                 (orderListData != null ? orderListData.referralDesc : ""),
      //                 textAlign: TextAlign.center,
      //                 style: TextStyle(
      //                   fontFamily: "Montserrat",
      //                   fontSize: 15,
      //                   color: Colors.white,
      //                 ),
      //               ),
      //             )
      //           ],
      //         ),
      //       ),
      //     ),
      // //     InkWell(
      // //   onTap:() {
      // //     setState(() {
      // //       customHeight = MediaQuery.of(context).size.height;
      // //     });
      // //
      // //   },
      // //   child: ClipRRect(
      // //     borderRadius: BorderRadius.only(
      // //         topRight: Radius.circular(15),
      // //         topLeft: Radius.circular(15)),
      // //     child: Container(
      // //       height: customHeight,
      // //       //margin: EdgeInsets.only(top: 20),
      // //         color: Colors.white,
      // //         child: SingleChildScrollView(
      // //           controller: controller1,
      // //           child: Column(
      // //             children: [
      // //               Container(
      // //                 height: 130,
      // //                 child: Column(
      // //                   children: [
      // //                     Padding(padding: EdgeInsets.only(top: 15)),
      // //                     Text(
      // //                       "Invite",
      // //                       style: TextStyle(
      // //                           fontFamily: "Montserrat",
      // //                           fontWeight: FontWeight.w600,
      // //                           fontSize: 17),
      // //                     ),
      // //                     Container(
      // //                       margin: EdgeInsets.only(top: 15),
      // //                       height: 1,
      // //                       color: Colors.grey,
      // //                     ),
      // //                     Container(
      // //                       margin: EdgeInsets.only(
      // //                           left: 20, right: 20, top: 15),
      // //                       //color: Colors.blue,
      // //                       height: 50,
      // //                       child: TextField(
      // //                         focusNode: myFocusNode,
      // //                         // autofocus: true,
      // //                         keyboardType: TextInputType.name,
      // //                         textInputAction: TextInputAction.next,
      // //                         //style: TextStyle(color: CustomColors.side_bar_text),
      // //                         onChanged: (value) {
      // //                           addFillter(value);
      // //                         },
      // //                         decoration: InputDecoration(
      // //                           suffixIcon: IconButton(
      // //                             icon: Icon(Icons.search),
      // //                             onPressed: () {},
      // //                           ),
      // //                           filled: true,
      // //                           fillColor: Colors.white,
      // //                           hintText: "Search",
      // //
      // //                           // labelText: 'Search',
      // //                           // labelStyle: TextStyle(
      // //                           //     color: myFocusNode.hasFocus ? Colors.black : Colors.black,
      // //                           //     fontFamily: "Montserrat"
      // //                           //
      // //                           // ),
      // //                           //hintStyle: TextStyle(color: CustomColors.side_bar_text),
      // //                           contentPadding: const EdgeInsets.only(
      // //                               left: 20.0,
      // //                               bottom: 8.0,
      // //                               top: 8.0,
      // //                               right: 20),
      // //                           focusedBorder: OutlineInputBorder(
      // //                               borderSide: BorderSide(
      // //                                   color: Colors.grey[200]),
      // //                               borderRadius: BorderRadius.all(
      // //                                   Radius.circular(10))),
      // //
      // //                           // errorText: !isNameValidate
      // //                           //     ? "Can\'t be empty" : null,
      // //                           enabledBorder: OutlineInputBorder(
      // //                               borderSide: BorderSide(
      // //                                   color: Colors.grey[200]),
      // //                               borderRadius: BorderRadius.all(
      // //                                   Radius.circular(10))),
      // //                         ),
      // //                       ),
      // //                     ),
      // //                   ],
      // //                 ),
      // //               ),
      // //               ListView.builder(
      // //                   controller: controller1,
      // //                   shrinkWrap: true,
      // //                   itemCount: _contacts.length,
      // //                   physics: NeverScrollableScrollPhysics(),
      // //                   itemBuilder: (context, i) {
      // //                     return
      // //                       ContactItem(_contacts[i], this);})
      // //             ],
      // //           ),
      // //         )),
      // //   ),
      // // ),
      //     // DraggableScrollableSheet(
      //     //     initialChildSize: 0.25,
      //     //     minChildSize: 0.25,
      //     //     maxChildSize: 1.0,
      //     //     builder: (context, controller) {
      //     //       //controller = controller1;
      //     //       // controller1 = controller;
      //     //       // controller1.addListener(_scrollListener1(controller));
      //     //       return
      //     //         ClipRRect(
      //     //         borderRadius: BorderRadius.only(
      //     //             topRight: Radius.circular(15),
      //     //             topLeft: Radius.circular(15)),
      //     //         child: Container(
      //     //             //margin: EdgeInsets.only(top: 20),
      //     //             color: Colors.white,
      //     //             child: SingleChildScrollView(
      //     //               controller: controller,
      //     //               child: Column(
      //     //                 children: [
      //     //                   Container(
      //     //                     height: 130,
      //     //                     child: Column(
      //     //                       children: [
      //     //                         Padding(padding: EdgeInsets.only(top: 15)),
      //     //                         Text(
      //     //                           "Invite",
      //     //                           style: TextStyle(
      //     //                               fontFamily: "Montserrat",
      //     //                               fontWeight: FontWeight.w600,
      //     //                               fontSize: 17),
      //     //                         ),
      //     //                         Container(
      //     //                           margin: EdgeInsets.only(top: 15),
      //     //                           height: 1,
      //     //                           color: Colors.grey,
      //     //                         ),
      //     //                         Container(
      //     //                           margin: EdgeInsets.only(
      //     //                               left: 20, right: 20, top: 15),
      //     //                           //color: Colors.blue,
      //     //                           height: 50,
      //     //                           child: TextField(
      //     //                             focusNode: myFocusNode,
      //     //                             // autofocus: true,
      //     //                             keyboardType: TextInputType.name,
      //     //                             textInputAction: TextInputAction.next,
      //     //                             //style: TextStyle(color: CustomColors.side_bar_text),
      //     //                             onChanged: (value) {
      //     //                               addFillter(value);
      //     //                             },
      //     //                             decoration: InputDecoration(
      //     //                               suffixIcon: IconButton(
      //     //                                 icon: Icon(Icons.search),
      //     //                                 onPressed: () {},
      //     //                               ),
      //     //                               filled: true,
      //     //                               fillColor: Colors.white,
      //     //                               hintText: "Search",
      //     //
      //     //                               // labelText: 'Search',
      //     //                               // labelStyle: TextStyle(
      //     //                               //     color: myFocusNode.hasFocus ? Colors.black : Colors.black,
      //     //                               //     fontFamily: "Montserrat"
      //     //                               //
      //     //                               // ),
      //     //                               //hintStyle: TextStyle(color: CustomColors.side_bar_text),
      //     //                               contentPadding: const EdgeInsets.only(
      //     //                                   left: 20.0,
      //     //                                   bottom: 8.0,
      //     //                                   top: 8.0,
      //     //                                   right: 20),
      //     //                               focusedBorder: OutlineInputBorder(
      //     //                                   borderSide: BorderSide(
      //     //                                       color: Colors.grey[200]),
      //     //                                   borderRadius: BorderRadius.all(
      //     //                                       Radius.circular(10))),
      //     //
      //     //                               // errorText: !isNameValidate
      //     //                               //     ? "Can\'t be empty" : null,
      //     //                               enabledBorder: OutlineInputBorder(
      //     //                                   borderSide: BorderSide(
      //     //                                       color: Colors.grey[200]),
      //     //                                   borderRadius: BorderRadius.all(
      //     //                                       Radius.circular(10))),
      //     //                             ),
      //     //                           ),
      //     //                         ),
      //     //                       ],
      //     //                     ),
      //     //                   ),
      //     //                   ListView.builder(
      //     //                        controller: controller,
      //     //                       shrinkWrap: true,
      //     //                       itemCount: _contacts.length,
      //     //                       physics: NeverScrollableScrollPhysics(),
      //     //                       itemBuilder: (context, i) {
      //     //                         return
      //     //                           ContactItem(_contacts[i], this);})
      //     //                 ],
      //     //               ),
      //     //             )),
      //     //       );
      //     //     }),
      //   ],
      // ),
    );
  }

  Future<void> getReferrel(BuildContext context) async {
    userData = await AppSharedPreferences.instance.getUserDetails();
    String platForm = Platform.isAndroid ? "android" : "ios";
    EasyLoading.show();
    provider
        .requestGetForApi(context, WebApiConstaint.URL_USERREFRRRL+ "?platForm=$platForm",
            userData.sessionKey, userData.authorization)
        .then((responce) {
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          print(responce);
          if (responce.data["error"] == false) {
            var resposdata = Referral.fromJson(responce.data);
            orderListData = resposdata.data;
            setState(() {});
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
            if (responce.data["authenticate"] == false) {
              AppSharedPreferences.instance.setLogin(false);
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                  ModalRoute.withName("/Home"));
            }
          }
        } catch (ex) {
          ToastUtils.showCustomToast(context, ex.toString());
        }
      }
    });
  }

  Future<void> requestPermissions() async {
    List<PermissionName> permissionNames = [];
    permissionNames.add(PermissionName.Contacts);
    var message = '';
    var permissions = await Permission.requestPermissions(permissionNames);
    permissions.forEach((permission) {
      message +=
          '${permission.permissionName}: ${permission.permissionStatus}\n';
      print("contact permission : " + message);
    });
    if (permissions[0].permissionStatus == PermissionStatus.allow) {
      refreshContacts();
    }
    // setState(() {});
  }

  Future<String> refreshContacts() async {
    // Load without thumbnails initially.
    var contacts =
        (await ContactsService.getContacts(withThumbnails: false)).toList();
//      var contacts = (await ContactsService.getContactsForPhone("8554964652"))
//          .toList();
    _contactsMain.clear();
    _contacts.clear();
    for (int i = 0; i < contacts.length; i++) {
      if (contacts[i].phones.length > 0) {
        _contactsMain.add(contacts[i]);
      }
    }
    pageNo++;
    if ((pageNo * 10) < _contactsMain.length) {
      _contacts
          .addAll(_contactsMain.sublist(((pageNo - 1) * 10), (pageNo * 10)));
    } else {
      _contacts.addAll(_contactsMain);
    }
    setState(() {
      _contacts;
    });
    print("contactList : " + _contacts.length.toString());
    return Future.value("_contacts");
    // Lazy load thumbnails after rendering initial contacts.
    // for (final contact in contacts) {
    //   ContactsService.getAvatar(contact).then((avatar) {
    //     if (avatar == null) return; // Don't redraw if no change.
    //     setState(() => contact.avatar = avatar);
    //   });
    // }
  }

  @override
  void onSelected(Contact) {
    String shareURL = orderListData.url;
    Share.share(shareURL);
  }

  _scrollListener() {
    print("Controller1");
    try {
      print(controller1.position.extentAfter);
      if (controller1.position.extentAfter < 500) {
        pageNo++;
        setState(() {
          if (_contactsMain.length > (pageNo * 10)) {
            _contacts.addAll(
                _contactsMain.sublist(((pageNo - 1) * 10), (pageNo * 10)));
          }
        });
      }
    } catch (_ex) {
      print(_ex);
    }
  }

  void addFillter(String text) {
    if (text.toString().trim().length > 0) {
      _contacts.clear();
      for (int i = 0; i < _contactsMain.length; i++) {
        if (_contactsMain[i]
            .displayName
            .toString()
            .toLowerCase()
            .contains(text.toString().trim().toLowerCase())) {
          _contacts.add(_contactsMain[i]);
        }
      }
      setState(() {});
      print(text);
      print(_contactsMain.length);
      print(_contacts.length);
    } else {
      _contacts.addAll(_contactsMain);
      setState(() {});
    }
  }
}

abstract class InviteClickListner {
  void onSelected(Contact);
}
