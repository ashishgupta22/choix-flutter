// @dart=2.9
import 'dart:async';

import 'package:choix_customer/Data/CancelReason.dart';
import 'package:choix_customer/Data/PendingOrder.dart';
import 'package:choix_customer/Data/RouteFetch.dart';
import 'package:choix_customer/Data/UserData.dart';
import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:choix_customer/widget/permission_utils.dart';
import 'package:choix_customer/widget/permission_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:url_launcher/url_launcher.dart';

class VendorOrderAcceptance extends StatefulWidget {
  PendingOrders listData;

  VendorOrderAcceptance(PendingOrders this.listData) : super();

  @override
  _VendorOrderAcceptanceState createState() => _VendorOrderAcceptanceState();
}

class _VendorOrderAcceptanceState extends State<VendorOrderAcceptance>
    implements PermissionCheckListner {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  ApiProvider provider = ApiProvider();
  UserData userData;
  GoogleMap mapView;
  RouteFetchData routeFetch;
  PolylinePoints polylinePoints;

// List of coordinates to join
  List<LatLng> polylineCoordinates = [];

// Map storing polylines created by connecting two points
  Map<PolylineId, Polyline> polylines = {};
  LatLng center = LatLng(0.00, 0.00);
  Timer _timer;

  Completer<GoogleMapController> _controller = Completer();
  CameraPosition initialCameraPosition;
  BitmapDescriptor bikeIcon;
  BitmapDescriptor startLocation;
  BitmapDescriptor destinationLocation;

  GoogleMapController controller2;

  Set<Marker> allMarkers = Set();

  String statusCancel = "";

  String status = "";
  String vendor_image = "";

  bool isLocationPermission = false;

  @override
  void initState() {
    super.initState();
    BitmapDescriptor.fromAssetImage(
            ImageConfiguration(devicePixelRatio: 2.5, size: Size(40, 40)),
            'assets/images/bike.png')
        .then((d) {
      bikeIcon = d;
    });
    BitmapDescriptor.fromAssetImage(
            ImageConfiguration(devicePixelRatio: 2.5, size: Size(40, 40)),
            'assets/images/start_location.png')
        .then((d) {
      startLocation = d;
    });
    BitmapDescriptor.fromAssetImage(
            ImageConfiguration(devicePixelRatio: 2.5, size: Size(40, 40)),
            'assets/images/destination_location.png')
        .then((d) {
      destinationLocation = d;
    });
    if (widget.listData.status == "Onway") CAllAPICheckStatus();
    // WidgetsBinding.instance.addPostFrameCallback((_) {
    //   show(context);
    // });

    if (widget.listData.status == "Open") {
      status = "Pending";
      statusCancel = "Cancel";
    } else if (widget.listData.status == "Accepted") {
      status = "On the Way";
      statusCancel = "Cancel";
    } else if (widget.listData.status == "Onway") {
      status = "On the Way";
      statusCancel = "Call";
    } else if (widget.listData.status == "Delivered") {
      status = "Delivered";
      statusCancel = "";
    } else {
      status = "Canceled";
      statusCancel = "";
    }
    CheckPermission.checkLocationPermissionDash(context, this)
        .then((value) async {
      if (value) {
        print("Location Allowed");
        isLocationPermission = true;
        setState(() {});
      }
    });
    center = LatLng(double.parse(widget.listData.latitude),
        double.parse(widget.listData.longitude));
    // });

    if (widget.listData.status != "Onway") {
      if (allMarkers != null)
        allMarkers.add(Marker(
          markerId: MarkerId('start'),
          draggable: false,
          position: center,
        ));
    }
  }

  @override
  Widget build(BuildContext context) {
    initialCameraPosition =
        CameraPosition(zoom: 15, tilt: 80, bearing: 30, target: center);
    if (isLocationPermission)
      mapView = GoogleMap(
        mapType: MapType.normal,
        //mapToolbarEnabled: false,
        zoomGesturesEnabled: true,
        zoomControlsEnabled: true,
        polylines: Set<Polyline>.of(polylines.values),
        initialCameraPosition:
            CameraPosition(zoom: 15, tilt: 80, bearing: 30, target: center),
        onMapCreated: (GoogleMapController controller) {
          controller2 = controller;
          _controller.complete(controller);
          controller2.animateCamera(
            CameraUpdate.newCameraPosition(
              CameraPosition(target: center, zoom: 15),
            ),
          );
        },
        markers: allMarkers,
      );
    return new WillPopScope(
      onWillPop: () {
        if (_timer != null) {
          _timer.cancel();
        }
        Navigator.pop(context);
      },
      child: Scaffold(
          key: _scaffoldKey,
          backgroundColor: Colors.transparent,
          body: Stack(
            children: [
              Container(
                height: MediaQuery.of(context).size.height * 0.8,
                color: CustomColors.darkPinkColor,
                child: isLocationPermission ? mapView : SizedBox(),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    // borderRadius: BorderRadius.only(
                    //     topLeft: Radius.circular(10),
                    //     topRight: Radius.circular(10),
                    //     bottomLeft: Radius.circular(10),
                    //     bottomRight: Radius.circular(10)
                    // ),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(1.0),
                        spreadRadius: 7,
                        blurRadius: 15,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                  ),
                  padding: EdgeInsets.only(bottom: 30),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Container(
                          color: CustomColors.lightPinkColor,
                          child: Container(
                            //color: Colors.white,
                            margin: EdgeInsets.only(
                                left: 15.0,
                                top: 20.0,
                                right: 20.0,
                                bottom: 20.0),
                            child: ListTile(
                              onTap: () {},
                              leading: CircleAvatar(
                                backgroundImage: vendor_image.isNotEmpty
                                    ? NetworkImage(vendor_image)
                                    : AssetImage(
                                        "assets/images/logo.png",
                                      ),
                              ),
                              title: Text(
                                widget.listData.vendorName.isNotEmpty
                                    ? widget.listData.vendorName
                                    : "XXXXX",
                                style: TextStyle(
                                    color: CustomColors.blackColor,
                                    fontFamily: "Montserrat",
                                    fontSize: 15,
                                    fontWeight: FontWeight.w800),
                              ),
                              subtitle: Text(
                                (routeFetch != null
                                        ? routeFetch.distance.toString()
                                        : "XXXXX") +
                                    " | " +
                                    (routeFetch != null
                                        ? routeFetch.time
                                        : "XXXXX"),
                                style: TextStyle(
                                    fontSize: 13,
                                    fontFamily: "Montserrat",
                                    fontWeight: FontWeight.w600),
                              ),
                            ),
                          ),
                        ),
                        if (widget.listData.productData.length > 0)
                          Padding(
                            padding: EdgeInsets.only(
                                left: 20, top: 10, bottom: 0, right: 20),
                            child: Text(
                              "Order Details",
                              style: TextStyle(
                                  fontSize: 13,
                                  fontFamily: "Montserrat",
                                  fontWeight: FontWeight.w600),
                            ),
                          ),
                        if (widget.listData.productData.length > 0)
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 20, top: 0, bottom: 20, right: 20),
                              child: ListView.builder(
                                  itemCount: widget.listData.productData.length,
                                  shrinkWrap: true,
                                  itemBuilder: (context, index2) {
                                    return Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Flexible(
                                          fit: FlexFit.tight,
                                          flex: 2,
                                          child: Text(
                                            widget.listData.productData[index2]
                                                .name,
                                            style: TextStyle(
                                                fontSize: 14,
                                                fontFamily:
                                                    "Montserrat-Medium"),
                                          ),
                                        ),
                                        Flexible(
                                          fit: FlexFit.tight,
                                          flex: 1,
                                          child: Align(
                                            alignment: Alignment.centerRight,
                                            child: Text(
                                              "${widget.listData.productData[index2].quantity}",
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontFamily:
                                                      "Montserrat-Medium"),
                                            ),
                                          ),
                                        ),
                                        Flexible(
                                          fit: FlexFit.tight,
                                          flex: 1,
                                          child: Align(
                                            alignment: Alignment.centerRight,
                                            child: Text(
                                              "₹${widget.listData.productData[index2].price.toString()}",
                                              style: TextStyle(
                                                  fontSize: 14,
                                                  fontFamily:
                                                      "Montserrat-Medium"),
                                            ),
                                          ),
                                        ),
                                      ],
                                    );
                                  })),
                        if (widget.listData.productData.length <= 0)
                          SizedBox(
                            height: 10,
                          ),
                        Row(
                          children: [
                            InkWell(
                              onTap: () {},
                              child: Container(
                                margin: EdgeInsets.only(left: 10),
                                padding: EdgeInsets.only(left: 20, right: 10),
                                alignment: Alignment.center,
                                height: 35,
                                //width: 150,
                                decoration: new BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: widget.listData.status == "Onway"
                                      ? Colors.green
                                      : CustomColors.darkPinkColor,
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(30)),
                                ),
                                child: Text(
                                  status,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: "Montserrat",
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15),
                                ),
                              ),
                            ),
                            if (statusCancel.isNotEmpty)
                              InkWell(
                                onTap: () {
                                  if (statusCancel == "Cancel") {
                                    callAPICancelReason(widget.listData);
                                  } else if (statusCancel == "Call") {
                                    String phoneNo =
                                        widget.listData.vendorMobile;
                                    _makePhoneCall('tel:$phoneNo');
                                  }
                                },
                                child: Container(
                                  margin: EdgeInsets.only(left: 10),
                                  padding: EdgeInsets.only(left: 7),
                                  alignment: Alignment.center,
                                  height: 35,
                                  width: 100,
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.rectangle,
                                    color: CustomColors.blackColor,
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(30)),
                                  ),
                                  child: Text(
                                    statusCancel,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontFamily: "Montserrat",
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15),
                                  ),
                                ),
                              ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          )),
    );
  }

  Future<void> _makePhoneCall(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Future<void> permissionCheck(bool isGranted) async {
    // TODO: implement permissionCheck
    if (isGranted) {
      isLocationPermission = true;
      mapView = GoogleMap(
        mapType: MapType.normal,
        //mapToolbarEnabled: false,
        zoomGesturesEnabled: true,
        zoomControlsEnabled: true,
        polylines: Set<Polyline>.of(polylines.values),
        initialCameraPosition:
        CameraPosition(zoom: 15, tilt: 80, bearing: 30, target: center),
        onMapCreated: (GoogleMapController controller) {
          controller2 = controller;
          _controller.complete(controller);
          controller2.animateCamera(
            CameraUpdate.newCameraPosition(
              CameraPosition(target: center, zoom: 15),
            ),
          );
        },
        markers: allMarkers,
      );
      setState(() {});
    } else {
      Navigator.pop(context);
    }
  }

  _createPolylines(
    double startLatitude,
    double startLongitude,
    double destinationLatitude,
    double destinationLongitude,
  ) async {
    // Initializing PolylinePoints
    polylinePoints = PolylinePoints();
    // Generating the list of coordinates to be used for
    // drawing the polylines
    PolylineResult result = await polylinePoints.getRouteBetweenCoordinates(
      WebApiConstaint.GOOGLE_API_KEY, // Google Maps API Key
      PointLatLng(startLatitude, startLongitude),
      PointLatLng(destinationLatitude, destinationLongitude),
      // travelMode: TravelMode.bicycling,
    );

    // Adding the coordinates to the list
    polylines.clear();
    polylineCoordinates.clear();
    if (result.points.isNotEmpty) {
      result.points.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }
    // Defining an ID
    PolylineId id = PolylineId('poly');
    // Initializing Polyline
    Polyline polyline = Polyline(
      polylineId: id,
      color: Colors.blue,
      points: polylineCoordinates,
      width: 3,
    );
    // Adding the polyline to the map
    setState(() {
      polylines[id] = polyline;
      print("Polyline" + polylines.length.toString());
    });
  }

  CAllAPICheckStatus() async {
    userData = await AppSharedPreferences.instance.getUserDetails();

    String url = WebApiConstaint.URL_VENDORDETAILS +
        "order_id=" +
        widget.listData.id.toString();
    print("timer : " + url);
    provider
        .requestGetForApi(
            context, url, userData.sessionKey, userData.authorization)
        .then((responce) {
      if (responce != null) {
        print("timer : " + responce.data["message"]);
        if (responce.data["error"] == false) {
          var resposdata = RouteFetch.fromJson(responce.data);
          routeFetch = resposdata.data;
          if (routeFetch != null && routeFetch != null) {
            setState(() {
              routeFetch;
            });
            if (polylines.isEmpty) {
              print("Polyline");
              setState(() {
                routeFetch;
                vendor_image = routeFetch.vendor_image;
              });
              center = LatLng(double.parse(routeFetch.vendorLat),
                  double.parse(routeFetch.vendorLong));
              // });

              controller2.animateCamera(
                CameraUpdate.newCameraPosition(
                  CameraPosition(target: center, zoom: 15),
                ),
              );

              setState(() {
                if (widget.listData.status != "Onway")
                  allMarkers.add(Marker(
                      markerId: MarkerId('start'),
                      draggable: false,
                      icon: startLocation,
                      position: LatLng(double.parse(routeFetch.vendorLat),
                          double.parse(routeFetch.vendorLong))));
                allMarkers.add(Marker(
                    markerId: MarkerId('endDestination'),
                    draggable: false,
                    icon: destinationLocation,
                    position: LatLng(double.parse(routeFetch.orderLat),
                        double.parse(routeFetch.orderLong))));
              });
            }
            _createPolylines(
                double.parse(routeFetch.vendorLat),
                double.parse(routeFetch.vendorLong),
                double.parse(routeFetch.orderLat),
                double.parse(routeFetch.orderLong));
            setState(() {
              allMarkers.add(Marker(
                  markerId: MarkerId('vender'),
                  draggable: false,
                  icon: bikeIcon,
                  position: LatLng(double.parse(routeFetch.vendorLat),
                      double.parse(routeFetch.vendorLong))));
            });
            print("markerleng" + allMarkers.length.toString());
          }
        }
      }
    });
    //Timer Comment
    // _timer = Timer.periodic(Duration(seconds: timerSecond), (timer) {
    //   print("timer : " + timer.tick.toString());
    //   provider
    //       .requestGetForApi(
    //           context, url, userData.sessionKey, userData.authorization)
    //       .then((responce) {
    //     if (responce != null) {
    //       print("timer : " + responce.data["message"]);
    //       if (responce.data["error"] == false) {
    //         var resposdata = RouteFetch.fromJson(responce.data);
    //         routeFetch = resposdata.data;
    //         if (routeFetch != null && routeFetch != null) {
    //           setState(() {
    //             routeFetch;
    //           });
    //           if (polylines.isEmpty) {
    //             print("Polyline");
    //             setState(() {
    //               routeFetch;
    //               vendor_image = routeFetch.vendor_image;
    //             });
    //             center = LatLng(double.parse(routeFetch.vendorLat),
    //                 double.parse(routeFetch.vendorLong));
    //             // });
    //
    //             controller2.animateCamera(
    //               CameraUpdate.newCameraPosition(
    //                 CameraPosition(target: center, zoom: 15),
    //               ),
    //             );
    //
    //             setState(() {
    //
    //               if (widget.listData.status != "Onway")
    //               allMarkers.add(Marker(
    //                   markerId: MarkerId('start'),
    //                   draggable: false,
    //                   icon: startLocation,
    //                   position: LatLng(double.parse(routeFetch.vendorLat),
    //                       double.parse(routeFetch.vendorLong))));
    //               allMarkers.add(Marker(
    //                   markerId: MarkerId('endDestination'),
    //                   draggable: false,
    //                   icon: destinationLocation,
    //                   position: LatLng(double.parse(routeFetch.orderLat),
    //                       double.parse(routeFetch.orderLong))));
    //             });
    //           }
    //           _createPolylines(
    //               double.parse(routeFetch.vendorLat),
    //               double.parse(routeFetch.vendorLong),
    //               double.parse(routeFetch.orderLat),
    //               double.parse(routeFetch.orderLong));
    //           setState(() {
    //             allMarkers.add(Marker(
    //                 markerId: MarkerId('vender'),
    //                 draggable: false,
    //                 icon: bikeIcon,
    //                 position: LatLng(double.parse(routeFetch.vendorLat),
    //                     double.parse(routeFetch.vendorLong))));
    //           });
    //           print("markerleng" + allMarkers.length.toString());
    //         }
    //       }
    //     }
    //   });
    // });
  }

  Future<void> callAPICancelReason(PendingOrders listData) async {
    if (userData == null)
      userData = await AppSharedPreferences.instance.getUserDetails();
    EasyLoading.show();
    String strUrl = WebApiConstaint.URL_CANCELREASON;
    provider
        .requestGetForApi(
            context, strUrl, userData.sessionKey, userData.authorization)
        .then((responce) {
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            CancelReason order = CancelReason.fromJson(responce.data);
            show(context, order.data, listData);
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }

  callAcceptRejectAPI(
      BuildContext context, String order_id, String type, String resion) async {
    CheckPermission.checkLocationPermissionOnly(context).then((value) async {
      if (value != null && value) {
        if (userData == null)
          userData = await AppSharedPreferences.instance.getUserDetails();
        Location location = new Location();
        LocationData _pos = await location.getLocation();
        EasyLoading.show();
        String latitude = _pos.latitude.toString();
        String longitude = _pos.longitude.toString();
        Map<String, dynamic> dictParam = {
          'order_id': order_id,
          'latitude': latitude,
          'longitude': longitude,
          'type': type,
          'reason': resion
        };
        print(dictParam);
        provider
            .requestPostForApi(
                context,
                dictParam,
                WebApiConstaint.URL_ACCEPTREJECT,
                userData.sessionKey,
                userData.authorization)
            .then((responce) {
          EasyLoading.dismiss();
          if (responce != null) {
            try {
              if (responce.data["error"] == false) {
                Fluttertoast.showToast(msg: responce.data["message"]);
                // print(responce.data["data"]);
                // getPendingData(context);
                if (_timer != null) _timer.cancel();
                Navigator.of(context, rootNavigator: true).pop();
              } else {
                Fluttertoast.showToast(msg: responce.data["message"]);
              }
            } catch (e) {
              print("error : " + e.toString());
              // ToastUtils.showCustomToast(context, e.toString());
            }
          }
        });
      }
      ;
    });
  }

  void show(
    BuildContext context2,
    List<CancelReasonData> data,
    PendingOrders listData,
  ) {
    int lastchecked = 0;
    if (data.length > 0) {
      data[0].selected = true;
      showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(10),
            topLeft: Radius.circular(10),
          ),
        ),
        context: _scaffoldKey.currentContext,
        builder: (context) {
          return StatefulBuilder(
            builder: (BuildContext context1, StateSetter setState) {
              return SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: CustomColors.lightPinkColor,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10)),
                      ),
                      child: Column(
                        children: [
                          Padding(
                              padding: EdgeInsets.only(top: 20, bottom: 10)),
                          Row(
                            children: [
                              Padding(
                                  padding: EdgeInsets.only(
                                left: 15,
                              )),
                              Text(
                                "Name :",
                                style: TextStyle(
                                    fontFamily: "Montserrat",
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                              Padding(padding: EdgeInsets.only(right: 3)),
                              Text(
                                listData.vendorName,
                                style: TextStyle(
                                    fontFamily: "Montserrat", fontSize: 14),
                              ),
                              Spacer(),
                              Text(
                                listData.created,
                                style: TextStyle(
                                    fontFamily: "Montserrat",
                                    fontSize: 12,
                                    color: Colors.grey),
                              ),
                              Padding(padding: EdgeInsets.only(right: 15))
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(bottom: 15)),
                          Row(
                            children: [
                              Padding(
                                  padding: EdgeInsets.only(left: 15, top: 8)),
                              Flexible(
                                  child: RichText(
                                      text: TextSpan(
                                style: new TextStyle(
                                  fontFamily: "Montserrat",
                                  fontSize: 14.0,
                                  color: Colors.black,
                                ),
                                children: <TextSpan>[
                                  new TextSpan(
                                      text: 'Address : ',
                                      style: new TextStyle(
                                          fontWeight: FontWeight.bold)),
                                  new TextSpan(
                                      text: listData.address,
                                      style: new TextStyle(fontSize: 14)),
                                ],
                              ))),
                              Padding(
                                  padding: EdgeInsets.only(
                                right: 15,
                              )),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(bottom: 15))
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10, left: 15, right: 15),
                      child: Text(
                        "Order issues",
                        style: TextStyle(
                            fontFamily: "Montserrat-Bold",
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      ),
                    ),
                    ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: data.length,
                        itemBuilder: (context, index) {
                          return Container(
                              margin: EdgeInsets.only(
                                  left: 15, right: 15, bottom: 8),
                              child: Row(
                                children: [
                                  Checkbox(
                                      value: data[index].selected,
                                      checkColor: Colors.white,
                                      activeColor: CustomColors.darkPinkColor,
                                      onChanged: (bool value) {
                                        setState(() {
                                          data[lastchecked].selected = false;
                                          lastchecked = index;
                                          data[index].selected = value;
                                        });
                                      }),
                                  Padding(padding: EdgeInsets.only(left: 5)),
                                  Flexible(
                                    child: Text(
                                      data[index].title,
                                      style: TextStyle(
                                          fontFamily: "Montserrat",
                                          fontSize: 14),
                                    ),
                                  ),
                                  Padding(padding: EdgeInsets.only(right: 5)),
                                ],
                              ));
                        }),
                    Padding(
                      padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                      child: InkWell(
                        onTap: () {
                          callAcceptRejectAPI(context2, listData.id.toString(),
                              "Cancel", data[lastchecked].title);
                          Navigator.of(context).pop();
                        },
                        child: Container(
                          padding: EdgeInsets.only(left: 25, right: 25),
                          alignment: Alignment.center,
                          height: 35,
                          decoration: new BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: CustomColors.darkPinkColor,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                          ),
                          child: Text(
                            "Submit",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          );
        },
      );
    }
  }
}

//
// void show(BuildContext context) {
//   showModalBottomSheet(
//       // shape: RoundedRectangleBorder(
//       //   borderRadius: BorderRadius.only(
//       //     topRight: Radius.circular(10),
//       //     topLeft: Radius.circular(10),
//       //   ),
//       // ),
//       context: context,
//       builder: (context) {
//         return Column(
//           mainAxisSize: MainAxisSize.min,
//           children: [
//             Container(
//               decoration: BoxDecoration(
//                   color: CustomColors.lightPinkColor,
//                   borderRadius: BorderRadius.only(
//                     topRight: Radius.circular(10),
//                     topLeft: Radius.circular(10),
//                   )),
//               child: Container(
//                 margin: EdgeInsets.only(
//                     left: 30.0, top: 20.0, right: 30.0, bottom: 20.0),
//                 child: ListTile(
//                   onTap: () {},
//                   leading: CircleAvatar(
//                     child: Icon(Icons.person_rounded),
//                   ),
//                   title: Text(
//                     "Rohit Kumar",
//                     style: TextStyle(color: CustomColors.blackColor),
//                   ),
//                   subtitle: Text(
//                     '3 kms away | 12 mins',
//                     style: TextStyle(fontSize: 11, fontFamily: "Montserrat"),
//                   ),
//                 ),
//               ),
//             ),
//             Container(
//               margin: EdgeInsets.only(
//                   left: 30.0, top: 10.0, right: 30.0, bottom: 20.0),
//               child: Text(
//                 'Near ridhi sidhi intersection, chayadip nagar, Surya nagar, Gopal Pura Mode, Jaipur',
//                 style: TextStyle(fontFamily: "Montserrat", fontSize: 12),
//               ),
//             ),
//             Row(
//               children: [
//                 Container(
//                   width: 80,
//                   decoration: BoxDecoration(
//                       color: CustomColors.darkPinkColor,
//                       borderRadius: BorderRadius.circular(15)),
//                   margin: EdgeInsets.only(
//                       left: 30.0, top: 10.0, right: 10.0, bottom: 10.0),
//                   child: Padding(
//                     padding: const EdgeInsets.all(10.0),
//                     child: Row(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: [
//                         Icon(
//                           Icons.call,
//                           color: Colors.white,
//                           size: 12,
//                         ),
//                         SizedBox(
//                           width: 5,
//                         ),
//                         Text(
//                           'Call',
//                           style: TextStyle(
//                             fontFamily: "Montserrat",
//                             fontSize: 10,
//                             color: Colors.white,
//                           ),
//                         ),
//                       ],
//                     ),
//                   ),
//                 ),
//                 Container(
//                   width: 80,
//                   decoration: BoxDecoration(
//                       color: CustomColors.blackColor,
//                       borderRadius: BorderRadius.circular(15)),
//                   margin: EdgeInsets.only(
//                       left: 30.0, top: 10.0, right: 10.0, bottom: 10.0),
//                   child: InkWell(
//                     onTap: () {
//                       Navigator.pushAndRemoveUntil(
//                           context,
//                           MaterialPageRoute(
//                               builder: (context) => DashboardPage()),
//                           ModalRoute.withName("/Home"));
//                     },
//                     child: Padding(
//                       padding: const EdgeInsets.all(10.0),
//                       child: Center(
//                         child: Text(
//                           'Cancel',
//                           style: TextStyle(
//                             fontFamily: "Montserrat",
//                             fontSize: 10,
//                             color: Colors.white,
//                           ),
//                         ),
//                       ),
//                     ),
//                   ),
//                 ),
//               ],
//             )
//           ],
//         );
//       });
// }
