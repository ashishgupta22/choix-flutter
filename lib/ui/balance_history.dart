// @dart=2.9

import 'package:choix_customer/Data/BalanceHistory1.dart';
import 'package:choix_customer/Data/UserData.dart';
import 'package:choix_customer/Data/items.dart';
import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/RetryClickListner.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:sticky_headers/sticky_headers/widget.dart';

import 'NoDataFound.dart';
import 'login_screen.dart';

class BalanceHistory extends StatefulWidget {
  String walletBalance;

  BalanceHistory(String this.walletBalance);

  @override
  _BalanceHistoryState createState() => _BalanceHistoryState();
}

class _BalanceHistoryState extends State<BalanceHistory> implements RetryClickListner{
  UserData userData;
  ApiProvider provider = ApiProvider();

  List<Items> listItems = <Items>[];
  List<String> androidChild = <String>[];
  List<String> javaChild = <String>[];
  List<String> flutterChild = <String>[];
  bool noRecordFound = false;
  List<BalanceHistoryData> dataList = [];

  void createStickyHeaderList() {}

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    CallAPIWalletHistory();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Balance History"),
        backgroundColor: CustomColors.darkPinkColor,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 15),
            margin: EdgeInsets.only(
              bottom: 10,
            ),
            color: CustomColors.lightPinkColor,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  children: [
                    Text(
                      "Available Balance",
                      style: TextStyle(
                          fontSize: 16,
                          fontFamily: "Montserrat",
                          fontWeight: FontWeight.w600),
                    ),
                    Spacer(),
                    Container(
                        padding: EdgeInsets.only(left: 10,bottom: 5),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(12),
                          color: CustomColors.darkPinkColor,
                          // border: Border.all(
                          //     color: CustomColors.darkPinkColor,width: 1
                          // )
                        ),
                        child: Row(
                          children: [
                            InkWell(
                              onTap: () => Navigator.pop(context),
                              child: Text(
                                'Add',
                                style: TextStyle(
                                    fontFamily: "Montserrat",
                                    fontSize: 15,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.white),
                              ),
                            ),
                            Icon(
                              Icons.add,
                              color: Colors.white,
                            )
                          ],
                        ))
                  ],
                ),
                Padding(padding: EdgeInsets.only(top: 10)),
                Text(
                  "₹ ${widget.walletBalance}",
                  style: TextStyle(
                      fontFamily: "Montserrat",
                      fontSize: 30,
                      fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 20, right: 20, top: 20, bottom: 15),
            height: 70,
            color: Colors.white,
            child: Text(
              "Payment History",
              style: TextStyle(
                  fontFamily: "Montserrat",
                  fontSize: 16,
                  fontWeight: FontWeight.w600),
            ),
          ),

          noRecordFound ? NoDataFound(this) : Expanded(
            child: ListView.builder(
                //physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: listItems.length,
                itemBuilder: (context, index) {
                  return StickyHeaderBuilder(
                      builder: (BuildContext context, double stuckAmount) {
                        stuckAmount = 1.0 - stuckAmount.clamp(0.0, 1.0);
                        return header(stuckAmount, context, index);
                      },
                      content: Column(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: headerChild(
                              listItems[index].childItems, index, context)));
                }),
          )
        ],
      ),
    );
  }

  @override
  void onClickListner() {
    // TODO: implement onClickListner
    CallAPIWalletHistory();
  }
  Widget header(double stuckAmount, BuildContext context, int index) {
    return Container(
      height: 50.0,
      color: CustomColors.lightPinkColor,
      padding: EdgeInsets.only(left: 20,top: 16,right: 16,bottom: 16,),
      alignment: Alignment.centerLeft,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              listItems[index].headerTitle,
              style: const TextStyle(
                  color: Colors.black87,
                  fontWeight: FontWeight.w600,
                  fontFamily: "Montserrat"),
            ),
          ),
        ],
      ),
    );
  }

  CallAPIWalletHistory() async {
    userData = await AppSharedPreferences.instance.getUserDetails();
    EasyLoading.show();
    provider
        .requestGetForApi(context, WebApiConstaint.URL_WALLET_HISTORY,
            userData.sessionKey, userData.authorization)
        .then((responce) {
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            var resposdata = BalanceHistory1.fromJson(responce.data);
            if (resposdata.data.isNotEmpty) {
              noRecordFound = false;
              dataList = resposdata.data;
              String dataList1 = "";
              List<BalanceHistoryData> dataList2 = [];
              setState(() {
                for (int i = 0; i < dataList.length; i++) {
                  initializeDateFormatting('IST');
                  DateFormat sdf =
                      new DateFormat("yyyy-MM-dd'T'HH:mm:ssZ", "IST");
                  DateFormat sdfFormat = new DateFormat('MMM yyyy');
                  DateTime date = sdf.parse(dataList[i].createdAt);
                  String dateMain = sdfFormat.format(date);
                  if (dataList1 == dateMain) {
                    print("dataListR" + dataList1);
                    dataList2.add(dataList[i]);
                    if ((dataList.length) - 1 == i) {
                      listItems.add(Items(dataList1, dataList2));
                    }
                    print("dataListR" + dataList2.length.toString());
                  } else {
                    print("dataListRR" + dataList1);
                    if (dataList2.isNotEmpty)
                      listItems.add(Items(dataList1, dataList2));
                    dataList2 = List();
                    dataList1 = "";
                    dataList1 = dateMain;
                    dataList2.add(dataList[i]);
                    if ((dataList.length) - 1 == i) {
                      listItems.add(Items(dataList1, dataList2));
                    }
                  }
                }
                print("dataList" + listItems.length.toString());
                print("dataList2" + dataList.length.toString());
              });
            }
            else{
              noRecordFound = true;
              setState(() {

              });
            }
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
            if (responce.data["authenticate"] == false) {
              AppSharedPreferences.instance.setLogin(false);
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginScreen()));
            }else{
              noRecordFound = true;
              setState(() {

              });
            }
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }
}

List<Widget> headerChild(
    List<BalanceHistoryData> child, int index, BuildContext context) {
  var listWidget = List<Widget>();

  child.forEach((listData) {

    // initializeDateFormatting('IST');
    // DateFormat sdf =
    // new DateFormat("yyyy-MM-dd'T'HH:mm:ssZ", "IST");
    // DateFormat sdfFormat = new DateFormat('dd-MM-yyyy HH:mm');
    // DateTime date = sdf.parse(listData.createdAt);
    // String dateMain = sdfFormat.format(date);
    listWidget.add(Container(
      color: Colors.white,
      // color: CustomColors.lightPinkColor,

      margin: EdgeInsets.only(left: 5, right: 5, top: 5, bottom: 5),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.only(top: 8, left: 15, right: 15, bottom: 8),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                //Padding(padding: EdgeInsets.only(left: 10)),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      //color: Colors.grey,
                      // margin: EdgeInsets.only(top: 10),
                      height: 45,
                      width: 45,
                      decoration: BoxDecoration(
                        color: CustomColors.darkPinkColor,
                        // image: backgroundImage != null
                        //     ? new DecorationImage(image: backgroundImage, fit: BoxFit.cover)
                        //     : null,

                        shape: BoxShape.circle,
                      ),
                      child: Image.asset(
                        "assets/images/bel.png",
                        height: 20,
                        color: Colors.white,
                      ),
                    ),
                    Padding(padding: EdgeInsets.only(left: 10)),
                    Expanded(
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Expanded(
                                child: Text(
                                  listData.notes,
                                  style: TextStyle(
                                      fontFamily: "Montserrat",
                                      fontSize: 16),
                                ),
                              ),
                              SizedBox(width: 10,),
                              Spacer(),
                              Text(
                                (listData.status == "Credit" ? "+" : "-") +
                                    "₹${listData.totalAmount.toString()}",
                                style: TextStyle(
                                  fontFamily: "Montserrat",
                                  fontSize: 15,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),

                  ],
                ),
                Container(
                  margin: EdgeInsets.only(left: 55),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      //s Text('Money added to choix wallet',style: TextStyle(fontFamily: "Montserrat",fontWeight: FontWeight.w600,fontSize: 15),),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        listData.created,
                        style: TextStyle(
                          fontFamily: "Montserrat",
                          fontSize: 15,
                          color: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 15),
                  height: 1,
                  color: Colors.grey,
                )
              ],
            ),
          )
        ],
      ),
    ));
  });

  return listWidget;
}
