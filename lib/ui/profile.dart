// @dart=2.9
import 'package:choix_customer/Data/UserData.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:choix_customer/widgets/CustomTextFiled.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:async';
import 'dart:io';

import 'dashboard_screen.dart';
import 'login_screen.dart';

class _ProfileScreenState extends State<ProfileScreen> {
  FocusNode myFocusNode = new FocusNode();
  FocusNode myFocusNode1 = new FocusNode();
  FocusNode myFocusNode2 = new FocusNode();
  TextEditingController txtNameController = TextEditingController();
  TextEditingController txtEmailController = TextEditingController();
  TextEditingController txtPhoneController = TextEditingController();
  TextEditingController txtAddressController = TextEditingController();
  TextEditingController txtAreaController = TextEditingController();
  TextEditingController txtPostCodeController = TextEditingController();
  TextEditingController txtDistrictController = TextEditingController();

  bool isNameValidate = true;
  bool isPhnValidate = true;
  bool isEmailValidate = true;
  UserData userData = UserData();
  ApiProvider provider = ApiProvider();

  List<XFile> _imageFileList = [];

  var txtMobileController = TextEditingController();

  bool isNameEnable = false;

  bool autoFocus = false;

  set _imageFile(XFile value) {
    _imageFileList = [];
    _imageFileList = value == null ? null : [value];
  }
  static final GlobalKey<FormFieldState<String>> _searchFormKey = GlobalKey<FormFieldState<String>>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getuserData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Profile"),
        backgroundColor: CustomColors.darkPinkColor,
        leading: InkWell(
          onTap: () {
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
        //automaticallyImplyLeading: false,
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            children: [
              Stack(
                children: [
                  Container(
                    color: Colors.white,
                  ),
                  Container(
                    height: 190,
                    color: CustomColors.lightPinkColor,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Align(
                        alignment: Alignment.center,
                        child: Container(
                          margin: EdgeInsets.only(top: 30),
                          height: 90,
                          width: 90,
                          child: Stack(
                            children: [
                              // if (_imageFileList.length <= 0)
                              // Container(
                              //   height: 70,
                              //   child: CircleAvatar(
                              //     radius: 35,
                              //     child: ClipRRect(
                              //       borderRadius: BorderRadius.circular(35),
                              //       child: Image.asset(
                              //         "assets/images/logo.png",
                              //       ),
                              //     ),
                              //   ),
                              // ),
                              // if (_imageFileList.length > 0)
                              //   Container(
                              //     height: 70,
                              //     child: CircleAvatar(
                              //       radius: 35,
                              //       child: ClipRRect(
                              //         child: Image.file(
                              //           File(_imageFileList[0].path),
                              //         ),
                              //       ),
                              //     ),
                              //   ),
                              CircleAvatar(
                                  radius: 55,
                                  child: _imageFileList.length > 0
                                      ? ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(50),
                                          child: Image.file(
                                            File(_imageFileList[0].path),
                                            width: 90,
                                            height: 90,
                                            fit: BoxFit.fitHeight,
                                          ),
                                        )
                                      : Container(
                                          width: 90,
                                          height: 90,
                                          decoration: BoxDecoration(
                                            image: DecorationImage(
                                              image:
                                                  userData.profileImage != null  && userData.profileImage.isNotEmpty
                                                      ? NetworkImage(
                                                          userData.profileImage)
                                                      : ExactAssetImage(
                                                          "assets/images/logo.png",
                                                        ),
                                              fit: BoxFit.cover,
                                            ),
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(100.0)),
                                            border: Border.all(
                                              color: Colors.white,
                                              width: 1.0,
                                            ),
                                          ),
                                        )
                                  // Container(
                                  //         height: 70,
                                  //         width: 70,
                                  //         child: CircleAvatar(
                                  //           radius: 35,
                                  //           child: ClipRRect(
                                  //             borderRadius:
                                  //                 BorderRadius.circular(35),
                                  //             child:
                                  //                 userData.profileImage.isNotEmpty
                                  //                     ? Image.network(
                                  //                         userData.profileImage,
                                  //                       )
                                  //                     : Image.network(
                                  //                         userData.profileImage,
                                  //                       ),
                                  //           ),
                                  //         ),
                                  //       ),
                                  ),
                              Align(
                                  alignment: Alignment.bottomRight,
                                  child: Container(
                                    padding: EdgeInsets.only(top: 0, left: 0),
                                    height: 28,
                                    width: 28,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(20)),
                                        color: Colors.white),
                                    child: InkWell(
                                      onTap: () {
                                        _showPicker(context);
                                      },
                                      child: CircleAvatar(
                                        radius: 18,
                                        backgroundColor: Colors.white,
                                        child: Icon(
                                          Icons.camera_alt,
                                          color: CustomColors.darkPinkColor,
                                          size: 17,
                                        ),
                                      ),
                                    ),
                                  )),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Text(
                        userData.name != null ? userData.name : "XXXX",
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w700,
                            fontFamily: "Montserrat"),
                      ),
                      Text(
                        "",
                        style: TextStyle(
                            fontSize: 10,
                            fontWeight: FontWeight.w300,
                            fontFamily: "Montserrat"),
                      )
                    ],
                  ),
                  Container(
                    margin: EdgeInsets.only(
                        top: 230, left: 20, right: 20, bottom: 20),
                    child: Column(
                      children: [
                        // Padding(padding: EdgeInsets.only(top: 210,bottom: 10)),

                        Stack(
                          children: [ TextField(
                            key: _searchFormKey,
                            focusNode: myFocusNode,
                            autofocus: true,
                            controller: txtNameController,
                            maxLength: 60,
                            keyboardType: TextInputType.name,
                            textInputAction: TextInputAction.done,
                            //style: TextStyle(color: CustomColors.side_bar_text),
                            decoration: InputDecoration(
                              enabled: isNameEnable,
                              counter: Offstage(),
                              filled: true,
                              fillColor: Colors.white,
                              hintText: "Enter your name",
                              hintStyle: TextStyle(color: CustomColors.hintColor),
                              labelText: "Name",
                              labelStyle: TextStyle(
                                  color: myFocusNode.hasFocus
                                      ? Colors.black
                                      : Colors.black,
                                  fontFamily: "Montserrat"),
                              //hintStyle: TextStyle(color: CustomColors.side_bar_text),
                              contentPadding: const EdgeInsets.only(
                                  left: 20.0, bottom: 8.0, top: 8.0, right: 20),
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey[300]),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(30))),

                              errorText: !isNameValidate ? "can't be empty" : null,
                              disabledBorder:OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey[200]),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(30))),
                              enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey[200]),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(30))),
                              errorBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey[200]),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(30))),
                              focusedErrorBorder:  OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.grey[200]),
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(30))),
                            ),
                          ),
                            Align(
                              alignment: Alignment.centerRight,
                              child:  IconButton(icon: Icon(Icons.edit),
                                onPressed: (){

                                    setState(() {
                                      isNameEnable = true;
                                      autoFocus = true;
                                      myFocusNode = FocusNode();
                                      FocusScope.of(context).requestFocus(
                                          myFocusNode);
                                    });
                                    setState(() {
                                      txtNameController.selection = TextSelection.fromPosition(TextPosition(offset: txtNameController.text.length));
                                    });
                                },),
                            )],

                        ),
                        SizedBox(
                          height: 20,
                        ),
                        CustomTextFiled(
                            true,
                            myFocusNode1,
                            txtEmailController,
                            isEmailValidate,
                            'Enter your email',
                            'Email',
                            TextInputType.emailAddress,
                            TextInputAction.done,
                            "Invalid Email",
                            50,
                            false),
                        // TextField(
                        //   //autofocus: true,
                        //   controller: txtEmailController,
                        //   keyboardType: TextInputType.emailAddress,
                        //   textInputAction: TextInputAction.next,
                        //   //style: TextStyle(color: CustomColors.side_bar_text),
                        //   decoration: InputDecoration(
                        //     filled: true,
                        //     fillColor: Colors.white,
                        //     hintText: 'Enter your email',
                        //     labelText: 'Email',
                        //     labelStyle: TextStyle(
                        //         color: myFocusNode.hasFocus
                        //             ? Colors.black
                        //             : Colors.black,
                        //         fontFamily: "Montserrat"),
                        //     //hintStyle: TextStyle(color: CustomColors.side_bar_text),
                        //     contentPadding: const EdgeInsets.only(
                        //         left: 20.0, bottom: 8.0, top: 8.0, right: 20),
                        //     focusedBorder: OutlineInputBorder(
                        //         borderSide: BorderSide(color: Colors.grey[300]),
                        //         borderRadius:
                        //             BorderRadius.all(Radius.circular(30))),
                        //
                        //     errorText:
                        //         !isEmailValidate ? "Can\'t be empty" : null,
                        //     enabledBorder: OutlineInputBorder(
                        //         borderSide: BorderSide(color: Colors.grey[200]),
                        //         borderRadius:
                        //             BorderRadius.all(Radius.circular(30))),
                        //   ),
                        // ),
                        SizedBox(
                          height: 20,
                        ),
                        CustomTextFiled(
                            true,
                            myFocusNode2,
                            txtMobileController,
                            true,
                            'Enter your number',
                            'Phone Number',
                            TextInputType.number,
                            TextInputAction.done,
                            "",
                            10,
                            false),
                        // TextField(
                        //   //autofocus: true,
                        //   controller: txtMobileController,
                        //   keyboardType: TextInputType.emailAddress,
                        //   textInputAction: TextInputAction.next,
                        //   //style: TextStyle(color: CustomColors.side_bar_text),
                        //   enableInteractiveSelection: false, // will disable paste operation
                        //   focusNode: new AlwaysDisabledFocusNode(),
                        //   decoration: InputDecoration(
                        //     filled: true,
                        //     fillColor: Colors.white,
                        //     hintText: 'Mobile Number',
                        //     labelText: 'Mobile Number',
                        //     labelStyle: TextStyle(
                        //         color: myFocusNode.hasFocus
                        //             ? Colors.black
                        //             : Colors.black,
                        //         fontFamily: "Montserrat"),
                        //     //hintStyle: TextStyle(color: CustomColors.side_bar_text),
                        //     contentPadding: const EdgeInsets.only(
                        //         left: 20.0, bottom: 8.0, top: 8.0, right: 20),
                        //     focusedBorder: OutlineInputBorder(
                        //         borderSide: BorderSide(color: Colors.grey[300]),
                        //         borderRadius:
                        //         BorderRadius.all(Radius.circular(30))),
                        //
                        //     errorText:
                        //     !isEmailValidate ? "Can\'t be empty" : null,
                        //     enabledBorder: OutlineInputBorder(
                        //         borderSide: BorderSide(color: Colors.grey[200]),
                        //         borderRadius:
                        //         BorderRadius.all(Radius.circular(30))),
                        //   ),
                        // ),

                        SizedBox(
                          height: 20,
                        ),
                        // TextField(
                        //  // autofocus: true,
                        //   controller: txtAddressController,
                        //   // keyboardType: TextInputType.name,
                        //   textInputAction: TextInputAction.next,
                        //   //style: TextStyle(color: CustomColors.side_bar_text),
                        //   decoration: InputDecoration(
                        //
                        //     filled: true,
                        //     fillColor: Colors.white,
                        //     hintText: 'Enter your address',
                        //     labelText: 'Address',
                        //     labelStyle: TextStyle(
                        //         color: myFocusNode.hasFocus ? Colors.black : Colors.black,
                        //         fontFamily: "Montserrat"
                        //
                        //     ),
                        //     //hintStyle: TextStyle(color: CustomColors.side_bar_text),
                        //     contentPadding:
                        //     const EdgeInsets.only(left: 20.0, bottom: 8.0, top: 8.0,right: 20),
                        //     focusedBorder: OutlineInputBorder(
                        //         borderSide: BorderSide(color: Colors.grey[300]),
                        //         borderRadius: BorderRadius.all(Radius.circular(30))
                        //     ),
                        //
                        //     // errorText: !isNameValidate
                        //     //     ? "Can\'t be empty" : null,
                        //     enabledBorder: OutlineInputBorder(
                        //         borderSide: BorderSide(color: Colors.grey[200]),
                        //         borderRadius:BorderRadius.all(Radius.circular(30))
                        //     ),
                        //   ),
                        // ),
                        // SizedBox(
                        //   height: 20,
                        // ),
                        // TextField(
                        //
                        //   //autofocus: true,
                        //   controller: txtAreaController,
                        //   keyboardType: TextInputType.name,
                        //   textInputAction: TextInputAction.next,
                        //   //style: TextStyle(color: CustomColors.side_bar_text),
                        //   decoration: InputDecoration(
                        //
                        //     filled: true,
                        //     fillColor: Colors.white,
                        //     hintText: 'Enter your area',
                        //     labelText: 'Area',
                        //     labelStyle: TextStyle(
                        //         color: myFocusNode.hasFocus ? Colors.black : Colors.black,
                        //         fontFamily: "Montserrat"
                        //
                        //     ),
                        //     //hintStyle: TextStyle(color: CustomColors.side_bar_text),
                        //     contentPadding:
                        //     const EdgeInsets.only(left: 20.0, bottom: 8.0, top: 8.0,right: 20),
                        //     focusedBorder: OutlineInputBorder(
                        //         borderSide: BorderSide(color: Colors.grey[300]),
                        //         borderRadius: BorderRadius.all(Radius.circular(30))
                        //     ),
                        //
                        //     // errorText: !isNameValidate
                        //     //     ? "Can\'t be empty" : null,
                        //     enabledBorder: OutlineInputBorder(
                        //         borderSide: BorderSide(color: Colors.grey[200]),
                        //         borderRadius:BorderRadius.all(Radius.circular(30))
                        //     ),
                        //   ),
                        // ),
                        // SizedBox(
                        //   height: 20,
                        // ),
                        // TextField(
                        //  // autofocus: true,
                        //   controller: txtPostCodeController,
                        //   keyboardType: TextInputType.name,
                        //   textInputAction: TextInputAction.next,
                        //   //style: TextStyle(color: CustomColors.side_bar_text),
                        //   decoration: InputDecoration(
                        //
                        //     filled: true,
                        //     fillColor: Colors.white,
                        //     hintText: 'Enter your postcode',
                        //     labelText: 'Postcode',
                        //     labelStyle: TextStyle(
                        //         color: myFocusNode.hasFocus ? Colors.black : Colors.black,
                        //         fontFamily: "Montserrat"
                        //
                        //     ),
                        //     //hintStyle: TextStyle(color: CustomColors.side_bar_text),
                        //     contentPadding:
                        //     const EdgeInsets.only(left: 20.0, bottom: 8.0, top: 8.0,right: 20),
                        //     focusedBorder: OutlineInputBorder(
                        //         borderSide: BorderSide(color: Colors.grey[300]),
                        //         borderRadius: BorderRadius.all(Radius.circular(30))
                        //     ),
                        //
                        //     // errorText: !isNameValidate
                        //     //     ? "Can\'t be empty" : null,
                        //     enabledBorder: OutlineInputBorder(
                        //         borderSide: BorderSide(color: Colors.grey[200]),
                        //         borderRadius:BorderRadius.all(Radius.circular(30))
                        //     ),
                        //   ),
                        // ),
                        // SizedBox(
                        //   height: 20,
                        // ),
                        // TextField(
                        // //  autofocus: true,
                        //   controller: txtDistrictController,
                        //   keyboardType: TextInputType.name,
                        //   textInputAction: TextInputAction.done,
                        //   //style: TextStyle(color: CustomColors.side_bar_text),
                        //   decoration: InputDecoration(
                        //
                        //     filled: true,
                        //     fillColor: Colors.white,
                        //     hintText: 'Enter your district',
                        //     labelText: 'District',
                        //     labelStyle: TextStyle(
                        //         color: myFocusNode.hasFocus ? Colors.black : Colors.black,
                        //         fontFamily: "Montserrat"
                        //
                        //     ),
                        //     //hintStyle: TextStyle(color: CustomColors.side_bar_text),
                        //     contentPadding:
                        //     const EdgeInsets.only(left: 20.0, bottom: 8.0, top: 8.0,right: 20),
                        //     focusedBorder: OutlineInputBorder(
                        //         borderSide: BorderSide(color: Colors.grey[300]),
                        //         borderRadius: BorderRadius.all(Radius.circular(30))
                        //     ),
                        //
                        //     // errorText: !isNameValidate
                        //     //     ? "Can\'t be empty" : null,
                        //     enabledBorder: OutlineInputBorder(
                        //         borderSide: BorderSide(color: Colors.grey[200]),
                        //         borderRadius:BorderRadius.all(Radius.circular(30))
                        //     ),
                        //   ),
                        // ),
                        // SizedBox(
                        //   height: 50,
                        // ),
                        InkWell(
                          child: Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.only(left: 15, right: 15),
                            height: 50,
                            width: MediaQuery.of(context).size.width,
                            decoration: new BoxDecoration(
                              gradient: LinearGradient(
                                  begin: const Alignment(0.0, -2.0),
                                  end: const Alignment(0.0, 0.3),
                                  colors: [
                                    Colors.white,
                                    CustomColors.darkPinkColor
                                  ]),
                              shape: BoxShape.rectangle,
                              color: CustomColors.darkPinkColor,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30)),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.pink[100],
                                  spreadRadius: 5,
                                  blurRadius: 10,
                                  offset: Offset(
                                      0, 2), // changes position of shadow
                                ),
                              ],
                            ),
                            child: Text(
                              "Update",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: "Montserrat",
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15),
                            ),
                          ),
                          onTap: () {
                            setState(() {
                              isNameValidate = true;
                              isEmailValidate = true;
                              if (txtNameController.text == "") {
                                isNameValidate = false;
                              } else {
                                if (txtEmailController.text == "") {
                                  isEmailValidate = false;
                                } else {
                                  updateProfile();
                                }
                              }
                            });
                          },
                        ),
                      ],
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
  Future<void> getuserData() async {
    userData = await AppSharedPreferences.instance.getUserDetails();
    setState(() {
      userData;
      txtNameController.text =
          userData.name != null ? userData.name.toString() : "";
      txtEmailController.text =
          userData.email != null ? userData.email.toString() : "";
      txtMobileController.text =
          userData.mobileNo != null ? userData.mobileNo.toString() : "";
    });
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _onImageButtonPressed(ImageSource.gallery,
                            context: context);
                        Navigator.of(context).pop();
                      }),
                  SizedBox(
                    height: 10,
                  ),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _onImageButtonPressed(ImageSource.camera,
                          context: context);
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  void _onImageButtonPressed(ImageSource source, {BuildContext context}) async {
    final ImagePicker _picker = ImagePicker();
    try {
      if (_picker != null) {
        final pickedFile =
            await _picker.pickImage(source: source, imageQuality: 70);
        if (pickedFile != null)
          setState(() {
            _imageFile = pickedFile;
          });
      }
    } catch (e) {
      setState(() {

      });
    }
  }

  Future<void> updateProfile() async {
    EasyLoading.show();
    // Map<String, String> dictParam = {'mobile_no': widget.mobileNO,
    //   'mobile_otp': otp,'device_type':"android",'device_id':deviceId};

    FormData formData = FormData.fromMap({
      if (_imageFileList.length > 0)
        "profile_image": await MultipartFile.fromFile(_imageFileList[0].path,
            filename: _imageFileList[0].name),
      "email": await MultipartFile.fromString(txtEmailController.text),
      "name": await MultipartFile.fromString(txtNameController.text),
    });
    // print(dictParam);
    provider
        .requestPostForApiFileUpload(
            context,
            formData,
            WebApiConstaint.URL_UPDATE_PROFILE,
            userData.sessionKey,
            userData.authorization)
        .then((responce) async {
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            Fluttertoast.showToast(msg: responce.data["message"]);
            Map<dynamic, dynamic> map = {
              'map1': {
                'session_key': userData.sessionKey,
                'authorization': userData.authorization
              }
            };
            Map<String, dynamic> userData1 = responce.data["data"];
            print(userData1);
            userData1.addAll((Map.from(map['map1'])));
            print(userData1);
            AppSharedPreferences.instance.setUserDetails(userData1);
            userData = await AppSharedPreferences.instance.getUserDetails();
            print(userData);
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => DashboardPage(),
                ),
                ModalRoute.withName('/login_screen'));
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            if (responce.data["authenticate"] == false) {
              AppSharedPreferences.instance.setLogin(false);
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                  ModalRoute.withName("/Home"));
            } else {
              Fluttertoast.showToast(msg: responce.data["message"]);
              print(responce.data["message"]);
              if (responce.data["authenticate"] == false) {
                AppSharedPreferences.instance.setLogin(false);
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => LoginScreen()),
                    ModalRoute.withName("/Home"));
              }
            }
          }
        } catch (e) {
          ToastUtils.showCustomToast(context, e.toString());
        }
      } else {
        print("response null");
      }
    });
  }
}

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}
