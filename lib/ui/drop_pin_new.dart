// @dart=2.9
import 'dart:async';
import 'package:choix_customer/Data/FetchAddress.dart';
import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/alert_dialog_new.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:choix_customer/ui/dashboard_screen.dart';
import 'package:choix_customer/ui/google_place_autocomplete.dart';
import 'package:choix_customer/ui/offers/best_offres.dart';
import 'package:choix_customer/ui/orders/order_list.dart';
import 'package:choix_customer/widget/permission_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'login_screen.dart';

class DropPinNew extends StatefulWidget {
  @override
  _DropPinNewState createState() => _DropPinNewState();
}

class _DropPinNewState extends State<DropPinNew> implements PermissionCheckListner{
  Completer<GoogleMapController> _controller = Completer();
  Position currentLocation;
  LatLng _center;

  Set<Marker> allMarkers = Set();

  CameraPosition cPosition;
  String userId, token;
  String routeId;
  String strAddress = "";

  List<Address> strAddressData;

  String locality = "";

  BitmapDescriptor pinIcon;

  List<Address> addresses = [];
  int selected = 1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    BitmapDescriptor.fromAssetImage(
            ImageConfiguration(devicePixelRatio: 2.5, size: Size(60, 60)),
            'assets/images/pin.png')
        .then((d) {
      pinIcon = d;
    });
    init();
    getUserLocation();
  }

  void init() async {}

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Stack(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height * 0.65,
                  child: Stack(
                    children: [
                      _center != null
                          ? GoogleMap(
                              // onTap: _handleTap,
                              mapType: MapType.normal,
                              zoomGesturesEnabled: true,
                              tiltGesturesEnabled: false,
                              myLocationEnabled: true,
                              initialCameraPosition: cPosition,
                              markers: allMarkers,
                              onMapCreated: (GoogleMapController controller) {
                                _controller.complete(controller);
                              },
                              onCameraIdle: () {
                                updateMarkerOnselectedLocation();
                              },
                              gestureRecognizers:
                                  <Factory<OneSequenceGestureRecognizer>>[
                                new Factory<OneSequenceGestureRecognizer>(
                                  () => new EagerGestureRecognizer(),
                                ),
                              ].toSet(),
                              onCameraMove: (position) {
                                print(
                                    'new Location1 ${position.target.longitude}');
                                print('new Location1 $position.longitude');
                                _center = LatLng(position.target.latitude,
                                    position.target.longitude);
                                setState(() {
                                  allMarkers.add(Marker(
                                      markerId: MarkerId("testtt"),
                                      draggable: false,
                                      icon: pinIcon,
                                      position: _center));
                                });
                              },
                            )
                          : Container(),
                      // Align(
                      //   alignment: Alignment.center,
                      //   child: Image.asset(
                      //     'assets/images/pin.png',
                      //     width: 50,
                      //     height: 50,
                      //     alignment: Alignment.center,
                      //     frameBuilder:
                      //         (context, child, frame, wasSynchronouslyLoaded) {
                      //       return Transform.translate(
                      //         offset: const Offset(8, -37),
                      //         child: child,
                      //       );
                      //     },
                      //   ),
                      // )
                    ],
                  ),
                ),
              ],
            ),
            Padding(
              padding:
                  const EdgeInsets.only(left: 10, top: 4, right: 10, bottom: 7),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 3,
                  ),
                  Text(
                    "Select delivery location",
                    style: TextStyle(fontSize: 18),
                  ),
                  Divider(
                    color: Colors.grey,
                  ),
                  SizedBox(
                    height: 3,
                  ),
                  Text(
                    "YOUR LOCATION",
                    style: TextStyle(fontSize: 10, color: Colors.grey),
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Text(
                    strAddress,
                    style: TextStyle(fontSize: 18),
                    maxLines: 2,
                  ),
                  InkWell(
                    onTap: () async {
                      displayPrediction();
                    },
                    child: Container(
                        width: MediaQuery.of(context).size.width,
                        alignment: Alignment.centerRight,
                        child: Text(
                          "Change",
                          style: TextStyle(color: CustomColors.darkPinkColor),
                        )),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Column(
                    children: [
                      Row(
                        children: [
                          InkWell(
                            onTap: () {
                              setState(() {
                                selected = 1;
                              });
                            },
                            child: Container(
                              padding: EdgeInsets.only(left: 10, right: 13),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: selected == 1
                                          ? CustomColors.darkPinkColor
                                          : Colors.grey),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10))),
                              child: Row(
                                children: [
                                  Icon(Icons.home),
                                  Text(" Work"),
                                ],
                              ),
                            ),
                          ),
                          Spacer(),
                          InkWell(
                            onTap: () {
                              setState(() {
                                selected = 2;
                              });
                            },
                            child: Container(
                              padding: EdgeInsets.only(left: 10, right: 13),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: selected == 2
                                          ? CustomColors.darkPinkColor
                                          : Colors.grey),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10))),
                              child: Row(
                                children: [
                                  Icon(Icons.home_outlined),
                                  Text(" Home"),
                                ],
                              ),
                            ),
                          ),
                          Spacer(),
                          InkWell(
                            onTap: () {
                              setState(() {
                                selected = 3;
                              });
                            },
                            child: Container(
                              padding: EdgeInsets.only(left: 10, right: 13),
                              decoration: BoxDecoration(
                                  border: Border.all(
                                      color: selected == 3
                                          ? CustomColors.darkPinkColor
                                          : Colors.grey),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10))),
                              child: Row(
                                children: [
                                  Icon(Icons.star_border_outlined),
                                  Text(" Other"),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 4,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        left: 0, top: 10, right: 10, bottom: 10),
                    child: InkWell(
                      child: Container(
                        alignment: Alignment.center,
                        padding: EdgeInsets.only(left: 15, right: 15),
                        height: 50,
                        width: MediaQuery.of(context).size.width,
                        decoration: new BoxDecoration(
                          gradient: LinearGradient(
                              begin: const Alignment(0.0, -2.0),
                              end: const Alignment(0.0, 0.3),
                              colors: [
                                Colors.white,
                                CustomColors.darkPinkColor
                              ]),
                          shape: BoxShape.rectangle,
                          color: CustomColors.darkPinkColor,
                          borderRadius: BorderRadius.all(Radius.circular(30)),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.pink[100],
                              spreadRadius: 5,
                              blurRadius: 10,
                              offset:
                                  Offset(0, 2), // changes position of shadow
                            ),
                          ],
                        ),
                        child: Text(
                          "Confirm Location",
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: "Montserrat",
                              fontWeight: FontWeight.bold,
                              fontSize: 15),
                        ),
                      ),
                      onTap: () {
                        FocusScope.of(context).requestFocus(FocusNode());
                        RedirectTo();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void RedirectTo() async {
    if (strAddress.isNotEmpty) {
      FetchAddressData data = FetchAddressData();
      data.address = strAddress;
      if (selected == 1) {
        data.tags = "Work";
      } else if (selected == 2) {
        data.tags = "Home";
      } else {
        data.tags = "Other";
      }
      data.id = 0;
      data.locality = locality;
      data.latitude = _center.latitude.toString();
      data.longitude = _center.longitude.toString();
      savedData.selectedAddres = data;
      // Navigator.pop(context, data);

      getCheckLocality(context);
    } else {
      Fluttertoast.showToast(msg: "Choose Address");
    }
  }

  LatLng currentPostion;

  Future<Position> locateUser() async {
    return Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high);
  }

  getUserLocation() async {
    CheckPermission.checkLocationPermissionDash(context,this).then((value) async {
      if (value) {
        currentLocation = await locateUser();
        setState(() {
          _center = LatLng(currentLocation.latitude, currentLocation.longitude);
          allMarkers.add(Marker(
              markerId: MarkerId("testtt"),
              draggable: false,
              icon: pinIcon,
              position: _center));
          updateMarkerOnselectedLocation();
        });
        print('center $_center');
      }
    });
  }

  Future<Null> displayPrediction() async {
    CheckPermission.checkLocationPermissionOnly(context).then((value) async {
      if (value) {
        callFrom = 1;
        FetchAddressData result = await Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => CustomSearchScaffold()),
        );
        if (result != null) {
          _center = LatLng(
              double.parse(result.latitude), double.parse(result.longitude));
          updateMarkerOnselectedLocation();
          setState(() {
            strAddress = result.address;
            locality = result.locality;
            print(strAddress);
          });
          // strAddress = address.first.addressLine;
          final GoogleMapController controller = await _controller.future;
          controller.animateCamera(CameraUpdate.newLatLng(_center));
        }
      }else{
        Fluttertoast.showToast(msg: "Location permission not enabled");
      }
    });
  }

  updateMarkerOnselectedLocation() async {
    cPosition = CameraPosition(
      bearing: 2,
      target: LatLng(_center.latitude, _center.longitude),
      zoom: 15,
    );

    final coordinates = new Coordinates(_center.latitude, _center.longitude);
    addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);

    strAddress = addresses.first.addressLine;
    locality = addresses.first.postalCode;
    print(strAddress);
    print("locality$locality");
    setState(() {});
  }

  Future<void> getCheckLocality(BuildContext context) async {
    userData = await AppSharedPreferences.instance.getUserDetails();
    EasyLoading.show();
    String strUrl = WebApiConstaint.URL_CHECKLOCALITY +
        "locality=" +
        savedData.selectedAddres.locality;
    provider
        .requestGetForApi(
            context, strUrl, userData.sessionKey, userData.authorization)
        .then((responce) {
      print(strUrl);
      EasyLoading.dismiss();

      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            if (responce.data["errorCode"] == 0) {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return ProductDetails();
                  },
                ),
              );
            } else {
              savedData.selectedAddres.address = "Select Address";
              savedData.selectedAddres.tags = "Select Address";
              savedData.selectedAddres.locality = "";
              savedData.selectedAddres.id = 0;
              setState(() {});
              // Fluttertoast.showToast(msg: responce.data["message"]);
              AlertDialogNew.show_dialog(
                  context, null, responce.data["message"], "Ok", "");
            }
          } else {
            if (responce.data["authenticate"] == false) {
              AppSharedPreferences.instance.setLogin(false);
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                  ModalRoute.withName("/Home"));
            }
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
          }
        } catch (_EX) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }

  @override
  Future<void> permissionCheck(bool isGranted) async {
    // TODO: implement permissionCheck
    if (isGranted) {
      currentLocation = await locateUser();
      setState(() {
        _center = LatLng(currentLocation.latitude, currentLocation.longitude);
        allMarkers.add(Marker(
            markerId: MarkerId("testtt"),
            draggable: false,
            icon: pinIcon,
            position: _center));
        updateMarkerOnselectedLocation();
      });
      print('center $_center');
    }else{
      Navigator.pop(context);
    }
  }
}
