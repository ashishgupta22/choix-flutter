// @dart=2.9

import 'package:choix_customer/Data/Notification.dart';
import 'package:choix_customer/Data/UserData.dart';
import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/RetryClickListner.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:choix_customer/widgets/notification_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'NoDataFound.dart';
import 'login_screen.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreen createState() => _NotificationScreen();
}

class _NotificationScreen extends State<NotificationScreen> implements RetryClickListner,NotificationSelectedListner{
  bool noRecordFound = false;
  UserData userData;
  ApiProvider provider = ApiProvider();

  List<NotificationData> dataList = [];
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    CallAPINotification();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Notification"),
        backgroundColor: CustomColors.darkPinkColor,
        automaticallyImplyLeading: false,
      ),
      body: RefreshIndicator(
          color: CustomColors.darkPinkColor,
          onRefresh: () {
            return CallAPINotification();
          },
          child:  SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            child:noRecordFound ? NoDataFound(this) : Container(
              margin: EdgeInsets.only(left: 10,right: 10,top: 10,bottom: 90),
              child:  ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemCount: dataList.length,
                  itemBuilder: (context, i) {
                    return NotificationWidget(dataList[i],this);
                  }),
            ),
          )),
    );
  }


  Future<void>CallAPINotification() async {
    userData = await AppSharedPreferences.instance.getUserDetails();
    EasyLoading.show();
    provider
        .requestGetForApi(context, WebApiConstaint.URL_NOTIFICATION,
        userData.sessionKey, userData.authorization)
        .then((responce) {
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            var resposdata = NotificationBean.fromJson(responce.data);
            if (resposdata.data.isNotEmpty) {
              noRecordFound = false;
              setState(() {
                dataList = resposdata.data;
              });
            }else{
              noRecordFound = true;
              setState(() {
              });
            }
          } else {
            if (responce.data["authenticate"] == false) {
              AppSharedPreferences.instance.setLogin(false);
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginScreen()));
            } else {
              noRecordFound = true;
              setState(() {});
            }
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
          }
        } catch (_) {
          if(mounted) {
            noRecordFound = true;
            setState(() {});
            ToastUtils.showCustomToast(
                context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
          }
        }
      }else {
        noRecordFound = true;
        setState(() {});
      }
    });
  }
  Future<void> deleteNotification(BuildContext context, int id, int is_admin) async {
    userData = await AppSharedPreferences.instance.getUserDetails();
    EasyLoading.show();
    String strUrl = WebApiConstaint.URL_DELETE_NOTIFCATION +
        "?id=" +
        id.toString() + "&is_admin=$is_admin";
    provider
        .requestGetForApi(
        context, strUrl, userData.sessionKey, userData.authorization)
        .then((responce) {
      print(strUrl);
      EasyLoading.dismiss();

      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            CallAPINotification();
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
            if (responce.data["authenticate"] == false) {
              AppSharedPreferences.instance.setLogin(false);
              Navigator.pushAndRemoveUntil(context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                  ModalRoute.withName("/Home"));
            }
          }
        } catch (_EX) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }
  @override
  void onClickListner() {
    CallAPINotification();
  }

  @override
  void isSelected(int type, NotificationData data) {
    // TODO: implement isSelected
    // 1 = Deleted
    deleteNotification(context, data.id,data.is_admin);
  }
}
abstract class NotificationSelectedListner{
  void isSelected(int type, NotificationData data);
}
