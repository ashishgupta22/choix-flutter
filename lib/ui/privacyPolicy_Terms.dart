// @dart=2.9

import 'package:choix_customer/Utill/custom_color.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';


class Web_View extends StatelessWidget {
  String url;
  String title;
  Web_View(this.url, this.title);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(title),
        backgroundColor: CustomColors.darkPinkColor,
        leading: InkWell(
          onTap: () => Navigator.of(context).pop(true),
          child: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
      ),
      body: WebView(
        initialUrl: url,
        javascriptMode: JavascriptMode.unrestricted,
      ),
    );
  }
}