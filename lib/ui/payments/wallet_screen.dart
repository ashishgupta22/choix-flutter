// @dart=2.9
import 'dart:async';

import 'package:cashfree_pg/cashfree_pg.dart';
import 'package:choix_customer/Data/BannerItem.dart';
import 'package:choix_customer/Data/UserData.dart';
import 'package:choix_customer/Data/WalletOrderID.dart';
import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:choix_customer/ui/balance_history.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';

class WalletScreen extends StatefulWidget {
  @override
  StateWallet createState() => StateWallet();
}

class StateWallet extends State<WalletScreen> {
  UserData userData;
  ApiProvider provider = ApiProvider();
  TextEditingController txtAmpountController;
  final controller = PageController(initialPage: 1);
  FocusNode myFocusNode = new FocusNode();
  bool isAmountValidate = true;

  var walletBalance = "0.00";

  List<BannerData> bannerList = [];

  String errorMsg = "Can't be empty";

  String vat = "0.0";

  String vatMessage = "";
  Pattern pattern = r'[0-9]$';
  RegExp regex = new RegExp(r'[0-9]$');

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    new RegExp(pattern);
    txtAmpountController = TextEditingController(text: "");
    getUserData();
    CallAPIBanner();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        if (EasyLoading.isShow) {
          EasyLoading.dismiss();
        }
        Navigator.pop(context);
      },
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: CustomColors.darkPinkColor,
          title: Text("Wallet"),
          actions: [
            InkWell(
              onTap: () {
                if (EasyLoading.isShow) {
                  EasyLoading.dismiss();
                }
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => BalanceHistory(walletBalance)),
                );
              },
              child: Icon(
                Icons.history,
                color: Colors.white,
              ),
            ),
            SizedBox(
              width: 10,
            )
          ],
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Directionality(
                    textDirection: TextDirection.ltr,
                    child: SizedBox(
                      height: 120,
                      child: PageView(
                        controller: controller,
                        children: [
                          ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, index) {
                              return Container(
                                width: MediaQuery.of(context).size.width -
                                    (bannerList.length > 1 ? 70 : 20),
                                margin: EdgeInsets.all(8),
                                padding: EdgeInsets.only(
                                    left: 2, bottom: 2, top: 2, right: 2),
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    color: CustomColors.offerCartColor),
                                child: Row(
                                  children: [
                                    Container(
                                      width: MediaQuery.of(context).size.width -
                                          (bannerList.length > 1 ? 74 : 24),
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                          image: NetworkImage(
                                              bannerList[index].image),
                                          fit: BoxFit.cover,
                                        ),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)),
                                        border: Border.all(
                                          color: Colors.white,
                                          width: 1.0,
                                        ),
                                      ),
                                    ),
                                    // SizedBox(
                                    //   height: 0,
                                    //   width: 15,
                                    // ),
                                    // Expanded(
                                    //     child: Column(
                                    //   children: [
                                    //     Align(
                                    //       alignment: Alignment.topRight,
                                    //       child: Container(
                                    //         padding: EdgeInsets.only(
                                    //             left: 6,
                                    //             right: 8,
                                    //             top: 2,
                                    //             bottom: 2),
                                    //         decoration: BoxDecoration(
                                    //             borderRadius: BorderRadius.only(
                                    //                 bottomLeft:
                                    //                     Radius.circular(10),
                                    //                 topLeft: Radius.circular(10)),
                                    //             color:
                                    //                 CustomColors.darkPinkColor),
                                    //         child: Text("Wallet",
                                    //             style: TextStyle(
                                    //                 color: Colors.white,
                                    //                 fontSize: 12,
                                    //                 fontWeight:
                                    //                     FontWeight.normal)),
                                    //       ),
                                    //     ),
                                    //     SizedBox(
                                    //       height: 10,
                                    //       width: 1,
                                    //     ),
                                    //     Text(bannerList[index].name,
                                    //         style: TextStyle(
                                    //             color: Colors.black,
                                    //             fontSize: 12,
                                    //             fontWeight: FontWeight.normal))
                                    //   ],
                                    // )),
                                  ],
                                ),
                              );
                            },
                            itemCount: bannerList.length,
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.only(
                        left: 20, right: 20, top: 10, bottom: 10),
                    child: Card(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                left: 20, right: 20, top: 20, bottom: 10),
                            child: Text("Balance",
                                style: TextStyle(
                                    color: CustomColors.darkPinkColor,
                                    fontSize: 12,
                                    fontWeight: FontWeight.normal)),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: 20, right: 20, top: 10, bottom: 10),
                            child: Text("₹ $walletBalance",
                                style: TextStyle(
                                    color: CustomColors.darkPinkColor,
                                    fontSize: 34,
                                    fontWeight: FontWeight.w600)),
                          ),
                          Container(
                            height: 1,
                            width: MediaQuery.of(context).size.width,
                            color: Colors.grey,
                          ),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 20, right: 20, top: 5, bottom: 10),
                              child: Text(
                                "Please maintain a minimum balance of ₹20 to be able to place order",
                                style: TextStyle(
                                    color: CustomColors.darkPinkColor,
                                    fontSize: 10),
                              )),
                          Padding(
                            padding:
                                EdgeInsets.only(left: 20, right: 20, top: 20),
                            child: Text("Topup Wallet",
                                style: TextStyle(
                                    color: CustomColors.blackColor,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500)),
                          ),
                          Padding(
                            padding: EdgeInsets.only(
                                left: 20, right: 20, top: 20, bottom: 0),
                            // child:  CustomTextFiled(true,null,txtAmpountController,isAmountValidate,'Enter amount',"",TextInputType.number,TextInputAction.done),
                            child: TextField(
                              autofocus: false,
                              maxLength: 4,
                              controller: txtAmpountController,
                              keyboardType: TextInputType.number,
                              textInputAction: TextInputAction.done,
                              showCursor: true,
                              onChanged: (value) {
                                calculateGST();
                              },
                              //style: TextStyle(color: CustomColors.side_bar_text),
                              decoration: InputDecoration(
                                counter: Offstage(),
                                filled: true,
                                fillColor: Colors.white,
                                hintText: 'Enter amount',
                                errorText: !isAmountValidate ? errorMsg : null,
                                prefixText: "₹ ",
                                //hintStyle: TextStyle(color: CustomColors.side_bar_text),
                                contentPadding: const EdgeInsets.only(
                                    left: 20.0,
                                    bottom: 8.0,
                                    top: 8.0,
                                    right: 20),
                                focusedBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.grey[300]),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),

                                enabledBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.grey[200]),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                errorBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.grey[200]),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                focusedErrorBorder: OutlineInputBorder(
                                    borderSide:
                                        BorderSide(color: Colors.grey[200]),
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                              ),
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 20, right: 20, top: 0, bottom: 10),
                              child: Text(
                                vatMessage,
                                style: TextStyle(
                                    color: CustomColors.darkPinkColor,
                                    fontSize: 10),
                              )),
                          Padding(
                            padding: EdgeInsets.only(
                                left: 20, right: 20, bottom: 20),
                            child: Text("Recommended",
                                style: TextStyle(
                                    color: Colors.black54,
                                    fontSize: 14,
                                    fontWeight: FontWeight.w400)),
                          ),
                          Padding(
                              padding: EdgeInsets.only(
                                  left: 20, right: 20, bottom: 20),
                              child: Row(
                                children: [
                                  InkWell(
                                    child: Container(
                                      padding: EdgeInsets.only(
                                          left: 10,
                                          right: 10,
                                          top: 5,
                                          bottom: 5),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5)),
                                          border: Border.all(
                                            color: CustomColors.darkPinkColor,
                                            width: 1,
                                          )),
                                      child: Text("₹ 50",
                                          style: TextStyle(
                                              color: CustomColors.darkPinkColor,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500)),
                                    ),
                                    onTap: () {
                                      setState(() {
                                        txtAmpountController.text = "50.00";
                                        txtAmpountController.selection =
                                            TextSelection.fromPosition(
                                                TextPosition(
                                                    offset: txtAmpountController
                                                        .text.length));
                                      });
                                      calculateGST();
                                    },
                                  ),
                                  SizedBox(
                                    width: 15,
                                    height: 0,
                                  ),
                                  InkWell(
                                    child: Container(
                                      padding: EdgeInsets.only(
                                          left: 10,
                                          right: 10,
                                          top: 5,
                                          bottom: 5),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(3)),
                                          border: Border.all(
                                            color: CustomColors.darkPinkColor,
                                            width: 1,
                                          )),
                                      child: Text("₹ 100",
                                          style: TextStyle(
                                              color: CustomColors.darkPinkColor,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500)),
                                    ),
                                    onTap: () {
                                      setState(() {
                                        txtAmpountController..text = "100.00";
                                        txtAmpountController.selection =
                                            TextSelection.fromPosition(
                                                TextPosition(
                                                    offset: txtAmpountController
                                                        .text.length));
                                      });
                                      calculateGST();
                                    },
                                  ),
                                  SizedBox(
                                    width: 15,
                                    height: 0,
                                  ),
                                  InkWell(
                                    child: Container(
                                      padding: EdgeInsets.only(
                                          left: 10,
                                          right: 10,
                                          top: 5,
                                          bottom: 5),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(3)),
                                          border: Border.all(
                                            color: CustomColors.darkPinkColor,
                                            width: 1,
                                          )),
                                      child: Text("₹ 200",
                                          style: TextStyle(
                                              color: CustomColors.darkPinkColor,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500)),
                                    ),
                                    onTap: () {
                                      setState(() {
                                        txtAmpountController..text = "200.00";

                                        txtAmpountController.selection =
                                            TextSelection.fromPosition(
                                                TextPosition(
                                                    offset: txtAmpountController
                                                        .text.length));
                                      });
                                      calculateGST();
                                    },
                                  ),
                                  SizedBox(
                                    width: 15,
                                    height: 0,
                                  ),
                                  InkWell(
                                    child: Container(
                                      padding: EdgeInsets.only(
                                          left: 10,
                                          right: 10,
                                          top: 5,
                                          bottom: 5),
                                      decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(3)),
                                          border: Border.all(
                                            color: CustomColors.darkPinkColor,
                                            width: 1,
                                          )),
                                      child: Text("₹ 500",
                                          style: TextStyle(
                                              color: CustomColors.darkPinkColor,
                                              fontSize: 14,
                                              fontWeight: FontWeight.w500)),
                                    ),
                                    onTap: () {
                                      setState(() {
                                        txtAmpountController..text = "500.00";

                                        txtAmpountController.selection =
                                            TextSelection.fromPosition(
                                                TextPosition(
                                                    offset: txtAmpountController
                                                        .text.length));
                                      });
                                      calculateGST();
                                    },
                                  ),
                                ],
                              )),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 80,
                  )
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: InkWell(
                onTap: () => CallAPIOrderID(),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 60,
                  padding:
                      EdgeInsets.only(left: 20, top: 20, right: 20, bottom: 20),
                  color: CustomColors.darkPinkColor,
                  alignment: Alignment.center,
                  child: Text("TOPUP WALLET",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                          fontWeight: FontWeight.w500)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void payNow(String token, String orderid, String amount, String stage1,
      String appId1, String notifyUrl) {
//Replace with actual values
    String stage = stage1;
    String orderId = orderid;
    String orderAmount = amount;
    String tokenData = token;
    String customerName = userData.name;
    String orderNote = "Recharge Wallet";
    String orderCurrency = "INR";
    String appId = appId1;
    String customerPhone = userData.mobileNo;
    String customerEmail = userData.email;
    Map<String, dynamic> inputParams = {
      "orderId": orderId,
      "orderAmount": orderAmount,
      "customerName": customerName,
      "orderNote": orderNote,
      "orderCurrency": orderCurrency,
      "appId": appId,
      "customerPhone": customerPhone,
      "customerEmail": customerEmail,
      "stage": stage,
      "notifyUrl": notifyUrl,
      "tokenData": tokenData,
      "color1": "#F40057",
      "color2": "#ffffff" //ffffff
    };
    CashfreePGSDK.doPayment(inputParams).then((value) {
      value?.forEach((key, value) {
        print("$key : $value");
        if (key == "txStatus") {
          if (value == "SUCCESS") {
            setState(() {
              txtAmpountController..text = "";
              vatMessage = "";
            });
            Fluttertoast.showToast(msg: "Transaction Success");
            CallAPIWalletBalance(1);
          } else {
            Fluttertoast.showToast(msg: value);
          }
        }
      });
    });
  }

  Future<void> getUserData() async {
    userData = await AppSharedPreferences.instance.getUserDetails();
  }

  CallAPIOrderID() async {
    Pattern pattern = r'[0-9]$';
    RegExp regex = new RegExp(pattern);

    if (txtAmpountController.text == "") {
      setState(() {
        isAmountValidate = false;
        errorMsg = "Can't be empty";
      });
    } else if (!regex.hasMatch(txtAmpountController.text)) {
      setState(() {
        errorMsg = "Not Valid Amount...";
        isAmountValidate = false;
      });
    } else if (double.parse(txtAmpountController.text) <= 0) {
      setState(() {
        isAmountValidate = false;
        errorMsg = "Not Valid Amount...";
      });
    } else {
      userData = await AppSharedPreferences.instance.getUserDetails();
      EasyLoading.show();
      provider
          .requestGetForApi(
              context,
              WebApiConstaint.URL_WALLET_ORDER_ID +
                  "amount=" +
                  txtAmpountController.text,
              userData.sessionKey,
              userData.authorization)
          .then((responce) {
        EasyLoading.dismiss();
        if (responce != null) {
          try {
            if (responce.data["error"] == false) {
              var resposdata = WalletOrderID.fromJson(responce.data);
              print("yes");
              print(resposdata);
              if (resposdata.data != null) {
                payNow(
                    resposdata.data.token,
                    resposdata.data.orderid,
                    resposdata.data.amount.toString(),
                    resposdata.data.stage,
                    resposdata.data.appId,
                    resposdata.data.notifyUrl);
              }
            } else {
              Fluttertoast.showToast(msg: responce.data["message"]);
              print(responce.data["message"]);
            }
          } catch (_) {
            print(_);
            ToastUtils.showCustomToast(
                context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
          }
        }
      });
    }
  }

  CallAPIWalletBalance(int type) async {
    //0 from init method 1 from redirect gatway
    userData = await AppSharedPreferences.instance.getUserDetails();
    EasyLoading.show();
    int timeSecond = 0;
    if (type == 1) timeSecond = 5;
    Timer(
        Duration(seconds: timeSecond),
        () => {
              provider
                  .requestGetForApi(context, WebApiConstaint.URL_WALLET_BALANCE,
                      userData.sessionKey, userData.authorization)
                  .then((responce) {
                EasyLoading.dismiss();
                if (responce != null) {
                  try {
                    if (responce.data["error"] == false) {
                      setState(() {
                        walletBalance =
                            responce.data["data"]["balance"].toString();
                        vat = responce.data["data"]["vat"].toString();
                      });
                    } else {
                      Fluttertoast.showToast(msg: responce.data["message"]);
                      print(responce.data["message"]);
                    }
                  } catch (_) {
                    ToastUtils.showCustomToast(
                        context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
                  }
                }
              })
            });
  }

  CallAPIBanner() async {
    userData = await AppSharedPreferences.instance.getUserDetails();
    EasyLoading.show();
    provider
        .requestGetForApi(context, WebApiConstaint.URL_BANNER_LIST + "Wallet",
            userData.sessionKey, userData.authorization)
        .then((responce) {
      CallAPIWalletBalance(0);
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            var resposdata = BannerItem.fromJson(responce.data);
            if (resposdata.data.length > 0) {
              bannerList = resposdata.data;
            }
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }

  void calculateGST() {
    isAmountValidate = true;
    if (txtAmpountController.text.length > 0) {
      if (regex.hasMatch(txtAmpountController.text)) {
        if (double.parse(txtAmpountController.text) > 0) {
          // String vatNew = vat;
          // if(vatNew == null || vatNew.isEmpty){
          //   vatNew = "18";
          // }
          if (vat.isNotEmpty && vat.length > 0) {
            double vat1 = double.parse(vat);
            double amount = double.parse(txtAmpountController.text.toString());
            double total = (amount * vat1 / 100) + amount;
            vatMessage = "  ₹${total.toStringAsFixed(2)} inclusive GST";
          }
        } else {
          vatMessage = "";
        }
      } else {
        vatMessage = "";
      }
    } else {
      vatMessage = "";
    }
    setState(() {
      vatMessage;
      isAmountValidate;
    });
  }
}
