// @dart=2.9

import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class StoreLocationMap extends StatefulWidget {

  @override
  _StoreLocationMapState createState() => _StoreLocationMapState();
}

class _StoreLocationMapState extends State<StoreLocationMap> {
  final List<Marker> _markers = [];
  Position currentLocation;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    locateUser();
  }
  Future<Position> locateUser() async {
    currentLocation = await Geolocator.getCurrentPosition(
        desiredAccuracy: LocationAccuracy.high).then((value) {
      setState(() {
        value;
        print("location ${value.longitude}");
      });
    });

    // return Geolocator.getCurrentPosition(
    //     desiredAccuracy: LocationAccuracy.high);
  }
  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
       currentLocation != null ? GoogleMap(
          initialCameraPosition: CameraPosition(
              target: LatLng(
                  currentLocation.latitude, currentLocation.longitude),
              zoom: 14),
          markers: _markers.toSet(),
          onMapCreated: (controller) {
            final marker = Marker(
              markerId: MarkerId('0'),
              position: LatLng(
                  currentLocation.latitude, currentLocation.longitude),

            );

            _markers.add(marker);
          },
          onCameraMove: (position) {
            setState(() {
              _markers.first =
                  _markers.first.copyWith(positionParam: position.target);
            });
          },
        ) : SizedBox(),
        Image.asset(
          'assets/images/pin.png',
          frameBuilder: (context, child, frame, wasSynchronouslyLoaded) {
            return Transform.translate(
              offset: const Offset(8, -37),
              child: child,
            );
          },
        )
      ],
    );
  }
}