// @dart=2.9
import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:choix_customer/ui/otp_pin.dart';
import 'package:choix_customer/ui/privacyPolicy_Terms.dart';
import 'package:choix_customer/widgets/CustomRegisterTextFiled.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';

class RegisterScreen extends StatefulWidget {
  String strName, strEmail;

  RegisterScreen(this.strName, this.strEmail) : super();

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  TextEditingController txtNameController = TextEditingController();
  TextEditingController txtEmailController = TextEditingController();
  TextEditingController txtPhoneController = TextEditingController();
  TextEditingController txtReferralController = TextEditingController();

  FocusNode myFocusNode = new FocusNode();
  ApiProvider provider = ApiProvider();
  bool isNameValidate = true,
      isEmailValidate = true,
      isPhnValidate = true,
      isReferral = true;
  var value;
  bool newValue = false;

  var onChanged;

  var isCheckedTerms = false;

  bool ifReferral = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    txtNameController.addListener(_printLatestValue);
    txtNameController.text = widget.strName;
    txtEmailController.text = widget.strEmail;
    setState(() {});
    CheckReferralAPI(context);
  }

  @override
  void dispose() {
    txtNameController.dispose();
    super.dispose();
  }

  void _printLatestValue() {
    print('Second text field: ${txtNameController.text}');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        toolbarHeight: 0,
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Stack(children: [
            Column(
              children: [
                Padding(
                    padding: EdgeInsets.only(left: 0, bottom: 0, right: 0),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      color: Colors.white,
                      child: Image.asset(
                        "assets/images/top_bg.png",
                        fit: BoxFit.fitWidth,
                      ),
                    )),
                Container(
                  margin:
                      EdgeInsets.only(top: 25, left: 30, right: 30, bottom: 30),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      // SizedBox(height: MediaQuery
                      //     .of(context)
                      //     .size
                      //     .height * 0.08, width: 0,),

                      Text(
                        "Register",
                        style: TextStyle(
                            fontSize: 50,
                            fontWeight: FontWeight.w600,
                            fontFamily: "Nueva"),
                      ),

                      //SizedBox(height: 5,width: 0,),
                      Text(
                        "Please create your account",
                        style: TextStyle(
                            fontSize: 14,
                            fontFamily: "Montserrat",
                            fontWeight: FontWeight.w300),
                      ),
                      SizedBox(
                        height: 50,
                        width: 0,
                      ),
                      CustomRegisterTextFiled(
                          true,
                          myFocusNode,
                          txtNameController,
                          isNameValidate,
                          'Enter your name',
                          "Name",
                          Icons.person_outline,
                          TextInputType.name,
                          TextInputAction.next,
                          50,
                          "Can't be empty"),
                      // TextField(
                      //
                      //   focusNode: myFocusNode,
                      //   autofocus: true,
                      //   controller: txtNameController,
                      //   keyboardType: TextInputType.name,
                      //   textInputAction: TextInputAction.next,
                      //   //style: TextStyle(color: CustomColors.side_bar_text),
                      //
                      //
                      //   decoration: InputDecoration(
                      //     suffixIcon: IconButton(
                      //       icon: Icon(Icons.drive_file_rename_outline),
                      //     ),
                      //     filled: true,
                      //     fillColor: Colors.white,
                      //     hintText: ,
                      //     labelText:,
                      //     labelStyle: TextStyle(
                      //         color: myFocusNode.hasFocus
                      //             ? Colors.black
                      //             : Colors.black,
                      //         fontFamily: "Montserrat"
                      //
                      //     ),
                      //     //hintStyle: TextStyle(color: CustomColors.side_bar_text),
                      //     contentPadding:
                      //     const EdgeInsets.only(
                      //         left: 20.0, bottom: 8.0, top: 8.0, right: 20),
                      //     focusedBorder: OutlineInputBorder(
                      //         borderSide: BorderSide(color: Colors.grey[300]),
                      //         borderRadius: BorderRadius.all(Radius.circular(
                      //             30))
                      //     ),
                      //
                      //     errorText: !isNameValidate
                      //         ? "Can\'t be empty" : null,
                      //     enabledBorder: OutlineInputBorder(
                      //         borderSide: BorderSide(color: Colors.grey[200]),
                      //         borderRadius: BorderRadius.all(Radius.circular(
                      //             30))
                      //     ),
                      //   ),
                      // ),

                      SizedBox(
                        height: 25,
                      ),
                      CustomRegisterTextFiled(
                          true,
                          myFocusNode,
                          txtEmailController,
                          isEmailValidate,
                          'Enter your email',
                          "Email",
                          Icons.email_outlined,
                          TextInputType.emailAddress,
                          TextInputAction.next,
                          100,
                          "Invalid Email"),
                      // TextField(
                      //   // focusNode: myFocusNode,
                      //   autofocus: true,
                      //   controller: txtEmailController,
                      //   //..text = widget.strEmail != "" ? widget.strEmail.trim() : "",
                      //   keyboardType: TextInputType.emailAddress,
                      //   textInputAction: TextInputAction.next,
                      //   //style: TextStyle(color: CustomColors.side_bar_text),
                      //   decoration: InputDecoration(
                      //     suffixIcon: IconButton(
                      //       icon: Icon(Icons.email_outlined),
                      //     ),
                      //     filled: true,
                      //     fillColor: Colors.white,
                      //     hintText: 'Enter your email',
                      //     labelText: "Email",
                      //     labelStyle: TextStyle(
                      //         color: myFocusNode.hasFocus
                      //             ? Colors.black
                      //             : Colors.black,
                      //         fontFamily: "Montserrat"
                      //
                      //     ),
                      //     //hintStyle: TextStyle(color: CustomColors.side_bar_text),
                      //     contentPadding:
                      //     const EdgeInsets.only(
                      //         left: 20.0, bottom: 8.0, top: 8.0, right: 20),
                      //     focusedBorder: OutlineInputBorder(
                      //         borderSide: BorderSide(color: Colors.grey[300]),
                      //         borderRadius: BorderRadius.all(Radius.circular(
                      //             30))
                      //     ),
                      //
                      //     errorText: !isEmailValidate
                      //         ? "Invalid email" : null,
                      //     enabledBorder: OutlineInputBorder(
                      //         borderSide: BorderSide(color: Colors.grey[200]),
                      //         borderRadius: BorderRadius.all(Radius.circular(
                      //             30))
                      //     ),
                      //   ),
                      // ),
                      SizedBox(
                        height: 25,
                      ),
                      CustomRegisterTextFiled(
                          true,
                          myFocusNode,
                          txtPhoneController,
                          isPhnValidate,
                          'Enter your number',
                          'Phone Number',
                          Icons.phone_outlined,
                          TextInputType.number,
                          TextInputAction.next,
                          10,
                          "Phone number can't be empty or less than 10 digits"),
                      // TextField(
                      //   //focusNode: myFocusNode,
                      //   autofocus: true,
                      //   controller: txtPhoneController,
                      //   maxLength: 10,
                      //   keyboardType: TextInputType.number,
                      //   textInputAction: TextInputAction.next,
                      //   //style: TextStyle(color: CustomColors.side_bar_text),
                      //   decoration: InputDecoration(
                      //     suffixIcon: IconButton(
                      //       icon: Icon(Icons.phone_outlined),
                      //     ),
                      //     counter: Offstage(),
                      //     filled: true,
                      //     fillColor: Colors.white,
                      //     hintText: 'Enter your number',
                      //     labelText: 'Phone Number',
                      //     labelStyle: TextStyle(
                      //         color: myFocusNode.hasFocus
                      //             ? Colors.black
                      //             : Colors.black,
                      //         fontFamily: "Montserrat"
                      //
                      //     ),
                      //
                      //     //hintStyle: TextStyle(color: CustomColors.side_bar_text),
                      //     contentPadding:
                      //     const EdgeInsets.only(
                      //         left: 20.0, bottom: 8.0, top: 8.0, right: 20),
                      //     focusedBorder: OutlineInputBorder(
                      //         borderSide: BorderSide(color: Colors.grey[300]),
                      //         borderRadius: BorderRadius.all(Radius.circular(
                      //             30))
                      //     ),
                      //
                      //     errorText: !isPhnValidate
                      //         ? "Phone number can\'t be empty or less than 10 digits"
                      //         : null,
                      //     enabledBorder: OutlineInputBorder(
                      //         borderSide: BorderSide(color: Colors.grey[200]),
                      //         borderRadius: BorderRadius.all(Radius.circular(
                      //             30))
                      //     ),
                      //   ),
                      // ),
                      SizedBox(
                        height: 25,
                      ),
                      ifReferral ?
                      CustomRegisterTextFiled(
                          true,
                          myFocusNode,
                          txtReferralController,
                          true,
                          'Enter your code',
                          'Referral code',
                          Icons.remove_red_eye_outlined,
                          TextInputType.text,
                          TextInputAction.done,
                          20,
                          ""):SizedBox(),
                      // TextField(
                      //   // focusNode: myFocusNode,
                      //   autofocus: true,
                      //   controller: txtReferralController,
                      //   // keyboardType: TextInputType.de,
                      //   textInputAction: TextInputAction.done,
                      //   //style: TextStyle(color: CustomColors.side_bar_text),
                      //   decoration: InputDecoration(
                      //     suffixIcon: IconButton(
                      //       icon: Icon(Icons.remove_red_eye_outlined),
                      //     ),
                      //     filled: true,
                      //     fillColor: Colors.white,
                      //     hintText: ,
                      //     labelText: ,
                      //     labelStyle: TextStyle(
                      //         color: myFocusNode.hasFocus
                      //             ? Colors.black
                      //             : Colors.black,
                      //         fontFamily: "Montserrat"
                      //
                      //     ),
                      //     //hintStyle: TextStyle(color: CustomColors.side_bar_text),
                      //     contentPadding:
                      //     const EdgeInsets.only(
                      //         left: 20.0, bottom: 8.0, top: 8.0, right: 20),
                      //     focusedBorder: OutlineInputBorder(
                      //         borderSide: BorderSide(color: Colors.grey[300]),
                      //         borderRadius: BorderRadius.all(Radius.circular(
                      //             30))
                      //     ),
                      //
                      //     enabledBorder: OutlineInputBorder(
                      //         borderSide: BorderSide(color: Colors.grey[200]),
                      //         borderRadius: BorderRadius.all(Radius.circular(
                      //             30))
                      //     ),
                      //   ),
                      // ),
                      Column(
                        children: [
                          // Checkbox(
                          //   value: isCheckedTerms,
                          //   onChanged: (bool value) {},
                          // )
                        ],
                      ),
                      SizedBox(
                        height: 15,
                        width: 0,
                      ),
                      Row(
                        children: [
                          Checkbox(
                            value: newValue,
                            activeColor: CustomColors.darkPinkColor,
                            onChanged: (Value) {
                              setState(() {
                                newValue = Value;
                              });
                            },
                          ),
                          Text("Accept ",style: TextStyle(fontSize: 10),),
                          InkWell(
                            onTap: (){
                              redirectURL(
                                  WebApiConstaint.TERMS_URL,"Terms & Conditions");
                            },
                            child: Text(
                              " Terms & Conditions",
                              style: TextStyle(
                                color: Colors.blue,
                                fontSize: 10
                              ),
                            ),
                          ),
                          Text(" and",style: TextStyle(fontSize: 10),),
                          InkWell(
                            onTap: (){
                              redirectURL(
                                    WebApiConstaint.POLICY_URL,"Privacy Policy");
                            },
                            child: Text(
                              " Privacy Policy",
                              style: TextStyle(
                                color: Colors.blue,
                                  fontSize: 10
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 40,
                        width: 0,
                      ),
                      InkWell(
                        child: Container(
                          alignment: Alignment.center,
                          padding: EdgeInsets.only(left: 15, right: 15),
                          height: 50,
                          width: MediaQuery.of(context).size.width,
                          decoration: new BoxDecoration(
                            gradient: LinearGradient(
                                begin: const Alignment(0.0, -2.0),
                                end: const Alignment(0.0, 0.3),
                                colors: [
                                  Colors.white,
                                  CustomColors.darkPinkColor
                                ]),
                            shape: BoxShape.rectangle,
                            color: CustomColors.darkPinkColor,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            boxShadow: [
                              BoxShadow(
                                color: Colors.pink[100],
                                spreadRadius: 5,
                                blurRadius: 10,
                                offset:
                                    Offset(0, 2), // changes position of shadow
                              ),
                            ],
                          ),
                          child: Text(
                            "Register",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                        onTap: () {
                          setState(() {
                            isPhnValidate = true;
                            isNameValidate = true;
                            isEmailValidate = true;
                            setState(() {
                              isPhnValidate = true;
                              isNameValidate = true;
                              isEmailValidate = true;
                              if (txtNameController.text == "") {
                                isNameValidate = false;
                              } else if (txtEmailController.text == "" ||
                                  !EmailValidator.validate(
                                      txtEmailController.text)) {
                                isEmailValidate = false;
                              } else if (txtPhoneController.text == "" ||
                                  txtPhoneController.text.length < 10) {
                                isPhnValidate = false;
                              } else {
                                if (newValue) {
                                  FocusScope.of(context)
                                      .requestFocus(FocusNode());
                                  registerAPI(context);
                                } else {
                                  Fluttertoast.showToast(
                                      msg:
                                          'Please Accept Our Terms & Condition and Privacy Policy');
                                }
                              }
                            });
                            // if (txtNameController.text == "") {
                            //   isNameValidate = false;
                            // } else if (txtEmailController.text == "" ||
                            //     !EmailValidator.validate(
                            //         txtEmailController.text)) {
                            //   isEmailValidate = false;
                            // } else if (txtPhoneController.text == "" ||
                            //     txtPhoneController.text.length < 10) {
                            //   isPhnValidate = false;
                            // } else {
                            //   FocusScope.of(context).requestFocus(FocusNode());
                            //   registerAPI(context);
                            // }
                          });
                        },
                      ),
                      SizedBox(
                        height: 50,
                        width: 0,
                      ),
                    ],
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: IconButton(
                icon: Icon(Icons.arrow_back_outlined),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ),
          ]),
        ),
      ),
    );
  }

  registerAPI(BuildContext context) {
    EasyLoading.show();
    String strUrl = WebApiConstaint.URL_REGISTER +
        "name=" +
        txtNameController.text +
        "&email=" +
        txtEmailController.text +
        "&mobile_no=" +
        txtPhoneController.text +
        "&referral_code=" +
        txtReferralController.text;
    provider.requestGetForApi(context, strUrl, "", "").then((responce) {
      print(strUrl);
      EasyLoading.dismiss();

      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            Fluttertoast.showToast(msg: responce.data["message"]);
            Navigator.push(context, MaterialPageRoute(
              builder: (context) {
                return OtpPin(txtPhoneController.text);
              },
            ));
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }else {
        ToastUtils.showCustomToast(
            context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
      }
    });
  }
  CheckReferralAPI(BuildContext context) {
    EasyLoading.show();
    String strUrl = WebApiConstaint.URL_REFFERAL;
    provider.requestGetForApi(context, strUrl, "", "").then((responce) {
      print(strUrl);
      EasyLoading.dismiss();

      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            ifReferral = responce.data["data"];
            setState(() {
              ifReferral;
            });
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }else {
        ToastUtils.showCustomToast(
            context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
      }
    });
  }

  void redirectURL(String url,String title) async {
    Navigator.push(context, MaterialPageRoute(builder: (context) => Web_View(url, title)));
  }
}
