// @dart=2.9
import 'package:cashfree_pg/cashfree_pg.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PaymentGateway extends StatefulWidget {
  @override
  _AllProduct createState() => _AllProduct();
}

class _AllProduct extends State<PaymentGateway> {


  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
      AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: CustomColors.darkPinkColor,
        title: Text("Payment Gateway"),
      ),
      body: InkWell(
        onTap : (){

          payNow();
        },
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Text("PayNow"),
          ),),
    );
  }
  void payNow() {
//Replace with actual values
    String stage = "TEST";
    String orderId = "Order0004";
    String orderAmount = "100";
    String tokenData = "FW9JCN4MzUIJiOicGbhJCLiQ1VKJiOiAXe0Jye.7l0nIwMGZhhTOiBjZ0ETM2IiOiQHbhN3XiwyN5IDN1MTMzYTM6ICc4VmIsIiUOlkI6ISej5WZyJXdDJXZkJ3biwCMwEjOiQnb19WbBJXZkJ3biwiI0ADMwIXZkJ3TiojIklkclRmcvJye.MNRxvQIXMfJEwlSw-HGpih2rPpAEZd7ImdQoEmN69czDwr2M0D9K_uU5FWO9RCF9xa";
    String customerName = "DMK Kumawat";
    String orderNote = "Order Note";
    String orderCurrency = "INR";
    String appId = "871707ae7aebe0881a62d6de507178";
    String customerPhone = "9999999999";
    String customerEmail = "sample@gmail.com";
    String notifyUrl = "https://test.gocashfree.com/notify";
    Map<String, dynamic> inputParams = {
      "orderId": orderId,
      "orderAmount": orderAmount,
      "customerName": customerName,
      "orderNote": orderNote,
      "orderCurrency": orderCurrency,
      "appId": appId,
      "customerPhone": customerPhone,
      "customerEmail": customerEmail,
      "stage": stage,
      "notifyUrl": notifyUrl,
      "tokenData": tokenData
    };
    CashfreePGSDK.doPayment(inputParams)
        .then((value) => value?.forEach((key, value) {
      print("$key : $value");
      //Do something with the result
    }));
  }
}
