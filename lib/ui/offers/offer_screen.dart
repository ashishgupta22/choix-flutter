// @dart=2.9
import 'package:choix_customer/Data/BannerItem.dart';
import 'package:choix_customer/Data/GetOffers.dart';
import 'package:choix_customer/Data/UserData.dart';
import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../login_screen.dart';

class OfferScreen extends StatefulWidget {
  @override
  OfferState createState() => OfferState();
}

class OfferState extends State<OfferScreen> {
  ApiProvider provider = ApiProvider();
  UserData userData;
  List<GetOffersData> offerList = [];
  final controller = PageController(initialPage: 1);

  List<BannerData> bannerList = [];

  bool isNoOffer = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getOffers(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: CustomColors.darkPinkColor,
          title: Text("All Offer"),
        ),
        body: RefreshIndicator(
            color: CustomColors.darkPinkColor,
            onRefresh: () {
              return getOffers(context);
            },
            child: SingleChildScrollView(
              physics: AlwaysScrollableScrollPhysics(),
              child: Column(
                children: [
                  Directionality(
                    textDirection: TextDirection.ltr,
                    child: SizedBox(
                      height: 120,
                      child: PageView(
                        controller: controller,
                        children: [
                          ListView.builder(
                            scrollDirection: Axis.horizontal,
                            itemBuilder: (context, index) {
                              return Container(
                                width: MediaQuery.of(context).size.width - 70,
                                margin: EdgeInsets.all(8),
                                padding: EdgeInsets.only(
                                    left: 2, bottom: 2, top: 2, right: 2),
                                decoration: BoxDecoration(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10)),
                                    color: CustomColors.offerCartColor),
                                child: Row(
                                  children: [
                                    // Container(
                                    //   width: 70.0,
                                    //   height: 70.0,
                                    //   decoration: BoxDecoration(
                                    //     image: DecorationImage(
                                    //       image:
                                    //           NetworkImage(bannerList[index].image),
                                    //       fit: BoxFit.cover,
                                    //     ),
                                    //     borderRadius: BorderRadius.all(
                                    //         Radius.circular(100.0)),
                                    //     border: Border.all(
                                    //       color: Colors.white,
                                    //       width: 1.0,
                                    //     ),
                                    //   ),
                                    // ),
                                    Container(
                                      width: MediaQuery.of(context).size.width -
                                          74,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                          image: NetworkImage(
                                            bannerList[index].image,
                                          ),
                                          fit: BoxFit.cover,
                                        ),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10.0)),
                                        border: Border.all(
                                          color: Colors.white,
                                          width: 1.0,
                                        ),
                                      ),
                                    ),
                                    // SizedBox(
                                    //   height: 0,
                                    //   width: 15,
                                    // ),
                                    // Expanded(
                                    //     child: Column(
                                    //   children: [
                                    //     Align(
                                    //       alignment: Alignment.topRight,
                                    //       child: Container(
                                    //         padding: EdgeInsets.only(
                                    //             left: 6,
                                    //             right: 8,
                                    //             top: 2,
                                    //             bottom: 2),
                                    //         decoration: BoxDecoration(
                                    //             borderRadius: BorderRadius.only(
                                    //                 bottomLeft: Radius.circular(10),
                                    //                 topLeft: Radius.circular(10)),
                                    //             color: CustomColors.darkPinkColor),
                                    //         child: Text("Offer",
                                    //             style: TextStyle(
                                    //                 color: Colors.white,
                                    //                 fontSize: 12,
                                    //                 fontWeight: FontWeight.normal)),
                                    //       ),
                                    //     ),
                                    //     // SizedBox(
                                    //     //   height: 10,
                                    //     //   width: 1,
                                    //     // ),
                                    //     // Text(bannerList[index].name,
                                    //     //     style: TextStyle(
                                    //     //         color: Colors.black,
                                    //     //         fontSize: 12,
                                    //     //         fontWeight: FontWeight.normal))
                                    //   ],
                                    // )),
                                  ],
                                ),
                              );
                            },
                            itemCount: bannerList.length,
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.only(
                        left: 20, right: 20, top: 10, bottom: 90),
                    child: Card(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(5),
                                    topRight: Radius.circular(5)),
                                color: CustomColors.darkPinkColor),
                            padding: EdgeInsets.only(
                                left: 20, right: 20, top: 10, bottom: 10),
                            child: Text("TOP OFFERS",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w600)),
                          ),
                          !isNoOffer
                              ? Container(
                                  child: ListView.builder(
                                      physics: NeverScrollableScrollPhysics(),
                                      shrinkWrap: true,
                                      itemCount: offerList.length,
                                      itemBuilder: (context, index) {
                                        return Container(
                                          padding: EdgeInsets.only(
                                            left: 20,
                                            right: 20,
                                          ),
                                          child: Column(
                                            children: [
                                              Container(
                                                padding: EdgeInsets.only(
                                                    bottom: 10, top: 10),
                                                child: Row(
                                                  children: [
                                                    Container(
                                                      width: 55.0,
                                                      height: 55.0,
                                                      decoration: BoxDecoration(
                                                        color: CustomColors
                                                            .darkPinkColor,
                                                        image: DecorationImage(
                                                          image: NetworkImage(
                                                              offerList[index]
                                                                  .image),
                                                          fit: BoxFit.cover,
                                                        ),
                                                        borderRadius:
                                                            BorderRadius.all(
                                                                Radius.circular(
                                                                    100.0)),
                                                        border: Border.all(
                                                          color: Colors.white,
                                                          width: 1.0,
                                                        ),
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: 0,
                                                      width: 15,
                                                    ),
                                                    Expanded(
                                                        child: Column(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .start,
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                          offerList[index]
                                                                      .name !=
                                                                  null
                                                              ? offerList[index]
                                                                  .name
                                                              : "",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .grey[400],
                                                              fontSize: 12,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal),
                                                        ),
                                                        SizedBox(
                                                          height: 5,
                                                          width: 1,
                                                        ),
                                                        Text(
                                                          (offerList[index]
                                                                      .description !=
                                                                  null
                                                              ? offerList[index]
                                                                  .description
                                                              : ""),
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.black,
                                                              fontSize: 14,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .normal),
                                                          maxLines: 4,
                                                          overflow: TextOverflow
                                                              .ellipsis,
                                                        ),
                                                        SizedBox(
                                                          height: 5,
                                                          width: 1,
                                                        ),
                                                        Row(
                                                          children: [
                                                            Padding(
                                                              padding:
                                                                  const EdgeInsets
                                                                          .only(
                                                                      right:
                                                                          8.0),
                                                              child: Icon(
                                                                Icons
                                                                    .date_range,
                                                                size: 18,
                                                                color: Colors
                                                                    .grey[400],
                                                              ),
                                                            ),
                                                            Text(
                                                                "End in ${(offerList[index].rest_days != null ? offerList[index].rest_days.toString() : "")} days",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                            .grey[
                                                                        400],
                                                                    fontSize:
                                                                        12,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .normal)),
                                                          ],
                                                        ),
                                                      ],
                                                    )),
                                                    Icon(
                                                      Icons.arrow_forward_ios,
                                                      size: 20,
                                                      color: Colors.grey[800],
                                                    )
                                                  ],
                                                ),
                                              ),
                                              Container(
                                                color: Colors.grey[300],
                                                height: 1,
                                              )
                                            ],
                                          ),
                                        );
                                      }))
                              : Column(
                                  children: [
                                    Center(
                                      child: Container(
                                          padding: EdgeInsets.only(
                                              left: 30.0,
                                              top: 20.0,
                                              right: 30.0,
                                              bottom: 0.0),
                                          child: Image.asset(
                                            "assets/images/offer_img.png",
                                            width: 80,
                                            height: 80,
                                          )),
                                    ),
                                    Center(
                                        child: Container(
                                            padding: EdgeInsets.only(
                                                left: 30.0,
                                                top: 10.0,
                                                right: 80.0,
                                                bottom: 10.0),
                                            child: Text(
                                              "Oops, presently, there are no offers available",
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  color: Colors.grey),
                                              textAlign: TextAlign.center,
                                            ))),
                                  ],
                                ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            )));
  }

  Future<void> getOffers(BuildContext context) async {
    userData = await AppSharedPreferences.instance.getUserDetails();
    EasyLoading.show();
    provider
        .requestGetForApi(context, WebApiConstaint.URL_GETOFFERS + "?type=all",
            userData.sessionKey, userData.authorization)
        .then((responce) {
      EasyLoading.dismiss();
      CallAPIBanner();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            var resposdata = GetOffers.fromJson(responce.data);
            if (resposdata.data.length > 0) {
              isNoOffer = false;
              offerList = resposdata.data;
            } else {
              isNoOffer = true;
            }
            setState(() {});
          } else {
            setState(() {});
            isNoOffer = true;
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }

  CallAPIBanner() async {
    userData = await AppSharedPreferences.instance.getUserDetails();
    EasyLoading.show();
    provider
        .requestGetForApi(context, WebApiConstaint.URL_BANNER_LIST + "Offer",
            userData.sessionKey, userData.authorization)
        .then((responce) {
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            var resposdata = BannerItem.fromJson(responce.data);
            if (resposdata.data.length > 0) {
              setState(() {
                bannerList = resposdata.data;
              });
            }
          } else {
            if (responce.data["authenticate"] == false) {
              Fluttertoast.showToast(msg: responce.data["message"]);
              AppSharedPreferences.instance.setLogin(false);
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginScreen()));
            }
            print(responce.data["message"]);
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }
}
