// @dart=2.9
import 'dart:async';
import 'dart:convert';

import 'package:choix_customer/Data/GetOffers.dart';
import 'package:choix_customer/Data/SelectedItemData.dart';
import 'package:choix_customer/Data/UserData.dart';
import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/alert_dialog_new.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:choix_customer/ui/dashboard_screen.dart';
import 'package:choix_customer/widget/best_offers_items.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:choix_customer/ui/orders/order_list.dart';

class ProductDetails extends StatefulWidget {
  @override
  _ProductDetails createState() => _ProductDetails();
}

class _ProductDetails extends State<ProductDetails>
    with SingleTickerProviderStateMixin
    implements OfferSelectedListner, DialogSelectedListner {
  ApiProvider provider = ApiProvider();
  UserData userData;
  List<GetOffersData> offerList = [];
  GetOffersData offerSelected;
  String offerDesc = "";
  AnimationController _controller;
  var lastIndex = 0;
  bool isProgress = false;

  Timer _timer;
  bool isBackPress = true;
  var lastMsg = "";

  dynamic OrderID = "";
  bool isVendorFound = false;

  bool isNoOffer = false;
  StreamSubscription fcmListener;



  bool continueClicked = false;

  @override
  void initState() {
    super.initState();
    initFCM();
    _controller = AnimationController(
      lowerBound: 0.1,
      duration: Duration(seconds: 8),
      vsync: this,
    )..repeat();
    getOffers(context);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    if (_timer != null) _timer.cancel();
    // fcmListener.cancel();
    _controller.dispose();
    super.dispose();
  }

  Future<bool> _onWillPop() async {
    if (isBackPress) {
      if (_timer != null) {
        _timer.cancel();
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(builder: (context) => DashboardPage()),
            ModalRoute.withName("/Home"));
      } else {
        Navigator.pop(context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
          appBar: AppBar(
              toolbarHeight: 0, backgroundColor: CustomColors.darkPinkColor),
          backgroundColor: CustomColors.lightPinkColor,
          body: Stack(
            children: [
              Column(
                children: [
                  Expanded(
                    child: SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            child: Scaffold(
                              backgroundColor: Colors.transparent,
                              appBar: AppBar(
                                elevation: 0,
                                backgroundColor: Colors.transparent,
                                leading: IconButton(
                                  icon: Icon(
                                    Icons.arrow_back,
                                    color: Colors.white,
                                  ),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                ),
                              ),
                            ),
                            width: double.infinity,
                            height: 220,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                    bottomRight: Radius.circular(20),
                                    bottomLeft: Radius.circular(20)),
                                image: DecorationImage(
                                    image: AssetImage(
                                        "assets/images/ic_tomato.png"),
                                    fit: BoxFit.cover)),
                          ),
                          SizedBox(
                            height: 20,
                          ),
                          Container(
                            margin: EdgeInsets.only(
                                left: 30.0,
                                top: 20.0,
                                right: 30.0,
                                bottom: 20.0),
                            child: Align(
                              alignment: Alignment.topLeft,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    'Best Offers',
                                    style: TextStyle(
                                        fontFamily: "Montserrat-SemiBold",
                                        fontSize: 24),
                                  ),
                                  Text(
                                    offerDesc,
                                    style: TextStyle(
                                        fontFamily: "Montserrat", fontSize: 12),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          !isNoOffer
                              ? ListView.builder(
                                  physics: NeverScrollableScrollPhysics(),
                                  shrinkWrap: true,
                                  itemCount: offerList.length,
                                  itemBuilder: (context, i) =>
                                      BestOfferItem(offerList[i], this, i))
                              : Column(
                                  children: [
                                    Center(
                                      child: Container(
                                          padding: EdgeInsets.only(
                                              left: 30.0,
                                              top: 20.0,
                                              right: 30.0,
                                              bottom: 0.0),
                                          child: Image.asset(
                                            "assets/images/offer_img.png",
                                            width: 80,
                                            height: 80,
                                          )),
                                    ),
                                    Center(
                                        child: Container(
                                            padding: EdgeInsets.only(
                                                left: 30.0,
                                                top: 10.0,
                                                right: 30.0,
                                                bottom: 10.0),
                                            child: Text(
                                              "Oops, presently, there are no offers available",
                                              style: TextStyle(
                                                  fontSize: 15,
                                                  color: Colors.grey),
                                              textAlign: TextAlign.center,
                                            ))),
                                  ],
                                ),
                        ],
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      width: double.infinity,
                      height: 50,
                      color: Colors.pinkAccent,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(
                                left: 30.0, top: 0.0, right: 0.0, bottom: 0.0),
                            child: Text(
                              'Continue',
                              style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontFamily: "Montserrat-Bold"),
                            ),
                          ),
                          Spacer(),
                          Container(
                            margin: EdgeInsets.only(
                                left: 0.0, top: 0.0, right: 14.0, bottom: 0.0),
                            child: IconButton(
                              icon: Icon(
                                Icons.arrow_forward,
                                color: Colors.white,
                              ),
                              onPressed: () {
                                if (!continueClicked) {
                                  continueClicked = true;
                                  callSubmitAPI(context);
                                }
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
              if (isProgress)
                Container(
                  color: CustomColors.orderProgress,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        height: 200,
                        child: AnimatedBuilder(
                          animation: CurvedAnimation(
                              parent: _controller, curve: Curves.fastOutSlowIn),
                          builder: (context, child) {
                            return Stack(
                              alignment: Alignment.center,
                              children: <Widget>[
                                // _buildContainer(150 * _controller.value),
                                _buildContainer(150 * _controller.value),
                                _buildContainer(180 * _controller.value),
                                // _buildContainer(300 * _controller.value),
                                // _buildContainer(350 * _controller.value),
                                Align(
                                    child: Icon(
                                  Icons.location_on,
                                  color: CustomColors.darkPinkColor,
                                  size: 44,
                                )),
                              ],
                            );
                          },
                        ),
                      ),
                      Text(
                        "Please wait searching for vendor",
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                    ],
                  ),
                )
            ],
          )),
    );
  }

  Widget _buildContainer(double radius) {
    return Container(
      width: radius,
      height: radius,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: CustomColors.darkPinkColor.withOpacity(1 - _controller.value),
      ),
    );
  }

  Future<void> getOffers(BuildContext context) async {
    userData = await AppSharedPreferences.instance.getUserDetails();
    EasyLoading.show();
    provider
        .requestGetForApi(
            context,
            WebApiConstaint.URL_GETOFFERS + "?type=discount",
            userData.sessionKey,
            userData.authorization)
        .then((responce) {
      // print(strUrl);
      EasyLoading.dismiss();
      print(responce);
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            var resposdata = GetOffers.fromJson(responce.data);
            print("data ");
            offerList = resposdata.data;
            offerDesc = resposdata.offerDesc;
            if (resposdata.data.length > 0) {
              // offerSelected = resposdata.data[0];
              isNoOffer = false;
            } else {
              isNoOffer = true;
            }
            setState(() {});
          } else {
            isNoOffer = true;
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }

  @override
  void onOfferSelected(GetOffersData data, int index) {
    if (lastIndex == index) {
      if (offerList[lastIndex].isSelected == true) {
        offerList[lastIndex].isSelected = false;
        offerSelected = null;
      } else {
        offerSelected = data;
        offerList[lastIndex].isSelected = true;
      }
    } else {
      offerList[lastIndex].isSelected = false;
      lastIndex = index;
      offerList[index].isSelected = true;
      offerSelected = data;
    }
    print(offerSelected ?? "null offer");
    setState(() {});
  }

  callSubmitAPI(BuildContext context) async {
    var productData;
    String timeSlote = "";
    EasyLoading.show();
    if (savedData.orderType == "Tomorrow") {
      List<SelectedItemData> data = [];
      for (int i = 0; i < savedData.selectedItem.length; i++) {
        SelectedItemData data1 = SelectedItemData();
        data1.id = savedData.selectedItem[i].id.toString();
        data1.quantity = savedData.selectedItem[i].count.toString();
        data1.name = savedData.selectedItem[i].name.toString();
        data1.price = savedData.selectedItem[i].price.toString();
        data.add(data1);
      }
      productData =
          jsonDecode(jsonEncode(data.map((e) => e.toJson()).toList()));
      timeSlote = savedData.timeSlot.startTime.toString() +
          "-" +
          savedData.timeSlot.endTime.toString();
    }
    String discount_code = "";
    if (offerSelected != null) {
      discount_code = offerSelected.code;
    }
    Map<String, dynamic> dictParam = {
      'latitude': savedData.selectedAddres.latitude,
      'longitude': savedData.selectedAddres.longitude,
      'time_slot': timeSlote,
      'address': savedData.selectedAddres.address,
      'address_id': savedData.selectedAddres.id,
      'address_tag': savedData.selectedAddres.tags,
      'discount_code': discount_code,
      'category_id': savedData.category_id,
      'locality': savedData.selectedAddres.locality,
      'order_type': savedData.orderType,
      'product_data': productData == null ? "" : productData
    };
    print(dictParam);
    print(userData.sessionKey);
    print(userData.authorization);
    provider
        .requestPostForApi(context, dictParam, WebApiConstaint.URL_Submit,
            userData.sessionKey, userData.authorization)
        .then((responce) {
      continueClicked = false;
      EasyLoading.dismiss();
      print(responce);

      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            // Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["data"]);
            print(savedData.orderType);
            savedData.selectedItem = [];
            savedData.category_id = "";
            if (savedData.orderType == "Tomorrow") {
              isBackPress = true;
              showPopup(responce.data["message"]);
            } else {
              Fluttertoast.showToast(msg: responce.data["message"]);
              isBackPress = false;
              isProgress = true;
              setState(() {});
              OrderID = responce.data["data"]["order_id"];
              CAllAPICheckStatus(responce.data["data"]["order_id"], 10);
            }
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
          }
        } catch (e) {
          ToastUtils.showCustomToast(context, e.toString());
        }
      }
    });
  }

  CAllAPICheckStatus(dynamic OrderID, int timerSecond) {
    int totalTime = 12;
    int lastTime = 0;
    _timer = new Timer.periodic(Duration(seconds: timerSecond), (timer) {
      print("timer : " + timer.tick.toString());
      if (timer.tick <= totalTime) {
        lastTime += 10;
        if (isVendorFound && lastTime == 30) {
          lastTime = 0;
          CallAPICheckStatus2(OrderID);
        } else {
          lastTime = 0;
          CallAPICheckStatus2(OrderID);
        }
      } else {
        isBackPress = true;
        _timer.cancel();
        showPopup(lastMsg);
      }
    });
  }

  CallAPICheckStatus2(dynamic OrderID) {
    String url =
        WebApiConstaint.URL_SEARCHVENDOR + "order_id=" + OrderID.toString();
    print("timer : " + url);
    provider
        .requestGetForApi(
            context, url, userData.sessionKey, userData.authorization)
        .then((responce) async {
      if (responce != null) {
        print("timer : " + responce.data["message"]);
        lastMsg = responce.data["message"];
        if (responce.data["error"] == false) {
          _timer.cancel();
          //Accepted
          // if (database != null && personDao != null) {
          //   final oldNotification = await personDao.findNotifiationByOrderIdAll(
          //       responce.data["data"]["order_id"]);
          //   if (oldNotification.isNotEmpty) {
          //     oldNotification[0].isRead = true;
          //     await personDao.updateNotification(oldNotification[0]);
          //   }else {
          //     NotificationModel firstUser =
          //     NotificationModel(message: responce.data["message"],
          //         orderID: responce.data["data"]["order_id"],
          //         isRead: true,
          //         status: 'Accepted');
          //     await personDao.insertPerson(firstUser);
          //   }
          // }
          showPopup(responce.data["message"]);
        }
      }
      if (responce.data["data"]["vendor_found"] == true) {
        isVendorFound = true;
        // _timer.cancel();
        // CAllAPICheckStatus(responce.data["data"]["order_id"],30);
      }
    });
  }

  void showPopup(String message) {
    AlertDialogNew.show_dialog(context, this, message, "Ok", "");
  }

  void initFCM() {
    fcmListener = FirebaseMessaging.onMessage
        .asBroadcastStream()
        .listen((RemoteMessage message) {
      // do stuff
      RemoteNotification notification = message.notification;
      if (notification != null) {
        String msgBody = notification.body;
        print("message " + msgBody);
        if (mounted) {
          if (savedData.orderType != "Tomorrow") {
            CallAPICheckStatus2(OrderID);
          }
        }
      } else {
        print("message " + "body is null");
      }
      // if (message.data.isNotEmpty) {
      //   if (message.data["type"] == "popupAccept") {
      //     floorDatabase(message.data);
      //   }
      // }
    });
  }

  void floorDatabase(Map<String, dynamic> data) async {
    // print("Message not null Best Offer111");
    // final database =
    // await $FloorAppDatabaseCustomer.databaseBuilder('app_database.db').build();
    // final personDao = database.personDao;
    // final oldNotification = await personDao.findNotifiationByOrderId(
    //     data["orderid"]);
    // print("order id : ${data["orderid"]}");
    // if (oldNotification != null) {
    //   print("Message not null best offer");
    // } else {
    //   NotificationModel firstUser =
    //   NotificationModel(message: data["message"],
    //       orderID: data["orderid"],
    //       isRead: true,
    //       status: data["status"]);
    //   await personDao.insertPerson(firstUser);
    // }
  }

  @override
  onSelected(bool isSelected) {
    // TODO: implement onSelected
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => DashboardPage()),
        ModalRoute.withName("/Home"));

    // Navigator.pushAndRemoveUntil<dynamic>(
    //   context,
    //   MaterialPageRoute<dynamic>(
    //     builder: (BuildContext context) => DashboardPage(),
    //   ),
    //       (route) => false, //if you want to disable back feature set to false
    // );
  }

}

abstract class OfferSelectedListner {
  void onOfferSelected(GetOffersData data, int index);
}
