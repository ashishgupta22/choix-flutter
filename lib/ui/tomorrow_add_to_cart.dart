// @dart=2.9
import 'package:choix_customer/Data/ProductSubcategory.dart';
import 'package:choix_customer/Data/Productcategory.dart';
import 'package:choix_customer/Data/UserData.dart';
import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:choix_customer/ui/orders/order_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';

class TomorrowAddToCart extends StatefulWidget {
  CatData catData;

  TomorrowAddToCart(CatData this.catData) : super();

  @override
  _TomorrowAddToCart createState() => _TomorrowAddToCart();
}

class _TomorrowAddToCart extends State<TomorrowAddToCart> {
  List<ProductSubCatData> prodcutSubCatList = [];
  int totalCount = 0;

  String topTitle = "";

  void show(BuildContext context, int index) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(10),
            topLeft: Radius.circular(10),
          ),
        ),
        context: context,
        builder: (context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                height: 130,
                margin: EdgeInsets.all(30),
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(prodcutSubCatList[index].image),
                      fit: BoxFit.contain),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                  topRight: Radius.circular(10),
                  topLeft: Radius.circular(10),
                )),
                child: Container(
                  margin: EdgeInsets.only(
                      left: 30.0, top: 0.0, right: 30.0, bottom: 0.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              prodcutSubCatList[index].name,
                              style: TextStyle(
                                  color: CustomColors.blackColor,
                                  fontFamily: "Montserrat-Bold",
                                  fontSize: 18),
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              Navigator.pop(context);
                              updateCount(1, index);
                            },
                            child: Container(
                              width: 60,
                              decoration: BoxDecoration(
                                  color: CustomColors.darkPinkColor,
                                  borderRadius: BorderRadius.circular(15)),
                              margin: EdgeInsets.only(
                                  left: 30.0,
                                  top: 10.0,
                                  right: 10.0,
                                  bottom: 10.0),
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Center(
                                  child: Text(
                                    'Add',
                                    style: TextStyle(
                                      fontFamily: "Montserrat",
                                      fontSize: 10,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        child: Text(
                            '\u{20B9}' +
                                prodcutSubCatList[index].price +
                                " / ${prodcutSubCatList[index].product_type}",
                            style: TextStyle(
                                fontFamily: "Montserrat-SemiBold",
                                fontSize: 16,
                                fontWeight: FontWeight.w500)),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical, //.horizontal
                  child: Container(
                    constraints: BoxConstraints(
                        maxWidth: MediaQuery.of(context).size.width - 50),
                    margin: EdgeInsets.only(
                        left: 30.0, top: 15.0, right: 30.0, bottom: 20.0),
                    child: Text(
                      prodcutSubCatList[index].description,
                      style: TextStyle(fontFamily: "Montserrat", fontSize: 12),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }

  UserData userData;
  ApiProvider provider = ApiProvider();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
      topTitle = widget.catData.name;
    });
    getProductCate(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:
          AppBar(toolbarHeight: 0, backgroundColor: CustomColors.darkPinkColor),
      body: Column(
        children: [
          // Stack(
          //   children: [
          Stack(
            children: [
              Container(
                alignment: Alignment.topLeft,
                width: MediaQuery.of(context).size.width,
                height: 220,
                margin: EdgeInsets.only(
                    left: 0.0, top: 0.0, right: 40.0, bottom: 0.0),
                decoration: BoxDecoration(
                  color: CustomColors.darkPinkColor,
                  borderRadius:
                      BorderRadius.only(bottomRight: Radius.circular(30)),
                  // image: DecorationImage(
                  //     image: AssetImage("assets/images/ic_tomato.jpeg"),
                  //     fit: BoxFit.cover),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(12.0),
                      child: InkWell(
                        onTap: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                            margin: EdgeInsets.only(
                                left: 10.0,
                                top: 10.0,
                                right: 00.0,
                                bottom: 0.0),
                            width: 26,
                            height: 26,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(5)),
                              color: Colors.white,
                            ),
                            child: Padding(
                              padding: EdgeInsets.all(5.0),
                              child:
                                  Image.asset("assets/images/arrow_back.png"),
                            )),
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Container(
                          margin: EdgeInsets.only(
                              left: 20.0, top: 10.0, right: 30.0, bottom: 0.0),
                          child: Text(
                            topTitle,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 34,
                                fontFamily: "Nueva"),
                          )),
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Container(
                    //width: 200,
                    padding: EdgeInsets.only(top: 50),
                    child: Image.asset(
                      "assets/images/cherry.png",
                      height: 160,
                    ),
                  ),
                ],
              ),
            ],
          ),

          //],
          //),

          Expanded(
              child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 10,bottom: 0,left: 10,right: 10),
                  child: Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey),
                      color: Colors.grey[100],
                      borderRadius: BorderRadius.all(
                          Radius.circular(5)
                      ),
                    ),
                    child: Padding(

                      padding: const EdgeInsets.only(top: 10,bottom: 10,left: 10,right: 10),
                      child: Text(
                        "The prices are representative. Please discuss the price with vendor before buying.\n\nVendors prefer visiting customers who buy atleast 5 items!!",
                        style: TextStyle(
                            fontSize: 12,
                            color: CustomColors.darkPinkColor,
                            fontFamily: "Montserrat-SemiBold"),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ),
                ),
                Container(
                  child: ListView.builder(
                      shrinkWrap: true,
                      physics: NeverScrollableScrollPhysics(),
                      itemCount: prodcutSubCatList.length,
                      itemBuilder: (context, index) {
                        return InkWell(
                          onTap: () {
                            show(context, index);
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                color: CustomColors.lightPinkColor,
                                borderRadius: BorderRadius.circular(20)),
                            margin: EdgeInsets.only(
                                left: 20.0,
                                top: 10.0,
                                right: 20.0,
                                bottom: 10.0),
                            child: Row(
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(20),
                                        topLeft: Radius.circular(20),
                                      ),
                                      image: DecorationImage(
                                          image: NetworkImage(
                                              prodcutSubCatList[index].image),
                                          fit: BoxFit.cover)),
                                  width: 100,
                                  height: 130,
                                ),
                                SizedBox(
                                  width: 8,
                                  height: 0,
                                ),
                                Container(
                                  child: Expanded(
                                    child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          children: [
                                            Expanded(
                                              flex: 1,
                                              child: Text(
                                                prodcutSubCatList[index].name,
                                                style: TextStyle(
                                                    color:
                                                        CustomColors.blackColor,
                                                    fontSize: 15,
                                                    fontFamily:
                                                        "Montserrat-SemiBold"),
                                              ),
                                            ),
                                            Expanded(
                                              flex: 1,
                                              child: Row(
                                                children: [
                                                  IconButton(
                                                      icon: Icon(
                                                          Icons
                                                              .remove_circle_outline_sharp,
                                                          color: CustomColors
                                                              .darkPinkColor),
                                                      onPressed: () {
                                                        updateCount(0, index);
                                                      }),
                                                  SizedBox(
                                                    width: 1,
                                                  ),
                                                  Text(
                                                    prodcutSubCatList[index]
                                                        .count
                                                        .toString(),
                                                    style: TextStyle(
                                                      color: CustomColors
                                                          .darkPinkColor,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    width: 1,
                                                  ),
                                                  IconButton(
                                                    icon: Icon(
                                                        Icons.add_circle_sharp),
                                                    color: CustomColors
                                                        .darkPinkColor,
                                                    onPressed: () {
                                                      updateCount(1, index);
                                                    },
                                                  )
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 1,
                                        ),
                                        Container(
                                          constraints: BoxConstraints(
                                              maxWidth: MediaQuery.of(context)
                                                      .size
                                                      .width -
                                                  150),
                                          child: Text(
                                            prodcutSubCatList[index]
                                                .description,
                                            textAlign: TextAlign.start,
                                            overflow: TextOverflow.ellipsis,
                                            maxLines: 3,
                                            style: TextStyle(
                                                fontSize: 12,
                                                color: Colors.grey,
                                                fontFamily: "Montserrat"),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Container(
                                          child: Text(
                                            '\u{20B9}' +
                                                prodcutSubCatList[index].price +
                                                "/" +
                                                prodcutSubCatList[index]
                                                    .product_type,
                                            style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.black,
                                                fontFamily:
                                                    "Montserrat-SemiBold"),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        );
                      }),
                ),
              ],
            ),
          )),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              width: double.infinity,
              height: 50,
              color: Colors.pinkAccent,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    margin: EdgeInsets.only(
                        left: 30.0, top: 0.0, right: 0.0, bottom: 0.0),
                    child: Text(
                      totalCount.toString() + ' Items selected',
                      style: TextStyle(
                          fontSize: 15,
                          color: Colors.white,
                          fontFamily: "Montserrat"),
                    ),
                  ),
                  Spacer(),
                  InkWell(
                    onTap: () {
                      if (savedData.selectedItem.length > 0) {
                        savedData.orderType = 'Tomorrow';
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) {
                              return OrderList();
                            },
                          ),
                        ).then((value) {
                          print("value");
                          updateOldSelected();
                        });
                      } else {
                        Fluttertoast.showToast(
                            msg: "Please select minimum 1 item..");
                      }
                    },
                    child: Container(
                      width: 60,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(15)),
                      margin: EdgeInsets.only(
                          left: 30.0, top: 10.0, right: 10.0, bottom: 10.0),
                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Center(
                          child: Text(
                            'NEXT',
                            style: TextStyle(
                              fontFamily: "Montserrat",
                              fontSize: 10,
                              color: CustomColors.darkPinkColor,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<void> getProductCate(BuildContext context) async {
    userData = await AppSharedPreferences.instance.getUserDetails();
    int catId = widget.catData.id;
    EasyLoading.show();
    provider
        .requestGetForApi(
            context,
            WebApiConstaint.URL_GETPRODUCTSUBCATEGORy + "?category_id=$catId",
            userData.sessionKey,
            userData.authorization)
        .then((responce) {
      print("Sesstion : " +
          userData.sessionKey +
          "  authKey : " +
          userData.authorization);
      EasyLoading.dismiss();

      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            var resposdata = ProductSubcategory.fromJson(responce.data);
            prodcutSubCatList = resposdata.data;
            updateOldSelected();
            setState(() {});
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }

  void updateCount(int type, int index) {
    // 1 = Add, 0 = remove
    if (prodcutSubCatList != null && prodcutSubCatList.length > 0) {
      if (type == 1) {
        prodcutSubCatList[index].count += 1;
        getTotalCount();
        updateSelectedItem(type, index);
      } else if (type == 0 && prodcutSubCatList[index].count > 0) {
        prodcutSubCatList[index].count -= 1;
        updateSelectedItem(type, index);
        getTotalCount();
      }
    }
  }

  void updateOldSelected() {
    try {
      if (prodcutSubCatList.isNotEmpty) {
        List<ProductSubCatData> prodcutSubCatList1 = [];
        for (int j = 0; j < savedData.selectedItem.length; j++) {
          ProductSubCatData data =
              ProductSubCatData.fromJson(savedData.selectedItem[j].toJson());
          data.count = savedData.selectedItem[j].count;
          prodcutSubCatList1.add(data);
        }
        print("value12 : " + prodcutSubCatList.length.toString());
        for (int j = 0; j < prodcutSubCatList.length; j++) {
          prodcutSubCatList[j].count = 0;
          for (int i = 0; i < prodcutSubCatList1.length; i++) {
            if (prodcutSubCatList1[i].id == prodcutSubCatList[j].id) {
              prodcutSubCatList[j].count = prodcutSubCatList1[i].count;
            }
          }
        }
        savedData.selectedItem = prodcutSubCatList1;
      }
    } catch (_) {
      print(_);
    }
    getTotalCount();
  }

  void getTotalCount() async {
    totalCount = 0;
    // selectedItem.clear();
    if (prodcutSubCatList != null && prodcutSubCatList.length > 0) {
      for (int i = 0; i < prodcutSubCatList.length; i++) {
        if (prodcutSubCatList[i].count > 0) {
          totalCount += prodcutSubCatList[i].count;
          // selectedItem.add(prodcutSubCatList[i]);
        }
      }
      setState(() {});
    }
    print("testt$totalCount");
  }

  void updateSelectedItem(int type, int index) {
    if (savedData.selectedItem.isNotEmpty) {
      bool isAvailable = false;
      for (int i = 0; i < savedData.selectedItem.length; i++) {
        if (prodcutSubCatList[index].id == savedData.selectedItem[i].id) {
          isAvailable = false;
          savedData.selectedItem.removeAt(i);
          if (prodcutSubCatList[index].count > 0)
            savedData.selectedItem.add(prodcutSubCatList[index]);
          break;
        } else
          isAvailable = true;
      }
      if (isAvailable) savedData.selectedItem.add(prodcutSubCatList[index]);
    } else {
      savedData.selectedItem.add(prodcutSubCatList[index]);
    }
  }
}
