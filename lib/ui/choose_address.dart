// @dart=2.9
import 'package:choix_customer/Data/FetchAddress.dart';
import 'package:choix_customer/Data/UserData.dart';
import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/alert_dialog_new.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:choix_customer/ui/offers/best_offres.dart';
import 'package:choix_customer/ui/savedAddress.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:choix_customer/ui/orders/order_list.dart';

import 'login_screen.dart';

class ChooseAddress extends StatefulWidget {
  @override
  _ChooseAddress createState() => _ChooseAddress();
}

class _ChooseAddress extends State<ChooseAddress> {
  FetchAddressData selectedAddres = FetchAddressData();


  ApiProvider provider = ApiProvider();
  UserData userData;
  List<FetchAddressData> addressList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    selectedAddres.address = "Select Address";
    selectedAddres.tags = "Select Address";
    selectedAddres.locality = "";
    selectedAddres.id = 0;
    getFetchAddress(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: CustomColors.darkPinkColor,
        title: Text(
          'Choose Address',
          style: TextStyle(fontFamily: "Montserrat"),
        ),
      ),
      body: Container(
        color: Colors.white,
        child: Column(
          children: [
            Container(
              child: Padding(
                padding: EdgeInsets.all(8),
                child: ListTile(
                  onTap: () {
                    if(selectedAddres.locality.isNotEmpty) {
                      savedData.selectedAddres = selectedAddres;
                      getCheckLocality(context);
                    }
                  },
                  leading: Icon(Icons.star_border_outlined),
                  title: Text(selectedAddres.tags != null ? selectedAddres.tags : ""),
                  subtitle: Text(
                    selectedAddres.address,
                    style: TextStyle(fontSize: 11, fontFamily: "Montserrat"),
                  ),
                ),
              ),
            ),
            Divider(height: 2, color: Colors.grey,),
            Container(
              child: Padding(
                padding: EdgeInsets.all(8),
                child: ListTile(
                  onTap: () {
                    _navigateAndDisplaySelection(context);
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(
                    //     builder: (context) {
                    //       return SavedAddress();
                    //     },
                    //   ),
                    // );
                  },
                  // leading: Icon(Icons.add_location),
                  title: Padding(
                    padding: const EdgeInsets.only(left: 3.0),
                    child: Text(
                      "Choose your delivery address",
                      style: TextStyle(color: CustomColors.darkPinkColor),
                    ),
                  ),
                  subtitle: Padding(
                    padding: const EdgeInsets.only(left: 3.0),
                    child: Text(
                      'Get your favorite destination faster',
                      style: TextStyle(fontSize: 11, fontFamily: "Montserrat"),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _navigateAndDisplaySelection(BuildContext context) async {
    // Navigator.push returns a Future that completes after calling
    // Navigator.pop on the Selection Screen.
    final result = await Navigator.push(
      context,
      // Create the SelectionScreen in the next step.
      MaterialPageRoute(builder: (context) => SavedAddress(addressList,0)),
    );
    if(result != null)
      selectedAddres = result;
    setState(() {});
    print(result);
  }

  Future<void> getFetchAddress(BuildContext context) async {
    userData = await AppSharedPreferences.instance.getUserDetails();
    EasyLoading.show();
    provider
        .requestGetForApi(context, WebApiConstaint.URL_GETFETCHADDRESS,
            userData.sessionKey, userData.authorization)
        .then((responce) {
      // print(strUrl);
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            var resposdata = FetchAddress.fromJson(responce.data);
            addressList = resposdata.data;
            if(addressList.length > 0) {
              selectedAddres = addressList[0];
              setState(() {});
            }
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
            if (responce.data["authenticate"] == false) {
              AppSharedPreferences.instance.setLogin(false);
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                  ModalRoute.withName("/Home"));
            }
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }


  Future<void> getCheckLocality(BuildContext context) async {
    userData = await AppSharedPreferences.instance.getUserDetails();
    EasyLoading.show();
    String strUrl = WebApiConstaint.URL_CHECKLOCALITY+"locality="+selectedAddres.locality;
    provider
        .requestGetForApi(context,strUrl,
        userData.sessionKey, userData.authorization)
        .then((responce) {
      print(strUrl);
      EasyLoading.dismiss();

      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            if(responce.data["errorCode"] == 0){
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return ProductDetails();
                  },
                ),
              );
            }else{
              selectedAddres.address = "Select Address";
              selectedAddres.tags = "Select Address";
              selectedAddres.locality = "";
              selectedAddres.id = 0;
              setState(() {
              });
              AlertDialogNew.show_dialog(context,null,  responce.data["message"], "Ok", "");
            }

          } else {
            if (responce.data["authenticate"] == false) {
              AppSharedPreferences.instance.setLogin(false);
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                  ModalRoute.withName("/Home"));
            }
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
          }
        } catch (_EX) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }
}
