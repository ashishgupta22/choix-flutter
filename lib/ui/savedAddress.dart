// @dart=2.9
import 'dart:async';

import 'package:choix_customer/Data/FetchAddress.dart';
import 'package:choix_customer/Data/UserData.dart';
import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/alert_dialog_new.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:choix_customer/widget/saved_address_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';

import 'package:google_maps_webservice/places.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'drop_pin_new.dart';
import 'google_place_autocomplete.dart';
import 'login_screen.dart';
import 'offers/best_offres.dart';
import 'orders/order_list.dart';

GoogleMapsPlaces _places =
    GoogleMapsPlaces(apiKey: WebApiConstaint.GOOGLE_API_KEY);

class SavedAddress extends StatefulWidget {
  List<FetchAddressData> addressList = [];
  int type = 0;
  SavedAddress(List<FetchAddressData> this.addressList,int this.type) : super();

  @override
  _SavedAddressState createState() => _SavedAddressState();
}
class _SavedAddressState extends State<SavedAddress>
    implements ItemAddressListner {
  ApiProvider provider = ApiProvider();
  UserData userData;
  CameraPosition cPosition;

  BitmapDescriptor pinIcon;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // updateMarkerOnselectedLocation();
    // getUserLocation();

    if (widget.addressList == null || widget.addressList.isEmpty) {
      getFetchAddress(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Saved Address"),
          leading: InkWell(
            onTap: (){
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          ),
          backgroundColor: CustomColors.darkPinkColor,
        ),
        body: Column(
          children: [
            SizedBox(),
            if(widget.type == 0)
            InkWell(
              onTap: () async {
                callFrom = 0;
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => CustomSearchScaffold()),
                );
              },
              child: Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.only(
                    left: 20.0, top: 20.0, right: 20.0, bottom: 10.0),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    border: Border.all(width: 1,color: Colors.grey[300]),
                    color: Colors.white,
                    boxShadow: [
                      // color: Colors.white, //background color of box
                      // BoxShadow(
                      //   color: Colors.grey,
                      //   blurRadius: 3.0, // soften the shadow
                      //   spreadRadius: 1.0, //extend the shadow
                      //   offset: Offset(
                      //     1.0, // Move to right 10  horizontally
                      //     1.0, // Move to bottom 10 Vertically
                      //   ),
                      // )
                    ]),
                child: Row(children: [
                  Icon(
                    Icons.search,
                    color: Colors.red,
                  ),
                  SizedBox(),
                  Text(
                    "Search for your location...",
                    style: TextStyle(color: Colors.grey),
                  ),
                ]),
              ),
            ),
            if(widget.type == 0)
            InkWell(
              onTap: chooseLocationMap,
              child: Container(
                margin: EdgeInsets.only(
                    left: 20.0, top: 5.0, right: 20.0, bottom: 5.0),
                child: Row(
                  children: [
                    Icon(
                      Icons.my_location,
                      color: Colors.red,
                    ),
                    SizedBox(),
                    Padding(
                      padding: const EdgeInsets.only(left: 12.0),
                      child: Text(
                        "Use current location",
                        style: TextStyle(color: Colors.pink),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: ListView.builder(
                          shrinkWrap: true,
                          itemCount: widget.addressList.length,
                          physics: NeverScrollableScrollPhysics(),
                          itemBuilder: (context, i) =>
                              SavedAddressItem(widget.addressList[i], this)),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ));
  }

  @override
  void onAddressSelected(FetchAddressData data, int type) {
    //1 For Delete Address, 0 choose Address
    if(type ==0 ) {
      savedData.selectedAddres = data;
      if (widget.type == 0)
        getCheckLocality(context);
    }else if(type == 1){
      deleteAddress(context,data.id);
    }
  }
  Future<void> getCheckLocality(BuildContext context) async {
    userData = await AppSharedPreferences.instance.getUserDetails();
    EasyLoading.show();
    String strUrl = WebApiConstaint.URL_CHECKLOCALITY +
        "locality=" +
        savedData.selectedAddres.locality;
    provider
        .requestGetForApi(
        context, strUrl, userData.sessionKey, userData.authorization)
        .then((responce) {
      print(strUrl);
      EasyLoading.dismiss();

      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            if (responce.data["errorCode"] == 0) {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return ProductDetails();
                  },
                ),
              );
            } else {
              savedData.selectedAddres.address = "Select Address";
              savedData.selectedAddres.tags = "Select Address";
              savedData.selectedAddres.locality = "";
              savedData.selectedAddres.id = 0;
              setState(() {});
              AlertDialogNew.show_dialog(context,null,  responce.data["message"], "Ok", "");
            }
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
            if (responce.data["authenticate"] == false) {

              AppSharedPreferences.instance.setLogin(false);
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                  ModalRoute.withName("/Home"));
            }else{

            }
          }
        } catch (_EX) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }

  Future<void> deleteAddress(BuildContext context, int id) async {
    userData = await AppSharedPreferences.instance.getUserDetails();
    EasyLoading.show();
    String strUrl = WebApiConstaint.URL_DELETE_ADDRESS +
        "?id=" +
        id.toString();
    provider
        .requestGetForApi(
        context, strUrl, userData.sessionKey, userData.authorization)
        .then((responce) {
      print(strUrl);
      EasyLoading.dismiss();

      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            getFetchAddress(context);
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
            if (responce.data["authenticate"] == false) {
              AppSharedPreferences.instance.setLogin(false);
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                  ModalRoute.withName("/Home"));
            }
          }
        } catch (_EX) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }

  Future<void> getFetchAddress(BuildContext context) async {
    userData = await AppSharedPreferences.instance.getUserDetails();
    EasyLoading.show();
    provider
        .requestGetForApi(context, WebApiConstaint.URL_GETFETCHADDRESS,
            userData.sessionKey, userData.authorization)
        .then((responce) {
      // print(strUrl);
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            var resposdata = FetchAddress.fromJson(responce.data);
            widget.addressList = resposdata.data;
            setState(() {});
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
            if (responce.data["authenticate"] == false) {
              AppSharedPreferences.instance.setLogin(false);
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                  ModalRoute.withName("/Home"));
            }
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }

  Future<Null> displayPrediction(Prediction p) async {
    if (p != null) {
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);
      double lat = detail.result.geometry.location.lat;
      double lng = detail.result.geometry.location.lng;
      print("latiiii - " + lat.toString());
      print("latiiii - " + lng.toString());
      var address = await Geocoder.local.findAddressesFromQuery(p.description);
      FetchAddressData data = FetchAddressData();
      data.address = address.single.addressLine;
      data.tags = "";
      data.id = 0;
      data.locality = address.single.postalCode;
      data.latitude = lat.toString();
      data.longitude = lng.toString();
      Navigator.pop(context, data);
    }
  }

  Future<void> chooseLocationMap() async {
    final result = await Navigator.push(
      context,
      // Create the SelectionScreen in the next step.
      MaterialPageRoute(builder: (context) => DropPinNew()),
    );
    if (result != null) {
      Navigator.pop(context, result);
    }
  }
}

abstract class ItemAddressListner {
  void onAddressSelected(FetchAddressData data, int type);
}
