import 'dart:io';

import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/cupertino.dart';

enum Stage { ERROR, LOADING, DONE }

class ContactsProvider with ChangeNotifier {
  String errorMessage = "Network Error";
  late Stage? stage;
  late List<Contact> _playlist = [];
  int? _currentVideoId;
  List<Contact> _contacts = [];
  List<Contact> _contactsMain = [];
  var pageNo = 2;
  ContactsProvider() {
    this.stage = Stage.LOADING;
    initScreen();
  }

  void initScreen() async {
    try {
      await fetchVideosList("");
      stage = Stage.DONE;
    } catch (e) {
      stage = Stage.ERROR;
    }
    notifyListeners();
  }

  List<Contact> get playlist => _playlist;

  void setPlayList(videosList) {
    _playlist = videosList;
  }

  void validateInput(String valueText) {
    if (valueText == ""){
      this._currentVideoId = 0;
      return;
    }
    try {
      int valueInt = int.parse(valueText);
      if (valueInt == 1 || valueInt == 2){
        this._currentVideoId = valueInt;
      }
      else {
        this.errorMessage = "Use only 1 and 2";
        throw 1;
      }
    } on FormatException catch (_Ex) {
      this.errorMessage = "Must be a number";
      throw 1;
    }
  }

  void updateCurrentVideoId(String value) async {
    this.stage = Stage.LOADING;
    notifyListeners();

    try {
      // validateInput(value);
      await fetchVideosList(value);
      stage = Stage.DONE;
    } on SocketException catch (e) {
      this.errorMessage = "Network Error";
      stage = Stage.ERROR;
    } catch (e) {
      stage = Stage.ERROR;
    }
    notifyListeners();
  }

  Future fetchVideosList(String text) async {
    print(text+"${_contactsMain.length}");
    if (text.toString().trim().length > 0) {
      _contacts.clear();
      for (int i = 0; i < _contactsMain.length; i++) {
        if (_contactsMain[i]
            .displayName
            .toString()
            .toLowerCase()
            .contains(text.toString().trim().toLowerCase())) {
          _contacts.add(_contactsMain[i]);
        }
      }
    } else {
      _contacts.addAll(_contactsMain);
    }
    if(_contactsMain.isEmpty) {
      var contacts =
      (await ContactsService.getContacts(withThumbnails: false)).toList();
      _contactsMain.clear();
      _contacts.clear();
      for (int i = 0; i < contacts.length; i++) {
        if (contacts[i].phones!.length > 0) {
          _contactsMain.add(contacts[i]);
        }
      }
      _contacts.addAll(_contactsMain);
    }
    print("contactList : " + _contacts.length.toString());
    setPlayList(_contacts);
  }
}