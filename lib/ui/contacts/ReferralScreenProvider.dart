import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/ui/contacts/referral_screen.dart';
import 'package:choix_customer/ui/contacts/contactProvider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class PlaylistScreenProvider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Referral"),
          leading: InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          ),
          backgroundColor: CustomColors.darkPinkColor,
        ),
      body: ChangeNotifierProvider<ContactsProvider>(
        create: (_) {
          return ContactsProvider();
        },
        child: RefferralScreen(),
      ),
    );
  }
}

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Screen'),
      ),
      body: Center(
        child: RaisedButton(
          child: Text("Go To StatefulWidget Screen"),
          onPressed: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                builder: (_) {
                  return PlaylistScreenProvider();
                },
              ),
            );
          },
        ),
      ),
    );
  }
}
