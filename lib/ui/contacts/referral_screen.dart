import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/ui/contacts/contactProvider.dart';
import 'package:choix_customer/widget/contact_item.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:share/share.dart';

import '../referral_screen.dart';

String shareURL = "";
class RefferralScreen extends StatefulWidget {

  RefferralScreen();

  @override
  _RefferralScreen createState() => _RefferralScreen();
}

class _RefferralScreen extends State<RefferralScreen> {
  late List<Contact> _playlistList;
  late String _errorMessage;
  late Stage _stage;

  final _searchTextCtrl = TextEditingController();
  FocusNode myFocusNode = new FocusNode();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  void dispose() {
    super.dispose();
    _searchTextCtrl.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final videosState = Provider.of<ContactsProvider>(context);
    _playlistList = videosState.playlist;
    _stage = videosState.stage!;
    _errorMessage = videosState.errorMessage;
  }

  void actionSearch() {
    String text = _searchTextCtrl.value.text;
    Provider.of<ContactsProvider>(context, listen: false)
        .updateCurrentVideoId(text);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(horizontal: 16.0),
        child: Column(
          children: <Widget>[

            Container(
              margin: EdgeInsets.only(
                  left: 20, right: 20, top: 15),
              //color: Colors.blue,
              height: 50,
              child: TextField(
                focusNode: myFocusNode,
                // autofocus: true,
                controller: _searchTextCtrl,
                keyboardType: TextInputType.name,
                textInputAction: TextInputAction.next,
                //style: TextStyle(color: CustomColors.side_bar_text),
                onChanged: (value) {
                  actionSearch();
                },
                decoration: InputDecoration(
                  suffixIcon: IconButton(
                    icon: Icon(Icons.search),
                    onPressed: () {},
                  ),
                  filled: true,
                  fillColor: Colors.white,
                  hintText: "Search",
                  //   \n (breank) \\ (\)
                  // labelText: 'Search',
                  // labelStyle: TextStyle(
                  //     color: myFocusNode.hasFocus ? Colors.black : Colors.black,
                  //     fontFamily: "Montserrat"
                  //
                  // ),
                  //hintStyle: TextStyle(color: CustomColors.side_bar_text),
                  contentPadding: const EdgeInsets.only(
                      left: 20.0,
                      bottom: 8.0,
                      top: 8.0,
                      right: 20),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: CustomColors.grey),
                      borderRadius: BorderRadius.all(
                          Radius.circular(10))),

                  // errorText: !isNameValidate
                  //     ? "Can\'t be empty" : null,
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                          color: CustomColors.grey),
                      borderRadius: BorderRadius.all(
                          Radius.circular(10))),
                ),
              ),
            ),
            Flexible(
              child: _stage == Stage.DONE
                  ? PlaylistTree(_playlistList)
                  : _stage == Stage.ERROR
                  ? Center(child: Text("$_errorMessage"))
                  : Center(
                child: CircularProgressIndicator(),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class PlaylistTree extends StatelessWidget implements InviteClickListner {
  PlaylistTree(this.playlistList);
  final List<Contact> playlistList;
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: playlistList.length,
      itemBuilder: (context, index) {
        var data = playlistList[index];
        return ContactItem(data, this);
      },
    );
  }

  @override
  void onSelected(Contact) {
    // TODO: implement onSelected
    Share.share(shareURL);
  }
}