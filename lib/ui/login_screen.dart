// @dart=2.9
import 'dart:convert';

import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:choix_customer/ui/register_screen.dart';
import 'package:choix_customer/widgets/CustomTextFiled.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_sign_in/google_sign_in.dart';

import 'otp_pin.dart';

class LoginScreen extends StatefulWidget {
  @override
  StateLoginScreen createState() => StateLoginScreen();
}

class StateLoginScreen extends State<LoginScreen> {
  GoogleSignInAccount _currentUser;
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  ApiProvider provider = ApiProvider();
  TextEditingController txtPhoneController = TextEditingController();
  bool isPhnValidate = true;
  FocusNode myFocusNode = new FocusNode();

  FToast fToast;

  @override
  void initState() {
    super.initState();
    fToast = FToast();
    fToast.init(context);
    try {
      _googleSignIn.signOut();
    } catch (_) {
      print("Google 11$_");
      print(_);
    }
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.white,
        appBar: AppBar(
          toolbarHeight: 0,
          backgroundColor: Colors.white,
        ),
        body: SafeArea(
          child: Stack(children: [
            SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                      padding: EdgeInsets.only(left: 0, bottom: 0, right: 0),
                      child: Container(
                        padding: EdgeInsets.only(left: 0, bottom: 0, right: 0),
                        width: MediaQuery.of(context).size.width,
                        color: Colors.white,
                        child: Image.asset(
                          "assets/images/top_bg.png",
                          fit: BoxFit.fitWidth,
                        ),
                      )),
                  Container(
                    margin: EdgeInsets.only(
                        top: 25, left: 30, right: 30, bottom: 30),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        // SizedBox(
                        //   height: MediaQuery.of(context).size.height * 0.08,
                        //   width: 0,
                        // ),

                        Text(
                          "Welcome",
                          style: TextStyle(
                              fontSize: 50,
                              fontWeight: FontWeight.w600,
                              fontFamily: "Nueva"),
                        ),

                        //SizedBox(height: 5,width: 0,),
                        Text(
                          "Please login to your account",
                          style: TextStyle(
                              fontSize: 14,
                              fontFamily: "Montserrat",
                              fontWeight: FontWeight.w300),
                        ),
                        SizedBox(
                          height: 50,
                          width: 0,
                        ),
                        // TextField(
                        //   focusNode: myFocusNode,
                        //   autofocus: true,
                        //   controller: txtPhoneController,
                        //   //maxLength: 10,
                        //   keyboardType: TextInputType.name,
                        //   textInputAction: TextInputAction.done,
                        //   //style: TextStyle(color: CustomColors.side_bar_text),
                        //   decoration: InputDecoration(
                        //     counter: Offstage(),
                        //     filled: true,
                        //     fillColor: Colors.white,
                        //     hintText: 'Enter your number or Email',
                        //     hintStyle: TextStyle(color: CustomColors.hintColor),
                        //     labelText: 'Phone Number/Email',
                        //     labelStyle: TextStyle(
                        //         color: myFocusNode.hasFocus
                        //             ? Colors.black
                        //             : Colors.black,
                        //         fontFamily: "Montserrat"),
                        //     //hintStyle: TextStyle(color: CustomColors.side_bar_text),
                        //     contentPadding: const EdgeInsets.only(
                        //         left: 20.0, bottom: 8.0, top: 8.0, right: 20),
                        //     focusedBorder: OutlineInputBorder(
                        //         borderSide: BorderSide(color: Colors.grey[300]),
                        //         borderRadius:
                        //             BorderRadius.all(Radius.circular(30))),
                        //
                        //     errorText: !isPhnValidate ? "Can't be empty" : null,
                        //     enabledBorder: OutlineInputBorder(
                        //         borderSide: BorderSide(color: Colors.grey[200]),
                        //         borderRadius:
                        //             BorderRadius.all(Radius.circular(30))),
                        //     errorBorder: OutlineInputBorder(
                        //         borderSide: BorderSide(color: Colors.grey[200]),
                        //         borderRadius:
                        //         BorderRadius.all(Radius.circular(30))),
                        //     focusedErrorBorder:  OutlineInputBorder(
                        //         borderSide: BorderSide(color: Colors.grey[200]),
                        //         borderRadius:
                        //         BorderRadius.all(Radius.circular(30))),
                        //   ),
                        // ),

                        CustomTextFiled(
                            true,
                            myFocusNode,
                            txtPhoneController,
                            isPhnValidate,
                            "Enter your number or Email",
                            "Phone Number/Email",
                            TextInputType.name,
                            TextInputAction.done,
                            "Phone number can't be empty or less than 10 digits",
                            50,
                            true),
                        SizedBox(
                          height: 40,
                          width: 0,
                        ),
                        InkWell(
                          child: Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.only(left: 15, right: 15),
                            height: 50,
                            width: MediaQuery.of(context).size.width,
                            decoration: new BoxDecoration(
                              gradient: LinearGradient(
                                  begin: const Alignment(0.0, -2.0),
                                  end: const Alignment(0.0, 0.3),
                                  colors: [
                                    Colors.white,
                                    CustomColors.darkPinkColor
                                  ]),
                              shape: BoxShape.rectangle,
                              color: CustomColors.darkPinkColor,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(30)),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.pink[100],
                                  spreadRadius: 5,
                                  blurRadius: 10,
                                  offset: Offset(
                                      0, 2), // changes position of shadow
                                ),
                              ],
                            ),
                            child: Text(
                              "Login",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontFamily: "Montserrat",
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15),
                            ),
                          ),
                          onTap: () {
                            FocusScope.of(context).requestFocus(FocusNode());

                            isPhnValidate = true;
                            if (txtPhoneController.text == "") {
                              setState(() {
                                isPhnValidate = false;
                              });
                            } else {
                              loginAPI(context, txtPhoneController.text);
                            }
                          },
                        ),

                        SizedBox(
                          height: 50,
                          width: 0,
                        ),

                        Container(
                          alignment: Alignment.center,
                          child: Text(
                            "or login with",
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w300,
                                fontFamily: "Montserrat"),
                          ),
                        ),

                        SizedBox(
                          height: 20,
                          width: 0,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              child: ElevatedButton(
                                onPressed: googleLogin,
                                child: Container(
                                    height: 20,
                                    width: 20,
                                    child: Image.asset(
                                        "assets/images/google.png")),
                                style: ElevatedButton.styleFrom(
                                  elevation: 6.0,
                                  primary: Colors.white,
                                  shape: CircleBorder(),
                                  padding: EdgeInsets.all(15),
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 0,
                              width: 10,
                            ),
                            Container(
                              child: ElevatedButton(
                                onPressed: () {
                                  initiateFacebookLogin();
                                },
                                child: Container(
                                    height: 20,
                                    width: 20,
                                    child: Image.asset(
                                        "assets/images/facebook.png")),
                                style: ElevatedButton.styleFrom(
                                  elevation: 6.0,
                                  primary: Colors.white,
                                  shape: CircleBorder(),
                                  padding: EdgeInsets.all(15),
                                ),
                              ),
                            ),
                          ],
                        ),

                        SizedBox(
                          height: 50,
                        ),

                        //  Row(
                        //    mainAxisAlignment: MainAxisAlignment.center,
                        //   children: [
                        //
                        //     Text("Don't have an account?",style: TextStyle(fontSize:10,fontFamily:"Montserrat" ,color: CustomColors.blackColor),),
                        //
                        //     InkWell(
                        //       child: Text('Register now',style: TextStyle(fontSize:10,fontFamily:"Montserrat" ,color: CustomColors.darkPinkColor,decoration: TextDecoration.underline),),
                        //       onTap: (){
                        //         Navigator.push(context, MaterialPageRoute(
                        //           builder: (context) {
                        //             return RegisterScreen();
                        //           },
                        //         ));
                        //       },
                        //     ),
                        //
                        //   ],
                        // )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text.rich(
                      TextSpan(
                        text: "Don't have an account?",
                        style: TextStyle(
                            fontSize: 13,
                            fontFamily: "Montserrat",
                            color: CustomColors.blackColor),
                        children: <TextSpan>[
                          TextSpan(
                            recognizer: new TapGestureRecognizer()
                              ..onTap = () {
                                Navigator.push(context, MaterialPageRoute(
                                  builder: (context) {
                                    return RegisterScreen("", "");
                                  },
                                ));
                              },
                            text: 'Register now',
                            style: TextStyle(
                                fontSize: 14,
                                fontFamily: "Montserrat",
                                color: CustomColors.darkPinkColor,
                                decoration: TextDecoration.underline),
                          )
                          // can add more TextSpans here...
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }

  Future<bool> _onWillPop() async {
    return (await showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text(AlertMessage.msgAlertTitle),
            content: new Text(AlertMessage.msgAppExit),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text(WebApiConstaint.btnNo),
              ),
              new FlatButton(
                onPressed: () => SystemNavigator.pop(),
                child: new Text(WebApiConstaint.btnYes),
              ),
            ],
          ),
        )) ??
        false;
  }

  loginAPI(BuildContext context, String emailMobile) async {
    EasyLoading.show();
    String strUrl = WebApiConstaint.URL_LOGIN + "mobile_no=" + emailMobile;
    provider.requestGetForApi(context, strUrl, "", "").then((responce) {
      print(strUrl);
      EasyLoading.dismiss();

      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            Fluttertoast.showToast(msg: responce.data["message"],toastLength: Toast.LENGTH_LONG,);
            // ToastUtils.showToastOTP(fToast, responce.data["message"]);

            Navigator.push(context, MaterialPageRoute(
              builder: (context) {
                return OtpPin(responce.data["mobile_no"]);
              },
            ));
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
          }
        } catch (_) {
          print(_);
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }

  socialLoginAPI(BuildContext context, String email, String name) async {
    EasyLoading.show();
    String strUrl = WebApiConstaint.URL_LOGIN + "mobile_no=" + email;
    provider.requestGetForApi(context, strUrl, "", "").then((responce) {
      print(strUrl);
      EasyLoading.dismiss();

      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            _currentUser = null;
            Fluttertoast.showToast(msg: responce.data["message"]);

            Navigator.push(context, MaterialPageRoute(
              builder: (context) {
                return OtpPin(responce.data["mobile_no"]);
              },
            ));
          } else {
            _currentUser = null;
            // Fluttertoast.showToast(msg: responce.data["message"]);
            // print(responce.data["message"]);
            Navigator.push(context, MaterialPageRoute(
              builder: (context) {
                return RegisterScreen(name, email);
              },
            ));
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }

  void googleLogin() {
    print("Google 1");
    if (_currentUser != null) {
      print("Google 2");
      socialLoginAPI(context, _currentUser.email, _currentUser.displayName);
    } else {
      _handleSignIn();
    }
  }

  Future<void> _handleSignIn() async {
    //
    // GoogleSignInAccount googleSignInAccount = await _googleSignIn.signIn();
    //
    // GoogleSignInAuthentication googleSignInAuthentication = await googleSignInAccount.authentication;
    // if(googleSignInAuthentication != null){
    //   print("Google 3");
    // }
    _googleSignIn.signOut();
    try {
      await _googleSignIn.signIn().then((value) {
        print("Google 4${ value.displayName}" );

        _currentUser = _googleSignIn.currentUser;
        if (_currentUser != null) {
          print("Google 6");
          socialLoginAPI(context, _currentUser.email, _currentUser.displayName);
        }
      });
    } catch (error) {
      print("Google 5");
      print("error" + error.toString());
    }
  }


  void initiateFacebookLogin() async {
    var facebookLogin = FacebookLogin();
    var facebookLoginResult =
        await facebookLogin.logInWithReadPermissions(['email']);
    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.error:
        print(facebookLoginResult.errorMessage);
        break;
      case FacebookLoginStatus.cancelledByUser:
        print("CancelledByUser");
        break;
      case FacebookLoginStatus.loggedIn:
        print("LoggedIn${facebookLogin.currentAccessToken}");
        String url =
            'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${facebookLoginResult.accessToken.token}';
        provider.requestGetForApi(context, url, "", "").then((response) {
          print(response);
          var profile = json.decode(response.data);
          print(profile.toString());
            socialLoginAPI(
              context,
              profile["email"]  ?? "",
              profile["name"],
            );
           //1 login from soc
        });
        break;
    }
  }
}
