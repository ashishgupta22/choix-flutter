// @dart=2.9
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import 'login_screen.dart';


class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  PageController controller=PageController(
    initialPage: 0
  );
  _onPageViewChange(int page) {
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();


  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body:
      Container(
        padding: EdgeInsets.only(bottom: 50),
        child: Stack(

          children: [
            Container(
                margin: EdgeInsets.only(bottom: 50),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(bottomRight: Radius.circular(20),bottomLeft: Radius.circular(20),topRight: Radius.circular(20),topLeft: Radius.circular(20)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 4,
                      blurRadius: 5,
                      offset: Offset(0, 2), // changes position of shadow
                    ),
                  ],
                  color: Colors.white,

                ),
                child: Stack(
                  children: [
                    PageView(
                      controller:controller,
                      scrollDirection: Axis.horizontal,
                      children:
                      [
                        Container(
                          padding: EdgeInsets.all(20),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(bottomRight: Radius.circular(20),bottomLeft: Radius.circular(20))
                          ),
                          child: SingleChildScrollView(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  height: MediaQuery.of(context).size.height * 0.45,
                                  alignment: Alignment.center,
                                  //padding: EdgeInsets.all(20),
                                  margin: EdgeInsets.only(top:60),
                                  child:Image.asset("assets/images/splash1.png") ,
                                ),
                                Text("",style: TextStyle(fontFamily: "Nueva",fontSize: 22,fontWeight: FontWeight.bold),),
                                SizedBox(
                                  height: 10,
                                ),
                                Text("Choix is an initiative that allows you to directly buy fruits and vegetables from your closest and best moving vendors in your area.",
                                  style: TextStyle(fontFamily: "Montserrat",fontSize: 16,fontWeight: FontWeight.w500,),textAlign: TextAlign.center,)
                              ],
                            ),
                          ),
                        ),
                        Container(
                          padding: EdgeInsets.all(20),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(bottomRight: Radius.circular(20),bottomLeft: Radius.circular(20))
                          ),
                          child: SingleChildScrollView(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,

                              children: [
                                Container(
                                  height: MediaQuery.of(context).size.height * 0.45,
                                  alignment: Alignment.center,
                                  //padding: EdgeInsets.all(20),
                                  margin: EdgeInsets.only(top: 60),
                                  child:Image.asset("assets/images/splash2.png") ,
                                ),
                                Text("",style: TextStyle(fontFamily: "Nueva",fontSize: 22,fontWeight: FontWeight.bold),),
                                SizedBox(
                                  height: 10,
                                ),
                                Text("And the best part is that you simply let us know what you want to buy - fruits, vegetables, or both and you get to choose them right at your doorstep.",
                                  style: TextStyle(fontFamily: "Montserrat",fontSize: 16,fontWeight: FontWeight.w500,),textAlign: TextAlign.center,)
                              ],
                            ),
                          ),

                        ),
                        Container(
                          padding: EdgeInsets.all(20),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(bottomRight: Radius.circular(20),bottomLeft: Radius.circular(20))
                          ),
                          child: SingleChildScrollView(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,

                              children: [
                                Container(
                                  height: MediaQuery.of(context).size.height * 0.45,
                                  alignment: Alignment.center,
                                  //padding: EdgeInsets.all(20),
                                  margin: EdgeInsets.only(top: 60),
                                  child:Image.asset("assets/images/splash3.png") ,
                                ),


                                Text("",style: TextStyle(fontFamily: "Nueva",fontSize: 22,fontWeight: FontWeight.bold),),
                                SizedBox(
                                  height: 10,
                                ),
                                Text("And we don't even fix prices ! You can discuss with vendor the price at which you want to buy. Slowly, with your help, we would be introducing more products in our catalogue.",
                                  style: TextStyle(fontFamily: "Montserrat",fontSize: 16,fontWeight: FontWeight.w500,),textAlign: TextAlign.center,)
                              ],
                            ),
                          ),

                        ),],
                      onPageChanged: _onPageViewChange,

                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          padding: EdgeInsets.all(10),
                          child: Center(
                            child: SmoothPageIndicator(
                              controller: controller,
                              count: 3,
                              effect: SlideEffect(
                                  spacing:  5.0,
                                  radius:  5.0,
                                  dotWidth:  10.0,
                                  dotHeight:  10.0,
                                  paintStyle:  PaintingStyle.stroke,
                                  strokeWidth:  1.5,
                                  dotColor:  CustomColors.lightPinkColor,
                                  activeDotColor:  CustomColors.darkPinkColor
                              ),
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                )
              // children: _list,


            ),

            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Center(
                  child:InkWell(
                    onTap: (){
                      Navigator.push(context, MaterialPageRoute(builder: (context) => LoginScreen()));
                    },
                    child: Text(
                      "SKIP",style: TextStyle(
                        fontFamily: "Montserrat",color: CustomColors.darkPinkColor,fontWeight: FontWeight.w700
                    ),
                    ),
                  ) ,
                )

              ],
            ),


          ],
        ),
      )










    );
  }
  // {
  //   return Scaffold(
  //     body: Stack(
  //       children: [
  //         Container(
  //
  //           color: Colors.white,
  //
  //         ),
  //         Container(
  //           height: MediaQuery.of(context).size.height*0.45,
  //           color: CustomColors.lightPinkColor,
  //
  //         ),
  //          Container(
  //            height: 80,
  //              alignment: Alignment.center,
  //            margin: EdgeInsets.only(top: 60),
  //            child:Image.asset("assets/images/logo.png") ,
  //          )
  //          ,
  //          Container(
  //            margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.40 - 120,left: 40),
  //            height: MediaQuery.of(context).size.height*0.45 - 60,
  //            child:Image.asset("assets/images/man.png",height: 250,width: 250,),
  //          ),
  //
  //          Container(
  //            margin: EdgeInsets.only(top: MediaQuery.of(context).size.height*0.45+70),
  //            child: Column(
  //              crossAxisAlignment: CrossAxisAlignment.center ,
  //              mainAxisAlignment: MainAxisAlignment.center,
  //              children: [
  //                Text("Let's Buy",style: TextStyle(color: CustomColors.blackColor,fontSize: 40,fontFamily: "Nueva"),),
  //                SizedBox(height: 5,width: 0,),
  //                Padding(padding: EdgeInsets.only(left: 20,right: 20),
  //                child: Text("Choix is an initiative that allows you to directly buy fruits and vegetables from your closest and best moving vendors in your area. ",
  //                  style: TextStyle(color: CustomColors.blackColor,fontSize: 18,fontFamily: "Montserrat"),),)
  //                ,
  //
  //
  //             Padding(padding: EdgeInsets.only(top: 40),
  //               child: FloatingActionButton(
  //
  //               onPressed: (){
  //                 Navigator.push(context, MaterialPageRoute(builder: (context) => LoginScreen()),
  //                 );
  //                 // Navigator.push(context, MaterialPageRoute(builder: (context) => OrderAcceptNReject()),
  //                 // );
  //
  //               },
  //               child: Container(
  //                 width: 70,
  //                 height: 70,
  //                 child: Icon(Icons.navigate_next,size: 35,),
  //                 decoration: BoxDecoration(
  //                   shape: BoxShape.circle,
  //                   gradient: LinearGradient(begin: const Alignment(0.0, -1.5),
  //                       end: const Alignment(0.0,0.3),
  //                       colors: [Colors.white,CustomColors.darkPinkColor])
  //                 ),
  //                 //child: ,
  //               )
  //                 ,
  //             ),)
  //
  //              ],
  //            ),
  //          )
  //       ],
  //     ),
  //
  //   );
  // }
}
