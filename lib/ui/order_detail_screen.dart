
// @dart=2.9

import 'package:choix_customer/Data/CancelReason.dart';
import 'package:choix_customer/Data/PendingOrder.dart';
import 'package:choix_customer/Data/UserData.dart';
import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:choix_customer/widget/permission_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:url_launcher/url_launcher.dart';

import 'login_screen.dart';

class OrderDetailScreen extends StatefulWidget {
  PendingOrders listData;

  OrderDetailScreen(PendingOrders this.listData) : super();

  @override
  _OrderDetailScreenState createState() => _OrderDetailScreenState();
}

class _OrderDetailScreenState extends State<OrderDetailScreen> {
  bool isOnMyway = false;
  String status = "";
  String statusCancel = "";
  ApiProvider provider = ApiProvider();
  UserData userData;
  GoogleMapController mapController;
  Set<Marker> allMarkers = Set();

  //
  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    // _location.onLocationChanged.listen((l) {
    //   mapController.animateCamera(
    //     CameraUpdate.newCameraPosition(
    //       CameraPosition(target: LatLng(l.latitude, l.longitude), zoom: 15),
    //     ),
    //   );
    // });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (widget.listData.status == "Open") {
      statusCancel = "Cancel";
    } else if (widget.listData.status == "Accepted") {
      status = "On Way";
      statusCancel = "Cancel";
    } else if (widget.listData.status == "Onway") {
      status = "On Way";
      statusCancel = "Call";
    } else if (widget.listData.status == "Delivered") {
      status = "Delivered";
      statusCancel = "";
    } else {
      status = "Canceled";
      statusCancel = "";
    }
    allMarkers.add(Marker(
        markerId: MarkerId('orderLocation'),
        draggable: false,
        position: LatLng(double.parse(widget.listData.latitude),
            double.parse(widget.listData.longitude))));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Order#" + widget.listData.orderNumber.toString()),
        backgroundColor: CustomColors.darkPinkColor,
      ),
      body: SingleChildScrollView(
        //child: SingleChildScrollView(
        //child : SingleChildScrollView(
        child: Column(
          // crossAxisAlignment: CrossAxisAlignment.start,
          // mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              height: MediaQuery
                  .of(context)
                  .size
                  .height * .4,
              color: Colors.black87,
              child: GoogleMap(
                onMapCreated: _onMapCreated,
                mapType: MapType.normal,
                initialCameraPosition: CameraPosition(
                  target: LatLng(double.parse(widget.listData.latitude),
                      double.parse(widget.listData.longitude)),
                  // _center = LatLng(double.parse(widget.listData.latitude), double.parse(widget.listData.latitude));
                  zoom: 13.0,
                ),
                markers:allMarkers,
              ),
            ),
            Container(
              // height: 30,
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.only(top: 30, left: 30, right: 20),
              // color: Colors.blue,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text(
                    "Vendor Name",
                    style: TextStyle(
                        fontFamily: "Montserrat",
                        fontSize: 14,
                        fontWeight: FontWeight.w900),
                  ),
                  Padding(padding: EdgeInsets.only(top: 8)),
                  Text(
                    widget.listData.vendorName.isEmpty ? "Vendor Not Assigned" :  widget.listData.vendorName,
                    style: TextStyle(
                        fontFamily: "Montserrat",
                        fontSize: 13,
                        fontWeight: FontWeight.w500),
                  ),
                  Padding(padding: EdgeInsets.only(top: 8)),
                  Text(
                    "Order Details :",
                    style: TextStyle(
                        fontFamily: "Montserrat",
                        fontSize: 14,
                        fontWeight: FontWeight.w900),
                  ),
                  Padding(
                      padding: EdgeInsets.only(left: 0, top: 10, bottom: 10,right: 20),
                      child:  Flexible(
                        child: ListView.builder(
                            itemCount:  widget.listData.productData.length,
                            shrinkWrap: true,
                            itemBuilder: (context, index2) {
                              return Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                                children: [
                                  Flexible(
                                    fit: FlexFit.tight,
                                    flex: 2,
                                    child: Text(
                                      widget.listData.productData[index2].name,
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontFamily: "Montserrat-Medium"),
                                    ),
                                  ),
                                  Flexible(
                                    fit: FlexFit.tight,
                                    flex: 1,
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Text(
                                        "${ widget.listData.productData[index2].quantity}",
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontFamily:
                                            "Montserrat-Medium"),
                                      ),
                                    ),
                                  ),
                                  Flexible(
                                    fit: FlexFit.tight,
                                    flex: 1,
                                    child: Align(
                                      alignment: Alignment.centerRight,
                                      child: Text(
                                        "₹" +  widget.listData.productData[index2].price.toString(),
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontFamily:
                                            "Montserrat-Medium"),
                                      ),
                                    ),
                                  ),
                                ],
                              );
                            }
                        ),
                      )),
                  Padding(padding: EdgeInsets.only(top: 8)),
                  Text(
                    "Order date",
                    style: TextStyle(
                        fontFamily: "Montserrat",
                        fontSize: 14,
                        fontWeight: FontWeight.w900),
                  ),
                  Padding(padding: EdgeInsets.only(top: 8)),
                  Text(
                    widget.listData.createdAt,
                    style: TextStyle(
                      fontFamily: "Montserrat",
                      fontSize: 13,
                    ),
                  ),
                  Padding(padding: EdgeInsets.only(top: 30)),
                  Visibility(
                      visible: !isOnMyway,
                      child: Row(
                        children: [
                          InkWell(
                            onTap: () {
                              String status = "";
                              if (widget.listData.status == "Open") {
                                status = "accept";
                              } else if (widget.listData.status == "Accepted") {
                                status = "Onway";
                              }
                              if (status.isNotEmpty)
                                callAcceptRejectAPI(context,
                                    widget.listData.id.toString(), status, 0);
                            },
                            child: Container(
                              padding: EdgeInsets.only(left: 10, right: 10),
                              alignment: Alignment.center,
                              height: 35,
                              //width: 150,
                              decoration: new BoxDecoration(
                                shape: BoxShape.rectangle,
                                color: widget.listData.status == "Onway"
                                    ? Colors.green
                                    : CustomColors.darkPinkColor,
                                borderRadius:
                                BorderRadius.all(Radius.circular(30)),
                              ),
                              child: Text(
                                status,
                                style: TextStyle(
                                    color: Colors.white,
                                    fontFamily: "Montserrat",
                                    fontWeight: FontWeight.bold,
                                    fontSize: 15),
                              ),
                            ),
                          ),
                          if(statusCancel.isNotEmpty)
                            InkWell(
                              onTap: () {
                                if (statusCancel == "Decline") {
                                  callAcceptRejectAPI(context,
                                      widget.listData.id.toString(), "Cancel",
                                      0);
                                  Navigator.pop(context);
                                } else if (statusCancel == "Cancel") {
                                  callAPICancelReason(widget.listData);
                                } else if (statusCancel == "Call") {
                                  String phoneNo = widget.listData.vendorMobile;
                                  _makePhoneCall('tel:$phoneNo');
                                }
                              },
                              child: Container(
                                margin: EdgeInsets.only(left: 10),
                                padding: EdgeInsets.only(left: 7),
                                alignment: Alignment.center,
                                height: 35,
                                width: 100,
                                decoration: new BoxDecoration(
                                  shape: BoxShape.rectangle,
                                  color: CustomColors.blackColor,
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(30)),
                                ),
                                child: Text(
                                  statusCancel,
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontFamily: "Montserrat",
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15),
                                ),
                              ),
                            ),
                        ],
                      )),
                  Visibility(
                      visible: isOnMyway,
                      child: Column(
                        children: [
                          Container(
                            // color : Colors.black,
                            //margin : EdgeInsets.only(left:10,right:10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Flexible(
                                  child: InkWell(
                                    onTap: () {
                                      // Navigator.push(context,MaterialPageRoute(
                                      //   builder: (context) {
                                      //     return OrderDetailScreen();
                                      //   },
                                      // ));
                                    },
                                    child: Container(
                                        padding: EdgeInsets.only(left: 7),
                                        alignment: Alignment.center,
                                        height: 30,
                                        //width: 120,
                                        decoration: new BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          color: CustomColors.darkPinkColor,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(30)),
                                          // boxShadow: [
                                          //   BoxShadow(
                                          //     color: Colors.grey.withOpacity(0.5),
                                          //     spreadRadius: 4,
                                          //     blurRadius: 5,
                                          //     offset: Offset(0, 2), // changes position of shadow
                                          //   ),
                                          // ],
                                        ),
                                        child: Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: [
                                            Icon(
                                              Icons.navigation,
                                              color: Colors.white,
                                              size: 18,
                                            ),
                                            Text(
                                              "Navigate",
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontFamily: "Montserrat",
                                                fontWeight: FontWeight.bold,
                                                fontSize: 15,
                                              ),
                                            ),
                                          ],
                                        )),
                                  ),
                                  flex: 1,
                                  fit: FlexFit.tight,
                                ),
                                Flexible(
                                  child: InkWell(
                                    onTap: () {
                                      // Navigator.push(context,MaterialPageRoute(
                                      //   builder: (context) {
                                      //     return OrderDetailScreen();
                                      //   },
                                      // ));
                                    },
                                    child: Container(
                                        margin: EdgeInsets.only(left: 10),
                                        padding: EdgeInsets.only(left: 7),
                                        alignment: Alignment.center,
                                        height: 30,
                                        // width: 150,
                                        decoration: new BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          color: CustomColors.blackColor,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(30)),
                                          // boxShadow: [
                                          //   BoxShadow(
                                          //     color: Colors.grey.withOpacity(0.5),
                                          //     spreadRadius: 4,
                                          //     blurRadius: 5,
                                          //     offset: Offset(0, 2), // changes position of shadow
                                          //   ),
                                          // ],
                                        ),
                                        child: Row(
                                          mainAxisAlignment:
                                          MainAxisAlignment.center,
                                          children: [
                                            Icon(
                                              Icons.call,
                                              color: Colors.white,
                                              size: 18,
                                            ),
                                            Text(
                                              "+12345678900",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontFamily: "Montserrat",
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 15),
                                            ),
                                          ],
                                        )),
                                  ),
                                  flex: 1,
                                  fit: FlexFit.tight,
                                )
                              ],
                            ),
                          ),
                          Padding(padding: EdgeInsets.only(top: 15)),
                          FlatButton(
                            child: Text(
                              'Close order',
                              style: TextStyle(
                                fontSize: 15.0,
                                fontFamily: "Montserrat",
                              ),
                            ),
                            //color: Colors.blueAccent,
                            textColor: CustomColors.darkPinkColor,
                            onPressed: () {
                              Navigator.pop(context);
                              // Navigator.push(context,MaterialPageRoute(
                              //   builder: (context) {
                              //     return OrderDetailScreen();
                              //   },
                              // ));
                            },
                          ),
                        ],
                      )),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  callAcceptRejectAPI(BuildContext context, String order_id, String type,
      int resionID) async {
    CheckPermission.checkLocationPermission(context).then((value) async {
    if(userData == null)
      userData = await AppSharedPreferences.instance.getUserDetails();
    if(value != null && value){
      Location location = new Location();
      LocationData _pos = await location.getLocation();
      EasyLoading.show();
      String latitude = _pos.latitude.toString();
      String longitude = _pos.longitude.toString();
      Map<String, dynamic> dictParam = {
        'order_id': order_id,
        'latitude': latitude,
        'longitude': longitude,
        'type': type,
        'cancel_reason_id': resionID
      };
      print(dictParam);
      provider
          .requestPostForApi(context, dictParam, WebApiConstaint.URL_ACCEPTREJECT,
          userData.sessionKey, userData.authorization)
          .then((responce) {
        EasyLoading.dismiss();
        if (responce != null) {
          try {
            if (responce.data["error"] == false) {
              Fluttertoast.showToast(msg: responce.data["message"]);
              print(responce.data["data"]);
              // getPendingData(context);
            } else {
              Fluttertoast.showToast(msg: responce.data["message"]);
              if (responce.data["authenticate"] == false) {
                AppSharedPreferences.instance.setLogin(false);
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => LoginScreen()),
                    ModalRoute.withName("/Home"));
              }
            }
          } catch (e) {
            ToastUtils.showCustomToast(context, e.toString());
          }
        }
      });
    }
    });
  }

  Future<void> _makePhoneCall(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<void> callAPICancelReason(PendingOrders listData) async {
    if (userData == null)
      userData = await AppSharedPreferences.instance.getUserDetails();
    EasyLoading.show();
    String strUrl = WebApiConstaint.URL_CANCELREASON;
    provider
        .requestGetForApi(
        context, strUrl, userData.sessionKey, userData.authorization)
        .then((responce) {
      EasyLoading.dismiss();
      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            CancelReason order = CancelReason.fromJson(responce.data);
            show(context, order.data, listData);
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
            if (responce.data["authenticate"] == false) {
              AppSharedPreferences.instance.setLogin(false);
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                  ModalRoute.withName("/Home"));
            }
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }

  void show(BuildContext context, List<CancelReasonData> data,
      PendingOrders listData) {
    int lastchecked = 0;
    if (data.length > 0) {
      data[0].selected = true;
      showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(10),
            topLeft: Radius.circular(10),
          ),
        ),
        context: context,
        builder: (context) {
          return StatefulBuilder(
            builder: (BuildContext context, StateSetter setState) {
              return SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: CustomColors.lightPinkColor,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(10),
                            topRight: Radius.circular(10)),
                      ),
                      child: Column(
                        children: [
                          Padding(
                              padding: EdgeInsets.only(top: 20, bottom: 10)),
                          Row(
                            children: [
                              Padding(
                                  padding: EdgeInsets.only(
                                    left: 15,
                                  )),
                              Text(
                                "Name :",
                                style: TextStyle(
                                    fontFamily: "Montserrat",
                                    fontSize: 14,
                                    fontWeight: FontWeight.bold),
                              ),
                              Padding(padding: EdgeInsets.only(right: 3)),
                              Text(
                                listData.vendorName,
                                style: TextStyle(
                                    fontFamily: "Montserrat", fontSize: 14),
                              ),
                              Spacer(),
                              Text(
                                listData.createdAt,
                                style: TextStyle(
                                    fontFamily: "Montserrat",
                                    fontSize: 12,
                                    color: Colors.grey),
                              ),
                              Padding(padding: EdgeInsets.only(right: 15))
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(bottom: 15)),
                          Row(
                            children: [
                              Padding(
                                  padding: EdgeInsets.only(left: 15, top: 8)),
                              Flexible(
                                  child: RichText(
                                      text: TextSpan(
                                        style: new TextStyle(
                                          fontFamily: "Montserrat",
                                          fontSize: 14.0,
                                          color: Colors.black,
                                        ),
                                        children: <TextSpan>[
                                          new TextSpan(
                                              text: 'Address : ',
                                              style: new TextStyle(
                                                  fontWeight: FontWeight.bold)),
                                          new TextSpan(
                                              text: listData.address,
                                              style: new TextStyle(
                                                  fontSize: 14)),
                                        ],
                                      ))),
                              Padding(
                                  padding: EdgeInsets.only(
                                    right: 15,
                                  )),
                            ],
                          ),
                          Padding(padding: EdgeInsets.only(bottom: 15))
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: 10, left: 15, right: 15),
                      child: Text(
                        "Order issues",
                        style: TextStyle(
                            fontFamily: "Montserrat-Bold",
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      ),
                    ),
                    ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: data.length,
                        itemBuilder: (context, index) {
                          return Container(
                              margin: EdgeInsets.only(
                                  left: 15, right: 15, bottom: 8),
                              child: Row(
                                children: [
                                  Checkbox(
                                      value: data[index].selected,
                                      checkColor: Colors.white,
                                      activeColor: CustomColors.darkPinkColor,
                                      onChanged: (bool value) {
                                        setState(() {
                                          data[lastchecked].selected = false;
                                          lastchecked = index;
                                          data[index].selected = value;
                                        });
                                      }),
                                  Padding(padding: EdgeInsets.only(left: 5)),
                                  Flexible(
                                    child: Text(
                                      data[index].title,
                                      style: TextStyle(
                                          fontFamily: "Montserrat",
                                          fontSize: 14),
                                    ),
                                  ),
                                  Padding(padding: EdgeInsets.only(right: 5)),
                                ],
                              ));
                        }),
                    Padding(
                      padding: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                      child: InkWell(
                        onTap: () {
                          callAcceptRejectAPI(context, listData.id.toString(),
                              "Cancel", data[lastchecked].id);
                          Navigator.pop(context);
                        },
                        child: Container(
                          padding: EdgeInsets.only(left: 25, right: 25),
                          alignment: Alignment.center,
                          height: 35,
                          decoration: new BoxDecoration(
                            shape: BoxShape.rectangle,
                            color: CustomColors.darkPinkColor,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                          ),
                          child: Text(
                            "Submit",
                            style: TextStyle(
                                color: Colors.white,
                                fontFamily: "Montserrat",
                                fontWeight: FontWeight.bold,
                                fontSize: 15),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            },
          );
        },
      );
    }
  }
}
