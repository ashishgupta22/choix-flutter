// @dart=2.9
import 'package:choix_customer/Data/TimeSlots.dart';
import 'package:choix_customer/Data/UserData.dart';
import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:choix_customer/ui/login_screen.dart';
import 'package:choix_customer/widget/slot_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'choose_address.dart';
import 'orders/order_list.dart';

class SelectTimeSlot extends StatefulWidget {
  @override
  _SelectTimeSlot createState() => _SelectTimeSlot();
}

class _SelectTimeSlot extends State<SelectTimeSlot>
    implements TimeSelectedListner {
  List<TimeSlotData> prodcutCatList = [];

  int lastIndex = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getProductCate(context);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: AppBar(
        backgroundColor: CustomColors.darkPinkColor,
        title: Text('Select Timeslot'),
        leading: InkWell(
          child: Padding(
            padding: const EdgeInsets.only(left: 15),
            child: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          ),
          onTap: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: Text(
              "Please select the time slot in which you want delivery tomorrow",
              style: TextStyle(fontSize: 18, color: CustomColors.darkPinkColor,fontFamily: "Montserrat-SemiBold"),
              textAlign: TextAlign.center,
            ),
          ),
          Container(
            child: Column(
              children: [
                ListView.builder(
                    shrinkWrap: true,
                    itemCount: prodcutCatList.length,
                    itemBuilder: (context, i) =>
                        SlotItem(prodcutCatList[i], this, i)),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Future<void> getProductCate(BuildContext context) async {
    UserData userData;
    ApiProvider provider = ApiProvider();
    userData = await AppSharedPreferences.instance.getUserDetails();
    EasyLoading.show();
    provider
        .requestGetForApi(context, WebApiConstaint.URL_GETTIMESLOT,
            userData.sessionKey, userData.authorization)
        .then((responce) {
      // print(strUrl);
      EasyLoading.dismiss();

      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            // setState(() {});
            // Fluttertoast.showToast(
            //      msg: responce.data["message"]);
            var resposdata = TimeSlot.fromJson(responce.data);
            prodcutCatList = resposdata.data;
            setState(() {});
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
            if (responce.data["authenticate"] == false) {
              AppSharedPreferences.instance.setLogin(false);
              Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),
                  ModalRoute.withName("/Home"));
            }
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }

  @override
  void onOfferSelected(TimeSlotData data, int index) {
    // TODO: implement onOfferSelected

    prodcutCatList[lastIndex].isSelected = false;
    lastIndex = index;
    prodcutCatList[index].isSelected = true;
    setState(() {});
    savedData.timeSlot = data;
    Navigator.push(context, MaterialPageRoute(
      builder: (context) {
        return ChooseAddress();
      },
    ));
  }
}

abstract class TimeSelectedListner {
  void onOfferSelected(TimeSlotData data, int index);
}
