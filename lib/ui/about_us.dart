// @dart=2.9
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AboutUs extends StatefulWidget {
  @override
  _AboutUs createState() => _AboutUs();
}

class _AboutUs extends State<AboutUs> {
  bool isLogin = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("About Us"),
        leading: InkWell(
          onTap: (){
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
        backgroundColor: CustomColors.darkPinkColor,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Image.asset("assets/images/logo.png"),
            ),
            SizedBox(height: 20,),
            Text("Choix is an initiative that allows you to directly buy fruits and vegetables from the closest and best moving vendors in your area. And the best part is that you got to choose fruits and vegetables right at your doorstep - simply let us know whether you want to buy fruits, vegetables, or both and we would connect you to the correct vendor. Once he comes to your doorstep, you pick and choose which item you want to buy - and we don't even fix prices for you! You can discuss the price at which you want to buy. Slowly, with your help, we would be introducing more products in our catalogue.")
          ],
        ),
      ),
    );
  }

}