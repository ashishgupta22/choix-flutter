// @dart=2.9
import 'dart:async';
import 'dart:math';

import 'package:choix_customer/Data/FetchAddress.dart';
import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/alert_dialog_new.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:choix_customer/ui/dashboard_screen.dart';
import 'package:choix_customer/ui/orders/order_list.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_api_headers/google_api_headers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:google_maps_webservice/places.dart';

import 'offers/best_offres.dart';

const kGoogleApiKey = WebApiConstaint.GOOGLE_API_KEY;

final homeScaffoldKey = GlobalKey<ScaffoldState>();
final searchScaffoldKey = GlobalKey<ScaffoldState>();

// custom scaffold that handle search
// basically your widget need to extends [GooglePlacesAutocompleteWidget]
// and your state [GooglePlacesAutocompleteState]
int callFrom = 0;

class CustomSearchScaffold extends PlacesAutocompleteWidget {

  CustomSearchScaffold()
      : super(
          offset: 0,
          radius: 1000,
          strictbounds: false,
          region: "us",
          language: "en",
          mode: Mode.fullscreen,
          apiKey: WebApiConstaint.GOOGLE_API_KEY,
          // sessionToken: sessionToken, Place ID: ChIJLbZ-NFv9DDkRzk0gTkm3wlI
          components: [new Component(Component.country, "in")],
          types: [""],
          hint: "Search Address",
        );

  @override
  _CustomSearchScaffoldState createState() => _CustomSearchScaffoldState();
}

class _CustomSearchScaffoldState extends PlacesAutocompleteState {
  @override
  Widget build(BuildContext context) {
    final appBar = AppBar(
        backgroundColor: CustomColors.darkPinkColor,
        leading: BackButton(
          color: Colors.white,
          onPressed: () {
            FocusScope.of(context).requestFocus(FocusNode());
            Navigator.of(context).pop();
            },
        ),
        title: AppBarPlacesAutoCompleteTextField());
    final body = PlacesAutocompleteResult(
      onTap: (p) {
        displayPrediction(p, searchScaffoldKey.currentState, context,);
      },
      logo: Row(
        children: [
          Container(
              height: 30,
              width: 100,
              margin: EdgeInsets.all(20),
              child: Image.asset("assets/images/logo.png"))
        ],
        mainAxisAlignment: MainAxisAlignment.center,
      ),
    );
    return Scaffold(key: searchScaffoldKey, appBar: appBar, body: body);
  }

  @override
  void onResponseError(PlacesAutocompleteResponse response) {
    super.onResponseError(response);
    searchScaffoldKey.currentState.showSnackBar(
      SnackBar(content: Text(response.errorMessage)),
    );
  }

  Future<void> displayPrediction (
      Prediction p, ScaffoldState scaffold, BuildContext context) async {
    if (p != null) {
      // get detail (lat/lng)
      GoogleMapsPlaces _places = GoogleMapsPlaces(
        apiKey: kGoogleApiKey,
        apiHeaders: await GoogleApiHeaders().getHeaders(),
      );
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);
      final lat = detail.result.geometry.location.lat;
      final lng = detail.result.geometry.location.lng;
      FetchAddressData data = FetchAddressData();
      var address = await Geocoder.local.findAddressesFromQuery(p.description);
      data.address = address.single.addressLine;
      data.tags = "";
      data.id = 0;
      data.locality = address.single.postalCode;
      data.latitude = lat.toString();
      data.longitude = lng.toString();
      savedData.selectedAddres = data;
      if(savedData.selectedAddres != null)
      if (callFrom == 0) {
        getCheckLocality(context);
      } else {
        FocusScope.of(context).requestFocus(FocusNode());
        Navigator.pop(context, data);
      }

    }
  }

  Future<void> getCheckLocality(BuildContext context) async {
    print(savedData.selectedAddres.locality == null ? "tessss" : savedData.selectedAddres.locality);
    userData = await AppSharedPreferences.instance.getUserDetails();
    EasyLoading.show();
    String strUrl = WebApiConstaint.URL_CHECKLOCALITY +
        "locality=" +
        (savedData.selectedAddres.locality == null ? "000000" : savedData.selectedAddres.locality);
    provider
        .requestGetForApi(
            context, strUrl, userData.sessionKey, userData.authorization)
        .then((responce) {
      print(strUrl);
      EasyLoading.dismiss();

      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            if (responce.data["errorCode"] == 0) {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) {
                    return ProductDetails();
                  },
                ),
              );
            } else {
              savedData.selectedAddres.address = "Select Address";
              savedData.selectedAddres.tags = "Select Address";
              savedData.selectedAddres.locality = "";
              savedData.selectedAddres.id = 0;
              setState(() {});

              AlertDialogNew.show_dialog(context,null,  responce.data["message"], "Ok", "");
            }
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
          }
        } catch (_EX) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }

  @override
  void onResponse(PlacesAutocompleteResponse response) {
    super.onResponse(response);
    if (response != null && response.predictions.isNotEmpty) {
      // searchScaffoldKey.currentState.showSnackBar(
      //   // SnackBar(content: Text("Got answer")),
      // );
    }
  }
}

class Uuid {
  final Random _random = Random();

  String generateV4() {
    // Generate xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx / 8-4-4-4-12.
    final int special = 8 + _random.nextInt(4);

    return '${_bitsDigits(16, 4)}${_bitsDigits(16, 4)}-'
        '${_bitsDigits(16, 4)}-'
        '4${_bitsDigits(12, 3)}-'
        '${_printDigits(special, 1)}${_bitsDigits(12, 3)}-'
        '${_bitsDigits(16, 4)}${_bitsDigits(16, 4)}${_bitsDigits(16, 4)}';
  }

  String _bitsDigits(int bitCount, int digitCount) =>
      _printDigits(_generateBits(bitCount), digitCount);

  int _generateBits(int bitCount) => _random.nextInt(1 << bitCount);

  String _printDigits(int value, int count) =>
      value.toRadixString(16).padLeft(count, '0');
}
