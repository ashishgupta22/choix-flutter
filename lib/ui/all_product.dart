// @dart=2.9
import 'package:choix_customer/Data/ProductSubcategory.dart';
import 'package:choix_customer/Data/UserData.dart';
import 'package:choix_customer/Network/const_error_message.dart';
import 'package:choix_customer/Utill/AppSharedPreferences.dart';
import 'package:choix_customer/Utill/custom_color.dart';
import 'package:choix_customer/Utill/toast_util.dart';
import 'package:choix_customer/api%20service/api_provider.dart';
import 'package:choix_customer/api%20service/web_api_constant.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'login_screen.dart';

class AllProduct extends StatefulWidget {
  @override
  _AllProduct createState() => _AllProduct();
}

class _AllProduct extends State<AllProduct> {
  List<ProductSubCatData> prodcutSubCatList = [];
  UserData userData;
  ApiProvider provider = ApiProvider();

  @override
  void initState() {
    super.initState();
    getProductCate(context);
  }

  @override
  Widget build(BuildContext context) {
    home:
    return Scaffold(
      appBar:
      AppBar(
        backgroundColor: CustomColors.darkPinkColor,
        title: Text("Prices"),
        leading: InkWell(
          onTap: (){
            Navigator.pop(context);
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10,bottom: 0,left: 10,right: 10),
            child: Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.grey),
                  color: Colors.grey[100],
                  borderRadius: BorderRadius.all(
                      Radius.circular(5)
                  ),
                 ),
              child: Padding(

                padding: const EdgeInsets.only(top: 10,bottom: 10,left: 10,right: 10),
                child: Text(
                  "The prices are representative. Please discuss the price with vendor before buying.\n\nVendors prefer visiting customers who buy atleast 5 items!!",
                  style: TextStyle(
                      fontSize: 12,
                      color: CustomColors.darkPinkColor,
                      fontFamily: "Montserrat-SemiBold"),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
          ),
          Flexible(
              child: ListView.builder(
                  itemCount: prodcutSubCatList.length,
                  itemBuilder: (context, index) {
                    return InkWell(
                      onTap: () {
                        show(context, index);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            color: CustomColors.lightPinkColor,
                            borderRadius: BorderRadius.circular(20)),
                        margin: EdgeInsets.only(
                            left: 20.0, top: 10.0, right: 20.0, bottom: 10.0),
                        child: Row(
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(
                                    bottomLeft: Radius.circular(20),
                                    topLeft: Radius.circular(20),
                                  ),
                                  image: DecorationImage(
                                      image: NetworkImage(
                                          prodcutSubCatList[index].image),
                                      fit: BoxFit.cover)),
                              width: 100,
                              height: 130,
                            ),
                            SizedBox(
                              width: 10,
                              height: 0,
                            ),
                            Container(
                              child: Expanded(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Expanded(
                                          child: Text(
                                            prodcutSubCatList[index].name,
                                            style: TextStyle(
                                                color: CustomColors.blackColor,
                                                fontSize: 15,
                                                fontFamily: "Montserrat"),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 1,
                                    ),
                                    Container(
                                      constraints: BoxConstraints(
                                          maxWidth:
                                              MediaQuery.of(context).size.width -
                                                  150),
                                      child: Text(
                                        prodcutSubCatList[index].description,
                                        textAlign: TextAlign.start,
                                        overflow: TextOverflow.ellipsis,
                                        maxLines: 3,
                                        style: TextStyle(
                                            fontSize: 12,
                                            color: Colors.grey,
                                            fontFamily: "Montserrat"),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Container(
                                      child: Text(
                                        '\u{20B9}' +
                                            prodcutSubCatList[index].price +"/"+ prodcutSubCatList[index].product_type,
                                        style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.black,
                                            fontFamily: "Montserrat"),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  })),

        ],
      ),
    );
  }
  void show(BuildContext context, int index) {
    showModalBottomSheet(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(10),
            topLeft: Radius.circular(10),
          ),
        ),
        context: context,
        builder: (context) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              Container(
                height: 130,
                margin: EdgeInsets.all(30),
                decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(prodcutSubCatList[index].image),
                      fit: BoxFit.contain),
                ),
              ),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topRight: Radius.circular(10),
                      topLeft: Radius.circular(10),
                    )),
                child: Container(
                  margin: EdgeInsets.only(
                      left: 30.0, top: 0.0, right: 30.0, bottom: 0.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: Text(
                              prodcutSubCatList[index].name,
                              style: TextStyle(
                                  color: CustomColors.blackColor,
                                  fontFamily: "Montserrat-Bold",
                                  fontSize: 18),
                            ),
                          ),
                        ],
                      ),
                      Container(
                        child: Text(
                            '\u{20B9}' + prodcutSubCatList[index].price + " / ${prodcutSubCatList[index].product_type}",
                            style: TextStyle(fontFamily: "Montserrat-SemiBold", fontSize: 16,fontWeight: FontWeight.w500)
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                child:SingleChildScrollView(
                  scrollDirection: Axis.vertical,//.horizontal
                  child: Container(
                    constraints: BoxConstraints(
                        maxWidth: MediaQuery
                            .of(context)
                            .size
                            .width - 50),
                    margin: EdgeInsets.only(
                        left: 30.0, top: 10.0, right: 30.0, bottom: 10.0),
                    child: Text(
                      prodcutSubCatList[index].description,
                      style: TextStyle(fontFamily: "Montserrat", fontSize: 12),
                    ),
                  ),
                ),
              ),
            ],
          );
        });
  }
  Future<void> getProductCate(BuildContext context) async {
    userData = await AppSharedPreferences.instance.getUserDetails();
    int catId = 1;
    EasyLoading.show();
    provider
        .requestGetForApi(
            context,
            WebApiConstaint.URL_GETPRODUCTSUBCATEGORy + "?category_id=$catId",
            userData.sessionKey,
            userData.authorization)
        .then((responce) {
      print("Sesstion : " +
          userData.sessionKey +
          "  authKey : " +
          userData.authorization);
      EasyLoading.dismiss();

      if (responce != null) {
        try {
          if (responce.data["error"] == false) {
            var resposdata = ProductSubcategory.fromJson(responce.data);
            prodcutSubCatList = resposdata.data;
            setState(() {});
          } else {
            Fluttertoast.showToast(msg: responce.data["message"]);
            print(responce.data["message"]);
            if (responce.data["authenticate"] == false) {
              AppSharedPreferences.instance.setLogin(false);
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginScreen()));
            }
          }
        } catch (_) {
          ToastUtils.showCustomToast(
              context, ErrorMessage.ERROR_DATA_NOT_PROPER_FORM);
        }
      }
    });
  }
}
