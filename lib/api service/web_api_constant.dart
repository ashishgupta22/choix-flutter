
class WebApiConstaint {

  static const String BASE_URL                   =  "https://www.choix.live/api/v3/customer/";
  static const String URL_GET_OTP                =  BASE_URL + "userSignup/verifyOTP";
  static const String URL_REGISTER               =  BASE_URL + "userSignup/register?";
  static const String URL_REFFERAL               =  BASE_URL + "userSignup/checkReferral";
  static const String URL_LOGIN                  =  BASE_URL + "userSignup/login?";
  static const String URL_VERIFYOTP              =  BASE_URL + "userSignup/verifyOTP";
  static const String URL_RSENDOTP               =  BASE_URL + "userSignup/resendOtp?";
  static const String URL_GETPRODUCTCATEGORy     =  BASE_URL + "userProduct/getproductcategory";
  static const String URL_GETPRODUCTSUBCATEGORy  =  BASE_URL + "userProduct/getproducts";
  static const String URL_GETFETCHADDRESS        =  BASE_URL + "userAddress/getAddress";
  static const String URL_GETTIMESLOT            =  BASE_URL + "userAddress/getTimeSlots";
  static const String URL_GETOFFERS              =  BASE_URL + "userOrder/getoffers";
  static const String URL_Submit                 =  BASE_URL + "userOrder/submitOrder";
  static const String URL_ORDERHSISTORY          =  BASE_URL + "userOrder/orderHistory";
  static const String URL_USERREFRRRL            =  BASE_URL + "userSignup/getReferralData";
  static const String URL_CHECKLOCALITY          =  BASE_URL + "userOrder/checkLocality?";
  static const String URL_SEARCHVENDOR           =  BASE_URL + "userOrder/searchVendor?";
  static const String URL_VENDORDETAILS          =  BASE_URL + "userOrder/orderVendorDetail?";
  static const String URL_LOGOUT                 =  BASE_URL + "userSignup/logout?";
  static const String URL_UPDATE_PROFILE         =  BASE_URL + "userSignup/updateProfile";
  static const String URL_WALLET_ORDER_ID        =  BASE_URL + "userWallet/getWalletOrderId?";
  static const String URL_WALLET_BALANCE         =  BASE_URL + "userWallet/getWalletBalance";
  static const String URL_WALLET_HISTORY         =  BASE_URL + "userWallet/getwallet";
  static const String URL_NOTIFICATION           =  BASE_URL + "userOrder/getNotifications";
  static const String URL_BANNER_LIST            =  BASE_URL + "userProduct/getSlider?type=";
  static const String URL_ACCEPTREJECT           =  BASE_URL + "userOrder/changeOrderStatus";
  static const String URL_CANCELREASON           =  BASE_URL + "userOrder/getcancelreason";
  static const String URL_PENDINGORDER           =  BASE_URL + "userOrder/getPendingOrder";
  static const String URL_PENDINGPAYMENTORDER    =  BASE_URL + "userOrder/pendingPaymentOrder";
  static const String URL_DELETE_NOTIFCATION     =  BASE_URL + "userOrder/deleteNotifications";
  static const String URL_DELETE_ADDRESS         =  BASE_URL + "userOrder/deleteAddress";
  static const String URL_ALLVENDOR              =  BASE_URL + "userOrder/getAllVendor";
  static const String URL_WALLET_STATUS          =  BASE_URL + "userOrder/checkWalletStatus";
  static const String URL_POPUP                  =  BASE_URL + "userOrder/getPoppupNotifications";


  static const String btnYes = 'Yes';
  static const String btnLogout = 'Logout';
  static const String btnNo = 'No';
  static const String btnOK = 'OK';
  static const String btnDone = 'Done';

  //Google PAI KEY
  static const String GOOGLE_API_KEY = "AIzaSyBBYMxSMkIlNxBzXGvSiUeEKlro5xsCceY";

  static const String TERMS_URL = "https://www.choixinfinitum.com/terms-conditions";

  static const String POLICY_URL = "https://www.choixinfinitum.com/privacy-policy";
  static const String APP_STORE_URL = "https://apps.apple.com/in/app/whatsapp-messenger/id310633997";
  static const String PLAY_STORE_URL = "https://play.google.com/store/apps/details?id=com.choix_customer";
}

class AlertMessage{
  static const String msgAlertTitle = "Choix";
  static const String msgAppExit = 'Do you want to Exit';
  static const String msgAppLogout = 'Do you want to logout?';
}