// @dart=2.9

import 'package:choix_customer/Utill/connection_validator.dart';
import 'package:choix_customer/api%20service/loggingInterceptor.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';


class ApiProvider {
  Dio _dio = new Dio();
  ApiProvider() {
    BaseOptions options = BaseOptions(
        baseUrl: "WebApiConstaint.BASE_URL",
        receiveTimeout: 5000,
        connectTimeout: 5000);
    _dio = Dio(options);
    _dio.interceptors.add(LoggingInterceptor());
  }
  Future<Response> requestGetForApi(BuildContext context, String url , String authKey, String authentication) async {
    if(await new ConnectionValidator().check()){
      try {
        Response response = await _dio.get(url,
            options: Options(contentType: Headers.jsonContentType, headers: {
              "accesstoken": "uyh678l-07b8-4386-8f6b-c0c11c1e3khb",
              "authkey" : authKey,
              "authentication" : authentication
            })
        );
        print(response);
        return response;
      }catch (error) {
        return null;
      }
    }else{
      Fluttertoast.showToast(msg: 'Please check network connection and try again !');
      return null;
    }
  }
  Future<Response> requestGoogleForApi(BuildContext context, String url, Future<Map<String, String>> authHeaders ) async {
    if(await new ConnectionValidator().check()){
      try {
        Response response = await _dio.get(url,
            options: Options(headers: await authHeaders)
        );
        print(response);
        return response;
      }catch (error) {
        return null;
      }
    }else{
      Fluttertoast.showToast(msg: 'Please check network connection and try again !');
      return null;
    }
  }
  Future<Response> requestPostForApi(BuildContext context, Map<dynamic, dynamic>  dictParameter , String url , String authKey,String authentication) async {
    if(await new ConnectionValidator().check()){
      try {
        Response response = await _dio.post(url , data:dictParameter,

            options: Options(contentType: Headers.jsonContentType, headers: {
              "accesstoken": "uyh678l-07b8-4386-8f6b-c0c11c1e3khb",
              "authkey" : authKey,
              "authentication" : authentication
            })
        );
        print(response);
        return response;
      } catch (error) {
        // _errorHandler(error,context);
        return null;
      }
    }else{
      Fluttertoast.showToast(msg: 'Please check network connection and try again !');
      return null;
    }



  }
  Future<Response> requestPostForApiFileUpload(BuildContext context,FormData  dictParameter , String url , String authKey,String authentication) async {
    if(await new ConnectionValidator().check()){
      try {
        Response response = await _dio.post(url , data:dictParameter,

            options: Options(headers: {
              "accesstoken": "uyh678l-07b8-4386-8f6b-c0c11c1e3khb",
              "authkey" : authKey,
              "authentication" : authentication,
              "Content-Type" : "multipart/form-data"
            })
        );

        print(response);

        return response;
      } catch (error) {
        print("Error : " + error.toString());
        // _errorHandler(error,context);
        return null;
      }
    }else{
      Fluttertoast.showToast(msg: 'Please check network connection and try again !');
      return null;
    }



  }
}