// @dart=2.9

class RouteFetch {
  bool error;
  String message;
  bool authenticate;
  String authenticateMessage;
  String state;
  int errorCode;
  RouteFetchData data;

  RouteFetch(
      {this.error,
        this.message,
        this.authenticate,
        this.authenticateMessage,
        this.state,
        this.errorCode,
        this.data});

  RouteFetch.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    authenticate = json['authenticate'];
    authenticateMessage = json['authenticate_message'];
    state = json['state'];
    errorCode = json['errorCode'];
    data = json['data'] != null ? new RouteFetchData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['authenticate'] = this.authenticate;
    data['authenticate_message'] = this.authenticateMessage;
    data['state'] = this.state;
    data['errorCode'] = this.errorCode;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class RouteFetchData {
  String orderLat;
  String orderLong;
  String orderAddress;
  String vendorName;
  String vendorMobile;
  String vendor_image;
  String vendorLat;
  String vendorLong;
  dynamic distance;
  String time;

  RouteFetchData(
      {this.orderLat,
        this.orderLong,
        this.orderAddress,
        this.vendorName,
        this.vendorMobile,
        this.vendorLat,
        this.distance,
        this.vendor_image,
        this.time,
        this.vendorLong});

  RouteFetchData.fromJson(Map<String, dynamic> json) {
    orderLat = json['order_lat'];
    orderLong = json['order_long'];
    orderAddress = json['order_address'];
    vendorName = json['vendor_name'];
    vendorMobile = json['vendor_mobile'];
    vendorLat = json['vendor_lat'];
    vendorLong = json['vendor_long'];
    distance = json['distance'];
    vendor_image = json['vendor_image'];
    time = json['time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['order_lat'] = this.orderLat;
    data['order_long'] = this.orderLong;
    data['order_address'] = this.orderAddress;
    data['vendor_name'] = this.vendorName;
    data['vendor_mobile'] = this.vendorMobile;
    data['vendor_lat'] = this.vendorLat;
    data['vendor_long'] = this.vendorLong;
    data['distance'] = this.distance;
    data['vendor_image'] = this.vendor_image;
    data['time'] = this.time;
    return data;
  }
}