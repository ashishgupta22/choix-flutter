// @dart=2.9

class FetchAddress {
  bool error;
  String message;
  bool authenticate;
  String authenticateMessage;
  String state;
  int errorCode;
  List<FetchAddressData> data;

  FetchAddress(
      {this.error,
        this.message,
        this.authenticate,
        this.authenticateMessage,
        this.state,
        this.errorCode,
        this.data});

  FetchAddress.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    authenticate = json['authenticate'];
    authenticateMessage = json['authenticate_message'];
    state = json['state'];
    errorCode = json['errorCode'];
    if (json['data'] != null) {
      data = new List<FetchAddressData>();
      json['data'].forEach((v) {
        data.add(new FetchAddressData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['authenticate'] = this.authenticate;
    data['authenticate_message'] = this.authenticateMessage;
    data['state'] = this.state;
    data['errorCode'] = this.errorCode;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class FetchAddressData {
  int id;
  int userId;
  String address = "Select Address";
  String latitude;
  String longitude;
  int defaultAddress;
  String tags = "Select Address";
  String deletedAt;
  String createdAt;
  String updatedAt;
  String locality;

  FetchAddressData(
      {this.id,
        this.userId,
        this.address,
        this.latitude,
        this.longitude,
        this.defaultAddress,
        this.tags,
        this.deletedAt,
        this.createdAt,
        this.updatedAt,
        this.locality
      });

  FetchAddressData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    address = json['address'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    defaultAddress = json['default_address'];
    tags = json['tags'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    locality = json['locality'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['user_id'] = this.userId;
    data['address'] = this.address;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['default_address'] = this.defaultAddress;
    data['tags'] = this.tags;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['locality'] = this.locality;
    return data;
  }
}