// @dart=2.9

class GetOffers {
  bool error;
  String message;
  bool authenticate;
  String authenticateMessage;
  String state;
  int errorCode;
  String offerDesc;
  List<GetOffersData> data;

  GetOffers(
      {this.error,
        this.message,
        this.authenticate,
        this.authenticateMessage,
        this.state,
        this.errorCode,
        this.offerDesc,
        this.data});

  GetOffers.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    authenticate = json['authenticate'];
    authenticateMessage = json['authenticate_message'];
    state = json['state'];
    errorCode = json['errorCode'];
    offerDesc = json['offer_desc'];
    if (json['data'] != null) {
      data = new List<GetOffersData>();
      json['data'].forEach((v) {
        data.add(new GetOffersData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['authenticate'] = this.authenticate;
    data['authenticate_message'] = this.authenticateMessage;
    data['state'] = this.state;
    data['errorCode'] = this.errorCode;
    data['offer_desc'] = this.offerDesc;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class GetOffersData {
  String name;
  String code;
  String discountAmount;
  String minValue;
  String maxValue;
  String userType;
  String description;
  String end_date;
  String image;
  int rest_days;
  bool isSelected = false;

  GetOffersData(
      {this.name,
        this.code,
        this.discountAmount,
        this.minValue,
        this.maxValue,
        this.userType,
        this.rest_days,
        this.end_date,
        this.image,
        this.description});

  GetOffersData.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    code = json['code'];
    discountAmount = json['discount_amount'];
    minValue = json['min_value'];
    maxValue = json['max_value'];
    userType = json['user_type'];
    description = json['description'];
    rest_days = json['rest_days'];
    end_date = json['end_date'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['code'] = this.code;
    data['discount_amount'] = this.discountAmount;
    data['min_value'] = this.minValue;
    data['max_value'] = this.maxValue;
    data['user_type'] = this.userType;
    data['description'] = this.description;
    data['rest_days'] = this.rest_days;
    data['end_date'] = this.end_date;
    data['image'] = this.image;
    return data;
  }
}