// @dart=2.9

class ProductSubcategory {
  bool error;
  String message;
  bool authenticate;
  String authenticateMessage;
  String state;
  int errorCode;
  List<ProductSubCatData> data;

  ProductSubcategory(
      {this.error,
        this.message,
        this.authenticate,
        this.authenticateMessage,
        this.state,
        this.errorCode,
        this.data});

  ProductSubcategory.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    authenticate = json['authenticate'];
    authenticateMessage = json['authenticate_message'];
    state = json['state'];
    errorCode = json['errorCode'];
    if (json['data'] != null) {
      data = new List<ProductSubCatData>();
      json['data'].forEach((v) {
        data.add(new ProductSubCatData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['authenticate'] = this.authenticate;
    data['authenticate_message'] = this.authenticateMessage;
    data['state'] = this.state;
    data['errorCode'] = this.errorCode;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ProductSubCatData {
  int id;
  String name;
  String skucode;
  String description;
  int productCategoryId;
  String price;
  String image;
  String product_type;
  int count = 0;
  ProductSubCatData(
      {this.id,
        this.name,
        this.skucode,
        this.description,
        this.productCategoryId,
        this.price,
        this.image});

  ProductSubCatData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    skucode = json['skucode'];
    description = json['description'];
    productCategoryId = json['product_category_id'];
    price = json['price'];
    product_type = json['product_type'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['skucode'] = this.skucode;
    data['description'] = this.description;
    data['product_category_id'] = this.productCategoryId;
    data['price'] = this.price;
    data['product_type'] = this.product_type;
    data['image'] = this.image;
    return data;
  }
}