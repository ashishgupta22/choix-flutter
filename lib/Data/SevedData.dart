// @dart=2.9
import 'package:choix_customer/Data/FetchAddress.dart';
import 'package:choix_customer/Data/GetOffers.dart';
import 'package:choix_customer/Data/TimeSlots.dart';

import 'ProductSubcategory.dart';

class SavedData {
  static final SavedData _singleton = SavedData._internal();
  factory SavedData() {
    return _singleton;
  }
  SavedData._internal();
  List<ProductSubCatData> selectedItem = [] ;
  TimeSlotData timeSlot = TimeSlotData();
  FetchAddressData selectedAddres = FetchAddressData();
  GetOffersData offerSelected = GetOffersData();
  String orderType = "";
  String category_id = "";
}