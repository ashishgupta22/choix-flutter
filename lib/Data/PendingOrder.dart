// @dart=2.9
class PendingOrderData {
  bool error;
  String message;
  bool authenticate;
  String authenticateMessage;
  String state;
  int errorCode;
  Data data;

  PendingOrderData(
      {this.error,
      this.message,
      this.authenticate,
      this.authenticateMessage,
      this.state,
      this.errorCode,
      this.data});

  PendingOrderData.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    authenticate = json['authenticate'];
    authenticateMessage = json['authenticate_message'];
    state = json['state'];
    errorCode = json['errorCode'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['authenticate'] = this.authenticate;
    data['authenticate_message'] = this.authenticateMessage;
    data['state'] = this.state;
    data['errorCode'] = this.errorCode;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<PendingOrders> pendingOrders;
  List<PendingOrders> ongoingOrders;

  Data({this.pendingOrders, this.ongoingOrders});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['pending_orders'] != null) {
      pendingOrders = new List<PendingOrders>();
      json['pending_orders'].forEach((v) {
        pendingOrders.add(new PendingOrders.fromJson(v));
      });
    }
    if (json['ongoing_orders'] != null) {
      ongoingOrders = new List<PendingOrders>();
      json['ongoing_orders'].forEach((v) {
        ongoingOrders.add(new PendingOrders.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.pendingOrders != null) {
      data['pending_orders'] =
          this.pendingOrders.map((v) => v.toJson()).toList();
    }
    if (this.ongoingOrders != null) {
      data['ongoing_orders'] =
          this.ongoingOrders.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class PendingOrders {
  int id;
  String address;
  String latitude;
  String longitude;
  String status;
  int vendorId;
  String createdAt;
  String created;
  String orderNumber;
  String orderType;
  String vendorName;
  String vendorMobile;
  List<ProductData> productData;

  PendingOrders(
      {this.id,
      this.address,
      this.latitude,
      this.longitude,
      this.status,
      this.vendorId,
      this.createdAt,
      this.orderNumber,
      this.orderType,
      this.created,
      this.vendorName,
      this.vendorMobile,
      this.productData});

  PendingOrders.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    address = json['address'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    status = json['status'];
    vendorId = json['vendor_id'];
    createdAt = json['created_at'];
    orderNumber = json['order_number'];
    orderType = json['order_type'];
    vendorName = json['vendor_name'];
    created = json['created'];
    vendorMobile = json['vendor_mobile'];
    if (json['product_data'] != null) {
      productData = new List<ProductData>();
      json['product_data'].forEach((v) {
        productData.add(new ProductData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['address'] = this.address;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    data['status'] = this.status;
    data['vendor_id'] = this.vendorId;
    data['created'] = this.created;
    data['created_at'] = this.createdAt;
    data['order_number'] = this.orderNumber;
    data['order_type'] = this.orderType;
    data['vendor_name'] = this.vendorName;
    data['vendor_mobile'] = this.vendorMobile;
    if (this.productData != null) {
      data['product_data'] = this.productData.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ProductData {
  String name;
  String quantity;
  dynamic price;

  ProductData({this.name, this.quantity, this.price});

  ProductData.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    quantity = json['quantity'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['quantity'] = this.quantity;
    data['price'] = this.price;
    return data;
  }
}
