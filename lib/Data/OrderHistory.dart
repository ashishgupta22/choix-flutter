// @dart=2.9

class OrderHistory {
  bool error;
  String message;
  bool authenticate;
  String authenticateMessage;
  String state;
  int errorCode;
  List<OrderHistoryData> data;

  OrderHistory(
      {this.error,
        this.message,
        this.authenticate,
        this.authenticateMessage,
        this.state,
        this.errorCode,
        this.data});

  OrderHistory.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    authenticate = json['authenticate'];
    authenticateMessage = json['authenticate_message'];
    state = json['state'];
    errorCode = json['errorCode'];
    if (json['data'] != null) {
      data = new List<OrderHistoryData>();
      json['data'].forEach((v) {
        data.add(new OrderHistoryData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['authenticate'] = this.authenticate;
    data['authenticate_message'] = this.authenticateMessage;
    data['state'] = this.state;
    data['errorCode'] = this.errorCode;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OrderHistoryData {
  String orderNo;
  dynamic convenienceFee;
  dynamic discount_amount;
  dynamic totalAmount;
  String orderTime;
  String deliverTime;
  String userName;
  String status;
  String cancel_time;
  String cancel_reason;
  bool isVisible = false;
  List<ProductData> productData;

  OrderHistoryData(
      {this.orderNo,
        this.convenienceFee,
        this.totalAmount,
        this.orderTime,
        this.deliverTime,
        this.status,
        this.cancel_reason,
        this.userName,
        this.cancel_time,
        this.discount_amount,
        this.productData});

  OrderHistoryData.fromJson(Map<String, dynamic> json) {
    orderNo = json['order_no'];
    convenienceFee = json['convenience_fee'];
    totalAmount = json['total_amount'];
    status = json['status'];
    orderTime = json['order_time'];
    deliverTime = json['deliver_time'];
    cancel_reason = json['cancel_reason'];
    userName = json['user_name'];
    cancel_time = json['cancel_time'];
    discount_amount = json['discount_amount'];
    if (json['product_data'] != null) {
      productData = new List<ProductData>();
      json['product_data'].forEach((v) {
        productData.add(new ProductData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['order_no'] = this.orderNo;
    data['convenience_fee'] = this.convenienceFee;
    data['total_amount'] = this.totalAmount;
    data['order_time'] = this.orderTime;
    data['status'] = this.status;
    data['deliver_time'] = this.deliverTime;
    data['user_name'] = this.userName;
    data['cancel_reason'] = this.cancel_reason;
    data['cancel_time'] = this.cancel_time;
    data['discount_amount'] = this.discount_amount;
    if (this.productData != null) {
      data['product_data'] = this.productData.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ProductData {
  String name;
  String quantity;
  dynamic price;

  ProductData({this.name, this.quantity, this.price});

  ProductData.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    quantity = json['quantity'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['quantity'] = this.quantity;
    data['price'] = this.price;
    return data;
  }
}