// @dart=2.9

class SelectedItemData {
  String id;
  String quantity;
  String name;
  String price;

  SelectedItemData({this.id, this.quantity});

  SelectedItemData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    quantity = json['quantity'];
    name = json['name'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['quantity'] = this.quantity;
    data['name'] = this.name;
    data['price'] = this.price;
    return data;
  }
}