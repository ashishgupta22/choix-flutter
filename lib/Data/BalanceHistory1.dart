// @dart=2.9

class BalanceHistory1 {
  bool error;
  String message;
  bool authenticate;
  String authenticateMessage;
  String state;
  int errorCode;
  List<BalanceHistoryData> data;

  BalanceHistory1(
      { this.error,
       this.message,
        this.authenticate,
       this.authenticateMessage,
       this.state,
        this.errorCode,
        this.data});

  BalanceHistory1.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    authenticate = json['authenticate'];
    authenticateMessage = json['authenticate_message'];
    state = json['state'];
    errorCode = json['errorCode'];
    if (json['data'] != null) {
      data = <BalanceHistoryData>[];
      json['data'].forEach((v) {
        data.add(new BalanceHistoryData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['authenticate'] = this.authenticate;
    data['authenticate_message'] = this.authenticateMessage;
    data['state'] = this.state;
    data['errorCode'] = this.errorCode;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class BalanceHistoryData {
  int id;
  String paymentNumber;
  int userId;
  int referenceId;
  String status;
  String entryType;
  dynamic totalAmount;
  String notes;
  String createdAt;
  String created;
  String updatedAt;

  BalanceHistoryData(
      {this.id,
        this.paymentNumber,
        this.userId,
        this.referenceId,
        this.status,
        this.entryType,
        this.totalAmount,
        this.notes,
        this.createdAt,
        this.created,
        this.updatedAt});

  BalanceHistoryData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    paymentNumber = json['payment_number'];
    userId = json['user_id'];
    referenceId = json['reference_id'];
    status = json['status'];
    entryType = json['entry_type'];
    totalAmount = json['total_amount'];
    notes = json['notes'];
    created = json['created'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['payment_number'] = this.paymentNumber;
    data['user_id'] = this.userId;
    data['reference_id'] = this.referenceId;
    data['status'] = this.status;
    data['entry_type'] = this.entryType;
    data['total_amount'] = this.totalAmount;
    data['created'] = this.created;
    data['notes'] = this.notes;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    return data;
  }
}