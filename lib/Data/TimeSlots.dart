// @dart=2.9
class TimeSlot {
  bool error;
  String message;
  bool authenticate;
  String authenticateMessage;
  String state;
  int errorCode;
  List<TimeSlotData> data;

  TimeSlot(
      {this.error,
        this.message,
        this.authenticate,
        this.authenticateMessage,
        this.state,
        this.errorCode,
        this.data});

  TimeSlot.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    authenticate = json['authenticate'];
    authenticateMessage = json['authenticate_message'];
    state = json['state'];
    errorCode = json['errorCode'];
    if (json['data'] != null) {
      data = new List<TimeSlotData>();
      json['data'].forEach((v) {
        data.add(new TimeSlotData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['authenticate'] = this.authenticate;
    data['authenticate_message'] = this.authenticateMessage;
    data['state'] = this.state;
    data['errorCode'] = this.errorCode;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class TimeSlotData {
  String startTime;
  String endTime;
  bool isSelected =false;
  TimeSlotData({this.startTime, this.endTime});

  TimeSlotData.fromJson(Map<String, dynamic> json) {
    startTime = json['start_time'];
    endTime = json['end_time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    return data;
  }
}