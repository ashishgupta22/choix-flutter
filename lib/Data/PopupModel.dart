// @dart=2.9
class PopupModel {
  bool error;
  String message;
  bool authenticate;
  String authenticateMessage;
  String state;
  int errorCode;
  PopupData data;

  PopupModel(
      {this.error,
        this.message,
        this.authenticate,
        this.authenticateMessage,
        this.state,
        this.errorCode,
        this.data});

  PopupModel.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    authenticate = json['authenticate'];
    authenticateMessage = json['authenticate_message'];
    state = json['state'] ?? "";
    errorCode = json['errorCode'];
    data = json['data'] != null ? new PopupData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['authenticate'] = this.authenticate;
    data['authenticate_message'] = this.authenticateMessage;
    data['state'] = this.state;
    data['errorCode'] = this.errorCode;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class PopupData {
  int id;
  int order_id;
  String title;
  String data;
  String type;
  String createdAt;
  String created;
  String vendor_name;


  PopupData(
      {this.id,
        this.order_id,
        this.title,
        this.data,
        this.type,
        this.createdAt,
        this.created,
        this.vendor_name,});

  PopupData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    order_id = json['order_id'];
    title = json['title'];
    data = json['data'];
    type = json['type'];
    createdAt = json['created_at'];
    vendor_name = json['vendor_name'];

    created = json['created'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['order_id'] = this.order_id;
    data['title'] = this.title;
    data['data'] = this.data;
    data['type'] = this.type;
    data['created_at'] = this.createdAt;

    data['vendor_name'] = this.vendor_name;

    data['created'] = this.created;

    return data;
  }
}

class ProductData {
  String name;
  String quantity;
  String price;

  ProductData({this.name, this.quantity, this.price});

  ProductData.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    quantity = json['quantity'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['quantity'] = this.quantity;
    data['price'] = this.price;
    return data;
  }
}