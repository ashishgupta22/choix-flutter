// @dart=2.9

class Productcategory {
  bool error;
  String message;
  String version;
  bool authenticate;
  String authenticateMessage;
  String state;
  int errorCode;
  Data data;

  Productcategory(
      {this.error,
        this.message,
        this.version,
        this.authenticate,
        this.authenticateMessage,
        this.state,
        this.errorCode,
        this.data});

  Productcategory.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    version = json['version'];
    authenticate = json['authenticate'];
    authenticateMessage = json['authenticate_message'];
    state = json['state'];
    errorCode = json['errorCode'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['version'] = this.version;
    data['authenticate'] = this.authenticate;
    data['authenticate_message'] = this.authenticateMessage;
    data['state'] = this.state;
    data['errorCode'] = this.errorCode;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  List<CatData> catData;
  List<TodayTimings> todayTimings;
  List<TodayTimings> tommorowTimings;

  Data({this.catData, this.todayTimings, this.tommorowTimings});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['cat_data'] != null) {
      catData = new List<CatData>();
      json['cat_data'].forEach((v) {
        catData.add(new CatData.fromJson(v));
      });
    }
    if (json['today_timings'] != null) {
      todayTimings = new List<TodayTimings>();
      json['today_timings'].forEach((v) {
        todayTimings.add(new TodayTimings.fromJson(v));
      });
    }
    if (json['tommorow_timings'] != null) {
      tommorowTimings = new List<TodayTimings>();
      json['tommorow_timings'].forEach((v) {
        tommorowTimings.add(new TodayTimings.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.catData != null) {
      data['cat_data'] = this.catData.map((v) => v.toJson()).toList();
    }
    if (this.todayTimings != null) {
      data['today_timings'] = this.todayTimings.map((v) => v.toJson()).toList();
    }
    if (this.tommorowTimings != null) {
      data['tommorow_timings'] =
          this.tommorowTimings.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CatData {
  int id;
  String name;
  String image;

  CatData({this.id, this.name, this.image});

  CatData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['image'] = this.image;
    return data;
  }
}

class TodayTimings {
  String startTime;
  String endTime;

  TodayTimings({this.startTime, this.endTime});

  TodayTimings.fromJson(Map<String, dynamic> json) {
    startTime = json['start_time'];
    endTime = json['end_time'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['start_time'] = this.startTime;
    data['end_time'] = this.endTime;
    return data;
  }
}