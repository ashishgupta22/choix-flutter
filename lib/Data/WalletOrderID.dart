// @dart=2.9

class WalletOrderID {
  bool error;
  String message;
  bool authenticate;
  String authenticateMessage;
  String state;
  int errorCode;
  WalletOrderIDData data;

  WalletOrderID(
      {this.error,
        this.message,
        this.authenticate,
        this.authenticateMessage,
        this.state,
        this.errorCode,
        this.data});

  WalletOrderID.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    authenticate = json['authenticate'];
    authenticateMessage = json['authenticate_message'];
    state = json['state'];
    errorCode = json['errorCode'];
    data = json['data'] != null ? new WalletOrderIDData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['authenticate'] = this.authenticate;
    data['authenticate_message'] = this.authenticateMessage;
    data['state'] = this.state;
    data['errorCode'] = this.errorCode;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class WalletOrderIDData {
  String token;
  String orderid;
  dynamic amount;
  String appId;
  String stage;
  String notifyUrl;

  WalletOrderIDData({this.token, this.orderid, this.amount, this.appId, this.stage,this.notifyUrl});

  WalletOrderIDData.fromJson(Map<String, dynamic> json) {
    token = json['token'];
    orderid = json['orderid'];
    amount = json['amount'];
    appId = json['app_id'];
    stage = json['stage'];
    notifyUrl = json['notifyUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['token'] = this.token;
    data['orderid'] = this.orderid;
    data['amount'] = this.amount;
    data['app_id'] = this.appId;
    data['stage'] = this.stage;
    data['notifyUrl'] = this.notifyUrl;
    return data;
  }
}