// @dart=2.9
//
class BannerItem {
  bool error;
  String message;
  bool authenticate;
  String authenticateMessage;
  String state;
  int errorCode;
  List<BannerData> data;

  BannerItem(
      {this.error,
        this.message,
        this.authenticate,
        this.authenticateMessage,
        this.state,
        this.errorCode,
        this.data});

  BannerItem.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    authenticate = json['authenticate'];
    authenticateMessage = json['authenticate_message'];
    state = json['state'];
    errorCode = json['errorCode'];
    if (json['data'] != null) {
      data = new List<BannerData>();
      json['data'].forEach((v) {
        data.add(new BannerData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['authenticate'] = this.authenticate;
    data['authenticate_message'] = this.authenticateMessage;
    data['state'] = this.state;
    data['errorCode'] = this.errorCode;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class BannerData {
  int id;
  String name;
  String image;

  BannerData({this.id, this.name, this.image});

  BannerData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['image'] = this.image;
    return data;
  }
}