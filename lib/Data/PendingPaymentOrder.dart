// @dart=2.9
class PendingPaymentOrder {
  bool error;
  String message;
  bool authenticate;
  String authenticateMessage;
  String state;
  int errorCode;
  PendingPaymentOrderData data;

  PendingPaymentOrder(
      {this.error,
        this.message,
        this.authenticate,
        this.authenticateMessage,
        this.state,
        this.errorCode,
        this.data});

  PendingPaymentOrder.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    authenticate = json['authenticate'];
    authenticateMessage = json['authenticate_message'];
    state = json['state'] ?? "";
    errorCode = json['errorCode'];
    data = json['data'] != null ? new PendingPaymentOrderData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['authenticate'] = this.authenticate;
    data['authenticate_message'] = this.authenticateMessage;
    data['state'] = this.state;
    data['errorCode'] = this.errorCode;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class PendingPaymentOrderData {
  int id;
  String orderNumber;
  String address;
  dynamic totalAmount;
  dynamic convenienceFee;
  dynamic discount_amount;
  String createdAt;
  String created;
  String customerName;
  String vendor_name;
  String cat_name;
  String customerMobile;
  List<ProductData> productData;

  PendingPaymentOrderData(
      {this.id,
        this.orderNumber,
        this.address,
        this.totalAmount,
        this.convenienceFee,
        this.createdAt,
        this.created,
        this.vendor_name,
        this.discount_amount,
        this.cat_name,
        this.customerName,
        this.customerMobile,
        this.productData});

  PendingPaymentOrderData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    orderNumber = json['order_number'];
    address = json['address'];
    totalAmount = json['total_amount'];
    convenienceFee = json['convenience_fee'];
    createdAt = json['created_at'];
    customerName = json['customer_name'];
    discount_amount = json['discount_amount'];
    customerMobile = json['customer_mobile'];
    vendor_name = json['vendor_name'];
    cat_name = json['cat_name'];
    created = json['created'];
    if (json['product_data'] != null) {
      productData = new List<ProductData>();
      json['product_data'].forEach((v) {
        productData.add(new ProductData.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['order_number'] = this.orderNumber;
    data['address'] = this.address;
    data['total_amount'] = this.totalAmount;
    data['convenience_fee'] = this.convenienceFee;
    data['created_at'] = this.createdAt;
    data['customer_name'] = this.customerName;
    data['customer_mobile'] = this.customerMobile;
    data['vendor_name'] = this.vendor_name;
    data['discount_amount'] = this.discount_amount;
    data['created'] = this.created;
    data['cat_name'] = this.cat_name;
    if (this.productData != null) {
      data['product_data'] = this.productData.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class ProductData {
  String name;
  String quantity;
  String price;

  ProductData({this.name, this.quantity, this.price});

  ProductData.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    quantity = json['quantity'];
    price = json['price'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['quantity'] = this.quantity;
    data['price'] = this.price;
    return data;
  }
}