// @dart=2.9

import 'BalanceHistory1.dart';

class Items {
  String headerTitle;
  List<BalanceHistoryData> childItems;

  Items(String subjectName, List<BalanceHistoryData> topicNameList) {
    this.headerTitle = subjectName;
    this.childItems = topicNameList;
  }
}