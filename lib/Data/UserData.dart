// @dart=2.9
class UserData {
  int id;
  String name;
  String email;
  String mobileNo;
  String vendorType;
  String adminVendor;
  String profileImage;
  String area;
  String emailVerifiedAt;
  String mobileOtp;
  String referralCode;
  String assignReferralCode;
  String status;
  String isVarified;
  String deletedAt;
  String createdAt;
  String updatedAt;
  String sessionKey;
  String authorization;

  UserData(
      {this.id,
        this.name,
        this.email,
        this.mobileNo,
        this.vendorType,
        this.adminVendor,
        this.profileImage,
        this.area,
        this.emailVerifiedAt,
        this.mobileOtp,
        this.referralCode,
        this.assignReferralCode,
        this.status,
        this.isVarified,
        this.deletedAt,
        this.createdAt,
        this.updatedAt,
        this.sessionKey,
        this.authorization});

  UserData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    email = json['email'];
    mobileNo = json['mobile_no'];
    vendorType = json['vendor_type'];
    adminVendor = json['admin_vendor'];
    profileImage = json['profile_image'];
    area = json['area'];
    emailVerifiedAt = json['email_verified_at'];
    mobileOtp = json['mobile_otp'];
    referralCode = json['referral_code'];
    assignReferralCode = json['assign_referral_code'];
    status = json['status'];
    isVarified = json['is_varified'];
    deletedAt = json['deleted_at'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    sessionKey = json['session_key'];
    authorization = json['authorization'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['email'] = this.email;
    data['mobile_no'] = this.mobileNo;
    data['vendor_type'] = this.vendorType;
    data['admin_vendor'] = this.adminVendor;
    data['profile_image'] = this.profileImage;
    data['area'] = this.area;
    data['email_verified_at'] = this.emailVerifiedAt;
    data['mobile_otp'] = this.mobileOtp;
    data['referral_code'] = this.referralCode;
    data['assign_referral_code'] = this.assignReferralCode;
    data['status'] = this.status;
    data['is_varified'] = this.isVarified;
    data['deleted_at'] = this.deletedAt;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['session_key'] = this.sessionKey;
    data['authorization'] = this.authorization;
    return data;
  }
}