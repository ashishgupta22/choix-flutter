// @dart=2.9

class Referral {
  bool error;
  String message;
  bool authenticate;
  String authenticateMessage;
  String state;
  int errorCode;
  ReferralData data;

  Referral(
      {this.error,
        this.message,
        this.authenticate,
        this.authenticateMessage,
        this.state,
        this.errorCode,
        this.data});

  Referral.fromJson(Map<String, dynamic> json) {
    error = json['error'];
    message = json['message'];
    authenticate = json['authenticate'];
    authenticateMessage = json['authenticate_message'];
    state = json['state'];
    errorCode = json['errorCode'];
    data = json['data'] != String ? new ReferralData.fromJson(json['data']) : String;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['error'] = this.error;
    data['message'] = this.message;
    data['authenticate'] = this.authenticate;
    data['authenticate_message'] = this.authenticateMessage;
    data['state'] = this.state;
    data['errorCode'] = this.errorCode;
    if (this.data != String) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class ReferralData {
  int referralAmount;
  String referralDesc;
  String url;

  ReferralData({this.referralAmount, this.referralDesc, this.url});

  ReferralData.fromJson(Map<String, dynamic> json) {
    referralAmount = json['referral_amount'];
    referralDesc = json['referral_desc'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['referral_amount'] = this.referralAmount;
    data['referral_desc'] = this.referralDesc;
    data['url'] = this.url;
    return data;
  }
}